import os, sys
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('setnolog setoverwrite')
dirname = os.path.join(mdpp_dir, 'runs/YSZ_GB/CE')
os.makedirs(dirname, exist_ok=True)
mdpp.cmd('dirname = %s'%dirname)
mdpp.cmd('NNM = 1000')

#-------------------------------------------------------------
# Species Setting ( Zr/Ce : 0 | O : 1 | Y/Gd : 3 )
#-------------------------------------------------------------

mdpp.cmd('nspecies = 4')
#mdpp.cmd('YSZ_or_GDC = 1')
mdpp.cmd('element0 = "Zr" element1 = "O" element3 = "Y"')
mdpp.cmd('atommass = [  91.22400   15.99940  0.0   88.90585]')
mdpp.cmd('atomcharge = [ 4.0  -2.0  0.0  3.0 ]')

#-------------------------------------------------------------
# Cut-off Radius
#-------------------------------------------------------------
mdpp.cmd('SKIN = 0.75 RLIST = 9.75')

#-------------------------------------------------------------
# Read Initial Structure
#-------------------------------------------------------------
initfile = os.path.join(mdpp_dir, 'scripts', 'ME346B', 'initial.cn')
mdpp.cmd('writeall = 1 incnfile = %s readcn'%initfile)

#-------------------------------------------------------------
# Cut-off Radius and Ewald Settings
#-------------------------------------------------------------
mdpp.cmd('''
Ewald_Rcut = 9.0
Ewald_CE_or_PME = 1
MCMD_CE_or_PME = 0
Ewald_option_Alpha = 1
Ewald_precision = 4.05
Ewald_Alpha = 0.45
PME_K1 = 150
PME_K2 = 75
PME_K3 = 75
PME_bsp_n = 10
Ewald_init
''')

mdpp.cmd('finalcnfile=final.LAMMPS writeLAMMPS')

mdpp.cmd('eval')

print('bmb energy:', mdpp.get('EPOT'))
print('Ewald energy:', mdpp.get('EPOT_Ewald'))
