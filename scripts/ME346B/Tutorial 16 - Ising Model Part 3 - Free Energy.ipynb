{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Tutorial-16:-Free-Energy-of-Ising-Model\" data-toc-modified-id=\"Tutorial-16:-Free-Energy-of-Ising-Model-16\"><span class=\"toc-item-num\">16&nbsp;&nbsp;</span>Tutorial 16: Free Energy of Ising Model</a></span><ul class=\"toc-item\"><li><span><a href=\"#Initialization\" data-toc-modified-id=\"Initialization-16.1\"><span class=\"toc-item-num\">16.1&nbsp;&nbsp;</span>Initialization</a></span></li><li><span><a href=\"#Analytic-Solution-for-the-Free-Energy-of-2D-Ising-Model\" data-toc-modified-id=\"Analytic-Solution-for-the-Free-Energy-of-2D-Ising-Model-16.2\"><span class=\"toc-item-num\">16.2&nbsp;&nbsp;</span>Analytic Solution for the Free Energy of 2D Ising Model</a></span></li><li><span><a href=\"#Compute-Free-Energy-by-Monte-Carlo-Simulation\" data-toc-modified-id=\"Compute-Free-Energy-by-Monte-Carlo-Simulation-16.3\"><span class=\"toc-item-num\">16.3&nbsp;&nbsp;</span>Compute Free Energy by Monte Carlo Simulation</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 16: Free Energy of Ising Model\n",
    "Yifan Wang and Wei Cai\n",
    "\n",
    "**2019-06-06**\n",
    "\n",
    "## Initialization\n",
    "\n",
    "\n",
    "**1. This notebook uses the following extensions, please set them up in nbextensions before using this notebook**\n",
    "* Table of Content (2)\n",
    "\n",
    "<sub>Instructions for nbextension installation is in [Tutorial 01 1.2.2.2](Tutorial%2001%20-%20Introduction%20to%20MD%2B%2B.ipynb)</sub>\n",
    "\n",
    "**2. Ipython widgets for interactive widgets in jupyter notebook**\n",
    "\n",
    "<sub>Instructions for ipywidget installation in [Jupyter Widget Installation](https://ipywidgets.readthedocs.io/en/stable/user_install.html) </sub>\n",
    "\n",
    "**3. If you have not set the environment variables, please add the following 4 lines into `~/.bashrc`, and reboot Ubuntu to setup the environment variables**\n",
    "\n",
    "These environmental variables specifies the MD++ root directory, the MD++ compiling system, and name of the MD++ executable, respectively."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "export MDPLUS_DIR=$HOME/Codes/MD++.git\n",
    "export MDPLUS_EXE=python3\n",
    "export MDPLUS_SYS=gpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Check if environmental variables are set. Change current working directory into the MD++ root folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os, sys\n",
    "\n",
    "envvar_test = True\n",
    "envvars = ['MDPLUS_DIR', 'MDPLUS_EXE', 'MDPLUS_SYS']\n",
    "for envvar in envvars:\n",
    "    if envvar not in os.environ.keys():\n",
    "        print('Environment variable \"'+envvar+'\" not set')\n",
    "        envvar_test = False\n",
    "    else:\n",
    "        print('Environment variable \"'+envvar+'\" set to '+os.environ[envvar])\n",
    "\n",
    "if not envvar_test:\n",
    "    raise OSError\n",
    "\n",
    "mdpp_dir = os.environ['MDPLUS_DIR']\n",
    "os.chdir(mdpp_dir)\n",
    "\n",
    "runs_dir = os.path.join(mdpp_dir, 'runs/MCIsing')\n",
    "os.makedirs(runs_dir, exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analytic Solution for the Free Energy of 2D Ising Model\n",
    "\n",
    "The 2D **Ising model** is a rare case where analytic solution is available for a statistical mechanics model that exhibits a non-trivial phase transition.  The Hamiltonian of the system is\n",
    "\n",
    "$$\n",
    "H(\\{s_i\\}) = -J\\sum_{<i,j>}s_is_j - h\\sum_{i}s_i\n",
    "$$\n",
    "\n",
    "where $j$ are the neighbors of spin $i$, $J$ is the interaction parameter, and $H$ is the external magnetic field.\n",
    "\n",
    "Lars Onsager found the analytic expression (Phys. Rev. 65, 117, 1943) for the free energy of 2D Ising model (when $h = 0$), which is,\n",
    "\n",
    "$$\n",
    "F = -N\\,k_{\\rm B}\\,T\\,\\left\\{ \\ln(2\\cosh 2\\beta J) \n",
    "  + \\frac{1}{\\pi} \\int_0^{\\pi/2} dw \\, \\ln \\left[ \n",
    "     \\frac{1}{2} \\left\\{ 1 + (1-K^2\\,\\sin^2 w)^{1/2} \\right\\}\n",
    "  \\right] \\right\\} \\\\\n",
    "K = \\frac{2\\,\\sinh 2\\beta J}{(\\cosh 2\\beta J)^2}\n",
    "$$\n",
    "where $N$ is the total number of spins.\n",
    "\n",
    "Onsager's solution predicts a critical temperature\n",
    "$$\n",
    "k_{\\rm B}T_c = \\frac{2\\,J}{\\ln \\left(1+\\sqrt{2}\\right)} \\approx 2.269\\,J\n",
    "$$\n",
    "At temperature below $T_c$, there are two (degenerate) ordered phases (one with most spins +1, the other with most spins -1); at temperature above $T_c$, there is a single disordered phase.\n",
    "\n",
    "Onsager's original solution used the transfer matrix method, and was very complicated.  Nine years later, Kac and Ward (Phys. Rev. 88, 1332, 1952) re-derived the result using a much simpler graphical/combinatorial approach.  The combinatorial approach was also explained in Feynman's book ''Statistical Mechanics: A Set of Lectures'', W. A. Benjamin, Inc. (1972).\n",
    "\n",
    "In the following, we plot the free energy, energy, and heat capacity of the 2D Ising model, as predicted by Onsager's analytic solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy import integrate\n",
    "\n",
    "J = 1.0\n",
    "kBTc = 2*J/np.log(1+np.sqrt(2.0))\n",
    "kBT = np.arange(0.1,4.0,0.001)\n",
    "\n",
    "indc = np.argmin(np.abs(kBT-kBTc))\n",
    "\n",
    "beta = 1.0/kBT\n",
    "K = 2*np.divide(np.sinh(2*beta*J), np.cosh(2*beta*J)**2)\n",
    "dKdbeta = np.multiply(4*J/np.cosh(2*beta*J), (1-2*J*(np.tanh(2*beta*J)**2)))\n",
    "\n",
    "fint = np.zeros(len(kBT))\n",
    "dfintdbeta = np.zeros(len(kBT))\n",
    "f = lambda x, q: np.log(1+np.sqrt(1-(q**2)*(np.sin(x)**2)))\n",
    "dfdbeta = lambda x, q, b: -q*np.divide(np.sin(x)**2, np.sqrt(1-(q**2)*(np.sin(x)**2)) + (1-(q**2)*(np.sin(x)**2))) * b\n",
    "\n",
    "for i in range(len(kBT)):\n",
    "    fint[i], err = integrate.quad(f, 0, np.pi, args=(K[i],))\n",
    "    fint[i] /= (2.0*np.pi)\n",
    "    dfintdbeta[i], err = integrate.quad(dfdbeta, 0, np.pi, args=(K[i],dKdbeta[i]))\n",
    "    dfintdbeta[i] /= (2.0*np.pi)                               \n",
    "    \n",
    "F = -np.multiply(kBT, np.log(np.sqrt(2.0)*np.cosh(2.0*beta*J)) + fint) \n",
    "E = -(np.tanh(2*beta*J)*2*J + dfintdbeta)\n",
    "\n",
    "dEdT = (E[2:] - E[0:-2]) / (kBT[2]-kBT[0])\n",
    "dEdT = np.pad(dEdT, (1,1), 'edge')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# define plotting functions\n",
    "font = {'size' : 12}\n",
    "\n",
    "fig0, (ax1,ax2,ax3) = plt.subplots(3, 1, figsize=(9, 9))\n",
    "ax1.plot(kBT, F, '-', kBTc, F[indc], 'r.')\n",
    "ax1.set_ylabel(r'F / N', **font)\n",
    "ax1.set_title(r'Free Energy')\n",
    "\n",
    "ax2.plot(kBT, E, '-', kBTc, E[indc], 'r.')\n",
    "ax2.set_ylabel(r'E / N', **font)\n",
    "ax2.set_title(r'Energy')\n",
    "\n",
    "ax3.plot(kBT, dEdT, '-', kBTc, dEdT[indc], 'r.')\n",
    "ax3.set_xlabel(r'$k_B$ T', **font)\n",
    "ax3.set_ylabel(r'$C_V$ / N', **font)\n",
    "ax3.set_title(r'Heat Capacity')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute Free Energy by Monte Carlo Simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now use $H_0$ to represent the original Hamiltonian,\n",
    "\n",
    "$$\n",
    "H_0(\\{s_i\\}) = -J\\sum_{<i,j>}s_is_j - h\\sum_{i}s_i\n",
    "$$\n",
    "\n",
    "and use $F_0(T)$ to represent the free energy of the (original) Ising model as a function of temperature $T$,\n",
    "\n",
    "$$\n",
    "F_0(T) = - k_{\\rm B}T \\ln \\sum_{\\{s_i\\}} \\exp \\left[ -\\frac{H_0(\\{s_i\\})}{k_{\\rm B}T} \\right]\n",
    "$$\n",
    "\n",
    "We now define a scaled Hamiltonian $H_{\\rm sc}$, which also depends on a scaling parameter $\\lambda$,\n",
    "\n",
    "$$\n",
    "H_{\\rm sc}(\\{s_i\\}, \\lambda) = \\lambda \\, H_0(\\{s_i\\})\n",
    "$$\n",
    "\n",
    "and use $F_{\\rm sc}(T, \\lambda)$ to represent the free energy of the scaled Ising model as a function of both temperature $T$ and parameter $\\lambda$,\n",
    "\n",
    "$$\n",
    "F_{\\rm sc}(T, \\lambda) = - k_{\\rm B}T \\ln \\sum_{\\{s_i\\}} \\exp \\left[ -\\frac{\\lambda \\, H_0(\\{s_i\\})}{k_{\\rm B}T} \\right]\n",
    "$$\n",
    "\n",
    "It is easy to see that free energy of the scaled Hamiltonian at temperature $T$ is related to the free energy of the original Hamiltonian at a different temperature.  Specifically,\n",
    "\n",
    "$$\n",
    "\\frac{F_{\\rm sc}(T, \\lambda)}{T} = \\frac{F_0(T / \\lambda)}{T / \\lambda} = - k_{\\rm B} \\ln \\sum_{\\{s_i\\}} \\exp \\left[ -\\frac{\\lambda \\, H_0(\\{s_i\\})}{k_{\\rm B}T} \\right]\n",
    "$$\n",
    "\n",
    "This means that if we can compute $F_{\\rm sc}(T_0, \\lambda)$ at some fixed temperature $T_0$ as a function of $\\lambda$, we can obtain $F_0(T)$ as a function of temperature.  Specifically,\n",
    "\n",
    "$$\n",
    "F_0(T) = \\frac{1}{\\lambda} F_{\\rm sc} (T_0, \\lambda)\n",
    "$$\n",
    "\n",
    "where $\\lambda = T_0 \\, / \\, T$.\n",
    "\n",
    "\n",
    "For more details, see M. de Koning, W. Cai, A. Antonelli and S. Yip, \"Efficient Free-Energy Calculations by the Simulation of Nonequilibrium Processes\", Computing in Science and Engineering, [2, 88 (2000)](https://web.stanford.edu/~caiwei/papers.html#3), on which this test case is based."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now compute $F_{\\rm sc}(T_0, \\lambda)$ at some fixed reference temperature (e.g. $k_{\\rm B}T_0 = 1$) as a function of $\\lambda$.  We know the analytic expression for $F_{\\rm sc}$ at $\\lambda = 0$.\n",
    "\n",
    "$$\n",
    "F_{\\rm sc}(T_0, \\lambda = 0) = - k_{\\rm B}T_0 \\ln \\sum_{\\{s_i\\}} 1 = - k_{\\rm B}T_0 \\ln 2^N = - N\\,k_{\\rm B}T_0 \\ln 2\n",
    "$$\n",
    "\n",
    "where $N$ is the number of spins.  Therefore, we can use $\\lambda = 0$ as the reference state and perform Monte Carlo simulations using the scaled Hamiltonian, $H_{\\rm sc}(\\{s_i\\},\\lambda)$, in which $\\lambda$ gradually switches from $0$ to $1$.  The dynamic work done to the system, $W_{\\rm dyn}(\\lambda)$, during the switching simulation, gives an estiamte of the free energy $F_{\\rm sc}$ as a function of $\\lambda$.\n",
    "\n",
    "$$\n",
    "F_{\\rm sc}(T_0, \\tilde{\\lambda}) - F_{\\rm sc}(T_0, 0) = \\int_0^{\\tilde{\\lambda}} \\frac{\\partial F_{\\rm sc}(T_0, \\lambda)}{\\partial \\lambda} \\, d\\lambda = \\int_0^{\\tilde{\\lambda}} \\left\\langle \\frac{\\partial H_{\\rm sc}(\\{s_i\\}, \\lambda)}{\\partial \\lambda} \\right\\rangle_{\\lambda} \\, d\\lambda = \\int_0^{\\tilde{\\lambda}} \\left\\langle H_0(\\{s_i\\}) \\right\\rangle_{\\lambda} \\, d\\lambda \\approx W_{\\rm dyn}(\\tilde{\\lambda})\n",
    "$$\n",
    "\n",
    "For simplicity, let $\\lambda = t \\, / \\, t_{\\rm sim}$, where $t$ is the simulation step, and $t_{\\rm sim}$ is the maximum number of simulation steps.  Then $W_{\\rm dyn}$ is simply proportional to the cumulated sum of $H_0$ for a Monte Carlo simulation using $H_{\\rm sc}(\\{s_i\\},\\lambda)$ in which $\\lambda$ changes gradually from $0$ to $1$.\n",
    "\n",
    "$$\n",
    "W_{\\rm dyn}(\\tilde{\\lambda}) = \\sum_{t = 0}^{\\tilde{\\lambda}\\cdot t_{\\rm sim}} H_0 (\\{ s_i(t) \\}) \\cdot  \\frac{d\\lambda}{dt} = \\frac{1}{t_{\\rm sim}} \\, \\sum_{t = 0}^{\\tilde{\\lambda}\\cdot t_{\\rm sim}} H_0 (\\{ s_i(t) \\})\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Temperature (in units of kB)\n",
    "kT0 = 1.0                       # reference temperature\n",
    "\n",
    "# parameters in the Hamiltonian of the (original) Ising model\n",
    "h = 0.0\n",
    "J = 1.0\n",
    "\n",
    "# Lattice size\n",
    "L = 100\n",
    "Lx = Ly = L\n",
    "\n",
    "# Number of iterations\n",
    "t_sim = 1000\n",
    "\n",
    "Niter    = Lx * Ly * t_sim\n",
    "savefreq = Lx * Ly * 100\n",
    "print(Niter)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from ipywidgets import *\n",
    "import ipywidgets as widgets\n",
    "from IPython.display import display, clear_output\n",
    "import time\n",
    "\n",
    "# Initialize spin to be randomly +1 and -1, corresponding to the reference state at lam = 0\n",
    "s = 2*np.random.randint(2, size=(Lx, Ly)) - 1\n",
    "\n",
    "s_data = np.empty((Niter//savefreq, Lx, Ly), dtype=int)\n",
    "nlist = np.arange(Niter)+1\n",
    "# Magnetization\n",
    "Mt = np.empty(Niter)\n",
    "M = np.sum(s)\n",
    "# Energy\n",
    "Et = np.empty(Niter)\n",
    "snn = np.roll(s, 1, 0) + np.roll(s, -1, 0) + np.roll(s, 1, 1) + np.roll(s, -1, 1)\n",
    "\n",
    "# original Hamiltonian\n",
    "E0 = - h*M - J*snn.sum()/2\n",
    "\n",
    "# Obtain sampling\n",
    "ijdx = np.random.randint(Lx, size=(Niter, 2))\n",
    "eps = np.random.rand(Niter)\n",
    "\n",
    "IL = np.roll(np.arange(L), 1)\n",
    "IR = np.roll(np.arange(L),-1)\n",
    "\n",
    "lam = np.arange(t_sim) / t_sim\n",
    "for t in range(t_sim):\n",
    "    # pre-compute flip rate (for the given T0 and lam)\n",
    "    idx0 = 4; iR = np.arange(-4, 5);\n",
    "    E0p =  2*(J*iR+h)\n",
    "    E0m = -2*(J*iR+h)\n",
    "    Esp_over_kT0 = E0p*lam[t]/kT0; Rp = np.exp(-Esp_over_kT0)\n",
    "    Esm_over_kT0 = E0m*lam[t]/kT0; Rm = np.exp(-Esm_over_kT0)\n",
    "    \n",
    "    for sub_iter in range(Lx*Ly):\n",
    "        it = t*Lx*Ly + sub_iter\n",
    "        i, j = ijdx[it, 0], ijdx[it, 1]\n",
    "        il, ir = IL[i], IR[i]\n",
    "        jl, jr = IL[j], IR[j]\n",
    "        \n",
    "        # Spin of all neighbors\n",
    "        snn = s[il, j] + s[ir, j] + s[i, jl] + s[i, jr]\n",
    "        if s[i, j] > 0: # Probability and energy change for flipping a +1 spin\n",
    "            R = Rp[snn + idx0]; DE = E0p[snn + idx0];\n",
    "        else:           # Probability and energy change for flipping a -1 spin\n",
    "            R = Rm[snn + idx0]; DE = E0m[snn + idx0];\n",
    "\n",
    "        if eps[it] < R: # Accept the flip with probability\n",
    "            s[i, j] = -s[i, j];\n",
    "            Mt[it] = M + 2*s[i, j]; M = Mt[it];\n",
    "            Et[it] = E0 + DE; E0 = Et[it];\n",
    "        else:\n",
    "            Mt[it] = M; Et[it] = E0;\n",
    "            \n",
    "        if it%savefreq == 0:\n",
    "            s_data[it//savefreq, :, :] = s.copy()\n",
    "            clear_output(wait=True)\n",
    "            print('T0 = {}: {}/{} finished'.format(kT0, t+savefreq//(Lx*Ly), t_sim))\n",
    "        \n",
    "np.savez_compressed(os.path.join(runs_dir, 'data_T%.2f_switch_L%d_N%d.npz'%(kT0, L, t_sim)), spin=s_data, Et=Et, Mt=Mt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# If you have a long simulation and a large Ising model, visualization can be slow\n",
    "if not plt.fignum_exists('Ising'):\n",
    "    fig = plt.figure('Ising', figsize=(8, 5))\n",
    "    gs = fig.add_gridspec(2, 3)\n",
    "    ax1 = fig.add_subplot(gs[:, :2])\n",
    "    ax1.xaxis.set_visible(False)\n",
    "    ax1.yaxis.set_visible(False)\n",
    "    ax2 = fig.add_subplot(gs[0, 2])\n",
    "    ax2.xaxis.set_visible(False)\n",
    "    ax3 = fig.add_subplot(gs[1, 2], sharex = ax2)\n",
    "\n",
    "    fig.tight_layout()\n",
    "\n",
    "@interact(i=widgets.IntSlider(value=0, min=0, max=Niter/savefreq-1, layout=Layout(width='100%')), )\n",
    "def show_curve(i):\n",
    "    ax1.clear(); ax2.clear(); ax3.clear();\n",
    "    ax1.set_title('Spin Configuration')\n",
    "    ax1.spy(s_data[i, :, :]+1)\n",
    "    t = i*savefreq\n",
    "    ax2.plot(nlist, Et/(Lx*Ly))\n",
    "    ax2.plot(nlist[t], Et[t]/(Lx*Ly), 'o')\n",
    "    ax2.set_ylabel('$E_0(t) / L^2$')\n",
    "    ax3.plot(nlist, Mt/(Lx*Ly))\n",
    "    ax3.plot(nlist[t], Mt[t]/(Lx*Ly), 'o')\n",
    "    ax3.set_ylabel('m(t)')\n",
    "    ax3.set_xlabel('t')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now compute the work $W_{\\rm dyn}(\\lambda)$ and the free energy $F_0(T)$ from the Monte Carlo switching simulation data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig3, (ax7,ax8) = plt.subplots(1, 2, figsize=(9.5, 5))\n",
    "Et_avg = np.mean(Et.reshape((t_sim,Lx*Ly)),axis=1)\n",
    "W_dyn = np.cumsum(Et_avg) / t_sim\n",
    "\n",
    "lam1 = lam + 0.5 / t_sim # avoid divide by zero\n",
    "F_sc = -Lx*Ly*kT0*np.log(2) + W_dyn\n",
    "kT = kT0 / lam1\n",
    "F_0 = np.divide(F_sc, lam1)\n",
    "\n",
    "ax7.plot(lam, W_dyn)\n",
    "ax7.set_xlabel('$\\lambda$')\n",
    "ax7.set_ylabel('$W_{dyn}$')\n",
    "\n",
    "ax8.plot(kT[t_sim//4:], F_0[t_sim//4:]/(Lx*Ly))\n",
    "ax8.set_xlabel('$k_{B}T$')\n",
    "ax8.set_ylabel('$F_0$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=+1> **Exercise 1** </font>\n",
    "\n",
    "Repeat the MC switching simulation for longer simulation steps, $10^4$, i.e. at lower switching rates, and compare the free energy values against analytic results.  Plot the free energy (your numerical values and analytic results) as a function of temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your Code Here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=+1> **Exercise 2** </font>\n",
    "\n",
    "Redo the MC switching simulation using a different switching function $\\lambda = (t / t_{\\rm sim})^4$, and compare the free energy values against analytic results.  Plot the free energy (your numerical values and analytic results) as a function of temperature.\n",
    "\n",
    "Note that the expression of the dynamic work needs to be changed now that $d\\lambda / dt$ is no longer a constant during the switching simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your Code Here\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  },
  "toc": {
   "base_numbering": "16",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
