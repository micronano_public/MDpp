import os, sys, time
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('setnolog setoverwrite')
dirname = os.path.join(mdpp_dir, 'runs/Ewald/NaCl')
os.makedirs(dirname, exist_ok=True)
mdpp.cmd('dirname = %s'%dirname)

#-------------------------------------------------------------
# Number of Neighbors and Atoms in a Cell
#-------------------------------------------------------------

mdpp.cmd('NNM = 2000')

#-------------------------------------------------------------
# Species Setting ( Na : 0 | Cl : 1 )
#-------------------------------------------------------------

mdpp.cmd('nspecies = 2')
mdpp.cmd('element0 = "Na" element1 = "Cl"')
mdpp.cmd('atommass = [  22.98977   35.45300 ]')
mdpp.cmd('atomcharge = [ 1.0  -1.0 ]')

#-------------------------------------------------------------
# Create crystal structure
#-------------------------------------------------------------
mdpp.cmd('crystalstructure = NaCl latticeconst = 5.6404')
mdpp.cmd('''latticesize = [ 1 0 0 10
                            0 1 0 10
                            0 0 1 10 ]
        ''')
mdpp.cmd('makecrystal')

mdpp.cmd('writeall = 1 finalcnfile = final.cn writecn')
mdpp.cmd('finalcnfile = final.LAMMPS writeLAMMPS')

#-------------------------------------------------------------
# Cut-off Radius
#-------------------------------------------------------------
mdpp.cmd('SKIN = 0.75 RLIST = 15.75')

#-------------------------------------------------------------
# Cut-off Radius and Ewald Settings
#-------------------------------------------------------------
mdpp.cmd('''
# Ewald_Rcut = 15.0
Ewald_CE_or_PME = 0
# Ewald_option_Alpha = 1
Ewald_option_Alpha = 0
# Ewald_precision = 5
# Ewald_Alpha = 0.45
# PME_K1 = 150
# PME_K2 = 75
# PME_K3 = 75
# PME_bsp_n = 10
Ewald_init
''')

tic = time.time()
mdpp.cmd('eval')
toc = time.time()
print('Calculation time: %.4fs'%(toc-tic))

print('Ewald energy: %.17g eV'%mdpp.get('EPOT_Ewald'))
print('Electrostatic energy per ion: %.17g eV'%(mdpp.get('EPOT_Ewald')/mdpp.get('NP')))
