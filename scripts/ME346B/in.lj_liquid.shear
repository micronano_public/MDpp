# Non-equilibrium MD for calculating viscosity

units           metal
boundary        p p p
atom_style      atomic
timestep        0.005                           # ps (metal units)

# Simulation information
variable        L       equal 29.88
variable        N       equal 500
variable        seed    equal 20150812
variable        stepequil equal 50000           # steps for const-rate eq.
variable        steprun equal 100000             # steps for actual NEMD run
variable        dumpstep equal 1000
variable        Tinit   equal 80.0              # temperature: K
variable        srate   equal 0.01              # shear rate:  ps^(-1)

# LJ parameters (Argon)
variable        rc      equal 8.3333            # rcut:        A
variable        sig     equal 3.405             # sigma:       A
variable        eps     equal 0.01032358        # epsilon:     eV
variable        molmass equal 39.948            # molmass:     g/mol

# Set up simulation:
### Use prism to allow shear, start with LxLxL cube
### Randomly generate N atoms
region          box     prism 0 $L 0 $L 0 $L 0.0 0.0 0.0 units box
create_box      1       box
create_atoms    1       random $N ${seed} NULL
mass            1       ${molmass}

# LJ pair potential
pair_style      lj/smooth/linear ${rc}
pair_coeff      * *     ${eps} ${sig}

# Minimize the configuration 
thermo          1000
thermo_style    custom  step temp pe etotal press vol
minimize        1e-3 1e-4 100 1000

# log             log.zero.stress
# Zero-Stress equilibrium
# dump            1 all   atom 1000 dump.zero.stress.gz
# velocity        all     create ${Tinit} ${seed} rot yes dist gaussian
# fix             1 all   npt temp ${Tinit} ${Tinit} 0.1 iso 0.0 0.0 0.1
# reset_timestep  0
# variable        step1   equal 10000             # steps for zero-stress eq.
# run             ${step1}
# unfix           1

log             log.nemd.equil
# Enable NEMD
dump            2 all   custom ${dumpstep} dump.nemd.equil.gz id type x y z vx
velocity        all     create ${Tinit} ${seed} rot yes dist gaussian

# Set up nvt ensemble, and const shear rate (tilt box)
fix             1 all   nvt/sllod temp ${Tinit} ${Tinit} 0.1
fix             2 all   deform 1 xy erate ${srate} remap v

# compute temperature
compute         usual   all temp
compute         tilt    all temp/deform
# Set up thermo output
thermo          1000
thermo_style    custom  step temp c_usual epair etotal press pxy
thermo_modify   temp    tilt
# Equilibrium run
run             ${stepequil}

log             log.nemd.run
# NEMD run: Continue the MD simulation for gathering data

thermo          10
thermo_style    custom  step temp press pxy
thermo_modify   temp    tilt

dump            3 all   custom ${dumpstep} dump.nemd.run.gz id type x y z vx
run             ${steprun}
