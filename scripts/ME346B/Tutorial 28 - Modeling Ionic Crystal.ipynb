{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Tutorial-28:-Modeling-Ionic-Crystal\" data-toc-modified-id=\"Tutorial-28:-Modeling-Ionic-Crystal-28\"><span class=\"toc-item-num\">28&nbsp;&nbsp;</span>Tutorial 28: Modeling Ionic Crystal</a></span><ul class=\"toc-item\"><li><span><a href=\"#Initialization\" data-toc-modified-id=\"Initialization-28.1\"><span class=\"toc-item-num\">28.1&nbsp;&nbsp;</span>Initialization</a></span></li><li><span><a href=\"#Madelung-constant\" data-toc-modified-id=\"Madelung-constant-28.2\"><span class=\"toc-item-num\">28.2&nbsp;&nbsp;</span>Madelung constant</a></span></li><li><span><a href=\"#Create-Rocksalt-crystal-and-evaluate-Ewald-energy\" data-toc-modified-id=\"Create-Rocksalt-crystal-and-evaluate-Ewald-energy-28.3\"><span class=\"toc-item-num\">28.3&nbsp;&nbsp;</span>Create Rocksalt crystal and evaluate Ewald energy</a></span></li><li><span><a href=\"#Create-Fluorite-crystal-and-evaluate-Ewald-energy\" data-toc-modified-id=\"Create-Fluorite-crystal-and-evaluate-Ewald-energy-28.4\"><span class=\"toc-item-num\">28.4&nbsp;&nbsp;</span>Create Fluorite crystal and evaluate Ewald energy</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 28: Modeling Ionic Crystal\n",
    "Yifan Wang, Hark B. Lee and Wei Cai\n",
    "\n",
    "**2021-05-12**\n",
    "\n",
    "## Initialization\n",
    "\n",
    "\n",
    "**1. This notebook uses the following extensions, please set them up in nbextensions before using this notebook**\n",
    "* Table of Content (2)\n",
    "\n",
    "<sub>Instructions for nbextension installation is in [Tutorial 01 1.2.2.2](Tutorial%2001%20-%20Introduction%20to%20MD%2B%2B.ipynb)</sub>\n",
    "\n",
    "**2. If you have not, please add the following 3 lines into `~/.bashrc`, and reboot Ubuntu to setup the environment variables**\n",
    "\n",
    "These environmental variables specifies the MD++ root directory, the MD++ compiling system, and name of the MD++ executable, respectively."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "export MDPLUS_DIR=$HOME/Codes/MD++.git\n",
    "export MDPLUS_EXE=python3\n",
    "export MDPLUS_SYS=gpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Check if environmental variables are set. Change current working directory into the MD++ root folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "\n",
    "envvar_test = True\n",
    "envvars = ['MDPLUS_DIR', 'MDPLUS_EXE', 'MDPLUS_SYS']\n",
    "for envvar in envvars:\n",
    "    if envvar not in os.environ.keys():\n",
    "        print('Environment variable \"'+envvar+'\" not set')\n",
    "        envvar_test = False\n",
    "    else:\n",
    "        print('Environment variable \"'+envvar+'\" set to '+os.environ[envvar])\n",
    "\n",
    "if not envvar_test:\n",
    "    raise OSError\n",
    "\n",
    "mdpp_dir = os.environ['MDPLUS_DIR']\n",
    "os.chdir(mdpp_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Compile the bmb executable**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "make clean; make bmb SYS=$MDPLUS_SYS build=R PY=yes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Madelung constant\n",
    "\n",
    "The Madelung constant is used in determining the electrostatic potential of a single ion in a crystal by approximating the ions by point charges ([Wikipedia Madelung constant](https://en.wikipedia.org/wiki/Madelung_constant)). In a ionic crystal, the total Coulomb interaction potential of the $i^{th}$ atom in the system is:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "V_i=\\frac{e}{4\\pi\\varepsilon_0}\\sum_{j\\neq i}\\frac{z_j}{r_{ij}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where $r_{ij}=\\mathbf{r}_j-\\mathbf{r}_i$ is the distance vector between the $i^{th}$ and the $j^{th}$ ion. The vacuum permittivity is $\\varepsilon_0=8.854\\times10^{-12}\\,C^2/N\\cdot m^2$, and the sum is over all ionic pairs $(i,j)$ ($N(N-1)/2$ pairs in total). If we normalize the distances $r_{ij}$ to the nearest neighbor distance $r_0$, the potential can be written as,"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "V_i=\\frac{e}{4\\pi\\varepsilon_0r_0}\\sum_{j\\neq i}\\frac{z_jr_0}{r_{ij}}=\\frac{e}{4\\pi\\varepsilon_0r_0}M_i\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, the electrostatic energy of the ion can be written as:\n",
    "\n",
    "$$\n",
    "E_i=q_iM_i=\\frac{e^2}{4\\pi\\varepsilon_0r_0}z_iM_i\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The infinite sum above is _conditionally convergent_, meaning that the result depends on the order of the summation. **To speed up the summation and improve the convergence, Ewald method is introduced to speed up the calculation and improve the convergence.** It is readily seen that the Madelung constant only depends on the crystal structure and the charge of the ionic crystal.\n",
    "\n",
    "For example, the Madelung constant a couple of commonly seen crystals are:\n",
    "\n",
    "|Name|   |$a(\\overset{\\circ}{\\rm A})$|$z_-$|$M_-$(based on $r_0$)|$z_+$|$M_+$(based on $r_0$)|\n",
    "|----|---|---------------------------|-----|---------------------|-----|---------------------|\n",
    "|${\\rm NaCl}$|<img src=\"https://media.gettyimages.com/vectors/sodium-chloride-nacl-molecular-structure-vector-id519343330\" alt=\"drawing\" width=\"200\"/>|5.6404|${\\rm Cl}^-$|+1.747565|${\\rm Na}^+$|-1.747565|\n",
    "|${\\rm CsCl}$|<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/CsCl_crystal.png/170px-CsCl_crystal.png\" alt=\"drawing\" width=\"200\"/>|4.119|${\\rm Cl}^-$|+1.762675|${\\rm Cs}^+$|-1.762675|\n",
    "|${\\rm CaF_2}$|<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Fluorite_Structure.jpg/230px-Fluorite_Structure.jpg\" alt=\"drawing\" width=\"200\"/>|5.463|${\\rm F}^-$|+1.762675|${\\rm Ca}^{2+}$|-3.276110|\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now calculate the Ewald summation of ${\\rm NaCl}$ crystal, and calibrate the value with the values provided above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Rocksalt crystal and evaluate Ewald energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "$MDPLUS_EXE scripts/ME346B/NaCl_Ewald.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=+1> **Exercise 1** </font>\n",
    "\n",
    "Based on the Madelung constant provided in the above table, calculate the electrostatic energy for ${\\rm Na}^+$ and ${\\rm Cl}^-$. Are they the same as the average Ewald energy from the MD++ calculation? (vacuum permitivity $\\varepsilon_0=8.854\\times10^{-12}\\,C^2/N\\cdot m^2$, elementary charge $e=1.602\\times10^{-19}\\,C$)\n",
    "\n",
    "Hint: Remember to divide by 2 to account for double-counting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Fluorite crystal and evaluate Ewald energy\n",
    "\n",
    "<font size=+1> **Exercise 2** </font>\n",
    "\n",
    "Create a new script [`CaF2_Ewald.mdpp.py`](http://localhost:8889/edit/Codes/MD%2B%2B.git/scripts/ME346B/CaF2_Ewald.mdpp.py) based on [`NaCl_Ewald.mdpp.py`](http://localhost:8889/edit/Codes/MD%2B%2B.git/scripts/ME346B/NaCl_Ewald.mdpp.py), output the electrostatic energy per ion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "$MDPLUS_EXE scripts/ME346B/CaF2_Ewald.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question** Based on the Madelung constant provided in the above table, calculate the electrostatic energy for ${\\rm Ca}^{2+}$ and ${\\rm F}^-$. Are they the same as the average Ewald energy from the MD++ calculation? (vacuum permitivity $\\varepsilon_0=8.854\\times10^{-12}\\,C^2/N\\cdot m^2$, elementary charge $e=1.602\\times10^{-19}\\,C$)\n",
    "\n",
    "**Answer**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  },
  "toc": {
   "base_numbering": "28",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "288px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
