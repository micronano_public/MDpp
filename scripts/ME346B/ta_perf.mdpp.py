# MD simulations of perfect crystal of BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd():
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ta-perf")
    mdpp.cmd("NNM = 200")

#--------------------------------------------
# Read the potential file
def readpot():
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
    mdpp.cmd('readpot')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal(n, a):
    # create perfect crystal of specified size n and lattice constant a
    # equilibrium lattice constant of Ta is 3.3058 Angstrom
    mdpp.cmd("crystalstructure = body-centered-cubic")
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(n,n,n))
    mdpp.cmd("makecrystal finalcnfile = perf.cn writecn")
    mdpp.cmd("eval")

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width=400 win_height=400
    #atomradius = 0.9 bondradius = 0.3 bondlength = 0 
    atomcolor = cyan highlightcolor = purple  bondcolor = red
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 1 # display reduced coordinates of atoms when clicked
    #plot_atom_info = 2 # display real coordinates of atoms
    #plot_atom_info = 3 # displayenergy of atoms
    plotfreq = 10
    #
    rotateangles = [ 0 0 0 1.2 ]
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# MD simulation setting
def setup_md():
    mdpp.cmd('''
    equilsteps = 0  timestep = 0.001 # (ps)
    atommass = 180.94788 # (g/mol)
    DOUBLE_T = 1
    saveprop = 1 savepropfreq = 10 openpropfile #run
    savecn = 1 savecnfreq = 1000 openintercnfile
    savecfg = 1 savecfgfreq = 1000
    plotfreq = 100 printfreq = 10
    vt2 = 1e28  #1e28 2e28 5e28
    wallmass = 2e4     # atommass * NP 
    boxdamp = 1e-3     #
    saveH # Use current H as reference (H0), needed for specifying stress
    fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
    ''')

#*******************************************
# Main program starts here
#*******************************************
# status 0: NVE simulations
#        1: NVT simulations
#        2: NVT simulations with adjusting volume to zero stress
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

val = float(sys.argv[2]) if len(sys.argv) > 2 else 0
print('val = %f'%(val))

if status == 0:
    # NVE MD simulations
    print("status == 0: NVE MD simulations")
    initmd()
    readpot()

    nx = 5
    a0 = 3.3058
    create_crystal(nx, a0)

    setup_md()
    mdpp.cmd('ensemble_type = "NVE" integrator_type = "VVerlet"')
    if (val > 0):
        mdpp.cmd("timestep = %f"%val)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd("T_OBJ = 300 DOUBLE_T = 1 randseed = 12345 srand48 initvelocity")
    mdpp.cmd("totalsteps = 100000 run")

elif status == 1:
    # NVT MD simulations that adjusts volume to reach zero stress
    initmd()
    readpot()

    nx = 5
    a0 = 3.3058
    create_crystal(nx, a0)

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = 300 DOUBLE_T = 1 randseed = 12345 srand48 initvelocity")
    mdpp.cmd("totalsteps = 100000 run")

elif status == 2: 
    # NVT MD simulations that adjusts volume to reach zero stress
    initmd()
    readpot()

    nx = 5
    a0 = 3.3058
    create_crystal(nx, a0)
    Lx0 = mdpp.get("H_11")

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = 300 DOUBLE_T = 1 randseed = 12345 srand48 initvelocity")

    maxiter  = 50
    mdsteps  = 2000
    propfreq = 10

    fid = open("P_L.dat","w")
    for iter in range(maxiter):
        mdpp.cmd("continue_curstep = 1")
        mdpp.cmd("totalsteps = %d savepropfreq = %d run"%(mdsteps,propfreq))
        property_data = np.loadtxt("prop.out")
        # average over second half of the simulation
        nstart = mdsteps//propfreq//2
        sig_xx = np.mean( property_data[-nstart:,6] )
        sig_yy = np.mean( property_data[-nstart:,7] )
        sig_zz = np.mean( property_data[-nstart:,8] )
        pressure = (sig_xx + sig_yy + sig_zz) / 3.0
        Lx = mdpp.get("H_11")
        strain = (Lx - Lx0)/Lx0
        print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
        print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
        fid.flush()

        # Adjust box size according to pressure
        if iter < maxiter-1:
            eps_T = pressure * 5e-7
            mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

    fid.close()

else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for a couple of seconds
import time
sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
