import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

#--------------------------------------------
# Initialization
mdpp.cmd('''
setnolog
setoverwrite
dirname = runs/si-surf # specify run directory
''')

#--------------------------------------------
#Create Perfect Lattice Configuration

mdpp.cmd('''
latticestructure = diamond-cubic 
latticeconst = 5.4309529817532409 #(A) for Si
makecnspec = [ -1  1 0  3
                0  0 1  4
                1  1 0  3 ]
makecrystal finalcnfile = perf.cn writecn
eval # evaluate the potential of perfect crystal
''')

N  = mdpp.get("NP")
Lx = mdpp.get("H_11")
Ly = mdpp.get("H_22")
Lz = mdpp.get("H_33")
E0 = mdpp.get("EPOT")

#--------------------------------------------
# Create surface
mdpp.cmd('''
input = [ 2 2 0.5 ]
changeH_keepR
finalcnfile = ta-surf.cn writecn
eval # evaluate the energy of crystal with surface
''')

E1 = mdpp.get("EPOT")

#---------------------------------------------
# Plot Configuration
mdpp.cmd('''
atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si
atomcolor = orange highlightcolor = purple  
bondcolor = red backgroundcolor = white #gray70
plotfreq = 10  rotateangles = [ 0 0 0 1.25 ]
openwin alloccolors rotate saverot refreshnnlist plot
''')


#---------------------------------------------
# Move surface atoms to facilitate reconstruction (if status == 1)
if status == 1:
    print("status == 1: help surface atoms reconstruct")
    N = mdpp.get("NP")
    # get scaled coordinates of all atoms into array s
    s = np.array(mdpp.get_array("SR",0,3*N)).reshape(N,3)

    for i in range(N):
        si = s[i,:]
        # mark top surface atoms into two groups
        if ((si[1] >  0.29) and (si[1] <  0.30)) :
            # compute atomic row_id (0,1,2,3,4,5) for surface atoms
            row_id = int(np.round((si[0]+0.5)*6-0.5))
            if row_id % 2 == 0:
                mdpp.cmd("group("+str(i)+") = 1")
            else:
                mdpp.cmd("group("+str(i)+") = 2")

        # bottom surface atoms into two groups
        if ((si[1] > -0.34) and (si[1] < -0.33)) :
            # compute atomic row_id (0,1,2,3,4,5) for surface atoms
            row_id = int(np.round((si[0]+0.5)*6))
            if row_id % 2 == 0:
                mdpp.cmd("group("+str(i)+") = 3")
            else:
                mdpp.cmd("group("+str(i)+") = 4")

    # move atomic groups defined above
    mdpp.cmd('''
        input = [ 2  0.5 0 0  1 3 ] movegroup 
        input = [ 2 -0.5 0 0  2 4 ] movegroup 
    ''')
    mdpp.cmd("eval plot")

#---------------------------------------------
# Move surface atoms to create not-fully reconstructed surfaces (if status == 2)
if status == 2:
    print("status == 1: move surface atoms to create not-fully reconstructed surfaces")
    N = mdpp.get("NP")
    # get scaled coordinates of all atoms into array s
    s = np.array(mdpp.get_array("SR",0,3*N)).reshape(N,3)

    for i in range(N):
        si = s[i,:]
        # mark top surface atoms into two groups
        if ((si[1] >  0.29) and (si[1] <  0.30)) :
            # compute atomic row_id (0,1,2,3,4,5) for surface atoms
            row_id = int(np.round((si[0]+0.5)*6-0.5))
            # Your code here
            #

        # bottom surface atoms into two groups
        if ((si[1] > -0.34) and (si[1] < -0.33)) :
            # compute atomic row_id (0,1,2,3,4,5) for surface atoms
            row_id = int(np.round((si[0]+0.5)*6))
            if row_id % 2 == 0:
                mdpp.cmd("group("+str(i)+") = 3")
            else:
                mdpp.cmd("group("+str(i)+") = 4")

    # move atomic groups defined above
    mdpp.cmd('''
        input = [ 2  0.5 0 0  1 3 ] movegroup 
        input = [ 2 -0.5 0 0  2 4 ] movegroup 
    ''')
    mdpp.cmd("eval plot")


#---------------------------------------------
# Conjugate-Gradient relaxation
mdpp.cmd('''
conj_ftol = 1e-7          # tolerance on the residual gradient
conj_fevalmax = 1000 # max. number of iterations
conj_fixbox = 1           # fix the simulation box
relax                     # CG relaxation command
finalcnfile = relaxed.cn writecn
eval                      # evaluate relaxed structure
''')

E2 = mdpp.get("EPOT")
Es_unrlx = (E1-E0)/(Lx*Lz*2)
Es_rlx   = (E2-E0)/(Lx*Lz*2)

#---------------------------------------------
# Print results to screen
print("")
print("***************************************************************")
print("E0 = %.4f   E1 = %.4f   E2 = %.4f eV"%(E0,E1,E2))
print("Es(unrelaxed) = %.4f eV/A^2 = %.4f J/m^2  "%(Es_unrlx,Es_unrlx*16.0218))
print("Es(relaxed)   = %.4f eV/A^2 = %.4f J/m^2  "%(Es_rlx,  Es_rlx  *16.0218))
print("***************************************************************")
print("")

#---------------------------------------------
# Write results to file (to be read by Jupyter notebook)
fo = open("si_surf.dat","w")
print(" %5d    %.12f    %.12f    %.12f   %.8f   %.8f   %.8f"%(N,E0,E1,E2,Lx,Ly,Lz), file=fo)
fo.close()

#---------------------------------------------
# Sleep for a couple of seconds
import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
