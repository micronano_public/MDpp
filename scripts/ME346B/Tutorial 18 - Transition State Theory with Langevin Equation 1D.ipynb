{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Tutorial-18:-Transition-State-Theory-with-Langevin-Equation-in-1D\" data-toc-modified-id=\"Tutorial-18:-Transition-State-Theory-with-Langevin-Equation-in-1D-18\"><span class=\"toc-item-num\">18&nbsp;&nbsp;</span>Tutorial 18: Transition State Theory with Langevin Equation in 1D</a></span><ul class=\"toc-item\"><li><span><a href=\"#Initialization\" data-toc-modified-id=\"Initialization-18.1\"><span class=\"toc-item-num\">18.1&nbsp;&nbsp;</span>Initialization</a></span></li><li><span><a href=\"#Langevin-Equation-for-a-Particle-in-a-Potential-Field\" data-toc-modified-id=\"Langevin-Equation-for-a-Particle-in-a-Potential-Field-18.2\"><span class=\"toc-item-num\">18.2&nbsp;&nbsp;</span>Langevin Equation for a Particle in a Potential Field</a></span><ul class=\"toc-item\"><li><span><a href=\"#Integrating-the-Langevin-Equation\" data-toc-modified-id=\"Integrating-the-Langevin-Equation-18.2.1\"><span class=\"toc-item-num\">18.2.1&nbsp;&nbsp;</span>Integrating the Langevin Equation</a></span></li><li><span><a href=\"#Find-the-Rate-of-Crossing\" data-toc-modified-id=\"Find-the-Rate-of-Crossing-18.2.2\"><span class=\"toc-item-num\">18.2.2&nbsp;&nbsp;</span>Find the Rate of Crossing</a></span></li></ul></li><li><span><a href=\"#Crossing-Rate-as-a-Function-of-Temperature\" data-toc-modified-id=\"Crossing-Rate-as-a-Function-of-Temperature-18.3\"><span class=\"toc-item-num\">18.3&nbsp;&nbsp;</span>Crossing Rate as a Function of Temperature</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 18: Transition State Theory with Langevin Equation in 1D\n",
    "Yikai Yin and Wei Cai\n",
    "\n",
    "**2019-06-26**\n",
    "\n",
    "## Initialization\n",
    "\n",
    "\n",
    "**1. This notebook uses the following extensions, please set them up in nbextensions before using this notebook**\n",
    "* Table of Content (2)\n",
    "\n",
    "<sub>Instructions for nbextension installation is in [Tutorial 01 1.2.2.2](Tutorial%2001%20-%20Introduction%20to%20MD%2B%2B.ipynb)</sub>\n",
    "\n",
    "**2. Ipython widgets for interactive widgets in jupyter notebook**\n",
    "\n",
    "<sub>Instructions for ipywidget installation in [Jupyter Widget Installation](https://ipywidgets.readthedocs.io/en/stable/user_install.html) </sub>\n",
    "\n",
    "**3. If you have not set the environment variables, please add the following 4 lines into `~/.bashrc`, and reboot Ubuntu to setup the environment variables**\n",
    "\n",
    "These environmental variables specifies the MD++ root directory, the MD++ compiling system, and name of the MD++ executable, respectively."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "export MDPLUS_DIR=$HOME/Codes/MD++.git\n",
    "export MDPLUS_EXE=python3\n",
    "export MDPLUS_SYS=gpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Check if environmental variables are set. Change current working directory into the MD++ root folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os, sys\n",
    "\n",
    "envvar_test = True\n",
    "envvars = ['MDPLUS_DIR', 'MDPLUS_EXE', 'MDPLUS_SYS']\n",
    "for envvar in envvars:\n",
    "    if envvar not in os.environ.keys():\n",
    "        print('Environment variable \"'+envvar+'\" not set')\n",
    "        envvar_test = False\n",
    "    else:\n",
    "        print('Environment variable \"'+envvar+'\" set to '+os.environ[envvar])\n",
    "\n",
    "if not envvar_test:\n",
    "    raise OSError\n",
    "\n",
    "mdpp_dir = os.environ['MDPLUS_DIR']\n",
    "os.chdir(mdpp_dir)\n",
    "\n",
    "runs_dir = os.path.join(mdpp_dir, 'runs/MCIsing')\n",
    "os.makedirs(runs_dir, exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Langevin Equation for a Particle in a Potential Field\n",
    "\n",
    "Consider a particle moving in a 1D potential field $U(x)$ and subjected to friction and random forces.\n",
    "\n",
    "$$\n",
    "U(x) = -\\frac{1}{2} x^2 + \\frac{1}{4} x^4  \\quad {\\rm in \\ (eV)}\n",
    "$$\n",
    "\n",
    "The Langevin equations of motion are as follows.\n",
    "\n",
    "$$\n",
    "\\dot{x}(t) = v(t) \\\\\n",
    "m\\dot{v}(t) = -\\frac{\\partial U(x(t))}{\\partial x} - \\gamma v(t) + \\sqrt{2\\gamma k_{\\rm B}T} R(t)\n",
    "$$\n",
    "\n",
    "where $-\\gamma v(t)$ is the frictional force and $\\sqrt{2\\gamma k_{\\rm B}T} R(t)$ is the random force.  $R(t)$ is a stationary Gaussian process with the following properties: $\\langle R(t)\\rangle = 0$, $\\langle R(t) R(t')\\rangle = \\delta(t-t')$.\n",
    "\n",
    "The Euler forward integrator for the Langevin equation works as follows.\n",
    "\n",
    "$$\n",
    "x(t+\\Delta t) = x(t) + v(t) \\Delta t\\\\\n",
    "v(t+\\Delta t) = \\left( 1 - \\frac{\\gamma}{m}\\Delta t\\right) v(t) -\\frac{1}{m} \\frac{\\partial U(x(t))}{\\partial x} \\Delta t + \\frac{\\sqrt{2\\gamma k_{\\rm B}T \\Delta t}}{m} \\xi\n",
    "$$\n",
    "\n",
    "where $\\xi$ satisfies the normal distribution, $\\xi \\sim N(0,1)$, with $\\langle \\xi \\rangle = 0$, $\\langle xi^2 \\rangle = 1$, i.e. $f_\\xi(x) = \\frac{1}{\\sqrt{2\\pi}} {\\rm e}^{-x^2/2}$.\n",
    "\n",
    "The friction coefficient $\\gamma$ and the temperature $T$ are the two parameters that control the dynamic behavior of the particle.  The potential field $U(x)$ has two local energy minima (corrresponding to states A and B) with an energy barrier of $E_{\\rm b} = -\\frac{1}{4}$ between them.  We will focus on the conditions of $k_{\\rm B}T \\ll E_{\\rm b}$ so that transitions between the two states is rare."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Integrating the Langevin Equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following cell to integrate the Langevin equation.  You may want to adjust the temperature (T) and friction coefficient (gamma)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import display, clear_output\n",
    "\n",
    "T = 1000           # in K\n",
    "gamma = 100        # friction coefficient (eV/A^2 * ps)\n",
    "kB = 8.6173324e-5  # in eV/K\n",
    "m  = 1e7           # mass (in g/mol)\n",
    "dt = 0.05          # time step (ps)\n",
    "\n",
    "U  = lambda x: -x**2/2 + x**4/4\n",
    "f  = lambda x: x - x**3\n",
    "\n",
    "# mass convertion factor from (g/mol) to (eV/(A/ps)^2)\n",
    "massconvert = 1e-3/6.0221367e23/1.6021772e-19/1e20*1e24 \n",
    "\n",
    "totalsteps = 2000000\n",
    "printfreq  = 10000\n",
    "\n",
    "x = np.zeros(totalsteps+1)\n",
    "v = np.zeros(totalsteps+1)\n",
    "t = np.arange(totalsteps+1)*dt\n",
    "\n",
    "# pre-generate all random numbers with normal distribution\n",
    "xi = np.random.normal(size=totalsteps) \n",
    "\n",
    "factor_ext_force = dt/(m*massconvert)\n",
    "factor_rnd_force = np.sqrt(2*kB*T*gamma*dt)/(m*massconvert)\n",
    "\n",
    "for curstep in range(totalsteps):\n",
    "    x[curstep+1] = x[curstep] + v[curstep]*dt\n",
    "    v[curstep+1] = (1-gamma/(m*massconvert)*dt)*v[curstep] + factor_ext_force*f(x[curstep]) + factor_rnd_force*xi[curstep]\n",
    "            \n",
    "    if (curstep+1)%printfreq == 0:\n",
    "        print(\"progress %d/%d\"%(curstep+1,totalsteps))\n",
    "        clear_output(wait=True)\n",
    "        \n",
    "print('T = %g K   kBT = %g eV  gamma = %g'%(T, kB*T, gamma))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the sampling\n",
    "plotfreq   = 1000\n",
    "fig, axs = plt.subplots(2, 1, figsize=(9, 4))\n",
    "axs[0].plot(t[::plotfreq], x[::plotfreq], 'C1', markersize=1)\n",
    "axs[0].set_ylabel('x (A)');\n",
    "axs[0].set_title('Langevin equation   T = %g K   $\\gamma$ = %g'%(T,gamma))\n",
    "axs[1].plot(t[::plotfreq], v[::plotfreq], 'C2', markersize=1)\n",
    "axs[1].set_xlabel('t (ps)')\n",
    "axs[1].set_ylabel('v (A/ps)')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Find the Rate of Crossing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following cell to find the number of crossing events (across the dividing surface $x=0$) and compute the cross rate.  Note that the crossing rate is an overestimate of the transition rates due to multi-crossing of the dividing surface by a single transition trajectory.\n",
    "\n",
    "Compare the number of crossings identified by the computer with the number of transitions that you can visually identify from the plot above, to see if there is overcounting due to recrossing events."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ind_crossing = np.argwhere(np.multiply(x[1:], x[:-1]) < 0).transpose()\n",
    "N_cross = ind_crossing.size\n",
    "\n",
    "print('Number of crossing = %d'%(N_cross))\n",
    "ind_crossing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Crossing Rate as a Function of Temperature\n",
    "\n",
    "We can run the above simulation at different temperature $T$ and friction coefficient $\\gamma$ values and obtain the crossing rate for each condition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=+1> **Exercise 1** </font>\n",
    "\n",
    "Repeat the simulation at $T = 500$ K, $1000$ K, $1500$ K and $2000$ K at two different friction coefficients $\\gamma = 100$ and $\\gamma = 1$.  Record the crossing rate from each simulation and plot them below in Arrhenius plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your Code Here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=+1> **Exercise 2** </font>\n",
    "\n",
    "Compare the crossing rates measured numerically from those predicted by the transition state theory (TST),\n",
    "\n",
    "$$\n",
    "  k^{\\rm TST} = \\sqrt{\\frac{k_{\\rm B}T}{2\\pi m}} \\frac{{\\rm e}^{-\\beta E_{\\rm b}}}{Z_{\\rm A}^{\\rm c}}\n",
    "$$\n",
    "\n",
    "where \n",
    "$$\n",
    "  Z_{\\rm A}^{\\rm c} = \\int_{-\\infty}^{0} {\\rm exp}\\left\\{-\\beta[U(x)-U(x_{\\rm A})] \\right\\} dx\n",
    "$$\n",
    "\n",
    "and as well as those predicted by the harmonic approximation of the transition state theory (HTST),\n",
    "\n",
    "$$\n",
    "  k^{\\rm HTST} = \\frac{1}{2\\pi} \\sqrt{\\frac{K}{m}} {\\rm e}^{-\\beta E_{\\rm b}}\n",
    "$$\n",
    "\n",
    "where $K = \\left.\\partial^2 U/\\partial x^2\\right|_{x=x_{\\rm A}} = 2$.  Compare the results in Arrhenius plot.\n",
    "\n",
    "$Z_{\\rm A}^{\\rm c}$ can be computed by numerical quadrature as shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python code to integrate Z_A^c by numerical quadrature\n",
    "from scipy import integrate\n",
    "\n",
    "T_array = np.array([500, 1000, 1500, 2000])\n",
    "ZAc_integrand = lambda x, Ti: np.exp(-(U(x)-U(-1))/(kB*Ti))\n",
    "ZAc_array = np.zeros(len(T_array))\n",
    "\n",
    "for i in range(len(T_array)):\n",
    "    ZAc_array[i], err = integrate.quad(ZAc_integrand, -100, 0, args=(T_array[i],))\n",
    "    \n",
    "ZAc_array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your Code Here\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  },
  "toc": {
   "base_numbering": "18",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
