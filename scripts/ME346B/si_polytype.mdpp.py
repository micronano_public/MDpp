import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

structure_list = ['diamond-cubic', 'face-centered-cubic', 'body-centered-cubic', 'simple-cubic']
lat_const_list = [5.4309, 4.1465, 3.245, 2.612]

#*******************************************
# Definition of procedures
#*******************************************

def initmd():
    mdpp.cmd('''
        #setnolog
        setoverwrite
        dirname = runs/si_polytype
        zipfiles = 1   # zip output files
        NIC = 200 NNM = 200
    ''')

def makecrystal():
    '''Create Perfect Lattice Configuration'''
    mdpp.cmd('''
        element0 = Si
        latticesize = [ 1 0 0 4
                        0 1 0 4
                        0 0 1 4 ]
        makecrystal
    ''')

def readnwrite(fout, a):
    '''read and write properties'''
    heading = r'#    lattice constant     ' + \
              r'  number density       ' + \
              r'  atomic volume        ' + \
              r'  potential energy     ' + \
              r'  lattice energy'
    print(heading, file=fout)
    for i in range(-50, 51):
        latt = a + i/10000.0
        mdpp.cmd('latticeconst = %f'%latt)
        makecrystal()
        
        mdpp.cmd('eval')
        Epot = mdpp.get('EPOT')
        N = mdpp.get('NP')
        vol = mdpp.get('OMEGA')
        Elat = Epot / N
        rho = N / vol
        AtomVol = vol / N
        print(r'%23.12e%23.12e%23.12e%23.12e%23.12e'%(latt, rho, AtomVol, Epot, Elat), file=fout)
        
#*******************************************
# Main script starts here
#*******************************************
initmd()

for i in range(len(structure_list)):
    st = structure_list[i]
    a  = lat_const_list[i]
    mdpp.cmd('crystalstructure = ' + st)
    with open(st + '.dat', 'w') as fout:
        readnwrite(fout, a)

mdpp.cmd('quit')
