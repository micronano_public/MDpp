# MD simulations of an interstitial in a crystal of BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd():
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ta-int")
    mdpp.cmd("NNM = 200")

#--------------------------------------------
# Read the potential file
def readpot():
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
    mdpp.cmd('readpot')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal(n, a):
    # create perfect crystal of specified size n and lattice constant a
    # equilibrium lattice constant of Ta is 3.3058 Angstrom
    mdpp.cmd("crystalstructure = body-centered-cubic")
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(n,n,n))
    mdpp.cmd("makecrystal finalcnfile = perf.cn writecn")
    mdpp.cmd("eval")

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width=400 win_height=400
    #atomradius = 0.9 bondradius = 0.3 bondlength = 0 
    atomcolor = cyan highlightcolor = purple  bondcolor = red
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 1 # display reduced coordinates of atoms when clicked
    #plot_atom_info = 2 # display real coordinates of atoms
    #plot_atom_info = 3 # displayenergy of atoms
    plotfreq = 10
    #
    rotateangles = [ 0 0 0 1.2 ]
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# MD simulation setting
def setup_md():
    mdpp.cmd('''
    equilsteps = 0  timestep = 0.001 # (ps)
    atommass = 180.94788 # (g/mol)
    DOUBLE_T = 1
    saveprop = 1 savepropfreq = 10 openpropfile #run
    savecn = 1 savecnfreq = 1000 openintercnfile
    #savecfg = 1 savecfgfreq = 1000
    plotfreq = 100 printfreq = 10
    vt2 = 1e28  #1e28 2e28 5e28
    wallmass = 2e4     # atommass * NP 
    boxdamp = 1e-3     #
    saveH # Use current H as reference (H0), needed for specifying stress
    fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
    ''')

#*******************************************
# Main program starts here
#*******************************************
# status 0: load initial configuration and visualize
#        1: NVT simulations
#        2: NVT simulations with adjusting volume to zero stress
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

T = float(sys.argv[2]) if len(sys.argv) > 2 else 300
print('T = %f'%(T))

randseed = int(sys.argv[3]) if len(sys.argv) > 3 else 12345
print('randseed = %d'%(randseed))

if status == 0:
    # Load initial configuratin and visualize
    print("status == 0: NVE MD simulations")
    initmd()
    readpot()

    mdpp.cmd('incnfile = lammps.ta-int.dump readLAMMPS')

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    #sleep_seconds = 10

elif status == 1:
    # NVT MD simulations 
    initmd()
    readpot()

    mdpp.cmd('incnfile = lammps.ta-int.dump readLAMMPS')

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))
    mdpp.cmd("totalsteps = 100000 run")

    mdpp.cmd("finalcnfile = equil_fix_box.cn writevelocity = 1 writecn")

elif status == 2: 
    # NVT MD simulations that adjusts volume to reach zero stress
    initmd()
    readpot()

    mdpp.cmd('incnfile = lammps.ta-int.dump readLAMMPS')
    Lx0 = mdpp.get("H_11")

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))

    maxiter  = 50
    mdsteps  = 2000
    propfreq = 10

    fid = open("P_L.dat","w")
    for iter in range(maxiter):
        mdpp.cmd("continue_curstep = 1")
        mdpp.cmd("totalsteps = %d savepropfreq = %d run"%(mdsteps,propfreq))
        property_data = np.loadtxt("prop.out")
        # average over second half of the simulation
        nstart = mdsteps//propfreq//2
        sig_xx = np.mean( property_data[-nstart:,6] )
        sig_yy = np.mean( property_data[-nstart:,7] )
        sig_zz = np.mean( property_data[-nstart:,8] )
        pressure = (sig_xx + sig_yy + sig_zz) / 3.0
        Lx = mdpp.get("H_11")
        strain = (Lx - Lx0)/Lx0
        print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
        print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
        fid.flush()

        # Adjust box size according to pressure
        if iter < maxiter-1:
            eps_T = pressure * 5e-7
            mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

    fid.close()

    mdpp.cmd("finalcnfile = equil_free_box.cn writevelocity = 1 writecn")

elif status == 3:
    # NVT MD simulations: long simulations to capture interstitial diffusion
    initmd()
    readpot()

    mdpp.cmd('incnfile = equil_free_box.cn readcn')

    # Read P_L.dat saved from (status == 2)
    iter, P, L, thermal_strain = np.loadtxt("P_L.dat", unpack=True)
    # Compute average box size
    Pavg = np.mean(P[len(P)//2:])
    Lavg = np.mean(L[len(L)//2:])

    # Set box size of MD simulation cell to Lavg
    mdpp.cmd("H_11 = %20.14f"%(Lavg))
    mdpp.cmd("H_22 = %20.14f"%(Lavg))
    mdpp.cmd("H_33 = %20.14f"%(Lavg))

    # change box to the average box size to reduce error (instead of the last box size in the P_L.dat file)

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    # Note that we no longer need to use DOUBLE_T = 1 when randomizing the velocities for already equilibrated structures
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 0 randseed = %d srand48 initvelocity"%(T,randseed))
    # plot_color_axis = 2 asks MD++ to compute central symmetry deviation (CSD) parameter at every step
    mdpp.cmd("plot_color_axis = 2  NCS = 8  writeall = 1")
    mdpp.cmd("totalsteps = 300000 run")

    mdpp.cmd("finalcnfile = final.cn writeall = 1 writecn")
    mdpp.cmd("finalcnfile = final.dump writeLAMMPS")

else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
