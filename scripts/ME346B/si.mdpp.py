import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('''
# -*-shell-script-*-
# make perfect crystal of Silicon
#
setnolog
setoverwrite
dirname = runs/si-example
#------------------------------------------------------------
#Create Perfect Lattice Configuration
#
latticestructure = diamond-cubic 
latticeconst = 5.4309529817532409 #(A) for Si
makecnspec = [ 1 0 0 2
               0 1 0 2
               0 0 1 3 ]
makecn  
finalcnfile = final.cn writecn
finalcnfile = final.lammps writeLAMMPS

#------------------------------------------------------------
#Plot Configuration
#
atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si
atomcolor = orange highlightcolor = purple  
bondcolor = red backgroundcolor = white #gray70
plotfreq = 10  rotateangles = [ 0 0 0 1.25 ]
openwin reversergb alloccolors rotate saverot eval plot
#openwin alloccolors rotate saverot refreshnnlist plot
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
