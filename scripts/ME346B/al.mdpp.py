import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('''
# -*-shell-script-*-
# make perfect crystal of Aluminum
#
setnolog
setoverwrite
dirname = runs/al-example # specify run directory

#--------------------------------------------
#Create Perfect Lattice Configuration
#
crystalstructure = face-centered-cubic latticeconst = 4.05 #(A) for Al
latticesize = [  1   0  0  3    # c1 = 4*[1 0 0]
                 0   1  0  3    # c2 = 4*[0 1 0]
                 0   0  1  3 ]  # c3 = 4*[0 0 1]
makecrystal #finalcnfile = perf.cn writecn
NNM = 60 NIC = 200
#-------------------------------------------------------------
#Plot Configuration
atomradius = 1.0 bondradius = 0.3 bondlength = 0
atomcolor = blue highlightcolor = purple backgroundcolor = white 
bondcolor = red fixatomcolor = yellow
rotateangles = [ 0 0 0 1.0 ]
#
win_width = 600 win_height = 600
openwin alloccolors rotate saverot refreshnnlist plot
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
