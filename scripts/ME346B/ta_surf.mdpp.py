import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

#--------------------------------------------
# Initialization
mdpp.cmd('''
setnolog
setoverwrite
dirname = runs/ta-surf # specify run directory
''')

#--------------------------------------------
# Read the potential file
mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
mdpp.cmd('readpot')

#--------------------------------------------
#Create Perfect Lattice Configuration

mdpp.cmd('''
crystalstructure = body-centered-cubic latticeconst = 3.3058 #(A)
latticesize = [ 1 0 0 5
                0 1 0 5
                0 0 1 5 ]
makecrystal finalcnfile = perf.cn writecn
eval # evaluate the potential of perfect crystal
''')

N  = mdpp.get("NP")
Lx = mdpp.get("H_11")
Ly = mdpp.get("H_22")
Lz = mdpp.get("H_33")
E0 = mdpp.get("EPOT")

#--------------------------------------------
# Create surface
mdpp.cmd('''
input = [ 2 2 0.5 ]
changeH_keepR
finalcnfile = ta-surf.cn writecn
eval # evaluate the energy of crystal with surface
''')

E1 = mdpp.get("EPOT")

#---------------------------------------------
# Plot Configuration
mdpp.cmd('''
atomradius = 1.0 bondradius = 0.3 bondlength = 0
atomcolor = blue highlightcolor = purple backgroundcolor = gray
bondcolor = red   fixatomcolor = yellow
plotfreq = 10 win_width = 600    win_height = 600
plot_atom_info = 3
color00 = "orange" color01 = "purple" color02 = "green"
color03 = "magenta" color04 = "cyan"   color05 = "purple"
color06 = "gray80" color07 = "white"
plot_color_axis = 0
plot_color_windows = [ 3             # number of color windows
                      -10  -8.0   6  # color06 = gray80
                      -8.0 -7.0   1  # color00 = orange
                      -7.0 -0.0   0  # color00 = orange
                     ]
rotateangles = [ 0 0 0 1 ]
openwin alloccolors rotate saverot plot
''')

#---------------------------------------------
# Conjugate-Gradient relaxation
mdpp.cmd('''
conj_ftol = 1e-7          # tolerance on the residual gradient
conj_fevalmax = 1000 # max. number of iterations
conj_fixbox = 1           # fix the simulation box
relax                     # CG relaxation command
finalcnfile = relaxed.cn writecn
eval                      # evaluate relaxed structure
''')

E2 = mdpp.get("EPOT")
Es_unrlx = (E1-E0)/(Lx*Lz*2)
Es_rlx   = (E2-E0)/(Lx*Lz*2)

#---------------------------------------------
# Print results to screen
print("")
print("***************************************************************")
print("E0 = %.4f   E1 = %.4f   E2 = %.4f eV"%(E0,E1,E2))
print("Es(unrelaxed) = %.4f eV/A^2 = %.4f J/m^2  "%(Es_unrlx,Es_unrlx*16.0218))
print("Es(relaxed)   = %.4f eV/A^2 = %.4f J/m^2  "%(Es_rlx,  Es_rlx  *16.0218))
print("***************************************************************")
print("")

#---------------------------------------------
# Write results to file (to be read by Jupyter notebook)
fo = open("ta_surf.dat","w")
print(" %5d    %.12f    %.12f    %.12f   %.8f   %.8f   %.8f"%(N,E0,E1,E2,Lx,Ly,Lz), file=fo)
fo.close()

#---------------------------------------------
# Sleep for a couple of seconds
import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
