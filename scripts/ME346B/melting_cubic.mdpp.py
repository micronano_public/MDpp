# This is a Python script for computing melting points of pure elements in MD++
#
# This file can be run with various empirical potentials developed in MD++ such as
#
#  meam-lammps, meam-baskes, fs, eam, sw, swge, sworig, tersoff
#
# It has been tested for various elements such as Si, Au, etc.
# For details, see Seunghwa Ryu and Wei Cai, 
#  "Comparison of Thermal Properties Predicted by Interatomic Potential Models", 
# Modelling and Simulation in Materials Science and Engineering, 16, 085006, (2008).
#
# Converted from melting_cubic.tcl
#
# Changes from melting_cubic.tcl: 
#   pot_type, time_step (in ps) for each case now specified in datastore.
#   all number of steps and constants specified in datastore, which is saved in pickle file.
#   no longer need to write and run Matlab files.  All calculations done in Python.
#   job (step) is now called status (changed from n).  nn is renamed to idx.
#   logfilename are called A$status.log instead of A.log to avoid being overwritten.
#   add srand48bytime in status 1 equilibration of solid at finite temperature 
#   initial solid (liquid) equilibration using NVTC ensemble with for loop instead of NPTC 
#   average volume in status 1 is now taken from the last 50% (instead of 30%) of data
#   added stress output in switching simulations
#   fixed universal constant kB = 8.617343e-5 (instead of 8.616343e-5 when writing Matlab file)
#   data files MEAM_F#.dat is now called work_F#.dat, MEAM_F#.out is now prop_F#.out
#
# How to use:
#
#   Step 1.  Compile MD++ with the potential you want to use, for example
#
#             make sworig build=R SYS=$MDPLUS_SYS PY=yes
#                                      or
#             make meam-lammps build=R SYS=$MDPLUS_SYS PY=yes
#  
#            see http://micro.stanford.edu/wiki/MD++_Manuals
#            for more instructions on compiling MD++
#
#   Step 2.  Run MD++.
#
#            Because there are many steps required to compute melting point,
#             we need to run the same python script using different input arguments.
#            The following jobs need to be run.
#  
#  (Determine equilibrium volume of liquid and solid and compute hessian matrix of solid)
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  1 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  7 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  2 1 SW_Si
#
#  (Switching simulation for free energy of solid)
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  3 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  4 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  5 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  6 1 SW_Si
#
#  (Switching simulation for free energy of liquid)
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  8 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py  9 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 10 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 11 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 12 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 13 1 SW_Si
#
#  (Compute free energy and determine melting point)
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 14 1 SW_Si
#
#            If you have a computer cluster, you can write a automatic script
#             that submit these jobs simultaneously. There are inter-dependence
#             between the jobs.  A job that depends on the completion of another
#             job will wait until a flag (file) is produced by the other job.
#
#            Describe the meaning of the three input arguments.
#
#               First argument specifies job status = 1,2,...,14, i.e. which job to run.
#
#               Second argument is job instance (idx)
#                   for status = 3,4,5,6 and status = 8,9,10,11,12,13, 
#                   you can run multiple jobs with different instance (idx)
#                   simultaneously to collect more statistics, for example,
#                   the following will run 4 jobs simultaneously for step 3:
#
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 1 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 2 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 3 SW_Si
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 4 SW_Si
#
#                Third argument is material name, which has to follow a convention
#             to be described below.
#
#            Describe the inter-dependence among steps: 
#            
#            1 -> 2, 7   (1 is needed before 2 and 7 can run)
#            2 -> 3, 4
#            3 -> 5, 6 
#            7 -> 8, 9
#            8 -> 12, 13
#            9 -> 10, 11
#
#            (14 is data analysis, and should be run after 1,2,...,13 have all finished)
#            
#            e.g. job 1 will produce equilibrium lattice constant of the solid
#                 at finite temperature, which is needed for computing the Hessian
#                 matrix in job 2, and for initial setup of switching simulation
#                 in job 7.
#           
#            Describe job status [1-16] :
#	        status 1 : determine thermal expansion of solid at T1 K 
#		status 2 : compute hessian matrix of quasi-harmonic crystal(QHC)
#		status 3 : adiabatic switching from real solid to QHC
#		status 4 : adiabatic switching from QHC to real solid
#		status 5 : reversible scaling from T1 to T1/lambda1
#               status 6 : reversible scaling from T1/lambda to T1
#            
#               status 7 : determine thermal expansion of liquid at T2 K
#		status 8 : adiabatic switching from real liquid to gaussian fluid
#		status 9 : adiabatic switching from gaussian fluid to real liquid
#		status 10: switching from gaussian fluid to ideal gas
#		status 11: switching from ideal gas to gaussian fluid
#		status 12: reversible scaling from T2 to T2/lambda2
#		status 13: reversible scaling from T2/lambda2 to T2
#
#		status 14: compute (QHA) free energy of solid from Hessian matrix
#		           compute free energy of solid and liquid and melting point
#
#           e.g. 
#                $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 1 SW_Si
#
#   First argument 3 means "Run job (status) 3"
#   Second argument 1 means "Save the results in ~~~_1.dat or ~~_1.out" 
#   Third arugement SW_Si means "Use the conditions specified for SW_Si"
#
#   Finally, the job (status) 15 will generate a plot of solid and liquid free energy
#     as functions of temperature. Melting point will be printed to the screen.
#     More data will be saved into file SW_Si_Publication_Info.dat, 
#     including entropy, latent heat, error analysis, etc.
# 
#---------------------------------------------------------------------------------------
#
# Advanced topics:
#
#    To turn graphic (X-window) on, use 'On' as the fourth argument in command line, e.g.
#
#                $MDPLUS_EXE scripts/ME346B/melting_cubic.py 1 1 SW_Si On
#
#    Do this only when running the job interactively (for testing purposes).
#
#    When running meam-lammps the last argument has to be the same
#             element type in the meamf file, such as
#
#             $MDPLUS_EXE scripts/ME346B/melting_cubic.py 3 1 Siz
#
#    To modify initial conditions such as T_solid_0, T_liquid_0, reversible scaling factor,
#    change settings in dictionary datastore after the function definitions before the 
#    main program.
#
#    Naming convention for the third argument (material) in command line is as follows
#
#      Ta, Mo, W, Siz, Cu, Au, Ag, Ge   => meam materials
#      ta, mo, w 			=> fs materials
#      auf, cuf			        => eam materials
#      SW_Si, SW_Ge			=> sw materials
#
#-----------------------------------------------------------------------

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import os
import time
import mdpp
import numpy as np
import pickle

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd( status, material ):
    #mdpp.cmd("setnolog")
    mdpp.cmd("logfilename = A%d.log "%status)
    mdpp.cmd("setoverwrite")
    try:
        os.mkdir("runs/Single_Elem_Tm")
    except OSError:
        print("runs/Single_Elem_Tm already exists")
    if status < 7:
        mdpp.cmd("dirname = runs/Single_Elem_Tm/%s_Solid"%(material))
    elif status < 14:
        mdpp.cmd("dirname = runs/Single_Elem_Tm/%s_Liquid"%(material))
    else: 
        raise Exception("initmd should not be called for status >= 14")
        

    mdpp.cmd("zipfiles = 0 writeall = 1")

#--------------------------------------------
# Translate CS to the crystal structure needed for makecrystal
def get_struc ( CS ):
    if CS == "ZB":
        return "zinc-blende"
    elif CS == "FCC":
        return "face-centered-cubic"
    elif CS == "BCC":
        return "body-centered-cubic"
    elif CS == "DC":
        return "diamond-cubic"
    elif CS == "HCP":
        return "hexagonal-ortho"
    else:
        return CS

#--------------------------------------------
# Read the potential file
def readfs( material ):
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', '%s_pot'%(material)))
    mdpp.cmd('readpot')

def readmeam_baskes( material ):
    mdpp.cmd('meamfile = ' + os.path.join(mdpp_dir, 'Fortran/MEAM-baskes/meamf'))
    if 'Au' in material or 'Si' in material:
        mdpp.cmd('meafile = ' + os.path.join(mdpp_dir, 'Fortran/MEAM-baskes/meafile_Au_Si_Stanford'))
        mdpp.cmd('ntypes = 2  ename0 = "Au1  " ename1 = "Si4  " ') # leave spaces to be compatible with fortran
        mdpp.cmd('rcut = 6.0  kode0 = "library" ')
    mdpp.cmd('readMEAM    NNM = 300 ')

def readmeam_lammps( material ):
    mdpp.cmd('meamfile = ' + os.path.join(mdpp_dir, 'Fortran/MEAM-baskes/meamf'))
    mdpp.cmd('nspecies = 1  element0 = %s'%(material))
    if 'Au' in material or 'Si' in material:
        mdpp.cmd('meafile = ' + os.path.join(mdpp_dir, 'Fortran/MEAM-baskes/AuSi.meam'))
    mdpp.cmd('rcut = 6.0  readMEAM  NNM = 300')

def readeam( material ):
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'EAMDATA/Eamdata_%soiles.txt'%(material)))
    mdpp.cmd('eamgrid = 500  readeam  NNM = 300')

def set_Au_Si_species ( material ):
    if material != "Au" and material != "Si":
        return
    else:
        mdpp.cmd('nspecies = 2  element0 = "Au"  element1 = "Si" ')
        mdpp.cmd('atommass = [ 196.96655 28.0855 ] ')
        if material == "Au":
            mdpp.cmd('fixallatoms input = 0 setfixedatomsspecies freeallatoms')
        elif material == 'Si':
            mdpp.cmd('fixallatoms input = 1 setfixedatomsspecies freeallatoms')

def readpotential( pot_type, material ):
    if pot_type == 'fs':
        readfs(material)
    elif pot_type == 'meam-baskes':
        readmeam_baskes(material)
    elif pot_type == 'meam-lammps':
        readmeam_lammps(material)
    elif pot_type == 'eam':
        readeam(material)
    elif pot_type == 'sw':
        print("no need to read file for sw potential")
    else:
        print("Unknown pot_type (%s), not reading any potential files"%(pot_type))

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width = 400 win_height = 400
    atomcolor = orange highlightcolor = purple  bondcolor = red
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 2 # 1: display scaled coordinates, 2: real coordinates, 3: energy of atoms when clicked
    plotfreq = 200
    rotateangles = [ 0 0 0 1.2 ]
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot plot")

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal( material, CS, a, L ):
    # create perfect crystal of specified lattice constant a and lattice size L
    mdpp.cmd("crystalstructure = %s"%(get_struc(CS)))
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(L,L,L))
    mdpp.cmd("makecrystal")
    mdpp.cmd("eval")

#--------------------------------------------
# Create Liquid Configuration
def create_liquid( material, CS, a, L ):
    create_crystal( material, CS, a, L)
    mdpp.cmd("srand48bytime  randomposition ")

#--------------------------------------------
# Conjugate gradient relaxatoin
def relax_fixbox():
    mdpp.cmd('''
    conj_ftol = 2e-6 conj_itmax = 2000 conj_fevalmax = 20000
    conj_fixbox = 1 conj_fixboxvec = [ 0 1 1
                                       1 0 1
                                       1 1 0 ]
    relax
    ''')

def relax_freebox():
    mdpp.cmd('''
    conj_ftol = 2e-6 conj_itmax = 2000 conj_fevalmax = 20000
    conj_fixbox = 0 conj_fixboxvec = [ 0 1 1
                                       1 0 1
                                       1 1 0 ]
    relax
    ''')

#--------------------------------------------
# MD simulation setting
def setup_md( status, idx, atom_mass, time_step ):
    mdpp.cmd(" atommass = %f"%(atom_mass)) # (g/mol)
    mdpp.cmd(" timestep = %g"%(time_step)) # (ps)
    mdpp.cmd('''
    equilsteps = 0  DOUBLE_T = 0
    saveprop = 1 savepropfreq = 100 
    savecn = 1 savecnfreq = 999999  
    savecfg = 1 savecfgfreq = 999999
    printfreq = 100
    ''')
    if status == 1 or status == 7:
        mdpp.cmd("savecnfreq = 99999  savecfgfreq = 99999")
    mdpp.cmd('outpropfile  = "prop_F%d_%d.out"'%(status,idx))
    mdpp.cmd('intercnfile  = "inter_F%d_.cn"'%(status))
    mdpp.cmd('intercfgfile = "inter_F%d_.cfg"'%(status))

    mdpp.cmd('''
    openpropfile
    openintercnfile
    openintercfgfile
    wallmass = 2e3
    vt2 = 2e28
    boxdamp = 1e-3
    saveH
    fixboxvec = [ 0 1 1
                  1 0 1
                  1 1 0 ]
    stress = [ 0 0 0 0 0 0 0 0 0 ]
    output_fmt = "curstep EPOT KATOM HELMP Tinst TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33 OMEGA"
    writeall = 1
    ''')

#--------------------------------------------
# Scale simulation box
def scale_H ( eT ):
    mdpp.cmd('input = [ 1 1 %g ]  changeH_keepS '%(eT))
    mdpp.cmd('input = [ 2 2 %g ]  changeH_keepS '%(eT))
    mdpp.cmd('input = [ 3 3 %g ]  changeH_keepS '%(eT))

#--------------------------------------------
# End MD++
def exitmd ():
    mdpp.cmd('quit ')

#--------------------------------------------
# Flag file utilities for enforcing dependencies between job (status)
def make_flag ( status ):
    f = open("flag", "a+")
    f.write("%d\n"%(status))
    f.close()

def wait_flag ( status ):
   if status == 1 or status == 7:
       fp = open("info.dat", "w")
       fp.write("a0 L eT Ecoh V T NP\n")
       fp.close()
   else:
       if not os.path.exists("flag"):
           print("file flag does not exist yet.  Run status == 1 to create file.")
       else:
           content = np.loadtxt("flag")
           if not np.isin(status, content):
               print("file flag does not contain %d.  Wait for dependency job to finish."%(status))
       while True:
           if os.path.exists("flag"):
               content = np.loadtxt("flag")
               if np.isin(status, content):
                   break
           time.sleep(60)
       

def protect_run_status ( status ):
    if os.path.exists("flag"):
        content = np.loadtxt("flag")
        if np.isin(status, content):
            print("status (%d) already contained in flag file! Exit MD++"%(status))
            exitmd()
    else:
        make_flag(status)

#--------------------------------------------
# Get data from information file
def get_data_from_info_file (data_type):
    data = np.loadtxt("info2.dat")
    if data_type == "a0":
        return data[0]
    elif data_type == "L":
        return data[1]
    elif data_type == "eT":
        return data[2]
    elif data_type == "Ecoh":
        return data[3]
    elif data_type == "V":
        return data[4]
    elif data_type == "T":
        return data[5]
    elif data_type == "NP":
        return data[6]

#--------------------------------------------
# Database for simulation setting
datastore = { 
  "constants": {             # Universal constants 
        "kB" :               8.617343e-5,     # eV/K
      "hbar" :               6.58211899e-16,  # eV s
        "Na" :               6.02214179e23    # unitless
      },
  "settings":  {             # Settings for free energy calculations
  "N_steps_equil_solid"  :   2000000, # Number of MD steps for initial equilibration of solid
  "N_steps_equil_liquid" :   2000000, # Number of MD steps for initial equilibration of liquid
  "N_steps_equil_pre_switch":100000,  # Number of MD steps equilibration before switching
  "N_steps_switch":          1000000, # Number of MD steps for switching simulation (for production runs may want to increase)
  "N_iters_pressure_equil":  200,     # Number of iterations for equilibrating pressure with NVTC simulation
  "coeff_deps_dp":           5e-7,    # coefficient for adjusting strain given average pressure
  "N_switch_runs" :          2,       # Number of switching runs (for production runs you may want to increase this to 6 or 20)
  "N_switch_runs_Gaussian" : 4        # Number of switching runs for status 10, 11
      },
    "SW_Si": {
        "a":              5.431, # lattice constant a : this will be recomputed in step 1
        "L":                  4, # size of cubic cell L : size of L x L x L simulation cell will be used. 
        "T_solid_0" :      1600, # initial temperature for solid free energy computation.
        "eT_solid_0":     0.002, # elastic strain at T_solid_0 : This will be computed in step 1.
        "factor_solid_RS":  0.8, # solid free energy will be obtained in the range of T=T_solid_0 ~ T=T_solid_0/factor_solid_RS
        "T_liquid_0":      2000, # initial temperature for liquid free energy computation.
        "eT_liquid_0":     0.02, # strain at T_liquid_0 : This will be computed in step 7.
        "factor_liquid_RS":1.25, # liquid free energy will be obtained in the range of T=T_liquid_0 ~ T=T_liquid_0/factor_liquid_RS
        "factor_gauss":     0.1, # in ideal gas - Gaussian potential switching step, gaussian potential will be changed from U 
                                 # to factor_gauss*U in 1 switching. total six switching will lead to (0.1)^6*U, practically zero 
        "atom_mass":     28.086, # atomic mass of the material
        "CS":              "DC", # crystal structure of the material
        "pot_type":        "sw", # interatomic potential type
        "time_step":     0.0001  # time step in ps
    }, 
 "SW_Ge":{ "a": 5.6575,        "L": 4,              "T_solid_0": 2500,        "eT_solid_0": 0.002, "factor_solid_RS": 0.8, 
           "T_liquid_0": 3125, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.25, "factor_gauss": 0.1, "atom_mass": 72.64, "CS": "DC",
           "pot_type"  : "sw", "time_step": 0.0001
    },
   "auf":{ "a": 4.078,         "L": 6,              "T_solid_0": 730,        "eT_solid_0": 0.002, "factor_solid_RS": 0.2, 
           "T_liquid_0": 1170, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.3, "factor_gauss": 0.1, "atom_mass": 196.9665, "CS": "FCC",
           "pot_type"  : "eam", "time_step": 0.0001
    },
   "cuf":{ "a": 3.615,         "L": 6,              "T_solid_0": 1080,       "eT_solid_0": 0.00,  "factor_solid_RS": 0.8, 
           "T_liquid_0": 1350, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.25,"factor_gauss": 0.1, "atom_mass": 63.54,    "CS": "FCC",
           "pot_type"  : "eam", "time_step": 0.0001
    },
    "w": { "a": 3.1652,        "L": 6,              "T_solid_0": 2400,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.5, 
           "T_liquid_0": 4500, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.25,"factor_gauss": 0.1,  "atom_mass": 183.85,  "CS": "BCC",
           "pot_type"  : "fs", "time_step": 0.0001
    },
    "ta":{ "a": 3.3058,        "L": 6,              "T_solid_0": 3600,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.8, 
           "T_liquid_0": 4500, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.25,"factor_gauss": 0.1,  "atom_mass": 180.948, "CS": "BCC",
           "pot_type"  : "fs", "time_step": 0.0001
    },
    "mo":{ "a": 3.1472,         "L": 6,              "T_solid_0": 1600,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.5, 
           "T_liquid_0": 3400, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.3, "factor_gauss": 0.1,  "atom_mass": 95.94,  "CS": "BCC",
           "pot_type"  : "fs", "time_step": 0.0001
    },
   "Siz":{ "a": 5.431,         "L": 4,              "T_solid_0": 1600,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.8, 
           "T_liquid_0": 2000, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.25,"factor_gauss": 0.1,  "atom_mass": 28.086, "CS": "DC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
 "TR_Si":{ "a": 5.431,         "L": 4,              "T_solid_0": 2210,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.85, 
           "T_liquid_0": 2760, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.15,"factor_gauss": 0.1,  "atom_mass": 28.086, "CS": "DC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Ta":{ "a": 3.303,         "L": 6,              "T_solid_0": 800,        "eT_solid_0": 0.002,  "factor_solid_RS": 0.2, 
           "T_liquid_0": 4600, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.3, "factor_gauss": 0.1,  "atom_mass": 180.948, "CS": "BCC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Mo":{ "a": 3.15,          "L": 6,              "T_solid_0": 800,        "eT_solid_0": 0.002,  "factor_solid_RS": 0.2, 
           "T_liquid_0": 3400, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.4, "factor_gauss": 0.1,  "atom_mass": 95.94,  "CS": "BCC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Au":{ "a": 4.078,         "L": 5,              "T_solid_0": 840,        "eT_solid_0": 0.002636, "factor_solid_RS": 0.7, 
        "T_liquid_0": 1170, "eT_liquid_0": -0.02486,"factor_liquid_RS": 1.3, "factor_gauss": 0.1, "atom_mass": 196.9665, "CS": "FCC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Ge":{ "a": 5.6575,        "L": 4,              "T_solid_0": 1200,        "eT_solid_0": 0.0168, "factor_solid_RS": 0.8, 
        "T_liquid_0": 1500, "eT_liquid_0":-0.00388, "factor_liquid_RS": 1.25, "factor_gauss": 0.1, "atom_mass": 72.64, "CS": "DC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Ag":{ "a": 4.08,          "L": 5,              "T_solid_0": 840,        "eT_solid_0": 0.002,  "factor_solid_RS": 0.7, 
           "T_liquid_0": 1200, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.4, "factor_gauss": 0.1, "atom_mass": 107.87, "CS": "FCC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
    "Cu":{ "a": 3.603,         "L": 5,              "T_solid_0": 1000,       "eT_solid_0": 0.002,  "factor_solid_RS": 0.7, 
           "T_liquid_0": 1400, "eT_liquid_0": 0.02, "factor_liquid_RS": 1.4, "factor_gauss": 0.1, "atom_mass": 63.54,  "CS": "FCC",
           "pot_type"  : "meam-lammps", "time_step": 0.0001
    },
}

#*******************************************
# Main program starts here
#*******************************************
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 1
print('status = %d'%(status))

idx = int(sys.argv[2]) if len(sys.argv) > 2 else 1
print('idx = %f'%(idx))

material = str(sys.argv[3]) if len(sys.argv) > 3 else 'SW_Si'
print('material = %s'%(material))

graphic = str(sys.argv[4]) if len(sys.argv) > 4 else 'Off'
print('graphic = %s'%(graphic))

# Unpack variables from datastore
a                = datastore[material]["a"]
L                = datastore[material]["L"]
T_solid_0        = datastore[material]["T_solid_0"]
eT_solid_0       = datastore[material]["eT_solid_0"]
factor_solid_RS  = datastore[material]["factor_solid_RS"]
T_liquid_0       = datastore[material]["T_liquid_0"]
eT_liquid_0      = datastore[material]["eT_liquid_0"]
factor_liquid_RS = datastore[material]["factor_liquid_RS"]
factor_gauss     = datastore[material]["factor_gauss"]
atom_mass        = datastore[material]["atom_mass"]
CS               = datastore[material]["CS"]
pot_type         = datastore[material]["pot_type"]
time_step        = datastore[material]["time_step"]

N_steps_equil_solid     = datastore["settings"]["N_steps_equil_solid"]
N_steps_equil_liquid    = datastore["settings"]["N_steps_equil_liquid"]
N_steps_equil_pre_switch= datastore["settings"]["N_steps_equil_pre_switch"]
N_steps_switch          = datastore["settings"]["N_steps_switch"]
N_iters_pressure_equil  = datastore["settings"]["N_iters_pressure_equil"]
coeff_deps_dp           = datastore["settings"]["coeff_deps_dp"]
N_switch_runs           = datastore["settings"]["N_switch_runs"]
N_switch_runs_Gaussian  = datastore["settings"]["N_switch_runs_Gaussian"] 

if status < 14:
    initmd(status, material)
    # wait until any computation need for step n is done 
    # can run as long as its status shows up in flag file
    wait_flag(status)

    # save datastore and material into pickle file
    if status == 1:
        with open('datastore.pickle', 'wb') as f:
            print('save datastore into pickle file')
            pickle.dump([datastore, material], f)

    # do not run status 1 or 7 again if flag file shows it has run already
    if status == 1 or status == 7:
        protect_run_status(status)

    if graphic == 'On':
        openwindow()

    readpotential(pot_type, material)

    if status <= 6:
        T = T_solid_0
        T_factor = factor_solid_RS
    else:
        T = T_liquid_0
        T_factor = factor_liquid_RS

if status == 1:
    # Equilibrate solid to measure thermal strain eT at T1
    print("status == 1: Equilibrate solid at finite temperature")

    # get 0K properties for solid and save into info1.dat
    create_crystal(material, CS, a, L)
    mdpp.cmd("finalcnfile = %gK_perf_crystal.cn writecn"%(0))
    relax_freebox()
    mdpp.cmd("eval ")
    NP    = mdpp.get("NP")
    EPOT0 = mdpp.get("EPOT")
    Ecoh0 = EPOT0 / NP
    V0    = mdpp.get("OMEGA")
    a0    = V0**(1.0/3.0)/L
    fp = open("info1.dat", "w")
    fp.write("%20.12e %d %g %20.12e %20.12e %g %d\n"%(a0,L,0.0,Ecoh0,V0,0.0,NP))
    fp.close()

    setup_md(status, idx, atom_mass, time_step)

    # run MD simulation of solid at T1 (using NPTC ensemble) - (old algorithm in Tcl code)
    #mdpp.cmd('NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] T_OBJ = %g '%(T))
    #mdpp.cmd('ensemble_type="NPTC" integrator_type="Gear6" ')
    #mdpp.cmd('NHChainLen = 4 srand48bytime initvelocity totalsteps=%d '%(N_steps_equil_solid))
    #mdpp.cmd('run ')

    # run MD simulation of solid at T1 (using NVTC ensemble and for loop)
    mdpp.cmd("NHMass = [ 2e-3 2e-6 2e-6 2e-2 ] T_OBJ = %g"%(T))
    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdsteps  = N_steps_equil_solid//N_iters_pressure_equil
    propfreq = mdpp.get("savepropfreq")
    mdpp.cmd("NHChainLen = 4 srand48bytime initvelocity totalsteps=%d "%(mdsteps))

    Lx0 = mdpp.get("H_11")
    fid = open("P_L.dat","w")
    for iter in range(N_iters_pressure_equil):
        mdpp.cmd("continue_curstep = 1 run ")
        property_data = np.loadtxt("prop_F%d_%d.out"%(status,idx))
        # average over second half of the simulation
        nstart = mdsteps//propfreq//2
        sig_xx = np.mean( property_data[-nstart:,5] )
        sig_yy = np.mean( property_data[-nstart:,6] )
        sig_zz = np.mean( property_data[-nstart:,7] )
        pressure = (sig_xx + sig_yy + sig_zz) / 3.0 * 160.2e3 # convert eV/A^3 to MPa
        Lx = mdpp.get("H_11")
        strain = (Lx - Lx0)/Lx0
        print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
        print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
        fid.flush()

        # Adjust box size according to pressure
        if iter < N_iters_pressure_equil-1:
            eps_T = pressure * coeff_deps_dp
            mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

    fid.close()

    # compute average volume at T1
    prop = np.loadtxt("prop_F%d_%d.out"%(status, idx))
    Ndata = prop.shape[0]
    V  = np.mean(prop[Ndata*5//10:,11])

    # compute thermal strain eT and Ecoh at T1 and save into info2.dat #      
    a  = V**(1.0/3.0)/L
    eT = (a/a0) - 1.0
    create_crystal(material, CS, a, L)
    relax_fixbox()
    mdpp.cmd("eval")
    EPOT = mdpp.get("EPOT")
    Ecoh = EPOT / NP
    fp = open("info2.dat", "w")
    fp.write("%20.12e %d %20.12e %20.12e %20.12e %20.12e %d\n"%(a0,L,eT,Ecoh0,V0,T,NP))
    fp.close()

    # activate step (status) 2 
    make_flag(2)
    
elif status == 2:
    # Computing hessian matrix for quasi-harmonic crystal
    # read info2.dat and make crystal with strain eT #
    a  = get_data_from_info_file("a0")
    L  = get_data_from_info_file("L")
    eT = get_data_from_info_file("eT")
    create_crystal(material, CS, a, L)
    scale_H(eT)
    relax_fixbox()

    # compute hessian matrix to get quasi-harmonic crystal
    mdpp.cmd("input = 0  calHessian ")
    os.rename("hessian.out", "hessian%g.out"%(T))

    # activate step (status) 3 and 4
    make_flag(3)
    make_flag(4)

elif status == 3:
    # Switching from real solid potential to quasi-harmonic approximation of the crystal
    # create crystal with thermal strain eT
    a  = get_data_from_info_file("a0")
    L  = get_data_from_info_file("L")
    eT = get_data_from_info_file("eT")
    create_crystal(material, CS, a, L)
    scale_H(eT)

    mdpp.cmd("finalcnfile = %gK_perf_crystal.cn writecn setconfig1"%(T))
    mdpp.cmd('incnfile = "hessian%g.out"  readHessian '%(T))

    # equilibrate solid before switching run
    print(" equilibrations simulation")
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NVTC" integrator_type="Gear6" ')
    mdpp.cmd('NHChainLen = 4 srand48bytime initvelocity totalsteps=%d saveprop = 0 '%(N_steps_equil_pre_switch))
    mdpp.cmd('run ')
    mdpp.cmd('finalcnfile = "%gK_equil_crystal.cn" writeall = 1 writecn '%(T))

    # activate step (status) 5 and 6
    make_flag(5)
    make_flag(6)

    # switching setup
    Ecoh = get_data_from_info_file("Ecoh")
    mdpp.cmd("Ecoh = %20.12e "%(Ecoh))
    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_crystal.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("initvelocity run ")

        # switching run
        mdpp.cmd("lambda0 = 0 lambda1 = 1 refpotential = 1 ")
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 4:
    # Switching from quasi-harmonic approximation of the crystal to the real solid
    # create crystal with thermal strain eT
    a  = get_data_from_info_file("a0")
    L  = get_data_from_info_file("L")
    eT = get_data_from_info_file("eT")
    create_crystal(material, CS, a, L)
    scale_H(eT)

    mdpp.cmd("finalcnfile = %gK_perf_crystal.cn writecn setconfig1"%(T))
    mdpp.cmd('incnfile = "hessian%g.out"  readHessian '%(T))

    # equilibrate harmonic crystal at T1
    print(" equilibrations simulation")
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NVTC" integrator_type="Gear6" ')
    mdpp.cmd('NHChainLen = 4 srand48bytime initvelocity totalsteps=%d saveprop = 0 '%(N_steps_equil_pre_switch))
    mdpp.cmd("Ecoh = 0 switchfreq = 10 ecspring = 0 ")
    mdpp.cmd("lambda0 = 1 lambda1 = 1 refpotential = 1 ")
    mdpp.cmd("runMDSWITCH ")
    mdpp.cmd('finalcnfile = "%gK_equil_ha_crystal.cn" writeall = 1 writecn '%(T))

    # switching setup
    Ecoh = get_data_from_info_file("Ecoh")
    mdpp.cmd("Ecoh = %20.12e "%(Ecoh))
    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_ha_crystal.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = 1 lambda1 = 1 refpotential = 1 ")
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = 1 lambda1 = 0 refpotential = 1 ")
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 5:
    # Reversible scaling from T to T/solid_factor_RS for solid 
    mdpp.cmd('incnfile    = "%gK_equil_crystal.cn"  readcn setconfig1 '%(T))

    # switching simulation setup
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NPTC" integrator_type="Gear6" ')
    mdpp.cmd('NHChainLen = 4 srand48bytime initvelocity ')

    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_crystal.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("initvelocity run ")

        # switching run
        mdpp.cmd("lambda0 = 1 lambda1 = %g switchfunc = 1 refpotential = 4 "%(T_factor))
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

    # combine all property files for reversible scaling runs into a single file
    os.system("cat prop_F%d_%d.out >> prop_F%d.out"%(status,idx,status))

elif status == 6:
    # Reversible scaling from T/solid_factor_RS to T for solid
    T1 = T / T_factor
    mdpp.cmd('incnfile    = "%gK_equil_crystal.cn" readcn setconfig1 '%(T))
    
    # equilibrate with potential scaled by solid_factor_RS #
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NPTC" integrator_type="Gear6" ')
    mdpp.cmd('NHChainLen = 4 srand48bytime initvelocity totalsteps = %d '%(N_steps_equil_pre_switch))

    mdpp.cmd("switchfreq = 10 saveprop = 0 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd("lambda0 = %g  lambda1 = %g  switchfunc = 1  refpotential = 4 "%(T_factor,T_factor))
    mdpp.cmd("runMDSWITCH")
    mdpp.cmd('finalcnfile = "%gK_equil_crystal.cn" writeall = 1 writecn '%(T1))

    # switching setup
    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_crystal.cn" '%(T1))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = %g lambda1 = %g switchfunc = 1 refpotential = 4 "%(T_factor,T_factor))
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = %g lambda1 = 1 switchfunc = 1 refpotential = 4 "%(T_factor))
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

    # combine all property files for reversible scaling runs into a single file
    os.system("cat prop_F%d_%d.out >> prop_F%d.out"%(status,idx,status))

elif status == 7:
    # Equilibrate liquid at T2 
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    # read 0K solid properties from info1.dat #
    time.sleep(2)
    infofile = '../%s_Solid/info1.dat'%(material) 
    while not os.path.exists(infofile):
       time.sleep(10)
    data = np.loadtxt(infofile)
    a0 = data[0]
    print("a0 = %g"%(a0))

    # using lattice constant from info1.dat, construct a supercell with randomized atoms
    create_liquid(material, CS, a0, L) # contains srand48bytime
    relax_fixbox()

    mdpp.cmd('finalcnfile = "0K_liquid.cn" writeall = 1 writecn ')

    setup_md(status, idx, atom_mass, time_step)

    # equilibrate the liquid at T2 (using NPTC ensemble) - (old algorithm in Tcl code)
    #mdpp.cmd('T_OBJ = %g '%(T))
    #mdpp.cmd('ensemble_type="NPT" integrator_type="Gear6" ')
    #mdpp.cmd('initvelocity totalsteps=%d '%(N_steps_equil_liquid))
    #mdpp.cmd('run ')

    # run MD simulation of solid at T1 (using NVTC ensemble and for loop)
    mdpp.cmd("NHMass = [ 2e-3 2e-6 2e-6 2e-2 ] T_OBJ = %g"%(T))
    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdsteps  = N_steps_equil_solid//N_iters_pressure_equil
    propfreq = mdpp.get("savepropfreq")
    mdpp.cmd("NHChainLen = 4 initvelocity totalsteps=%d "%(mdsteps))

    Lx0 = mdpp.get("H_11")
    fid = open("P_L.dat","w")
    for iter in range(N_iters_pressure_equil):
        mdpp.cmd("continue_curstep = 1 run ")
        property_data = np.loadtxt("prop_F%d_%d.out"%(status,idx))
        # average over second half of the simulation
        nstart = mdsteps//propfreq//2
        sig_xx = np.mean( property_data[-nstart:,5] )
        sig_yy = np.mean( property_data[-nstart:,6] )
        sig_zz = np.mean( property_data[-nstart:,7] )
        pressure = (sig_xx + sig_yy + sig_zz) / 3.0 * 160.2e3 # convert eV/A^3 to MPa
        Lx = mdpp.get("H_11")
        strain = (Lx - Lx0)/Lx0
        print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
        print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
        fid.flush()

        # Adjust box size according to pressure
        if iter < N_iters_pressure_equil-1:
            eps_T = pressure * coeff_deps_dp
            mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

    fid.close()

    mdpp.cmd('finalcnfile="%gK_equil_liquid_eT.cn" writecn '%(T))

    mdpp.cmd('eval ')
    Ecoh = mdpp.get('Ecoh')
    NP   = mdpp.get('NP')

    # compute average volume at T2
    prop = np.loadtxt("prop_F%d_%d.out"%(status, idx))
    Ndata = prop.shape[0]
    V  = np.mean(prop[Ndata*5//10:,11])

    # compute thermal strain eT and Ecoh at T2 and save into info2.dat #      
    a  = V**(1.0/3.0)/L
    eT = (a/a0) - 1.0

    fp = open("info2.dat", "w")
    fp.write("%20.12e %d %20.12e %20.12e %20.12e %20.12e %d\n"%(a0,L,eT,Ecoh,V,T,NP))
    fp.close()

    # activate step (status) 8 and 9
    make_flag(8)
    make_flag(9)
    
elif status == 8:
    # Switching from real liquid to gaussian fluid 
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    # create initial liquid structure
    a0 = get_data_from_info_file("a0")
    L  = get_data_from_info_file("L")
    eT = get_data_from_info_file("eT")
    create_liquid(material, CS, a0, L) # contains srand48bytime
    scale_H(eT)
    relax_fixbox()

    # equilibration run of real liquid
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type = "NVT" integrator_type = "VVerlet" ')
    mdpp.cmd('initvelocity saveprop = 0 totalsteps = %d '%(N_steps_equil_pre_switch*2)) # equilibrate twice long 
    mdpp.cmd('run ')
    mdpp.cmd('finalcnfile = "%gK_equil_liquid.cn" writeall = 1 writecn setconfig1 '%(T))

    # activate step 12 and 13 for reversible scaling steps 
    make_flag(12)
    make_flag(13)

    # switching simulation setup
    mdpp.cmd('switchfreq = 10 saveprop = 1 openpropfile printfreq = 5000 ')
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')
    mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_liquid.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("initvelocity run ")

        # switching run
        mdpp.cmd("lambda0 = 0 lambda1 = 1 refpotential = 5 ")
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 9:
    # Switching from gaussian fluid to real liquid
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    # create initial liquid structure
    a0 = get_data_from_info_file("a0")
    L  = get_data_from_info_file("L")
    eT = get_data_from_info_file("eT")
    create_liquid(material, CS, a0, L) # contains srand48bytime
    scale_H(eT)
    relax_fixbox()

    # equilibration run of gaussian liquid
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type = "NVT" integrator_type = "VVerlet" ')
    mdpp.cmd('initvelocity saveprop = 0 totalsteps = %d setconfig1 '%(N_steps_equil_pre_switch*2)) # equilibrate twice long

    mdpp.cmd('switchfreq = 10  lambda0 = 1  lambda1 = 1  refpotential = 5 ')
    mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
    mdpp.cmd('runMDSWITCH ')
    mdpp.cmd('finalcnfile = "%gK_equil_ga_liquid.cn" writeall = 1 writecn '%(T))

    # activate step 10 and 11 for Gaussian fluid - ideal gas switching run
    make_flag(10)
    make_flag(11)

    # switching simulation setup
    mdpp.cmd('switchfreq = 10 saveprop = 1 openpropfile printfreq = 5000 ')
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_ga_liquid.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = 1  lambda1 = 1  refpotential = 5 ")
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = 1 lambda1 = 0 refpotential = 5 ")
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 10:
    # Switching from gaussian fluid to ideal gas
    lam_factor = 0.1
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    # read gaussian fluid structure
    mdpp.cmd('incnfile    = "%gK_equil_ga_liquid.cn" readcn setconfig1 '%(T))

    # switching simulation set up
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type = "NVT" integrator_type = "VVerlet" ')
    mdpp.cmd('srand48bytime initvelocity saveprop = 0 totalsteps = %d '%(N_steps_equil_pre_switch*2)) # equilibrate twice long
    mdpp.cmd('switchfreq = 10 saveprop = 1 openpropfile printfreq = 5000 ')
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')
    mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_ga_liquid.cn" '%(T))
    for x in range(0, N_switch_runs_Gaussian, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs_Gaussian))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = 1  lambda1 = 1  refpotential = 5 ") # 5: interpolation of real potential and Gaussian potential
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = 1 lambda1 = %20.12e refpotential = 6 "%(lam_factor)) # 6: Gassuian potential * lambda
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        W1 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**1,lam_factor**2))
        mdpp.cmd("runMDSWITCH ")
        W2 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**2,lam_factor**3))
        mdpp.cmd("runMDSWITCH ")
        W3 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**3,lam_factor**4))
        mdpp.cmd("runMDSWITCH ")
        W4 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**4,lam_factor**5))
        mdpp.cmd("runMDSWITCH ")
        W5 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**5,lam_factor**6))
        mdpp.cmd("runMDSWITCH ")
        W6 = mdpp.get("Wavg")

        fp = open("work_F%d.dat"%(status), "a+")
        fp.write("%d  %20.12e  %20.12e  %20.12e  %20.12e  %20.12e  %20.12e\n"%(x, W1, W2, W3, W4, W5, W6))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 11:
    # Switching from ideal gas to gaussian fluid
    lam_factor = 0.1
    T1 = T / (lam_factor**6)
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    # read gaussian fluid structure
    mdpp.cmd('incnfile    = "%gK_equil_ga_liquid.cn" readcn setconfig1 '%(T))

    # equilibration run 
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type = "NVT" integrator_type = "VVerlet" ')
    mdpp.cmd('srand48bytime initvelocity saveprop = 0 totalsteps = %d '%(N_steps_equil_pre_switch*2)) # equilibrate twice long
    mdpp.cmd('switchfreq = 10 saveprop = 1 openpropfile printfreq = 5000 ')
    mdpp.cmd('lambda0 = %20.12e  lambda1 = %20.12e '%(lam_factor**6,lam_factor**6))
    mdpp.cmd('refpotential = 6 switchfunc = 1 ') # 6: Gaussian potential * lambda
    mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
    mdpp.cmd('runMDSWITCH ')
    mdpp.cmd('finalcnfile = "%gK_equil_ga_liquid.cn" writeall = 1 writecn '%(T1))

    # switching simulation set up
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_ga_liquid.cn" '%(T1))
    for x in range(0, N_switch_runs_Gaussian, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs_Gaussian))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = %20.12e  lambda1 = %20.12e  refpotential = 6 switchfunc = 1"%(lam_factor**6,lam_factor**6))
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**6,lam_factor**5))
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        W6 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**5,lam_factor**4))
        mdpp.cmd("runMDSWITCH ")
        W5 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**4,lam_factor**3))
        mdpp.cmd("runMDSWITCH ")
        W4 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**3,lam_factor**2))
        mdpp.cmd("runMDSWITCH ")
        W3 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = %20.12e "%(lam_factor**2,lam_factor**1))
        mdpp.cmd("runMDSWITCH ")
        W2 = mdpp.get("Wavg")

        mdpp.cmd("lambda0 = %20.12e lambda1 = 1 "%(lam_factor))
        mdpp.cmd("runMDSWITCH ")
        W1 = mdpp.get("Wavg")

        fp = open("work_F%d.dat"%(status), "a+")
        fp.write("%d  %20.12e  %20.12e  %20.12e  %20.12e  %20.12e  %20.12e\n"%(x, W6, W5, W4, W3, W2, W1))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

elif status == 12:
    # Reversible scaling from T to T/liquid_factor_RS for liquid
    mdpp.cmd('incnfile    = "%gK_equil_liquid.cn"  readcn setconfig1 '%(T))
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")

    # switching simulation setup
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NPT" integrator_type="Gear6" ')
    mdpp.cmd('srand48bytime initvelocity ')

    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_liquid.cn" '%(T))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("initvelocity run ")

        # switching run
        mdpp.cmd("lambda0 = 1 lambda1 = %g switchfunc = 1 refpotential = 4 "%(T_factor))
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

    # combine all property files for reversible scaling runs into a single file
    os.system("cat prop_F%d_%d.out >> prop_F%d.out"%(status,idx,status))

elif status == 13:
    # Reversible scaling form T/liquid_factor_RS to T for liquid
    T1 = T / T_factor
    mdpp.cmd('incnfile    = "%gK_equil_liquid.cn" readcn setconfig1 '%(T))
    # for plotting map all atoms to primary cell
    mdpp.cmd("plot_map_pbc = 1 ")
    
    # equilibrate with potential scaled by solid_factor_RS #
    setup_md(status, idx, atom_mass, time_step)
    mdpp.cmd('T_OBJ = %g '%(T))
    mdpp.cmd('ensemble_type="NPT" integrator_type="Gear6" ')
    mdpp.cmd('srand48bytime initvelocity totalsteps = %d '%(N_steps_equil_pre_switch*2)) # equilibrate twice long

    mdpp.cmd("switchfreq = 10 saveprop = 0 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd("lambda0 = %g  lambda1 = %g  switchfunc = 1  refpotential = 4 "%(T_factor,T_factor))
    mdpp.cmd("runMDSWITCH")
    mdpp.cmd('finalcnfile = "%gK_equil_liquid.cn" writeall = 1 writecn '%(T1))

    # switching setup
    mdpp.cmd("switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ")
    mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" ')

    # run switching simulation
    mdpp.cmd('incnfile    = "%gK_equil_liquid.cn" '%(T1))
    for x in range(0, N_switch_runs, 1):
        print(" switching simulation %d / %d"%(x, N_switch_runs))
        # equilibration run
        mdpp.cmd("readcn totalsteps = %d saveprop = 0 "%(N_steps_equil_pre_switch))
        mdpp.cmd("lambda0 = %g lambda1 = %g switchfunc = 1 refpotential = 4 "%(T_factor,T_factor))
        mdpp.cmd("initvelocity runMDSWITCH ")

        # switching run
        mdpp.cmd("lambda0 = %g lambda1 = 1 switchfunc = 1 refpotential = 4 "%(T_factor))
        mdpp.cmd("totalsteps = %d saveprop = 1 runMDSWITCH "%(N_steps_switch))
        fp = open("work_F%d.dat"%(status), "a+")
        Wavg = mdpp.get("Wavg")
        Wtot = mdpp.get("Wtot")
        fp.write("%d  %20.12e  %20.12e\n"%(x, Wavg, Wtot))
        fp.close()
        mdpp.cmd('finalcnfile = "final_F%d_%04d.cn" writeall = 1 writecn '%(status, x))

    # combine all property files for reversible scaling runs into a single file
    os.system("cat prop_F%d_%d.out >> prop_F%d.out"%(status,idx,status))

elif status == 14:
    # Analyze data to compute free energy and melting point
    mdpp.cmd("logfilename = A%d.log "%status)
    mdpp.cmd("setoverwrite")
    # You can change the dirname to analyze data from a different run
    mdpp.cmd("dirname = runs/Single_Elem_Tm")
    #mdpp.cmd("dirname = runs/Single_Elem_Tm_sav1")
    #   If the data were produced by old tcl script, you may need to copy data files
    #   MEAM_F#.dat to work_F#.dat and MEAM_F#.out to prop_F#.out

    # compute quasi-harmonic approximation (QHA) of solid free energy from Hessian matrix
    print('T_solid_0 = %g'%(T_solid_0))
    hessianfile = '%s_Solid/hessian%g.out'%(material,T_solid_0)
    info_solid  = '%s_Solid/info2.dat'%(material)
    temp        = np.loadtxt(info_solid)
    Vol         = temp[4]
    EPOT        = temp[3]
    NP          = temp[6].astype(int)

    # Universal constants
    kB                     = datastore["constants"]["kB"]
    hbar                   = datastore["constants"]["hbar"]
    Na                     = datastore["constants"]["Na"]

    data = np.loadtxt(hessianfile)
    data2= np.reshape(-data,(NP*3,NP*3))
    data2= (data2+np.transpose(data2))/2
    D1, V= np.linalg.eig(data2)
    D1   = np.sort(D1)
    w    = np.sqrt(D1[3:]*16.022*Na/(atom_mass*0.001))
    F1   = -kB*T_solid_0*np.sum(np.log(np.divide(kB*T_solid_0/hbar, w))).real

    F = F1
    F_per = F / NP
    Fha = F + EPOT*NP
    print('Quasi-harmonic approximation:')
    print('F = {:.6f} (eV) dFhar = {:.6f} (eV) F_per = {:.6f} (eV)'.format(F+EPOT*NP,F,F_per))

    # load the work done during switching simulations
    data3  = np.loadtxt('%s_Solid/work_F3.dat'  %(material))
    data4  = np.loadtxt('%s_Solid/work_F4.dat'  %(material))
    data5  = np.loadtxt('%s_Solid/prop_F5.out'  %(material))
    data6  = np.loadtxt('%s_Solid/prop_F6.out'  %(material))

    free3       = np.mean(data3[:,1])
    free3_e     = np.std (data3[:,1])
    free4       = np.mean(data4[:,1])
    free4_e     = np.std (data4[:,1])
    dFre_Fha    = (free4-free3)/2.0
    dFre_Fha_e  = (free3_e+free4_e)/2.0
    Fre_T1      = Fha + dFre_Fha

    print('Free energy of real solid at T_solid_0:')
    print('Fre_T1 = %20.12e'%(Fre_T1))

    # solid free energy
    d = factor_solid_RS - 1.0
    lam = np.arange(1,factor_solid_RS+d/100000,d/10000)
    iter = np.floor(data5.shape[0]/10001).astype(int)
    temp1 = data5[:10001*iter,7].reshape((10001,iter), order='F')
    iter = np.floor(data6.shape[0]/10001).astype(int)
    temp2 = data6[:10001*iter,7].reshape((10001,iter), order='F')
    w1 = np.mean(temp1, axis=1)
    w2 = np.mean(temp2, axis=1)

    w1_inv = w2[-1] - np.flip(w2)
    w = (w1 - w1_inv) / 2.0
    T_solid = T_solid_0 / lam
    Fs = np.multiply((Fre_T1 + w)/T_solid_0 - 3.0/2*(NP-1)*kB*np.log(T_solid/T_solid_0), T_solid)
    print('Free energy of solid as a function of temperature:')
    print('Fs = [%20.12e, %20.12e, ..., %20.12e, %20.12e]'%(Fs[0],Fs[1],Fs[-2],Fs[-1]))

    # load the work done during switching simulations
    data8  = np.loadtxt('%s_Liquid/work_F8.dat' %(material))
    data9  = np.loadtxt('%s_Liquid/work_F9.dat' %(material))
    data10 = np.loadtxt('%s_Liquid/work_F10.dat'%(material))
    data11 = np.loadtxt('%s_Liquid/work_F11.dat'%(material))
    data12 = np.loadtxt('%s_Liquid/prop_F12.out'%(material))
    data13 = np.loadtxt('%s_Liquid/prop_F13.out'%(material))

    info_liquid = '%s_Liquid/info2.dat'%(material)
    temp        = np.loadtxt(info_liquid)
    Vliq        = temp[4]
    NP          = temp[6].astype(int)
    Tliq        = T_liquid_0

    # liquid free energy
    logfactorial = lambda x: (x+0.5)*np.log(x)-x+0.5*np.log(2*np.pi)
    logfac = logfactorial(NP-1)
    gam = np.sqrt(6.626068**2 * 6.022 / (1.3806503*atom_mass*0.001*Tliq*2*np.pi)) * 0.1
    Fid = -kB*Tliq*((NP-1)*np.log(Vliq/gam**3)-logfac)

    free8       = np.mean(data8[:,1])
    free8_e     = np.std (data8[:,1])
    free9       = np.mean(data9[:,1])
    free9_e     = np.std (data9[:,1])
    dFsw_Fga    = (free9-free8)/2.0
    dFsw_Fga_e  = (free9_e+free8_e)/2.0
    free10      = np.mean(np.sum(data10[:,1:7],axis=1))
    free11      = np.mean(np.sum(data11[:,1:7],axis=1))
    dFga_Fid    = (free11-free10)/2.0
    Fsw_T2      = Fid+dFga_Fid+dFsw_Fga

    print('Free energy of real liquid at T_liquid_0:')
    print('Fsw_T2 = %20.12e'%(Fsw_T2))

    d = factor_liquid_RS - 1.0
    lam2 = np.arange(1,factor_liquid_RS+d/100000,d/10000)
    iter = np.floor(data12.shape[0]/10001).astype(int)
    temp3 = data12[:10001*iter,7].reshape((10001,iter), order='F')
    iter = np.floor(data13.shape[0]/10001).astype(int)
    temp4 = data13[:10001*iter,7].reshape((10001,iter), order='F')
    w3 = np.mean(temp3, axis=1)
    w4 = np.mean(temp4, axis=1)

    w3_inv = w4[-1] - np.flip(w4)
    ww = (w3 - w3_inv) / 2.0
    T_liquid = T_liquid_0 / lam2
    Fl = np.multiply((Fsw_T2 + ww)/T_liquid_0 - 3.0/2*(NP-1)*kB*np.log(T_liquid/T_liquid_0), T_liquid)
    print('Free energy of liquid as a function of temperature:')
    print('Fl = [%20.12e, %20.12e, ..., %20.12e, %20.12e]'%(Fl[0],Fl[1],Fl[-2],Fl[-1]))

    # interpolate solid and liquid free energy
    Tmin = max(T_solid[0], T_liquid[-1])
    Tmax = min(T_solid[-1], T_liquid[0])
    dT = (Tmax-Tmin) / 100000
    T_grid = np.arange(Tmin,Tmax+dT/10,dT)
    Fs_interp = np.interp(T_grid, T_solid,  Fs)
    Fl_interp = np.interp(T_grid, np.flip(T_liquid), np.flip(Fl))

    ind_min = np.argmin(np.abs(Fs_interp - Fl_interp))
    T_m = T_grid[ind_min]
    print('T_m = %g K'%(T_m))

    import matplotlib.pyplot as plt
    plt.plot(T_grid, Fs_interp, T_grid, Fl_interp)
    plt.show()

else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for some seconds if X-window is on
if graphic == 'On':
    sleep_seconds = 10
    print("Python is going to sleep for %d seconds."%sleep_seconds)
    time.sleep(sleep_seconds)
