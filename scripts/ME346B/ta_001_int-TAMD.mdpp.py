# MD simulations of adatom on (001) surface of BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np
import pickle
from shutil import copyfile

sys.path.append(os.path.join(mdpp_dir,'scripts/python'))
from mdutil import loadcn

runsdir = os.path.join(mdpp_dir, 'runs/ta-001-int')

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd():
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ta-001-int")
    mdpp.cmd("NNM = 200")

#--------------------------------------------
# Read the potential file
def readpot():
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
    mdpp.cmd('readpot')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal(n, a):
    # create perfect crystal of specified size n and lattice constant a
    # equilibrium lattice constant of Ta is 3.3058 Angstrom
    mdpp.cmd("crystalstructure = body-centered-cubic")
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(n,n,n))
    mdpp.cmd("makecrystal finalcnfile = perf.cn writecn")
    mdpp.cmd("eval")

#--------------------------------------------
# Convert TAD .dat file to MD++ .cn file
def convert_dat_to_cn(infile, outfile):
    with open(infile) as fi:
        with open(outfile, 'w') as fo:
            line = fi.readline(); NP = int(line.split()[0])
            print("%d" % NP, file=fo)
            line = fi.readline(); 
            line = fi.readline(); box_size = [ float(a) for a in line.split() ]
            for i in range(NP):
                line = fi.readline(); ri = [float(a) for a in line.split()[0:3]]
                si = [ri[0]/box_size[0], ri[1]/box_size[1], ri[2]/box_size[2]]
                # shift scaled coordinates to center
                si[0] -= 0.5; si[1] -= 0.5; si[2] -= 0.25
                print("%20.12e %20.12e %20.12e" % (si[0], si[1], si[2]), file=fo)
            # write box matrix
            print("%20.12e %20.12e %20.12e" % (box_size[0], 0, 0), file=fo)
            print("%20.12e %20.12e %20.12e" % (0, box_size[1], 0), file=fo)
            print("%20.12e %20.12e %20.12e" % (0, 0, box_size[2]), file=fo)
            print("1 Ta", file=fo)
            print("%20.12e %20.12e" % (0, 0), file=fo)
            
    print("NP = %d" % NP)
    print("box_size = %s" % box_size) 

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width=400 win_height=400
    #atomradius = 0.9 bondradius = 0.3 bondlength = 0 
    atomcolor = cyan highlightcolor = purple  bondcolor = red
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 1 # display reduced coordinates of atoms when clicked
    #plot_atom_info = 2 # display real coordinates of atoms
    #plot_atom_info = 3 # displayenergy of atoms
    plotfreq = 10
    #
    rotateangles = [ 0 0 0 1.5 ]
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# Conjugate-Gradient relaxation with box fixed
def relax_fixbox():
    mdpp.cmd(' conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000 ')
    mdpp.cmd(' conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1 ')
    mdpp.cmd(' relax ')

#--------------------------------------------
# Use the string method to compute the minimum energy path 
# and energy barrier for transition between State A and State B   
# Input: StateA (config file of State A),  StateB (config file of State B)

def NEB_calculation(StateA, StateB, Ncopy = 41):
    
        mdpp.cmd('incnfile = ' + StateA + ' readcn setconfig1') # State A
        mdpp.cmd('incnfile = ' + StateB + ' readcn setconfig2') # State B
        mdpp.cmd('incnfile = ' + StateA + ' readcn ') # State A

        setup_window()
        # Comment out the following line if you don't want to open a window
        #openwindow()

        # Include all atoms in energy barrier calculation
        mdpp.cmd('fixallatoms constrain_fixedatoms freeallatoms ')

        mdpp.cmd('chainlength = %d allocchain   totalsteps = 500'%(Ncopy))
        mdpp.cmd('timestep = 0.01 printfreq = 2')

        mdpp.cmd('initRchain ')

        mdpp.cmd('''
        stringspec = [ 0  #0: interpolate surrounding atoms, 1: relax surrounding atoms
                       1  #redistribution frequency
                     ]
        stringrelax''')

        mdpp.cmd('finalcnfile = string.chain.500 writeRchain')
        
        
        copyfile('stringeng.out', 'stringeng_temp.out')
        
        data = np.loadtxt('stringeng_temp.out')
        eng = data[:,2::5]
        
        Eb = max(eng[-1,:])
        E_path = eng[-1,:-1]
        
        ## Print average temperature and pressure in the second half of the simulation
        #print("Eb = %f eV"%(max(eng[-1,:])))
        
        return Eb, E_path

#--------------------------------------------
# Get the atom coordinates from cn file 
        
def get_atom_data(cn_file):
    filename = os.path.join(runsdir, cn_file)
    nparticles, rawdata, h = loadcn(filename, structured_array=True, verbose=False)
    atom_data = rawdata['s']

    return atom_data, h, nparticles
    
#--------------------------------------------
# MD simulation setting
def setup_md():
    mdpp.cmd('''
    equilsteps = 0  timestep = 0.001 # (ps)
    atommass = 180.94788 # (g/mol)
    DOUBLE_T = 1
    saveprop = 1 savepropfreq = 10 openpropfile #run
    #savecn = 1 savecnfreq = 1000 openintercnfile
    #savecfg = 1 savecfgfreq = 1000
    plotfreq = 100 printfreq = 10
    vt2 = 1e28  #1e28 2e28 5e28
    wallmass = 2e4     # atommass * NP 
    boxdamp = 1e-3     #
    saveH # Use current H as reference (H0), needed for specifying stress
    fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
    ''')
    

#*******************************************
# Main program starts here
#*******************************************
# status 0: load initial configuration and visualize
#        1: NVT simulations at precomputed thermal strain
#        2: skip
#        3: long NVT MD simulations to capture surface atom diffusion
#        4: compare relaxed structures from begining and end of long MD simulation
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

T_high = float(sys.argv[2]) if len(sys.argv) > 2 else 2000
print('T_high = %f'%(T_high))

T_low = float(sys.argv[3]) if len(sys.argv) > 3 else 300
print('T_low = %f'%(T_low))

nstates = int(sys.argv[4]) if len(sys.argv) > 4 else 1
print('nstates = %d'%(nstates))

njumps = int(sys.argv[5]) if len(sys.argv) > 5 else 1
print('njumps = %d'%(njumps))

randseed = int(sys.argv[6]) if len(sys.argv) > 6 else 12345
print('randseed = %d'%(randseed))



if status == 0:
    # Load initial configuratin and visualize
    print("status == 0: NVE MD simulations")
    initmd()
    readpot()

    convert_dat_to_cn('../../scripts/ME346B/ta-001-int.start', 'ta-001-int.start.cn')
    mdpp.cmd('incnfile = ta-001-int.start.cn readcn')
    # reduce the empty space in z direction (was too much in original file)
    mdpp.cmd('input = [3 3 -0.5 ] changeH_keepR')
    mdpp.cmd('finalcnfile = ta-001-int.start.short.cn writecn')

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    sleep_seconds = 10
    
elif status == 1:
    
    # Parameters

    t_check = 2000             # Check state per t_check steps, t_check*dt in ps
    max_iter = 100             # Maximum steps for searching each state 
    print_freq = 2             # Print state position
    print_info = 1             # Whether to print info, print info = 1 to print# 
    
    nu_min_star = 1e-2         # Assumption of the minimum frequency prefactor (stopping criterion)
    dist_one_jump = 0.0909     # Distcane of one surface atom jump ~ 0.09090 +- 0.00001
    rtol = 0.00001             # Distance torlarance to compare two positions by one suface atom jump (distance = 0.09090)
    dt = 0.001                 # Time step (ps)
    kB = 8.6173324e-5          # Boltzmann constant (in eV/K)
    
    Ncopy = 41                 # Number of copied states in a min energy path chain
    
    State_next_exist = False   # Preset the state check
    
    
    initmd()
    readpot()
    mdpp.cmd('incnfile = ta-001-int.start.short.cn readcn')
    
    valid_jump_count = 1             # Count the valid jump
    data_jump = {}             # initialte a dictionary to store data
    
    for ijump in range(njumps):
        
        state_count = 1        # Count the state
        
        if ijump == 0:   
            # Run a short MD and then relax to find a more stable configuratoin for State A
            setup_md()
            mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
            mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
            mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T_low,randseed))
            mdpp.cmd("totalsteps = 5000 run")
            relax_fixbox()

            mdpp.cmd('finalcnfile = ta-001-TAD_A.cn writecn')
            StateA = 'ta-001-TAD_A.cn'
        
        if ijump > 0:
            # Run a short MD and then relax to find a more stable configuratoin for State A
            if State_next_exist:
                StateA = State_next                 # load the State_next of the previous jump as new State A
                State_next_exist = False            # reset to false for the next check               
                valid_jump_count = valid_jump_count + 1
            else:
                print("No states availbale for this jump, continue to the next jump")
        
        print('Predict jump %d'%(ijump+1))
        
        xA = get_atom_data(StateA)[0]               # Get scaled coordinates of atoms
        hA = get_atom_data(StateA)[1]               # Get simulation box size
        nparticlesA = get_atom_data(StateA)[2]      # Get number of particles
        State0 = StateA    
            
        x_list = xA[:,:,None]
        t_hi_list = [0]
        t_lo_list = [0]
        Eb_list = [0]
        Epath_list = np.zeros([Ncopy])  

        t_high_stop = np.inf
        stop_search = False

        for istate in range(nstates):

            initmd()
            readpot()
            setup_md()
            setup_window()
            # Comment out the following line if you don't want to open a window
            # openwindow()

            if print_info == 1:
                print('Search for state %d'%(istate+1))

            if stop_search:
                break

            for iter_num in range(max_iter):

                mdpp.cmd('incnfile = ' + State0 + ' readcn') 

                # Run MD in high temperature periodically
                mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
                mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
                mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T_high,randseed))
                mdpp.cmd("totalsteps = %d run"%(t_check))

                State_end = 'ta-001-TAD_end.cn'
                mdpp.cmd('finalcnfile = ' + State_end + ' writecn')

                # relax 
                relax_fixbox()

                State_final = 'ta-001-TAD_final.cn'
                mdpp.cmd('finalcnfile = ' + State_final + ' writecn')

                t_high = (iter_num+1)*t_check*dt + t_hi_list[-1]

                if print_info == 1 and iter_num%print_freq ==0:
                    print('  iter %d/%d t_high %g/%g: searching states'%(iter_num+1, max_iter, t_high, t_high_stop))

                if t_high > t_high_stop:
                    print ('t_high = %g, t_high_stop = %g, Stop searching'%(t_high,t_high_stop))
                    stop_search = True
                    break

                # check if the new state is different from A by only one jump

                x_final = get_atom_data(State_final)[0]            

                ds_to_A = xA-x_final    # distance between atoms in state A and state final with scaled coordinates
                ds_to_A = ds_to_A - np.round(ds_to_A)  # consider pbc
                max_diff_A = np.max(np.linalg.norm(ds_to_A,axis=1))   # max distance value to identify a jump event

                if abs(max_diff_A - dist_one_jump) < rtol:
                    State0 = StateA

                    # additional print if you find a different state from A
                    if print_info == 1:
                        print('  iter %d/%d t_high %g/%g: find a different state '%(iter_num+1, max_iter, t_high, t_high_stop))
                else:
                    State0 = State_end

                # check if the new state is already in the list

                # distance between atoms in each stored state and final state with scaled coordinates
                ds_to_list = x_list-x_final[:,:,None] 
                ds_to_list = ds_to_list - np.round(ds_to_list) # consider pbc
                max_diff_list = np.max(np.linalg.norm(ds_to_list,axis=1),axis=0) # max distance value to identify difference

                if np.all(max_diff_list > dist_one_jump) and abs(max_diff_A - dist_one_jump) < rtol:  

                    x_list = np.dstack((x_list,x_final))
                    t_hi_list = np.append(t_hi_list,(iter_num+1)*t_check*dt + t_hi_list[-1])

                    Eb_final,Epath_final = NEB_calculation(StateA, State_final, Ncopy)
                    Eb_list =  np.append(Eb_list,Eb_final)  
                    Epath_list = np.vstack((Epath_list,Epath_final))

                    t_lo_list = t_hi_list * np.exp(Eb_list/kB* (1/T_low - 1/T_high))

                    ind_t_lo_min = np.argmin(t_lo_list[1:])+1
                    x_next = x_list[:,:,ind_t_lo_min]   
                    t_lo_min = t_lo_list[ind_t_lo_min]

                    if nu_min_star > 0:
                        t_high_stop = 1/nu_min_star * (nu_min_star * np.min(t_lo_list[1:]))**(T_low/T_high)

                    State_save = 'ta-001-TAD_%d-%d.cn'%(valid_jump_count, state_count)
                    mdpp.cmd('finalcnfile = ' + State_save + ' writecn')
                    state_count = state_count + 1                    
                    
                    State_next = 'ta-001-TAD_%d-%d.cn'%(valid_jump_count, ind_t_lo_min)               
                    State_next_exist = True
                    
                    break

                if print_info == 1 and iter_num == max_iter-1:
                    print('No new state found given the max iters')

        if print_info == 1:
            print('Time of finding each state (ps):',t_hi_list)
            print('Energy barrier of each state (eV):',Eb_list)
     
        # Store useful data to dictionary
        data_jump[str(ijump+1)+'_jump'] = {'T_high': T_high, 'T_low': T_low, 'Epath_list': Epath_list, 'Eb_list': Eb_list,
                                           't_hi_list':t_hi_list, 't_lo_list':t_lo_list, 'h': hA, 'nparticles': nparticlesA}

        # Add coordinates of neighboring states to dictionary
        for num_x in range(np.shape(x_list)[-1]):      
            data_jump[str(ijump+1)+'_jump']['coord_state'+str(num_x)] = x_list[:,:,num_x]
    
    f = open("TAD_data_all_jumps.pkl","wb")
    pickle.dump(data_jump,f)
    f.close()
    
    
else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
