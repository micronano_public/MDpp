import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

latt0 = 3.184
c_over_a = 1.633

mdpp.cmd('''
# -*-shell-script-*-
# HCP structure
setnolog
setoverwrite
dirname = runs/HCP
#------------------------------------------------------------
#Create Perfect Lattice Configuration
#
latticestructure = hexagonal-ortho
# x-axis: [1 0 0] = [-1 2 -1 0]/3, 
# y-axis: [0 1 0] = [1 0 -1 0], 
# z-axis: [0 0 1] = [0 0 0 1] c-axis
latticesize = [ 1 0 0 6 
                0 1 0 4
                0 0 1 4 ]
''')

mdpp.cmd('latticeconst = [%f 0.0 %f]'%(latt0, latt0*c_over_a))

mdpp.cmd('''
makecrystal 
element0 = Mg atommass = 24.305 # g/mol
makecrystal
finalcnfile = perf.cn writecn
finalcnfile = perf.lammps writeLAMMPS
#------------------------------------------------------------
#Plot Configuration
#
atomradius = [1.3] #bondradius = 0.3 bondlength = 0
atomcolor0 = yellow atomcolor1 = "light grey"
highlightcolor = purple  bondcolor = red backgroundcolor = gray
fixatomcolor = yellow
plot_atom_info = 1
plotfreq = 10
rotateangles = [ 0 0 0 1.2 ]
openwin alloccolors rotate saverot refreshnnlist eval plot
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
