# MD simulations of liquid with LJ potential

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd():
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/lj-liquid")
    mdpp.cmd("NNM = 200")

#--------------------------------------------
# Set the potential parameters
def set_lj_param():
    mdpp.cmd(' nspecies = 2  element0 = "solvent" element1 = "solute" ')
    mdpp.cmd(' atommass = [ 39.948  39.948 ] # mass in g/mol for Ar ')
    mdpp.cmd(' C12_00 = 4  C6_00 = 4  C9_00 = 0 ')
    mdpp.cmd(' C12_01 = 4  C6_01 = 4  C9_01 = 0 ')
    mdpp.cmd(' C12_11 = 4  C6_11 = 4  C9_11 = 0 ')
    mdpp.cmd(' BOND_R0 = 3.3333   # Angstrom')
    mdpp.cmd(' BOND_K  = 9.3278   # eV/A^2')
    mdpp.cmd(' Rcut    = 8.3333   # Angstrom')
    mdpp.cmd(' initLJ ')
    mdpp.cmd(' NNM = 400 ')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal(n, a):
    # create perfect crystal of specified size n and lattice constant a
    mdpp.cmd("crystalstructure = simple-cubic")
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(n,n,n))
    mdpp.cmd("makecrystal finalcnfile = perf.cn writecn")
    mdpp.cmd("eval")

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [0.60 0.80 0.70 0.70] bondradius = 0.4
    atomcolor0 = cyan atomcolor1 = green atomcolor2 = red atomcolor3 = blue
    bondcolor = green backgroundcolor = gray70 fixatomcolor = yellow
    plotfreq = 100  rotateangles = [ 0 0 0 1.5 ] 
    plot_map_pbc = 1
    win_width = 400 win_height = 400
    ''')

#--------------------------------------------
# Initialize simulation cell (for running with ljbond)
def create_liquid_ljbond(Lx, Ly, Lz, NP):
    mdpp.cmd('input = [ %f %f %f  %d 0 0 0 0 0 0 0 0 0 0 ]' % (Lx, Ly, Lz, NP) )
    mdpp.cmd('makelipids')

# Initialize simulation cell (for running with lj2)
def create_liquid_lj2(Lx, Ly, Lz, N0, N1):
    mdpp.cmd('input = [ %f %f %f  %d %d ]' % (Lx, Ly, Lz, N0, N1) )
    mdpp.cmd('makeliquid')

#--------------------------------------------
# Plot setting
def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# Conjugate-Gradient relaxation with box fixed
def relax_fixbox():
    mdpp.cmd(' conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000 ')
    mdpp.cmd(' conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1 ')
    mdpp.cmd(' relax ')

#--------------------------------------------
# MD simulation setting
def setup_md():
    mdpp.cmd('''
    equilsteps = 0  timestep = 0.005 # (ps)
    DOUBLE_T = 1
    saveprop = 1 savepropfreq = 1 openpropfile #run
    savecn = 1 savecnfreq = 100 openintercnfile
    savecfg = 1 savecfgfreq = 100
    plotfreq = 100 printfreq = 10
    vt2 = 1e28  #1e28 2e28 5e28
    wallmass = 2e4     # atommass * NP 
    boxdamp = 1e-3     #
    saveH # Use current H as reference (H0), needed for specifying stress
    fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
    ''')

#*******************************************
# Main program starts here
#*******************************************
# status 0: Create structure, relax, and NVE simulations
#        1: NVT simulations with adjusting volume to zero stress
#        2: long NVT simulations to collect data
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

T = float(sys.argv[2]) if len(sys.argv) > 2 else 80
print('T = %f'%(T))

randseed = int(sys.argv[3]) if len(sys.argv) > 3 else 12345
print('randseed = %d'%(randseed))

p_obj = float(sys.argv[4]) if len(sys.argv) > 4 else 0
print('p_obj = %d'%(p_obj))

if status == 0:
    # Create structure, relax, and NVE MD simulations (run by ljbond)
    print("status == 0: NVE MD simulations")
    initmd()
    set_lj_param()

    Lx = Ly = Lz = 28
    NP = 500
    create_liquid_ljbond(Lx, Ly, Lz, NP)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    relax_fixbox()
    mdpp.cmd("finalcnfile = relaxed.cn writeall = 1 writecn")

    setup_md()

    mdpp.cmd('ensemble_type = "NVE" integrator_type = "VVerlet"')
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))
    mdpp.cmd("totalsteps = 10000 run")
    mdpp.cmd("finalcnfile = equil-nve.cn writeall = 1 writecn")

    sleep_seconds = 4

elif status == 1:
    # NVT MD simulations that adjusts volume to reach zero stress
    initmd()
    set_lj_param()

    mdpp.cmd('incnfile = equil-nve.cn readcn')
    Lx0 = mdpp.get("H_11")

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))

    maxiter  = 50
    mdsteps  = 2000
    propfreq = 10

    fid = open("P_L.dat","w")
    for iter in range(maxiter):
        mdpp.cmd("continue_curstep = 1")
        mdpp.cmd("totalsteps = %d savepropfreq = %d run"%(mdsteps,propfreq))
        property_data = np.loadtxt("prop.out")
        # average over second half of the simulation
        nstart = mdsteps//propfreq//2
        sig_xx = np.mean( property_data[-nstart:,6] )
        sig_yy = np.mean( property_data[-nstart:,7] )
        sig_zz = np.mean( property_data[-nstart:,8] )
        pressure = (sig_xx + sig_yy + sig_zz) / 3.0
        Lx = mdpp.get("H_11")
        strain = (Lx - Lx0)/Lx0
        print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
        print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
        fid.flush()

        # Adjust box size according to pressure
        if iter < maxiter-1:
            eps_T = (pressure - p_obj) * 2e-4
            mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
            mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

    mdpp.cmd("finalcnfile = equil-nvt.cn writeall = 1 writecn")
    fid.close()

elif status == 2:
    # long NVT MD simulations to collect data
    initmd()
    set_lj_param()

    mdpp.cmd('incnfile = equil-nvt.cn readcn')

    # Read P_L.dat saved from (status == 1)
    iter, P, L, thermal_strain = np.loadtxt("P_L.dat", unpack=True)
    # Compute average box size
    Pavg = np.mean(P[len(P)//2:])
    Lavg = np.mean(L[len(L)//2:])

    # Set box size of MD simulation cell to Lavg
    mdpp.cmd("H_11 = %20.14f"%(Lavg))
    mdpp.cmd("H_22 = %20.14f"%(Lavg))
    mdpp.cmd("H_33 = %20.14f"%(Lavg))

    Lx0 = mdpp.get("H_11")

    setup_md()

    # Edit the output format of property file to include velocity of atoms, we are no longer outputing box sizes since they are not changing
    mdpp.cmd('output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz VSR(0) VSR(1) VSR(2) VSR(3) VSR(4) VSR(5) VSR(6) VSR(7) VSR(8)" ')

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))

    mdpp.cmd("totalsteps = 100000 run")
    mdpp.cmd("finalcnfile = after-status-2.cn writeall = 1 writecn")

elif status == 3:
    # long NVT MD simulations to compute stress flucutations (run by lj2)
    initmd()
    set_lj_param()

    # This is the equilibrium box size at 80 K as determined by status == 1
    Lx = Ly = Lz = 29.88
    NP = 500
    mdpp.cmd("randseed = %d srand48"%randseed)
    create_liquid_lj2(Lx, Ly, Lz, NP, 0)

    # Make sure structure can be relaxed to a reasonable configuration
    for itrial in range(1000):
        mdpp.cmd("randomposition_rc")
        relax_fixbox()
        E0 = mdpp.get("EPOT")
        if E0 < 0: break
    else:
        print("status = %d unable to initialize positions!" % status)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    setup_md()

    # Edit the output format of property file to include shear stress
    mdpp.cmd('output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz TSTRESSinMPa_xy TSTRESSinMPa_yz TSTRESSinMPa_zx " ')

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 initvelocity"%T)

    mdpp.cmd("totalsteps = 200000 run")
    mdpp.cmd("finalcnfile = after-status-3.cn writeall = 1 writecn")

else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
