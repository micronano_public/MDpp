{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Tutorial-05:-Vacancy-Formation-Energy-by-Energy-Minimization\" data-toc-modified-id=\"Tutorial-05:-Vacancy-Formation-Energy-by-Energy-Minimization-5\"><span class=\"toc-item-num\">5&nbsp;&nbsp;</span>Tutorial 05: Vacancy Formation Energy by Energy Minimization</a></span><ul class=\"toc-item\"><li><span><a href=\"#Initialization\" data-toc-modified-id=\"Initialization-5.1\"><span class=\"toc-item-num\">5.1&nbsp;&nbsp;</span>Initialization</a></span></li><li><span><a href=\"#Creating-a-Vacancy-in-a-Perfect-Crystal\" data-toc-modified-id=\"Creating-a-Vacancy-in-a-Perfect-Crystal-5.2\"><span class=\"toc-item-num\">5.2&nbsp;&nbsp;</span>Creating a Vacancy in a Perfect Crystal</a></span></li><li><span><a href=\"#Relaxation\" data-toc-modified-id=\"Relaxation-5.3\"><span class=\"toc-item-num\">5.3&nbsp;&nbsp;</span>Relaxation</a></span></li><li><span><a href=\"#Vacancy-Formation-Energy\" data-toc-modified-id=\"Vacancy-Formation-Energy-5.4\"><span class=\"toc-item-num\">5.4&nbsp;&nbsp;</span>Vacancy Formation Energy</a></span></li><li><span><a href=\"#Equilibrium-Vacancy-Concentration\" data-toc-modified-id=\"Equilibrium-Vacancy-Concentration-5.5\"><span class=\"toc-item-num\">5.5&nbsp;&nbsp;</span>Equilibrium Vacancy Concentration</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 05: Vacancy Formation Energy by Energy Minimization\n",
    "Yifan Wang, Keonwook Kang and Wei Cai\n",
    "\n",
    "**2019-04-06**\n",
    "\n",
    "## Initialization\n",
    "\n",
    "\n",
    "**1. This notebook uses the following extensions, please set them up in nbextensions before using this notebook**\n",
    "* Table of Content (2)\n",
    "\n",
    "<sub>Instructions for nbextension installation is in [Tutorial 01 1.2.2.2](Tutorial%2001%20-%20Introduction%20to%20MD%2B%2B.ipynb)</sub>\n",
    "\n",
    "**2. If you have not, please add the following 3 lines into `~/.bashrc`, and reboot Ubuntu to setup the environment variables**\n",
    "\n",
    "These environmental variables specifies the MD++ root directory, the MD++ compiling system, and name of the MD++ executable, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "    export MDPLUS_DIR=$HOME/Codes/MD++.git\n",
    "    export MDPLUS_EXE=python3\n",
    "    export MDPLUS_SYS=gpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Check if environmental variables are set. Change current working directory into the MD++ root folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "envvar_test = True\n",
    "envvars = ['MDPLUS_DIR', 'MDPLUS_EXE', 'MDPLUS_SYS']\n",
    "for envvar in envvars:\n",
    "    if envvar not in os.environ.keys():\n",
    "        print('Environment variable \"'+envvar+'\" not set')\n",
    "        envvar_test = False\n",
    "    else:\n",
    "        print('Environment variable \"'+envvar+'\" set to '+os.environ[envvar])\n",
    "\n",
    "if not envvar_test:\n",
    "    raise OSError\n",
    "\n",
    "mdpp_dir = os.environ['MDPLUS_DIR']\n",
    "os.chdir(mdpp_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a Vacancy in a Perfect Crystal\n",
    "\n",
    "A vacancy is a common point defect in solids. It is an empty site (i.e. a missing atom) in an otherwise prefect crystal structure. Here we discuss how to introduce a vacancy in a perfect crystal using MD++ and how to compute the vacancy formation energy. For more information, see [supplementary document: Vacancy formation energy by MD++](http://micro.stanford.edu/mediawiki/images/2/29/VFE.pdf). Consider the following MD++ input file, [`mo_vac.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac.mdpp.py), that creates a vacancy in a perfect BCC Molybdenum crystal.  Note that the perfect crystal is created with the equilibrium lattice constant $a_0 = 3.1472 \\, \\mathring{\\rm A}$ that has been previously determined for this (FS) potential.\n",
    "\n",
    "**We can first compile the Finnis-Sinclair potential:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "make clean; make fs SYS=$MDPLUS_SYS build=R PY=yes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run the script [`mo_vac.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac.mdpp.py)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "$MDPLUS_EXE scripts/ME346B/mo_vac.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, MD++ creates a 5×5×5 perfect cubic crystal of BCC Mo with edges along <100> directions. Then it ﬁxes atom 0 by\n",
    "\n",
    "    input = [ 1      # number of atoms to be fixed\n",
    "              0]     # index of an atom to be fixed\n",
    "    fixatoms_by_ID   # fix a set of atoms by their indices\n",
    "\n",
    "and then removs this atom by the command `removefixedatoms`. The ﬁrst number in the input array speciﬁes the number of atoms to be removed. For example, if you would like to remove two atoms, say 3 and 8, you can do so by\n",
    "\n",
    "    input = [ 2            #  number of atoms to be fixed\n",
    "              3 8 ]        #  index of atoms to be fixed\n",
    "    fixatoms_by_ID         #  fix a set of atoms by their index\n",
    "    removefixedatoms       #  remove fixed atoms\n",
    "\n",
    "Sometimes, you may want to remove a specific atom you see in the graphic window. You can obtain the index of this atom by clicking the atom with your mouse. Depending on the setting of [`plot_atom_info`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=plot_atom_info), other information is also displayed when you click on the atom. If `plot_atom_info = 1`, the atom index number and its scaled coordinates are printed when the atom is clicked. If `plot_atom_info = 2`, the atom index and its real coordinate (in Å) will be printed. If `plot_atom_info = 3`, the atom index and its local energy (in eV) will be printed.\n",
    "\n",
    "The script file also sets up two color windows ([`plot_color_windows`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=plot_color_windows)) to display the atoms. Atoms with local energy between -10 eV and -6.8 eV are shown in gray and the atoms whose energies lie between -6.7 eV and -6.0 eV are shown in orange. This will highlight the atoms near the vacancy because they usually have a higher local energy. Click the atoms and you will see the actual local energy of these atoms. If you compare the potential energy of the perfect structure and the vacancy-formed structure, you will know which structure has higher energy. \n",
    "\n",
    "**Question**: If a single atom, whose index is other than 0, is removed from the same perfect structure, do we expect the system to have a different potential energy?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Figure 1. Atomic structure of a Mo crystal containing a single vacancy.\n",
    "\n",
    "<img src=\"http://micro.stanford.edu/mediawiki/images/d/d2/Movacancy.jpg\" style=\"height:250px\">\n",
    "\n",
    "Figure 1 shows a vacancy in a BCC molybdenum crystal.  $\\mathbf{c}_1 = 4[100]$, $\\mathbf{c}_2 = 4[010]$, and $\\mathbf{c}_3 = 4[001]$ are the periodicity vectors of the simulation cell. The red dotted circle designates the missing atom and the atoms around the vacant site are shown in different color due to their relatively high energy than the others."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Relaxation\n",
    "\n",
    "When an atom is removed from a crystal, we expect the neighboring atoms to adjust its positions, e.g. to move toward the vacant site, in order to lower the potential energy. The relaxed structure can be obtained by an energy minimization algorithm, such as [the conjugate gradient (CG) method](http://micro.stanford.edu/wiki/M04A_Conjugate_Gradient_Method_in_MD%2B%2B). This can be done by the following lines:\n",
    "\n",
    "    #---------------------------------------------\n",
    "    # Conjugate-Gradient relaxation\n",
    "    conj_ftol = 1e-7          # tolerance on the residual gradient\n",
    "    conj_fevalmax = 1000 # max. number of iterations\n",
    "    conj_fixbox = 1           # fix the simulation box\n",
    "    relax                     # CG relaxation command\n",
    "    finalcnfile = relaxed.cn writecn\n",
    "    eval                      # evaluate relaxed structure\n",
    "\n",
    "Let’s add the above script and paste it to the [`mo_vac.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac.mdpp.py) right before the last command, as [`mo_vac_relax.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac_relax.mdpp.py). You will see that the potential energy of the simulated system keeps decreasing while the relaxation goes on. The potential energy printed by the second eval should be lower than that printed by the first eval.\n",
    "\n",
    "The command for the CG relaxation is [`relax`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=relax). There are several variables controlling the CG relaxation. [`conj_ftol`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=conj_ftol) is the tolerance of the residual gradient and [`conj_fevalmax`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=conj_fevalmax) is the maximum number of calls to the potential function (effectively limiting the number of iterations). If [`conj_fixbox`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=conj_fixbox) = 1, the shape and volume of simulation cell box is ﬁxed and only the scaled coordinates of the atoms can change during the relaxation. If `conj_fixbox = 0`, then all 9 components of the $\\mathbf{H}$ are allowed to change. Sometimes, we want to ﬁx some components of the $\\mathbf{H}$ matrix while allowing other components to vary. This can be done by specifying the [`conj_fixboxvec`](http://micro.stanford.edu/MDpp/entries?utf8=%E2%9C%93&search=conj_fixboxvec) matrix. For example, if\n",
    "\n",
    "    conj_fixbox = 0\n",
    "    conj_fixboxvec = [ 0 1 1\n",
    "                       1 0 1\n",
    "                       1 1 0 ]\n",
    "\n",
    "then only the diagonal components of the box matrix $\\mathbf{H}$ are allowed to relax. (The corresponding entries in `conj_fixboxvec` are zero). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run the script [`mo_vac_relax.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac_relax.mdpp.py)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "$MDPLUS_EXE scripts/ME346B/mo_vac_relax.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the output cell above contains a print out of the key results: the number of atoms $N$ of the perfect crystal, the energy $E_1$ of the perfect crystal, the relaxed energy $E_2$ of the configuration with one atom removed, and the vacancy formation energy $E_{\\rm v}$ are printed out as follows. (The results are also saved into the <tt>mo_vac.dat</tt> file.)\n",
    "\n",
    "<pre>\n",
    "N =   250   E1 = -1705.0006   E2 = -1695.6305   Ev = 2.5501 eV\n",
    "</pre>\n",
    "\n",
    "How to compute $E_{\\rm v}$ from $E_1$ and $E_2$ is described in the section below.\n",
    "\n",
    "Also, note the print out (near the bottom of the output cell) of the conjugate gradient relaxation algorithm.\n",
    "\n",
    "<pre>\n",
    "[I] ############################################################\n",
    "[I] iteration     neval    energy (eV)       |gradient|^2 (eV^2)\n",
    "[I] ############################################################\n",
    "         0          1 -1.69560109329983e+03 5.36449345288577e-01\n",
    "         1          2 -1.69560208368184e+03 5.16045207483585e-01\n",
    "  ...         \n",
    "         8         13 -1.69563052776619e+03 3.39854213660012e-07\n",
    "         9         14 -1.69563052777871e+03 7.22613915885354e-08\n",
    "\tconj_f =     -1695.63052777871485\n",
    "</pre>\n",
    "\n",
    "It shows that the relaxation terminates successfully at the potential energy of $E_2 = -1695.6305 \\, {\\rm eV}$ with residual gradient of $7.2 \\times 10^{-8}$ (a very small residual indeed).  The entire relaxation is completed within 14 evaluations of the potential energy function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vacancy Formation Energy\n",
    "\n",
    "The vacancy formation energy $E_{\\rm v}$ is the energy increase that is needed to create a vacancy in a perfect crystal. It can be used to predict the vacancy concentration at thermal equilibrium.  Let $\\chi_{\\rm v}$ be the fraction of vacancies (i.e. the number of vacancies divided by total number of atomic sites).  Then at thermal equilibrium,\n",
    "\n",
    "$$\\chi_{\\rm v} \\approx \\exp \\left( − \\frac{E_{\\rm v} }{ k_{\\rm B}T } \\right)$$\n",
    "\n",
    "where $k_{\\rm B} = 8.617 \\times 10^{−5} {\\rm eV}/{\\rm K}$ is Boltzmann's constant.  This is an approximation because we have ignored the vibration entropy contribution Sv of the vacancy. The exact expression is \n",
    "\n",
    "$$\\chi_{\\rm v} = \\exp \\left( − \\frac{F_{\\rm v} }{ k_{\\rm B}T } \\right)$$\n",
    "\n",
    "where $F_{\\rm v} = E_{\\rm v} - TS_{\\rm v}$ is the vacancy formation free energy, $S_{\\rm v}$ is the vacancy formation entropy. These expressions are similar to Boltzmann’s distribution and will be derived in Section 5 below.\n",
    "\n",
    "Here we describe how to obtain the vacancy formation energy $E_{\\rm v}$ from atomistic calculations.  Let $E_1$ be the energy of the perfect crystal with $N$ atoms, and $E_2$ be the relaxed potential energy of the $(N − 1)$-atom system (containing the vacancy).\n",
    "\n",
    "The vacancy formation energy is not simply the difference between the two, i.e.\n",
    "\n",
    "$$ E_{\\rm v} \\neq E_2 - E_1 $$\n",
    "\n",
    "because the two systems does not have the same number of atoms.  When compare energies of two systems, we need to make sure they have the same number of atoms.\n",
    "\n",
    "In a real crystal, no atom is destroyed when a vacancy forms. Instead, when a vacancy forms in a real crystal, the missing atom moves to the external surface of the crystal (or to grain boundaries), as shown in Figure 2(a).  If, on the other hand, we remove the atom from the crystal, which is equivalent to moving the atom to infinity (so that it does not interact with any other atoms), as shown in Figure 2(b), then the change in energy would be much larger than that in Figure 2(a).  This is why simply taking $E_2 - E_1$ would give a much higher energy than $E_{\\rm v}$.\n",
    "\n",
    "Figure 2. Vacancy formation process by (a) moving atom to the surface and (b) removing the atom from the crystal (i.e. moving it to infinity).\n",
    "\n",
    "<img src=\"http://micro.stanford.edu/mediawiki/images/2/23/VacancyVSremoving.jpg\">\n",
    "\n",
    "Because our simulation cell contains a very small crystal that is subjected to periodic boundary conditions, there is no surface on which we can place the atom to be removed from the interior. Therefore, we need to account for this effect in a different way. In the perfect crystal, all atoms contributes equally to the total energy.\n",
    "Hence, we expect the energy of (hypothetical) perfect crystal with $(N − 1)$ atoms to be $(N-1)/N \\times E_1$, even though we (usually) cannot construct a perfect crystal with $(N − 1)$ atoms in a periodic simulation cell. The vacancy formation energy $E_{\\rm v}$ can then be computed from\n",
    "\n",
    "$$ E_{\\rm v} = E_2 - \\frac{N-1}{N} E_1 $$\n",
    "\n",
    "Given that the cohesive energy of the perfect crystal is defined as\n",
    "\n",
    "$$ E_{\\rm coh} = E_1 / N$$\n",
    "\n",
    "the vacancy formation energy can also be expressed as,\n",
    "\n",
    "$$ E_{\\rm v} = E_2 - (N-1) E_{\\rm coh}$$\n",
    "\n",
    "or, equivalently,\n",
    "\n",
    "$$ E_{\\rm v} = (E_2 + E_{\\rm coh}) - E_{1}$$\n",
    "\n",
    "The last equation can be interpreted as follows.  By adding $E_{\\rm coh}$ to $E_2$, the result corresponds to the energy of an $N$-atom system, so that it can be compared with $E_1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result for BCC Mo using the FS potential is $E_{\\rm v} = 2.550$ eV.  This is consistent with the more accurate tight-binding (TB) model which predicts $E_{\\rm v} = 2.46$ eV.<sup>[1]</sup>  The corresponding equilibrium vacancy fraction at $T = 1000 \\,{\\rm K}$ is about\n",
    "\n",
    "$$ \\chi_{\\rm v} ≈ 1.43 \\times 10^{-13}$$ \n",
    "\n",
    "\n",
    "\n",
    "<sup>[1]</sup> M. J. Mehl and D. A. Papaconstantopoulos, “Applications of a tight-binding total-energy method for transition and noble metals: Elastic constants, vacancies, and surfaces of monatomic metals”, Physical Review B [54, 4519-4530](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.54.4519) (1996)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font size=+1>Exercise 1</font>**  \n",
    "\n",
    "Recompute $E_{\\rm v}$ with larger and smaller simulation boxes. Examine the dependence of $E_{\\rm v}$ on the number of atoms in the simulation cell, and obtain the converged value for $E_{\\rm v}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Create script [`mo_vac_conv.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac_conv.mdpp.py)**\n",
    "\n",
    "In this script, repeat the calculations in [`mo_vac_relax.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac_relax.mdpp.py) for simulation cell sizes of 4x4x4, 5x5x5, 6x6x6, 8x8x8.  Save the results (N, E1, E2, Ev) in a text file name <tt>mo_vac_conv.dat</tt>.  Each line of the data file contains the results of one simulation cell size.\n",
    "\n",
    "**Run script [`mo_vac_conv.mdpp.py`](http://localhost:8888/edit/scripts/ME346B/mo_vac_conv.mdpp.py)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "$MDPLUS_EXE scripts/ME346B/mo_vac_conv.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Plot results from data file**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "'''\n",
    "Python script to plot the vacancy formation energy in BCC Mo\n",
    "computed from different simulation cell sizes\n",
    "'''\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "runsdir = os.path.join(mdpp_dir, 'runs/mo-vac')\n",
    "font = {'size' : 12}\n",
    "\n",
    "# plot the lattice energy vs number density with inset figure\n",
    "fig0, ax0 = plt.subplots(1, 1)\n",
    "\n",
    "filename = os.path.join(runsdir, 'mo_vac_conv.dat')\n",
    "N, E1, E2, Ev = np.loadtxt(filename, unpack=True)\n",
    "ax0.plot(N, Ev, '.-')\n",
    "\n",
    "ax0.set_xlabel(r'Number of atoms, $N$', **font)\n",
    "ax0.set_ylabel(r'$E_v$ (eV)', **font)\n",
    "ax0.set_title(r'Figure 3 Predictions of vacancy formation energy')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:** \n",
    "\n",
    "Based on the above results, what is your prediction of vacancy formation energy and error bar?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Answer: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Equilibrium Vacancy Concentration\n",
    "Here we derive the equilibrium vacancy fraction $\\chi_{\\rm v}$ given the formation energy $E_{\\rm v}$ of a single vacancy. Consider a crystal with $N$ atomic sites, $n$ of which are vacant.  We can write the Gibbs free energy of the solid as follows (ignoring vibrational entropy),\n",
    "\n",
    "$$ G = G_0 + n E_{\\rm v} - T S_{\\rm c} $$\n",
    "\n",
    "where $G_0$ is the free energy of the perfect crystal (i.e. when $n = 0$).  Here we have ignored any interactions between the vacancies. This approximation is valid if $n \\ll N$.  $S_{\\rm c}$ is the conﬁgurational entropy due to the fact that there are $\\Omega$ different ways to arrange the $n$ vacancies on $N$ atomic sites.\n",
    "\n",
    "\n",
    "$$ \\begin{align} S_c &= k_B \\ln \\Omega = k_B \\ln \\frac{N!}{(N-n)!n!} \\\\\n",
    "                     &\\approx k_B [N\\ln N - (N-n)\\ln (N-n) - n\\ln n ] \\end{align} $$\n",
    "\n",
    "In the last step we have used Stirling’s approximation, $\\ln N! \\approx N\\ln N - N$. At equilibrium, the\n",
    "Gibbs free energy $G$ reaches minimum, i.e. $\\partial G / \\partial n = 0$.\n",
    "        \n",
    "$$ \\begin{align} \\frac{\\partial G}{\\partial n} &= E_{\\rm v} - T\\frac{\\partial S_{\\rm c}}{\\partial n} \\\\\n",
    "                          &= E_{\\rm v} - T k_B \\ln \\frac{N-n}{n} \\\\\n",
    "                          &\\approx E_{\\rm v} - T k_B \\ln \\frac{N}{n} = 0 \n",
    "   \\end{align}$$\n",
    "   \n",
    "Therefore,\n",
    "\n",
    "$$ \\chi_{\\rm v} \\equiv \\frac{n}{N} \\approx \\exp \\left(-\\frac{E_{\\rm v}}{k_B T}\\right)$$\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  },
  "toc": {
   "base_numbering": "5",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "288px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
