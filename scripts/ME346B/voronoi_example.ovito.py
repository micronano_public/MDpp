# Import OVITO modules
from ovito import *
from ovito.io import *
from ovito.modifiers import *
from ovito.data import *

# Import NumPy module.
import numpy as np

# Set up MD++ directory
import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
runs_dir = os.path.join(mdpp_dir, 'runs/cu_supercell')

# Load a simulation snapshot of fcc Cu
node = import_file(os.path.join(runs_dir, 'cu_supercell.lammps.gz'))

# Set up the Voronoi analysis modifier, same as you see in GUI
voro = VoronoiAnalysisModifier(
    compute_indices = False,
    use_radii = False
)
node.modifiers.append(voro)
                      
# Let OVITO compute the results.
node.compute()

# Export data
export_file(node, os.path.join(runs_dir, 'voronoi_example.dump'), # file name
    format = 'lammps_dump',                                       # file type
    columns = ['Particle Identifier',                             # selection of output columns
               'Particle Type',
               'Position.X',
               'Position.Y',
               'Position.Z',
               'Coordination',
               'Atomic Volume']
    )

################ More than what GUI can do ########################
# Access computed Voronoi volume as NumPy array.
# This is an (N, ) array.
voro_volume = node.output.particle_properties['Atomic Volume'].array
np.savetxt(os.path.join(runs_dir, 'voro_volume.txt'), voro_volume)
