import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'scripts', 'python'))
from mdutil import loadcn

import numpy as np

import ovito
from ovito.data import *
from ovito.io import export_file

# Obtain the file names of the chain, state A and state B
nargs = 4
if len(sys.argv) == nargs:
    scriptfile = sys.argv[0]
    chainsfile = sys.argv[1]
    stateAfile = sys.argv[2]
    stateBfile = sys.argv[3]
    chains_dir = os.path.dirname(chainsfile)
    for i in range(nargs):
        if not os.path.exists(sys.argv[i]):
            raise NameError(sys.argv[i] + ' not found!')
else:
    raise ValueError('Must have 3 arguments: chain, state A and B!')

# Obtain the chain info from the chain file
with open(chainsfile, 'r') as f:
    nchain = int(f.readline())+1
    nfixed = int(f.readline())
constrain_id = np.genfromtxt(chainsfile, skip_header=2, max_rows=nfixed, dtype=int)
constrain_rs = np.genfromtxt(chainsfile, skip_header=2+nfixed).reshape(nchain, nfixed, -1)
print('chain length: %d'%nchain)
print('# of constrain atom: %d'%nfixed)

# Read state A and state B
npA, rawA, hA = loadcn(stateAfile, structured_array=True)
npB, rawB, hB = loadcn(stateBfile, structured_array=True)
if not npA == npB:
    raise ValueError('Atom lost between state A and B')

# Initialize the chain with linear interpolation, and construct the chain
chain_x = np.arange(nchain)/(nchain-1)
h_chain = chain_x[:, None, None] * (hB - hA) + hA
s_chain = chain_x[:, None, None] * (rawB['s'] - rawA['s']) + rawA['s']
r_chain = np.empty_like(s_chain)
for i in range(nchain):
    r_chain[i, ...] = np.dot(h_chain[i, ...], s_chain[i, ...].T).T
    r_chain[i, constrain_id, :] = constrain_rs[i, ...]

# Create the chain configurations in OVITO
for i in range(nchain):
    # The number of particles we are going to create.
    num_particles = npA

    # Create the particle position property.
    pos_prop = ParticleProperty.create(ParticleProperty.Type.Position, num_particles)
    pos_prop.marray[:] = r_chain[i, ...].copy()

    # Create the particle type property and insert two atom types.
    type_prop = ParticleProperty.create(ParticleProperty.Type.ParticleType, num_particles)
    type_prop.type_list.append(ParticleType(id = 1, name = 'Ta', color = (0.0, 1.0, 0.0)))
    type_prop.marray[:] = 1

    # Create the simulation box.
    cell = SimulationCell()
    cell.matrix = np.hstack([h_chain[i, ...], -np.diag(h_chain[i, ...]).reshape(3, 1)/2])
    #print(i)
    #print(cell.matrix)
    cell.pbc = (True, True, True)
    cell.display.line_width = 0.1

    # Create a data collection to hold the particle properties, bonds, and simulation cell.
    data = DataCollection()
    data.add(pos_prop)
    data.add(type_prop)
    data.add(cell)

    # Create a node and insert it into the scene.
    node = ovito.ObjectNode()
    node.source = data
    export_file(node, os.path.join(chains_dir, 'chain.%02d.lammps')%i, 'lammps_data')