# Import OVITO modules
#from ovito import *
from ovito.io import *
#from ovito.modifiers import *
#from ovito.data import *

# Set up MD++ directory
import sys
#mdpp_dir = os.environ['MDPLUS_DIR']
#print('MD++ root directory:', mdpp_dir)
#runs_dir = os.path.join(mdpp_dir, 'runs/cu_supercell')
#input_dir = os.path.join(mdpp_dir, 'scripts/ME346B')

if len(sys.argv) < 2:
    print("Usage: ovitos lammps_data_to_dump.py <datafile> [dumpfile]")
    quit()
elif len(sys.argv) == 2:
    datafile = sys.argv[1]
    dumpfile = datafile + '.dump'
else:
    datafile = sys.argv[1]
    dumpfile = sys.argv[2]

# Load lammps data file
node = import_file(datafile)

# Export lammps dump file
export_file(node, dumpfile, format = 'lammps_dump',
    columns = ['Particle Identifier', 'Particle Type', 'Position.X', 'Position.Y', 'Position.Z'])

