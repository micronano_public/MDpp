'''
Utility for computing melting points of pure elements in MD++

Translated from melting_cubic.tcl in http://micro.stanford.edu/wiki/Computing_Melting_Point_by_Free_Energy_Method

 also see  scripts/ME346B/melting_cubic.mdpp.py

'''
import os, time, glob
import numpy as np

from mdpp_util import mdpp_util, datastore

class free_eng_util(mdpp_util):
    def __init__(self, runs_dir, folder_prefix=None, mdpp=None, setnolog=True, setoverwrite=True, zipfiles=True, 
                 supercell=None, NxNyNz=None, crystalstructure=None, lattice_constant=None, poisson_ratio=0.3,
                 cn_output_format=['cn', 'LAMMPS'], make_output_dir=True, material=None):

        super(free_eng_util, self).__init__(runs_dir, folder_prefix=folder_prefix, mdpp=mdpp, 
              setnolog=setnolog, setoverwrite=setoverwrite, zipfiles=zipfiles, supercell=supercell, NxNyNz=NxNyNz,
              crystalstructure=crystalstructure, lattice_constant=lattice_constant, poisson_ratio=poisson_ratio, 
              cn_output_format=cn_output_format, make_output_dir=make_output_dir)

        self.numsteps_equil_vol     = 1000000             # number of MD steps needed to equilibrate volume
        self.numsteps_md_switch     = 1000000             # number of MD steps for adiabatic switching
        self.numsteps_equil_atoms   =  100000             # number of MD steps for equilibration before switching
        self.numsteps_equil_liquid  =  200000             # number of MD steps for equilibration before switching
        self.numsteps_before_switch =   50000             # number of MD steps for equilibration before switching
        self.numsteps_before_switch_ig = 100000           # number of MD steps for equilibration before switching

        # for given job index x, we will have 1+iter1 iteration for each step
        self.iter_switch_ideal_gas = 3 # iteration number for ideal gas - Gaussian fluid step.
        self.iter_switch_other     = 1 # iteration number for other switching steps.

        self.material = material

        self.dispatch = { 1:self.step_1,   2:self.step_2,   3:self.step_3,   4:self.step_4,   5:self.step_5,   6:self.step_6,
                          7:self.step_7,   8:self.step_8,   9:self.step_9,  10:self.step_10, 11:self.step_11, 12:self.step_12,
                         13:self.step_13, 14:self.step_14, 15:self.step_15 }

    def set_flag(self, n):
        with open('flag', 'a+') as f:
            f.write("%d\n"%n)

    def wait_for_flag(self, n):
        success = ( n==1 ) # step 1 does not need to wait
        with open('info.dat', 'w') as f:
            f.write("a0 L eT Ecoh V T NP\n")

        while not success:
            if os.path.exists('flag'):
                success = n in np.loadtxt('flag')
            if not success:
                time.sleep(60)

    def already_run(self, n):
        success = False
        if os.path.exists('flag'):
            success = n in np.loadtxt('flag')
        self.set_flag(n)
        return success

    def get_data_from_info_file(self, filename):
        info_data = np.loadtxt(filename)
        a0   = info_data[0]
        L    = int(info_data[1])
        eT   = info_data[2]
        Ecoh = info_data[3]
        V    = info_data[4]
        T    = info_data[5]
        NP   = int(info_data[6])
        return a0, L, eT, Ecoh, V, T, NP

    def equilibrate_crystal_with_fixed_box_at_temperature(self, T, nsteps, n, jobid, simid=None):
        # run MD simulation of solid at T
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('''
        srand48bytime saveprop = 0
        NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NVTC" integrator_type = "Gear6"
        ''')
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d run '%(T,nsteps))
        #self.mdpp.cmd(' closepropfile ')
        self.mdpp.cmd('finalcnfile = "%gK_equil_crystal.cn" writeall = 1 writecn '%(T))

    def equilibrate_harmonic_crystal_at_temperature(self, T, nsteps, n, jobid, simid=None):
        # run MD simulation of reference potential at T
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('''
        srand48bytime saveprop = 0
        NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NVTC" integrator_type = "Gear6"
        ''')
        self.mdpp.cmd('Ecoh = 0 switchfreq = 10 ecspring = 0  lambda0 = 1 lambda1 = 1 refpotential = 1 ')
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d runMDSWITCH '%(T,nsteps))
        #self.mdpp.cmd(' closepropfile ')
        self.mdpp.cmd('finalcnfile = "%gK_equil_ha_crystal.cn" writeall = 1 writecn '%(T))

    def equilibrate_liquid_with_fixed_box_at_temperature(self, T, nsteps, n, jobid, simid=None):
        # run MD simulation of solid at T
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('srand48bytime saveprop = 0 ensemble_type = "NVT" integrator_type = "VVerlet" ')
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d run '%(T,nsteps))
        #self.mdpp.cmd(' closepropfile ')
        self.mdpp.cmd('finalcnfile = "%gK_equil_liquid.cn" writeall = 1 writecn '%(T))

    def equilibrate_gaussian_fluid_at_temperature(self, T, nsteps, n, jobid, simid=None):
        # run MD simulation of reference potential at T
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('srand48bytime saveprop = 0 ensemble_type = "NVT" integrator_type = "VVerlet" ')
        self.mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
        self.mdpp.cmd('switchfreq = 10  lambda0 = 1 lambda1 = 1 refpotential = 5 ')
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d runMDSWITCH '%(T,nsteps))
        #self.mdpp.cmd(' closepropfile ')
        self.mdpp.cmd('finalcnfile = "%gK_equil_ga_liquid.cn" writeall = 1 writecn '%(T))

    def equilibrate_gaussian_fluid_at_scaled_temperature(self, T, factor, nsteps, n, jobid, simid=None):
        # run MD simulation of reference potential at T
        T_scaled = T / factor
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('printfreq = 5000 ')
        self.mdpp.cmd('srand48bytime saveprop = 0 ensemble_type = "NVT" integrator_type = "VVerlet" ')
        self.mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
        self.mdpp.cmd('switchfreq = 10  lambda0 = %g lambda1 = %g refpotential = 6 switchfunc = 1 '%(factor,factor))
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d runMDSWITCH '%(T,nsteps))
        #self.mdpp.cmd(' closepropfile ')
        self.mdpp.cmd('finalcnfile = "%gK_equil_ga_liquid.cn" writeall = 1 writecn '%(T_scaled))

    def extract_solid_free_energy(self, initcn='perf'):
        if not os.path.exists('info2.dat'):
            return False
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if self.T_solid_0 != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(self.T_solid_0, temperature));
            return False

        kB   = datastore["constants"]["kB"]
        hbar = datastore["constants"]["hbar"]
        Na   = datastore["constants"]["Na"]

        print('read Fha from file %s_%s_%gK_L%d_Fha'%(self.material,initcn,self.T_solid_0,L))
        with open('%s_%s_%gK_L%d_Fha'%(self.material,initcn,self.T_solid_0,L), 'r') as f:
            Fha = float(f.readline())

        # load the work done during switching simulations
        if os.path.exists('work_F3.dat') and os.path.exists('work_F4.dat') and len(glob.glob('prop_5_*.out')) and len(glob.glob('prop_6_*.out')):
            data3  = np.loadtxt('work_F3.dat')
            data4  = np.loadtxt('work_F4.dat')
            for iter, propfile in enumerate(glob.glob('prop_5_*.out')):
                if iter == 0:
                    data5  = np.loadtxt(propfile)
                else:
                    data5  = np.append(data5, np.loadtxt(propfile), axis=0)
            for iter, propfile in enumerate(glob.glob('prop_6_*.out')):
                if iter == 0:
                    data6  = np.loadtxt(propfile)
                else:
                    data6  = np.append(data6, np.loadtxt(propfile), axis=0)
        else:
            print('extract_liquid_free_energy(): data files from MD switching runs not found')
            self.T_solid = None
            self.Fs = None
            return False

        free3       = np.mean(data3[:,1])
        free3_e     = np.std (data3[:,1])
        free4       = np.mean(data4[:,1])
        free4_e     = np.std (data4[:,1])
        dFre_Fha    = (free4-free3)/2.0
        dFre_Fha_e  = (free3_e+free4_e)/2.0
        Fre_T1      = Fha + dFre_Fha

        print('Free energy of real solid at T_solid_0:')
        print('Fre_T1 = %20.12e'%(Fre_T1))

        # solid free energy
        d = self.factor_solid_RS - 1.0
        lam = np.arange(1,self.factor_solid_RS+d/100000,d/10000)
        iter = np.floor(data5.shape[0]/10001).astype(int)
        temp1 = data5[:10001*iter,7].reshape((10001,iter), order='F')
        iter = np.floor(data6.shape[0]/10001).astype(int)
        temp2 = data6[:10001*iter,7].reshape((10001,iter), order='F')
        w1 = np.mean(temp1, axis=1)
        w2 = np.mean(temp2, axis=1)

        w1_inv = w2[-1] - np.flip(w2)
        w = (w1 - w1_inv) / 2.0
        self.T_solid = self.T_solid_0 / lam
        self.Fs = np.multiply((Fre_T1 + w)/self.T_solid_0 - 3.0/2*(NP-1)*kB*np.log(self.T_solid/self.T_solid_0), self.T_solid)
        print('Free energy of solid as a function of temperature:')
        print('Fs = [%20.12e, %20.12e, ..., %20.12e, %20.12e]'%(self.Fs[0],self.Fs[1],self.Fs[-2],self.Fs[-1]))
        return True

    def extract_liquid_free_energy(self):
        if not os.path.exists('info7.dat'):
            return False
        a0, L, eT, Ecoh, Vliq, Tliq, NP = self.get_data_from_info_file('info7.dat')
        if self.T_liquid_0 != Tliq:
            print("Error: T (%g K) does not match that in info file (%g K)"%(self.T_liquid_0, Tliq));
            return False

        kB   = datastore["constants"]["kB"]
        hbar = datastore["constants"]["hbar"]
        Na   = datastore["constants"]["Na"]

        # load the work done during switching simulations
        if os.path.exists('work_F8.dat') and os.path.exists('work_F9.dat') and os.path.exists('work_F10.dat') and os.path.exists('work_F11.dat') and len(glob.glob('prop_12_*.out')) and len(glob.glob('prop_13_*.out')):
            data8  = np.loadtxt('work_F8.dat')
            data9  = np.loadtxt('work_F9.dat')
            data10 = np.loadtxt('work_F10.dat')
            data11 = np.loadtxt('work_F11.dat')
            data12=np.zeros([0,3])
            for iter, propfile in enumerate(glob.glob('prop_12_*.out')):
                if iter == 0:
                    data12  = np.loadtxt(propfile)
                else:
                    data12  = np.append(data12, np.loadtxt(propfile), axis=0)
            for iter, propfile in enumerate(glob.glob('prop_13_*.out')):
                if iter == 0:
                    data13  = np.loadtxt(propfile)
                else:
                    data13  = np.append(data13, np.loadtxt(propfile), axis=0)
        else:
            print('extract_liquid_free_energy(): data files from MD switching runs not found')
            self.T_liquid = None
            self.Fl = None
            return False

        # liquid free energy
        logfactorial = lambda x: (x+0.5)*np.log(x)-x+0.5*np.log(2*np.pi)
        logfac = logfactorial(NP-1)
        gam = np.sqrt(6.626068**2 * 6.022 / (1.3806503*self.atom_mass*0.001*Tliq*2*np.pi)) * 0.1
        Fid = -kB*Tliq*((NP-1)*np.log(Vliq/gam**3)-logfac)

        free8       = np.mean(data8[:,1])
        free8_e     = np.std (data8[:,1])
        free9       = np.mean(data9[:,1])
        free9_e     = np.std (data9[:,1])
        dFsw_Fga    = (free9-free8)/2.0
        dFsw_Fga_e  = (free9_e+free8_e)/2.0
        free10      = np.mean(np.sum(data10[:,1:7],axis=1))
        free11      = np.mean(np.sum(data11[:,1:7],axis=1))
        dFga_Fid    = (free11-free10)/2.0
        Fsw_T2      = Fid+dFga_Fid+dFsw_Fga

        print('Free energy of real liquid at T_liquid_0:')
        print('Fsw_T2 = %20.12e'%(Fsw_T2))

        d = self.factor_liquid_RS - 1.0
        lam2 = np.arange(1,self.factor_liquid_RS+d/100000,d/10000)
        iter = np.floor(data12.shape[0]/10001).astype(int)
        temp3 = data12[:10001*iter,7].reshape((10001,iter), order='F')
        iter = np.floor(data13.shape[0]/10001).astype(int)
        temp4 = data13[:10001*iter,7].reshape((10001,iter), order='F')
        w3 = np.mean(temp3, axis=1)
        w4 = np.mean(temp4, axis=1)

        w3_inv = w4[-1] - np.flip(w4)
        ww = (w3 - w3_inv) / 2.0
        self.T_liquid = self.T_liquid_0 / lam2
        self.Fl = np.multiply((Fsw_T2 + ww)/self.T_liquid_0 - 3.0/2*(NP-1)*kB*np.log(self.T_liquid/self.T_liquid_0), self.T_liquid)
        print('Free energy of liquid as a function of temperature:')
        print('Fl = [%20.12e, %20.12e, ..., %20.12e, %20.12e]'%(self.Fl[0],self.Fl[1],self.Fl[-2],self.Fl[-1]))
        return True

    def free_energy_calculation(self, workstep, jobid, T, mult, initcn='perf'):
        if workstep in self.dispatch.keys():
            self.dispatch[workstep](jobid, T, mult, initcn)
        else:
            print("Error: unkown workstep " + str(workstep))

    def step_1(self, jobid, T, mult, initcn='perf'):
        workstep = 1
        print('working on step %d'%workstep)
        # get 0K properties for solid and save into info1.dat #
        if self.already_run(workstep):
            print("this step has already been run. Remove the flag file if you want to re-run it")
        else:
            self.cmd("incnfile = %s.cn  readcn "%(initcn))
            self.cmd('saveH eval ')
            self.relax_freebox(filename='relaxed')

            NP, V0, EPOT0, a0, Ecoh0 = self.get_nve()
            with open('info1.dat', 'w') as f:
                f.write("%.18g %d %.18g %.18g %.18g %.18g %d\n"%(a0,mult,0,Ecoh0,V0,0,NP))

            # allow workstep 7 to be run (step_7 requires info1.dat)
            self.set_flag(7)

            for iter, temperature in enumerate([T, T/self.factor_solid_RS]):
                V = self.equilibrate_volume_at_temperature(temperature, self.numsteps_equil_vol, workstep, jobid, iter+1)
                a = np.cbrt((V/NP)*self.natom_per_unitcell)
                eT = a/a0 - 1
                self.cmd('restoreH ')
                self.scale_H(eT)
                self.write_file('%s_T%g'%(initcn,temperature))

                self.relax_fixbox(filename='relaxed_T%g'%temperature)
                EPOT = self.get_epot()
                Ecoh = EPOT / NP
                with open('info%d.dat'%(iter+2), 'w') as f:
                    f.write("%.18g %d %.18g %.18g %.18g %.18g %d\n"%(a0,mult,eT,Ecoh,V,temperature,NP))

            # allow workstep 2 to be run
            self.set_flag(2)

    def step_2(self, jobid, T, mult, initcn='perf'):
        workstep = 2
        print('working on step %d'%workstep)
        # computing hessian matrix for quasi-harmonic crystal
        #self.make_perfect_crystal(savefile='perf')
        self.cmd("incnfile = %s.cn  readcn "%(initcn))
        self.cmd('saveH eval ')

        for filename in ['info2.dat', 'info3.dat']:
            a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file(filename)

            # apply thermal strain
            self.scale_H(eT)
            self.relax_fixbox(filename='relaxed_T%g'%temperature)

            # compute hessian matrix to get quasi-harmonic crystal(QHC)
            self.cmd('input = 0  calHessian ')
            os.rename('hessian.out', 'hessian%g.out'%temperature)

        # allow workstep 3, 4, 14 to be run
        self.set_flag(3)
        self.set_flag(4)
        self.set_flag(14)

    def step_3(self, jobid, T, mult, initcn='perf'):
        workstep = 3
        print('working on step %d'%workstep)
        # switching from real solid potential to QHC
        #self.make_perfect_crystal(savefile='perf')
        self.cmd("incnfile = %s.cn  readcn "%(initcn))
        self.cmd('saveH eval ')

        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        # apply thermal strain
        self.scale_H(eT)
        self.cmd('finalcnfile = "%gK_crystal.cn" writecn setconfig1 '%(T))
        self.cmd('incnfile = "hessian%g.out" readHessian '%(T))

        # equilibrate solid before switching run
        self.equilibrate_crystal_with_fixed_box_at_temperature(T, self.numsteps_equil_atoms, workstep, jobid)
        self.cmd('incnfile = "%gK_equil_crystal.cn" '%(T))

        # allow workstep 5 and 6 to be run
        self.set_flag(5)
        self.set_flag(6)

        # switching setup
        self.cmd('Ecoh = %.18g '%Ecoh)
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33" ')
        for x in range(self.iter_switch_other+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('initvelocity run ')
            # switching run
            self.cmd('lambda0 = 0 lambda1 = 1 refpotential = 1 ')
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

    def step_4(self, jobid, T, mult, initcn='perf'):
        workstep = 4
        print('working on step %d'%workstep)
        # switching from QHC to real solid potential
        #self.make_perfect_crystal(savefile='perf')
        self.cmd("incnfile = %s.cn  readcn "%(initcn))
        self.cmd('saveH eval ')

        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        # apply thermal strain
        self.scale_H(eT)
        self.cmd('finalcnfile = "%gK_crystal.cn" writecn setconfig1 '%(T))
        self.cmd('incnfile = "hessian%g.out" readHessian '%(T))

        # equilibrate QHC at T1 before switching run
        self.equilibrate_harmonic_crystal_at_temperature(T, self.numsteps_equil_atoms, workstep, jobid)
        self.cmd('incnfile = "%gK_equil_ha_crystal.cn" '%(T))

        # switching setup
        self.cmd('Ecoh = %.18g '%Ecoh)
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33" ')
        for x in range(self.iter_switch_other+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('lambda0 = 1 lambda1 = 1 refpotential = 1 ')
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('lambda0 = 1 lambda1 = 0 refpotential = 1 ')
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

    def step_5(self, jobid, T, mult, initcn='perf'):
        workstep = 5
        print('working on step %d'%workstep)
        # reversible scaling from T to T/solid_factor_RS
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        self.cmd('incnfile = "%gK_equil_crystal.cn" readcn setconfig1 '%(T))

        self.run_setup(self.atom_mass, workstep, jobid)
        self.mdpp.cmd('srand48bytime NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NPTC" integrator_type = "Gear6" ')
        self.mdpp.cmd('T_OBJ = %g initvelocity '%(T))

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33" ')
        for x in range(self.iter_switch_other+1):
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
                self.cmd('initvelocity run ')
            # switching run
            self.cmd('lambda0 = 1 lambda1 = %g switchfunc = 1 refpotential = 4 '%(self.factor_solid_RS))
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

        # moved to workstep 15
        #os.system('cat prop_%d_%d.out >> prop_%d.out '%(workstep,jobid,workstep))

    def step_6(self, jobid, T, mult, initcn='perf'):
        workstep = 6
        print('working on step %d'%workstep)
        # reversible scaling from T/solid_factor_RS to T
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        T1 = T/self.factor_solid_RS
        self.cmd('incnfile = "%gK_equil_crystal.cn" readcn setconfig1 '%(T))

        #self.equilibrate_volume_at_temperature(T1, self.numsteps_equil_atoms, workstep, jobid)
        # to make it consistent with melting_cubic.tcl
        self.run_setup(self.atom_mass, workstep, jobid)
        self.mdpp.cmd('''
        saveprop = 0
        fixboxvec=[ 0 1 1   1 0 1   1 1 0 ]   stress= [ 0 0 0 0 0 0 0 0 0 ]
        NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NPTC" integrator_type = "Gear6"
        ''')
        self.cmd('lambda0 = %g lambda1 = %g refpotential = 4 switchfunc = 1 '%(self.factor_solid_RS,self.factor_solid_RS))
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d runMDSWITCH '%(T,self.numsteps_equil_atoms))
        self.mdpp.cmd('finalcnfile = "%gK_equil_crystal.cn" writeall = 1 writecn '%(T1))

        self.cmd('incnfile = "%gK_equil_crystal.cn" '%(T1))

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33" ')
        for x in range(self.iter_switch_other+1):
            # equilibration run
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            self.cmd('lambda0 = %g lambda1 = %g refpotential = 4 switchfunc = 1 '%(self.factor_solid_RS,self.factor_solid_RS))
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('lambda0 = %g lambda1 = 1 switchfunc = 1 refpotential = 4 '%(self.factor_solid_RS))
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

        # moved to workstep 15
        #os.system('cat prop_%d_%d.out >> prop_%d.out '%(workstep,jobid,workstep))

    def step_7(self, jobid, T, mult, initcn=None):
        workstep = 7
        print('working on step %d'%workstep)
        # equilibrate liquid at T2
        time.sleep(1) # delay liquid calculations slightly after solid
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info1.dat')

        self.make_liquid(savefile='random_pos', a=a0) # using supercell size from info1.dat
        self.cmd('saveH eval ')
        self.relax_fixbox(filename='0K_liquid')

        # note: melting_cubic.tcl uses NPT ensemble, here it uses NPTC
        V = self.equilibrate_volume_at_temperature(T, self.numsteps_equil_vol, workstep, jobid, 1)
        a = np.cbrt((V/NP)*self.natom_per_unitcell)
        eT = a/a0 - 1
        Ecoh = self.mdpp.get("Ecoh") # note: this seems to be zero (should we use Ecoh0 instead?)
        with open('info7.dat', 'w') as f:  # note: filename is info2.dat in melting_cubic.tcl
            f.write("%.18g %d %.18g %.18g %.18g %.18g %d\n"%(a0,L,eT,Ecoh,V,T,NP))

        self.cmd('restoreH ')
        self.scale_H(eT)
        self.mdpp.cmd('finalcnfile = "%gK_equil_liquid_eT.cn" writeall = 1 writecn '%(T))

        # allow workstep 8, 9 to be run
        self.set_flag(8)
        self.set_flag(9)

    def step_8(self, jobid, T, mult, initcn=None):
        workstep = 8
        print('working on step %d'%workstep)
        # switching from real liquid to gaussian fluid
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info7.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        self.make_liquid(savefile='random_pos', a=a0) # using supercell size from info1.dat
        self.cmd('saveH eval ')
        self.relax_fixbox()

        self.scale_H(eT)
        self.equilibrate_liquid_with_fixed_box_at_temperature(T, self.numsteps_equil_liquid, workstep, jobid)
        self.cmd('setconfig1 ')
        self.cmd('incnfile = "%gK_equil_liquid.cn" '%(T))

        # allow workstep 12 and 13 to be run
        self.set_flag(12)
        self.set_flag(13)

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')
        self.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
        for x in range(self.iter_switch_other+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('initvelocity run ')
            # switching run
            self.cmd('lambda0 = 0 lambda1 = 1 refpotential = 5 ')
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

    def step_9(self, jobid, T, mult, initcn=None):
        workstep = 9
        print('working on step %d'%workstep)
        # switching from gaussian fluid to real liquid
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info7.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        self.make_liquid(savefile='random_pos', a=a0) # using supercell size from info1.dat
        self.cmd('saveH eval ')
        self.relax_fixbox()
        self.cmd('setconfig1 ')

        self.scale_H(eT)
        # note: melting_cubic.tcl uses NVT ensemble, here it uses NVTC
        self.equilibrate_gaussian_fluid_at_temperature(T, self.numsteps_equil_liquid, workstep, jobid)
        self.mdpp.cmd('incnfile = "%gK_equil_ga_liquid.cn" '%(T))

        # allow workstep 10 and 11 to be run (Gaussian fluid - ideal gas)
        self.set_flag(10)
        self.set_flag(11)

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')
        for x in range(self.iter_switch_other+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('lambda0 = 1 lambda1 = 1 refpotential = 5 ')
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('lambda0 = 1 lambda1 = 0 refpotential = 5 ')
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

    def step_10(self, jobid, T, mult, initcn=None):
        workstep = 10
        print('working on step %d'%workstep)
        # switching from gaussian fluid to ideal gas
        factor = 0.1
        self.mdpp.cmd('incnfile = "%gK_equil_ga_liquid.cn" readcn '%(T)) # note: setconfig1 removed
        self.cmd('setconfig1 ')

        # switching setup
        self.run_setup(self.atom_mass, workstep, jobid)
        self.mdpp.cmd('srand48bytime ensemble_type = "NVT" integrator_type = "VVerlet" ')
        self.mdpp.cmd('switchfreq = 10 saveprop = 1 openpropfile printfreq = 5000 ')
        self.mdpp.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')
        self.mdpp.cmd('Gauss_rc = 3.771 Gauss_sigma = 0.7 Gauss_epsilon = 50 ')
        self.mdpp.cmd('T_OBJ = %g initvelocity '%(T))

        for x in range(self.iter_switch_ideal_gas+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch_ig)
            # equilibration run
            if self.iter_switch_ideal_gas > 0: # (did we mean x > 0 here?)
                self.cmd('lambda0 = 1 lambda1 = 1 refpotential = 5 switchfunc = 1 ')
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('totalsteps = %d saveprop = 1 totalsteps = %d saveprop = 1 '%(self.numsteps_md_switch,self.numsteps_md_switch))
            W = np.zeros(6)
            for iter in range(6):
                self.cmd('lambda0 = %g lambda1 = %g refpotential = 6 runMDSWITCH '%(np.power(factor,iter),np.power(factor,iter+1)))
                W[iter] = self.mdpp.get('Wavg')

            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g %.18g %.18g %.18g %.18g\n"%(x,W[0],W[1],W[2],W[3],W[4],W[5]))

    def step_11(self, jobid, T, mult, initcn=None):
        workstep = 11
        print('working on step %d'%workstep)
        # switching from idea gas to gaussian fluid
        factor = 0.1
        self.mdpp.cmd('incnfile = "%gK_equil_ga_liquid.cn" readcn '%(T)) # note: setconfig1 removed
        self.cmd('setconfig1 ')

        # switching setup
        self.equilibrate_gaussian_fluid_at_scaled_temperature(T, np.power(factor,6), self.numsteps_equil_liquid, workstep, jobid)
        T_scaled = T / np.power(factor,6)
        self.mdpp.cmd('incnfile = "%gK_equil_ga_liquid.cn" writeall = 1 writecn '%(T_scaled))
        self.mdpp.cmd('output_fmt="curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')

        for x in range(self.iter_switch_ideal_gas+1):
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch_ig)
            # equilibration run
            if self.iter_switch_ideal_gas > 0: # (did we mean x > 0 here?)
                self.cmd('lambda0 = %g lambda1 = %g refpotential = 6 switchfunc = 1 '%(np.power(factor,6),np.power(factor,6)))
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('totalsteps = %d saveprop = 1 totalsteps = %d saveprop = 1 '%(self.numsteps_md_switch,self.numsteps_md_switch))
            W = np.zeros(6)
            for iter in range(6):
                self.cmd('lambda0 = %g lambda1 = %g refpotential = 6 runMDSWITCH '%(np.power(factor,6-iter),np.power(factor,5-iter)))
                W[iter] = self.mdpp.get('Wavg')

            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g %.18g %.18g %.18g %.18g\n"%(x,W[0],W[1],W[2],W[3],W[4],W[5]))

    def step_12(self, jobid, T, mult, initcn=None):
        workstep = 12
        print('working on step %d'%workstep)
        # reversible scaling from T to T/liquid_factor_RS for liquid
        self.cmd('incnfile = "%gK_equil_liquid.cn" readcn setconfig1 '%(T))

        # randomize velocity
        self.run_setup(self.atom_mass, workstep, jobid)
        self.mdpp.cmd('srand48bytime ensemble_type = "NPT" integrator_type = "Gear6" ')
        self.mdpp.cmd('T_OBJ = %g initvelocity '%(T))

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')
        for x in range(self.iter_switch_other+1):
            # equilibration run
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
                self.cmd('initvelocity run ')
            # switching run
            self.cmd('lambda0 = 1 lambda1 = %g switchfunc = 1 refpotential = 4 '%(self.factor_liquid_RS))
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot'))) # note: melting_cubic.tcl only writes Wavg

        # moved to workstep 15
        #os.system('cat prop_%d_%d.out >> prop_%d.out '%(workstep,jobid,workstep))

    def step_13(self, jobid, T, mult, initcn=None):
        workstep = 13
        print('working on step %d'%workstep)
        # reversible scaling form T/liquid_factor_RS to T for liquid
        T1 = T/self.factor_liquid_RS
        self.cmd('incnfile = "%gK_equil_liquid.cn" readcn setconfig1 '%(T))

        # to make it consistent with melting_cubic.tcl
        self.run_setup(self.atom_mass, workstep, jobid)
        self.mdpp.cmd('''
        saveprop = 0  switchfreq = 10  printfreq = 5000
        fixboxvec=[ 0 1 1   1 0 1   1 1 0 ]   stress= [ 0 0 0 0 0 0 0 0 0 ]
        ensemble_type = "NPT" integrator_type = "Gear6"
        ''')
        self.cmd('lambda0 = %g lambda1 = %g refpotential = 4 switchfunc = 1 '%(self.factor_liquid_RS,self.factor_liquid_RS))
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d runMDSWITCH '%(T,self.numsteps_equil_liquid))
        self.mdpp.cmd('finalcnfile = "%gK_equil_liquid.cn" writeall = 1 writecn '%(T1))

        self.cmd('incnfile = "%gK_equil_liquid.cn" '%(T1))

        # switching setup
        self.cmd('switchfreq = 10 saveprop = 1 savepropfreq = 100 openpropfile printfreq = 5000 ')
        self.cmd('output_fmt = "curstep EPOT KATOM HELMP Tinst dEdlambda Wtot Wavg H_11 H_22 H_33 TSTRESS_xx TSTRESS_yy TSTRESS_zz" ')
        for x in range(self.iter_switch_other+1):
            # equilibration run
            self.cmd('readcn totalsteps = %d saveprop = 0 '%self.numsteps_before_switch)
            self.cmd('lambda0 = %g lambda1 = %g refpotential = 4 switchfunc = 1 '%(self.factor_liquid_RS,self.factor_liquid_RS))
            if self.iter_switch_other > 0: # (did we mean x > 0 here?)
                self.cmd('initvelocity runMDSWITCH ')
            # switching run
            self.cmd('lambda0 = %g lambda1 = 1 switchfunc = 1 refpotential = 4 '%(self.factor_liquid_RS))
            self.cmd('totalsteps = %d saveprop = 1 runMDSWITCH '%self.numsteps_md_switch)
            self.cmd('finalcnfile = "f%d%04d.cn" writeall = 1 writecn '%(workstep, x))
            with open('work_F%d.dat'%(workstep), 'a+') as f:
                f.write("%d %.18g %.18g\n"%(x,self.mdpp.get('Wavg'),self.mdpp.get('Wtot')))

        # moved to workstep 15
        #os.system('cat prop_%d_%d.out >> prop_%d.out '%(workstep,jobid,workstep))

    def step_14(self, jobid, T, mult, initcn='perf'):
        workstep = 14
        print('working on step %d'%workstep)
        # diagonalizing Hessian matrix to compute QHC free energy
        a0, L, eT, Ecoh, V, temperature, NP = self.get_data_from_info_file('info2.dat')
        if T != temperature:
            print("Error: T (%g K) does not match that in info file (%g K)"%(T, temperature));
            return()

        kB   = datastore["constants"]["kB"]
        hbar = datastore["constants"]["hbar"]
        Na   = datastore["constants"]["Na"]

        print(" read hessan%g.out ..."%(T))
        t_begin = time.time()
        hessian = np.loadtxt("hessian%g.out"%(T)).reshape([NP*3,NP*3])
        hessian = (hessian + hessian.transpose()) / (-2)
        t_end = time.time()
        print('Elapsed time = %g s'%(t_end-t_begin))
        print(" diagonalizing ...")
        t_begin = time.time()
        D1, eig_vec = np.linalg.eig(hessian)
        t_end = time.time()
        print('Elapsed time = %g s'%(t_end-t_begin))
        w = np.sqrt( np.sort(D1.real)[3:] * 16.022*Na/(self.atom_mass*0.001) )

        F1 = -kB*T*np.sum(np.log(np.divide(kB*T/hbar*np.ones_like(w), w)))
        gam = np.sqrt( pow(6.626068,2)*6.022/(1.3806503*self.atom_mass*0.001*T*2*np.pi) )*0.1

        F     = F1
        F_per = F/NP
        Fha = F + Ecoh*NP
        print('Quasi-harmonic approximation:')
        print('F = %f (eV) dFhar = %f (eV) F_per = %f(eV)'%(Fha,F,F_per))
        with open('%s_%s_%gK_L%d_Fha'%(self.material,initcn,T,L), 'w') as f:
            f.write("%15.8f\n"%(Fha))
        with open('%s_%s_%gK_L%d_Intermediate_Values'%(self.material,initcn,T,L), 'w') as f:
            f.write("Fha = %15.8f\n"%(Fha))
            f.write('eT_solid at 300 = %15.8f\n'%(eT));
            f.write('EO for %d = %15.8f \nF = %15.8f\n'%(NP,Ecoh*NP,F));

        self.set_flag(15)

    def step_15(self, jobid, T, mult, initcn='perf'):
        workstep = 15
        print('working on step %d'%workstep)
        # Analyze data to compute free energy and melting point

        self.solid_success  = self.extract_solid_free_energy(initcn)
        self.liquid_success = self.extract_liquid_free_energy()

        # interpolate solid and liquid free energy and find the intersection point
        if self.solid_success and self.liquid_success:
            Tmin = max(self.T_solid[0], self.T_liquid[-1])
            Tmax = min(self.T_solid[-1], self.T_liquid[0])
            dT = (Tmax-Tmin) / 100000
            self.T_grid = np.arange(Tmin,Tmax+dT/10,dT)
            self.Fs_interp = np.interp(self.T_grid, self.T_solid,  self.Fs)
            self.Fl_interp = np.interp(self.T_grid, np.flip(self.T_liquid), np.flip(self.Fl))

            ind_min = np.argmin(np.abs(self.Fs_interp - self.Fl_interp))
            self.T_m = self.T_grid[ind_min]
            print('T_m = %g K'%(self.T_m))
