import sys, os, glob
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'scripts', 'python'))
from mdutil import loadcn

import numpy as np

import ovito
from ovito.data import *
from ovito.io import export_file

# Obtain the file names of the chain, state A and state B
nargs = 3
if len(sys.argv) == nargs:
    scriptfile = sys.argv[0]
    cnfile     = sys.argv[1]
    lammpsfile = sys.argv[2]
else:
    raise ValueError('Must have 2 arguments: cnfile and lammpsfile!')

num_particles, rawdata, hmat = loadcn(cnfile, structured_array=True)

# Create the configuration in OVITO
data = DataCollection()

# Create the particle position property.
pos_prop = ParticleProperty.create(ParticleProperty.Type.Position, num_particles)
pos_prop.marray[:] = np.dot(hmat, rawdata['s'].T).T
data.add(pos_prop)

# Create the particle type property.
if 'species' in rawdata.dtype.names:
    type_prop = ParticleProperty.create(ParticleProperty.Type.ParticleType, num_particles)
    species = np.unique(rawdata['species'] + 1)
    #print(species)
    for isp in species:
        type_prop.type_list.append(ParticleType(id = isp))
    type_prop.marray[:] = rawdata['species'] + 1
    data.add(type_prop)

# Create the simulation box.
cell = SimulationCell()
cell.matrix = np.hstack([hmat, -np.diag(hmat).reshape(3, 1)/2])
cell.pbc = (True, True, True)
cell.display.line_width = 0.1
data.add(cell)

# Create a node and insert it into the scene.
node = ovito.ObjectNode()
node.source = data
export_file(node, lammpsfile, 'lammps_data')