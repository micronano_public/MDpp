'''
Utility extension for MD++ in Python, behaves as a child class of mdpp

'''
import os, time
import numpy as np

list_to_str = lambda lst: '[ %s ]'%(' '.join([str(it) for it in lst]))

datastore = { 
  "constants": {             # Universal constants 
        "kB" :               8.617343e-5,     # eV/K
      "hbar" :               6.58211899e-16,  # eV s
        "Na" :               6.02214179e23    # unitless
      },
}

class mdpp_util:
    def __init__(self, runs_dir, folder_prefix=None, mdpp=None, setnolog=True, setoverwrite=True, zipfiles=True, 
                 supercell=None, NxNyNz=None, crystalstructure=None, lattice_constant=None, poisson_ratio=0.3,
                 cn_output_format=['cn', 'LAMMPS'], make_output_dir=True):
        ''' Initialize MD++ simulation
            If mdpp is not given, the class can only be used for creating directory name
        '''
        self.runs_dir = runs_dir
        self.folder_prefix = folder_prefix                # prefix of dirname
        self.mdpp = mdpp

        self.sc = supercell                               # supercell repeat direction vectors
        self.crystalstructure = crystalstructure
        self.a = lattice_constant                         # lattice constant, unit length in Angstrom
        self.nu = poisson_ratio                           # Poisson's ratio
        self.cn_output_format = cn_output_format
        self.sleep_seconds = 20

        if NxNyNz is not None:
            if len(NxNyNz) == 3:
                self.Nx, self.Ny, self.Nz = NxNyNz        # repeat of lattice vectors in x, y, z direction
            else:
                self.Nx, self.Ny, self.Nz = (0, 0, 0)
        else:
            self.Nx, self.Ny, self.Nz = (0, 0, 0)

        if self.Nx > 0 and self.Ny > 0 and self.Nz > 0:
            self.dirname = os.path.join(self.runs_dir, '%s-%dx%dx%d'%(self.folder_prefix,self.Nx,self.Ny,self.Nz))
        else:
            self.dirname = os.path.join(self.runs_dir, self.folder_prefix)

        if make_output_dir:
            self.create_output_dir(setnolog, setoverwrite, zipfiles)

    def create_output_dir(self, setnolog, setoverwrite, zipfiles):
        if not os.path.exists(self.runs_dir):
            os.makedirs(self.runs_dir)

        if self.mdpp is not None:
            if setnolog:
                self.mdpp.cmd("setnolog")
            if setoverwrite:
                self.mdpp.cmd("setoverwrite")
            self.mdpp.cmd("dirname = " + self.dirname)
            self.mdpp.cmd("zipfiles = %d"%zipfiles)
            self.mdpp.cmd("NNM = 500")  # make sure neighborlist is large enough
        else:
            print("WARNING: mdpp is None - just for creating directories")

    def cmd(self, arg):
        if self.mdpp is not None:
            self.mdpp.cmd(arg)
        else:
            print("WARNING: mdpp is None - command ignored")

    def get(self, arg):
        if self.mdpp is not None:
            return self.mdpp.get(arg)
        else:
            print("WARNING: mdpp is None - command ignored")
            return None
            
    def get_array(self, arg):
        if self.mdpp is not None:
            return self.mdpp.get_array(arg)
        else:
            print("WARNING: mdpp is None - command ignored")
            return None

    def write_file(self, filename, format=None):
        '''Write current configuration into file
        '''
        if filename is None:
            return
        if format is None:
            format = self.cn_output_format
        for ext in format:
            self.mdpp.cmd('writeall = 1 finalcnfile = %s.%s write%s'%(filename, ext, ext))

    def load_file(self, filename):
        '''Load a configuration into MD++, now only support reading .cn file
        '''
        base, ext = os.path.splitext(filename)
        if ((ext == '.gz' and os.path.splitext(base) != '.cn') or 
            (ext != '.gz' and ext != '.cn')):
            raise TypeError('load_file: only support .cn file!')
        self.mdpp.cmd('incnfile = %s readcn'%filename)

    def read_fs_pot(self, potfile):
        self.mdpp.cmd('potfile = ' + os.path.join(os.environ['MDPLUS_DIR'], 'potentials', potfile))
        self.mdpp.cmd('readpot')

    def read_eam_pot(self, potfile):
        self.mdpp.cmd('potfile = ' + os.path.join(os.environ['MDPLUS_DIR'], 'potentials', potfile))
        self.mdpp.cmd('eamgrid = 500 readeam NNM = 1000 ')

    def relax_fixbox(self, ftol=1e-7, fevalmax=1000, itmax=1000, fixboxvec=None, filename=None, format=None):
        '''Conjugate-Gradient relaxation
        '''
        self.mdpp.cmd('conj_itmax = %e'%itmax)
        self.mdpp.cmd('conj_ftol = %e'%ftol)
        self.mdpp.cmd('conj_fevalmax = %d'%fevalmax)
        if fixboxvec is not None:
            self.mdpp.cmd('conj_fixboxvec = %s'%list_to_str(fixboxvec))
        self.mdpp.cmd('conj_fixbox = 1 relax')
        self.write_file(filename, format=format)
        self.mdpp.cmd('eval')   # evaluate the potential of relaxed configuration

    def relax_freebox(self, ftol=1e-7, fevalmax=1000, itmax=1000, fixboxvec=[0,1,1,1,0,1,1,1,0], filename=None, format=None):
        '''Conjugate-Gradient relaxation
        '''
        self.mdpp.cmd('conj_itmax = %e'%itmax)
        self.mdpp.cmd('conj_ftol = %e'%ftol)
        self.mdpp.cmd('conj_fevalmax = %d'%fevalmax)
        if fixboxvec is not None:
            self.mdpp.cmd('conj_fixboxvec = %s'%list_to_str(fixboxvec))
        self.mdpp.cmd('conj_fixbox = 0 relax')
        self.write_file(filename, format=format)
        self.mdpp.cmd('eval')   # evaluate the potential of relaxed configuration

    def setup_window(self):
        '''Plot Configuration
        '''
        self.mdpp.cmd("""
            atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0 #2.8285 #for Si
            #
            atomcolor = cyan highlightcolor = purple  bondcolor = red
            fixatomcolor = yellow backgroundcolor = gray70
            color00 = "red" color01 = "blue" color02 = "green"
            color03 = "magenta" color04 = "cyan" color05 = "purple"
            color06 = "gray80" color07 = "white" color08 = "orange"
            #
            plot_color_axis = 2
            plot_color_windows = [ 5
                                   0.01 6   1  
                                   6  10   5
                                   40 200   8
                                   0 0.01  4
                                   10 40   6
                                 ]
            plot_atom_info = 2 plotfreq = 10
            rotateangles = [ 0 0 0 1.5 ]
            win_width = 600 win_height = 600
            """)

    def openwindow(self):
        self.setup_window()
        self.mdpp.cmd('openwin alloccolors rotate saverot refreshnnlist eval plot ')

    def create_perfect_crystal(self, crystalstructure, latticeconst, supercellsize, 
                               filename=None, format=None):
        ''' Create Perfect Lattice Configuration
        Parameters
        ----------
        crystalstructure : string
            Crystal structure to be created, must be one of the following:
            {'body-centered-cubic', 'face-centered-cubic', ...}
        latticeconst : float
            Lattice constant in Angstrom
        supercellsize : ndarray of shape (3, 3)
            supercellsize = [[a_1, a_2, a_3, repeat_a],
                             [b_1, b_2, b_3, repeat_b],
                             [c_1, c_2, c_3, repeat_c]]
            where [a_1, a_2, a_3] specifies the first repeat vector of the simulation
            supercell in Miller indices and repeat_a is the number of the repeats in
            that direction. [b_1, b_2, b_3] and [c_1, c_2, c_3] represent the second
            and third direction respectively, repeat repeat_b and repeat_c times.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''

        self.mdpp.cmd('crystalstructure = %s'%crystalstructure)
        if not hasattr(latticeconst, "__len__"):
            self.mdpp.cmd('latticeconst = ' + str(latticeconst) + ' #(A)')
        elif len(latticeconst) == 1:
            self.mdpp.cmd('latticeconst = ' + str(latticeconst[0]) + ' #(A)')
        elif len(latticeconst) == 3:
            self.mdpp.cmd('latticeconst = [ ' + str(latticeconst[0]) \
                                        + ' ' + str(latticeconst[1]) + ' ' + str(latticeconst[2]) + ' ] #(A)')
        else:
            print("Error: invalid length of latticeconst")

        if supercellsize.shape != (3, 4):
            raise ValueError('create_perfect_crystal: supercellsize must have shape of (3, 4)')
        supercell_size_str = list_to_str(supercellsize.flatten())
        self.mdpp.cmd('latticesize = %s makecrystal'%supercell_size_str)
        self.write_file(filename, format=format)
        #self.mdpp.cmd('eval') # evaluate the potential of perfect crystal

    def create_dislocation_dipole(self, linedirection, separationdir, burgersvector, x0, y0, y1, nu=None,
                                  num_images=[-10, 10, -10, 10], tilt_box=True, sz_limit=None, store=False,
                                  filename=None, format=None):
        ''' Create Dislocation Dipole
        Parameters
        ----------
        linedirection : number
            dislocation line direction, 1,2,3 = x,y,z
        separationdir : number
            separation direction between two dislocations in the dipole, 1,2,3 = x,y,z
        burgersvector : ndarray of shape (3, )
            Burgers vector, [bx, by, bz] in scaled coordinates
        x0,y0,y1 : float
            (x,y) position of the dislocation dipole, (x0, y0) and (x0, y1)
        nu : float
            Poisson's ratio of the material
        num_images : list of 4 int numbers
            Number of periodic image copies to calculate displacement of atoms
        tilt_box : bool
            if simulation box is tilted to keep periodic boundary condition
        sz_limit : tuple of size 2
            (sz_min, sz_max) represents the limit of dislocation in z direction
        store : bool
            If True, only define dislocation but not displace atoms.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''
        if sz_limit:
            limit_sz = [1, sz_limit[0], sz_limit[1]]
        else:
            limit_sz = [0, 0, 0]
        if nu is None: nu = self.nu
        dipole_input = [ linedirection, separationdir,
                         burgersvector[0], burgersvector[1], burgersvector[2],
                         x0, y0, y1, nu ] + num_images + [int(tilt_box), ] + limit_sz + [int(store), ]
        input_str = list_to_str(dipole_input)
        print('create_dislocation_dipole: input =', input_str)
        self.mdpp.cmd('input = %s  makedipole '%input_str)
        self.write_file(filename, format = format)
        if not store:
            self.mdpp.cmd('eval') # evaluate the potential of dislocation configuration

    def create_disl_polygon(self, burgersvector, xlist, ylist, zlist, nu, a, store=False,
                            filename = None, format = None):
        ''' Create Dislocation Dipole
        Parameters
        ----------
        burgersvector : ndarray of shape (3, )
            Burgers vector, [bx, by, bz] in scaled coordinates
        xlist,ylist,zlist : 1d-ndarray of same length
            (x,y,z) positions of the dislocation polygon
        nu : float
            Poisson's ratio of the material
        a : float
            a length scale (in Å) for the Burgers vector
        store : bool
            If True, only define dislocation but not displace atoms.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''
        if len(xlist) != len(ylist) or len(ylist) != len(zlist) or len(xlist) != len(zlist):
            raise ValueError('create_disl_polygon: xlist, ylist, zlist size not consistent!')
        xyzlist = np.stack([xlist, ylist, zlist], axis=-1).flatten()
        dislpolygon_input = [ 1, int(store), nu, a, burgersvector[0], burgersvector[1], burgersvector[2],
                              len(xlist)] + xyzlist.tolist()
        input_str = list_to_str(dislpolygon_input)
        print('create_disl_polygon: input =', input_str)
        self.mdpp.cmd('input = %s  makedislpolygon'%input_str)
        self.write_file(filename, format = format)
        if not store:
            self.mdpp.cmd('eval') # evaluate the potential of dislocation configuration

    def make_perfect_crystal(self, savefile=None, a=None, by_slice=None):
        if a is None: a = self.a
        createNx = self.Nx
        createNy = self.Ny
        createNz = self.Nz

        self.by_slice = by_slice
        if by_slice is not None:
            if by_slice == 1 :
                createNx = 1
                self.extend_mul = self.Nx
                self.mdpp.cmd('allocmultiple = %d '%(self.extend_mul))
            elif by_slice == 2:
                createNy = 1
                self.extend_mul = self.Ny
                self.mdpp.cmd('allocmultiple = %d '%(self.extend_mul))
            elif by_slice == 3:
                createNz = 1
                self.extend_mul = self.Nz
                self.mdpp.cmd('allocmultiple = %d '%(self.extend_mul))
            else:
                print("ERROR: unknown value of by_slice %d"%(by_slice))

        supercell_size = np.array([[ self.sc[0][0], self.sc[0][1], self.sc[0][2],  createNx],
                                   [ self.sc[1][0], self.sc[1][1], self.sc[1][2],  createNy],
                                   [ self.sc[2][0], self.sc[2][1], self.sc[2][2],  createNz]])

        self.create_perfect_crystal(self.crystalstructure, a, supercell_size)

        if self.by_slice and self.extend_mul > 1:
            self.mdpp.cmd('input = [ %d %d ] '%(self.by_slice, self.extend_mul))
            self.mdpp.cmd('extendbox ')

        if savefile is not None:
            self.write_file(savefile)
        
    def make_liquid(self, savefile=None, a=None):
        self.make_perfect_crystal(savefile=savefile, a=a)
        self.mdpp.cmd("srand48bytime randomposition ")

    def cut_thin_slice(self):
        self.mdpp.cmd('input = [ %d %d ] '%(self.by_slice, self.extend_mul))
        self.mdpp.cmd('cutslice ')
        #self.mdpp.cmd('plot ')

    def set_screw_dipole_geometry(self):
        # burger's vector in scaled coordinates
        self.bs = [0, 0, 1.0/(self.Nz)]            
        # position of the dislocation dipole in x direction
        self.x0 = -0.000123 + 1.0/6.0/self.Nx
        self.x1 = self.x0 + 0.5*self.ds
        self.x2 = self.x0 - 0.5*self.ds
        dy = -0.125/(self.Ny)
        # position of the dislocation dipole in y direction
        self.y0 = -0.50 + dy
        self.y1 =  0.00 + dy

    def make_screw_dipole(self, store, line_dir=3, sep_dir=2, nu=None):
        if nu is None: nu = self.nu
        self.set_screw_dipole_geometry()

        # create a screw dislocation at the center with full burger's vector
        line_direction = 3                 # x, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole

        self.create_dislocation_dipole(line_direction, separation_dir, self.bs, self.x0, self.y0, self.y1, nu=nu, store=store)
    
    def shear_atomic_plane(self):
        self.set_screw_dipole_geometry()
        # make the cut-plane of the dipole cut across entire crystal
        self.create_dislocation_dipole(3, 2, self.bs, self.x0, self.y0, self.y0+1.0, nu=self.nu, store=False)

    def make_screw_partials(self, store, nu=None, be_mul=1.0):
        if nu is None: nu = self.nu
        self.set_screw_dipole_geometry()

        # create a edge dislocation at the center (with offset) with full burger's vector
        line_direction = 3                            # z, dislocation line direction
        separation_dir = 1                            # x, separation direction between two dislocations in the dipole
        bp1 = [ be_mul*1.0/6.0/(self.Nx), 0,  0.5/(self.Nz)] # burger's vector in scaled coordinates
        bp2 = [ be_mul*1.0/6.0/(self.Nx), 0, -0.5/(self.Nz)] # burger's vector in scaled coordinates

        # split the bottom screw dislocation (only used for creating dislocation dipole in PBC)
        #self.create_dislocation_dipole(line_direction, separation_dir, bp1, self.y0, self.x0, self.x1, nu, store=True)
        #self.create_dislocation_dipole(line_direction, separation_dir, bp2, self.y0, self.x2, self.x0, nu, store=True)

        # split the top screw dislocation
        self.create_dislocation_dipole(line_direction, separation_dir, bp2, self.y1, self.x0, self.x1, nu, store=True)
        self.create_dislocation_dipole(line_direction, separation_dir, bp1, self.y1, self.x2, self.x0, nu, store=store)

    def set_edge_dipole_geometry(self):
        # burger's vector in scaled coordinates
        self.be = [1.0/(self.Nx), 0, 0]            
        # position of the dislocation dipole in x direction
        self.x0 = -0.000123 + 1.0/6.0/self.Nx
        self.x1 = self.x0 + 0.5*self.ds
        self.x2 = self.x0 - 0.5*self.ds
        dy = -0.125/(self.Ny)
        # position of the dislocation dipole in y direction
        self.y0 = -0.50 + dy
        self.y1 =  0.00 + dy

    def make_edge_dipole(self, store, nu=None):
        if nu is None: nu = self.nu
        self.set_edge_dipole_geometry()

        # create a edge dislocation at the center (with offset) with full burger's vector
        line_direction = 3                 # z, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole

        self.create_dislocation_dipole(line_direction, separation_dir, self.be, self.x0, self.y0, self.y1, nu, store=store)

    def remove_atomic_plane(self):
        self.set_edge_dipole_geometry()
        sxmin = self.x0 - self.be[0]*0.51
        sxmax = self.x0 + self.be[0]*0.50
        self.mdpp.cmd('input = [ 1 %g %g -10 10 -10 10 ] fixatoms_by_position '%(sxmin, sxmax))
        self.mdpp.cmd('markremovefixedatoms ')

    def make_edge_partials(self, store, nu=None, bs_mul=1.0):
        if nu is None: nu = self.nu
        self.set_edge_dipole_geometry()

        # create a edge dislocation at the center (with offset) with full burger's vector
        line_direction = 3                            # z, dislocation line direction
        separation_dir = 1                            # x, separation direction between two dislocations in the dipole
        bp1 = [ 0.5/(self.Nx), 0, bs_mul*(-1.0)/6.0/(self.Nz)] # burger's vector in scaled coordinates
        bp2 = [-0.5/(self.Nx), 0, bs_mul*(-1.0)/6.0/(self.Nz)] # burger's vector in scaled coordinates

        # split the bottom edge dislocation (only used for creating dislocation dipole in PBC)
        #self.create_dislocation_dipole(line_direction, separation_dir, bp1, self.y0, self.x0, self.x1, nu, store=True)
        #self.create_dislocation_dipole(line_direction, separation_dir, bp2, self.y0, self.x2, self.x0, nu, store=True)

        # split the top edge dislocation
        self.create_dislocation_dipole(line_direction, separation_dir, bp2, self.y1, self.x0, self.x1, nu, store=True)
        self.create_dislocation_dipole(line_direction, separation_dir, bp1, self.y1, self.x2, self.x0, nu, store=store)

    def group_from_list(self, id_list, group):
        for i in id_list:
            self.mdpp.cmd(r'group(%d) = %d'%(i+1, group))

    def get_state_relax_iter(self, save_iter_data, props=None, separate=False):
        '''Obtain energy and stress information during state A relaxation
        '''
        iter_data = np.loadtxt(save_iter_data)
        if iter_data.size == 0:
            raise ValueError('get_state_relax_iter: %s has no content'%save_iter_data)
        if len(iter_data.shape) == 1:
            iter_data = iter_data.reshape(1, -1)
        if props is not None:
            loc = 0; iter_data_sep = []
            for i in range(len(props)):
                iter_data_sep.append(iter_data[:, loc:(loc+len(props[i]))])
                loc += len(props[i])
            iter_data = np.hstack(iter_data_sep)
        if separate:
            return iter_data_sep
        else:
            return iter_data

    def scale_H(self, eT):
        self.mdpp.cmd('input = [ 1 1 %.18e ] shiftbox '%eT)
        self.mdpp.cmd('input = [ 2 2 %.18e ] shiftbox '%eT)
        self.mdpp.cmd('input = [ 3 3 %.18e ] shiftbox '%eT)

    def sleep_at_end(self):
        print("Python is going to sleep for " + str(self.sleep_seconds) + " seconds.")
        time.sleep(self.sleep_seconds)

    def get_epot(self):
        self.mdpp.cmd('eval ')
        EPOT = self.mdpp.get('EPOT')
        return EPOT

    def get_nve(self):
        self.mdpp.cmd('eval ')
        NP   = self.mdpp.get('NP')
        V    = self.mdpp.get('OMEGA')
        EPOT = self.mdpp.get('EPOT')
        a    = np.cbrt((V/NP)*self.natom_per_unitcell)
        Ecoh = EPOT / NP
        return NP, V, EPOT, a, Ecoh

    def get_average_from_datafile(self, filename, col, frac_begin, frac_end=1.0):
        prop_data = np.loadtxt(filename)
        ind_begin = int(round(prop_data.shape[0]*frac_begin))
        ind_end   = int(round(prop_data.shape[0]*frac_end))
        average = prop_data[ind_begin:ind_end,col].mean()
        return average

    def run_setup(self, atom_mass, n, jobid, simid=None):
        self.mdpp.cmd("atommass = %g"%atom_mass)
        self.mdpp.cmd("equilsteps=0 timestep=0.0001 DOUBLE_T=0 savecn=1 savecnfreq=999999 saveprop=1 savepropfreq=100 ")
        if n == 1 or n == 7:
            self.mdpp.cmd("savecnfreq = 99999 ")
        if simid == None:
            self.mdpp.cmd("outpropfile =  prop_%d_%d.out"%(n, jobid))
            self.mdpp.cmd("intercnfile = inter_%d_%d_.cn"%(n, jobid))
        else:
            self.mdpp.cmd("outpropfile =  prop_%d_%d_%d.out"%(n,jobid,simid))
            self.mdpp.cmd("intercnfile = inter_%d_%d_%d_.cn"%(n,jobid,simid))
        self.mdpp.cmd(" openpropfile openintercnfile wallmass=2e3 vt2=2e28 boxdamp=1e-3 saveH ")

    def equilibrate_volume_at_temperature(self, T, nsteps, n, jobid, simid=None):
        # run MD simulation of solid at T
        self.run_setup(self.atom_mass, n, jobid, simid)
        self.mdpp.cmd('''
        fixboxvec=[ 0 1 1   1 0 1   1 1 0 ]   stress= [ 0 0 0 0 0 0 0 0 0 ]
        output_fmt="curstep EPOT KATOM HELM Tinst TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33 OMEGA"
        writeall=1
        NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NPTC" integrator_type = "Gear6"
        ''')
        self.mdpp.cmd('T_OBJ = %g initvelocity totalsteps = %d run '%(T,nsteps))
        self.mdpp.cmd(' closepropfile ')

        # compute average volume at T
        V = self.get_average_from_datafile(self.mdpp.get("outpropfile"), 11, 0.7, 1.0)
        return V
		
    def setup_md(self,atom_mass,timestep = 0.001):
        self.mdpp.cmd('equilsteps = 0  timestep = %f'%timestep)	    
        self.mdpp.cmd('atommass = %f # (g/mol)'%atom_mass)
        self.mdpp.cmd('''
        DOUBLE_T = 1
        saveprop = 1 savepropfreq = 10 openpropfile #run
        savecn = 1 savecnfreq = 1000 openintercnfile
        savecfg = 1 savecfgfreq = 1000
        plotfreq = 10000 printfreq = 10000
        vt2 = 1e28  #1e28 2e28 5e28
        wallmass = 2e4     # atommass * NP 
        boxdamp = 1e-3     #
        saveH # Use current H as reference (H0), needed for specifying stress
        fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
        output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
        ''')	
		
    def run_nve(self, t_obj = 300, double_t = 1, totalsteps = 100000):
        self.mdpp.cmd('ensemble_type = "NVE" integrator_type = "VVerlet"')
        self.mdpp.cmd("T_OBJ = %d DOUBLE_T = % randseed = 12345 srand48 initvelocity"%(t_obj, double_t))
        self.mdpp.cmd("totalsteps = %d run"%totalsteps)
		
    def run_npt(self, t_obj = 300, double_t = 1, totalsteps = 100000, timestep = 0.0001):
        self.mdpp.cmd('ensemble_type = "NPTC" integrator_type = "Gear6" timestep = %f'%timestep)
        self.mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
        self.mdpp.cmd("T_OBJ = %d DOUBLE_T = % randseed = 12345 srand48 initvelocity"%(t_obj, double_t))
        self.mdpp.cmd("totalsteps = %d run"%totalsteps)
		
    def run_nvt(self, t_obj = 300, double_t = 1, totalsteps = 100000):
        self.mdpp.cmd('ensemble_type = "NVT" integrator_type = "VVerlet"')
        self.mdpp.cmd("T_OBJ = %d DOUBLE_T = % randseed = 12345 srand48 initvelocity"%(t_obj, double_t))
        self.mdpp.cmd("totalsteps = %d run"%totalsteps)

    def run_nvt_and_relax(self, t_obj = 300, double_t = 1, totalsteps = 100000, per_step = 50, propfreq=10):
        Lx0 = self.mdpp.get("H_11")  
        self.mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
        self.mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
        self.mdpp.cmd("T_OBJ = %d DOUBLE_T = % randseed = 12345 srand48 initvelocity"%(t_obj, double_t))
        mdsteps = int(totalsteps/per_step)
        fid = open("P_L.dat","w")
        for iter in range(per_step):
            self.mdpp.cmd("continue_curstep = 1")
            self.mdpp.cmd("totalsteps = %d run"%mdsteps)
            property_data = np.loadtxt("prop.out")
        # average over second half of the simulation
            nstart = mdsteps//propfreq//2
            sig_xx = np.mean( property_data[-nstart:,6] )
            sig_yy = np.mean( property_data[-nstart:,7] )
            sig_zz = np.mean( property_data[-nstart:,8] )
            pressure = (sig_xx + sig_yy + sig_zz) / 3.0
            Lx = self.mdpp.get("H_11")
            strain = (Lx - Lx0)/Lx0
            print("iter = %d  p = %.12f  Lx = %.12f strain = %.8f]"%(iter, pressure, Lx, strain))
            print(" %d   %.12f  %.12f  %.8f"%(iter, pressure, Lx, strain), file=fid)
            fid.flush()

        # Adjust box size according to pressure
            if iter < per_step-1:
                eps_T = pressure * 5e-7
                self.mdpp.cmd("input = [ 1 1 %f ] changeH_keepS"%(eps_T))
                self.mdpp.cmd("input = [ 2 2 %f ] changeH_keepS"%(eps_T))
                self.mdpp.cmd("input = [ 3 3 %f ] changeH_keepS"%(eps_T))

        fid.close()
		
       
