'''
mdutil
======

The utility functions for Molecular Dynamics simulation. Adapted from
Matlab code from Tools/matlab

Yifan Wang (yfwang09@stanford.edu)
Wei Cai (caiwei@stanford.edu)
2019.4.9

'''

import os
import numpy as np
from itertools import combinations

######################## io functions ########################

def loadcn(cnfilename, structured_array=False, verbose=True):
    '''Load .cn type configuration files
    
        For a given configuration file *.cn, LOADCN reads from the file
        and get the atom coordinates and cell size.
        
        Parameters
        ----------
        cnfilename : string
            Filename string, can be '.cn' or gzipped as '.cn.gz'.
        structured_array : bool
            If True, return structured array rawdata
        verbose : bool
            If True, print data information
        
        Returns
        -------
        nparticles : int
            number of atoms
        rawdata : structured ndarray
            's'       : Scaled coordinates, dimension (, 3)
            'vs'      : Scaled velocity, dimension (, 3)
            'epot'    : Potential energies per atom
            'fixed'   : Fixed status, 1-fixed, 0-unfixed
            'topol'   : Centro-symmetry deviation
            'species' : Atom species
            'group'   : Group ID
            'image'   : Image status, 1-image, -1-real
        h : ndarray
            Simulation box size (c1|c2|c3)

    '''
    if not os.path.exists(cnfilename):
        raise TypeError('File {} not exist!'.format(cnfilename))
    
    if verbose:
        print('Reading configuration file', cnfilename)
    nparticles = np.genfromtxt(cnfilename, dtype=np.int, max_rows=1).item()
    data = np.genfromtxt(cnfilename, skip_header=1, max_rows=nparticles)
    h = np.genfromtxt(cnfilename, skip_header=nparticles+1, max_rows=3)
    if data.shape[1] == 3:
        rawdata = np.empty(nparticles, dtype=[('s', float, 3)])
        rawdata['s'] = data[:, :3]
        if not structured_array:
            return (nparticles, rawdata['s'], h)
    elif data.shape[1] == 6:
        rawdata = np.empty(nparticles, dtype=[('s', float, 3), ('vs', float, 3)])
        rawdata['s'] = data[:, :3]; rawdata['vs'] = data[:, 3:6];
        if not structured_array:
            return (nparticles, rawdata['s'], rawdata['vs'], h)
    elif data.shape[1] == 12:
        rawdata = np.empty(nparticles, 
                           dtype=[('s', float, 3), ('vs', float, 3), ('epot', float), ('fixed', int),
                                  ('topol', float), ('species', int),('group', int), ('image', int)])
        rawdata['s']     = data[:, :3]; rawdata['vs']      = data[:, 3:6];
        rawdata['epot']  = data[:, 6];  rawdata['fixed']   = data[:, 7];
        rawdata['topol'] = data[:, 8];  rawdata['species'] = data[:, 9];
        rawdata['group'] = data[:, 10]; rawdata['image']   = data[:, 11];
        if not structured_array:
            return (nparticles, rawdata['s'], rawdata['vs'], rawdata['epot'], 
                    rawdata['fixed'], rawdata['topol'], rawdata['species'], 
                    rawdata['group'], rawdata['image'], h)
    else:
        print('loadcn: Invalid format of {}'.format(cnfilename))
        raise SyntaxError

    if verbose:
        print('Nparticles = {}'.format(nparticles))
        print('Data fields: ', rawdata.dtype)
        print('Simulation Cell')
        print(h)
    return (nparticles, rawdata, h)

################### boundary conditions ######################

def pbc(drij, h, hinv=None):
    '''calculate distance vector between i and j
    
        Considering periodic boundary conditions (PBC)
    
        Parameters
        ----------
        drij : float, dimension (npairs, 3)
            distance vectors of atom pairs (Angstrom)
        h : float, dimension (3, 3)
            Periodic box size h = (c1|c2|c3)
        hinv : optional, float, dimension (3, 3)
            inverse matrix of h, if None, it will be calculated

        Returns
        -------
        drij : float, dimension (npairs, 3)
            modified distance vectors of atom pairs considering PBC (Angstrom)
            
    '''
    # Check the input
    if len(drij.shape) == 1:         # Only one pair
        drij = drij.reshape(1, -1)
    if (len(drij.shape) != 2):
        raise ValueError('pbc: drij shape not correct, must be (npairs, nd), (nd = 2,3)')
    npairs, nd = drij.shape 
    if len(h.shape) != 2 or h.shape[0] != h.shape[1] or nd != h.shape[0]:
        raise ValueError('pbc: h matrix shape not consistent with drij')
    # Calculate inverse matrix of h
    if hinv is None:
        hinv = np.linalg.inv(h)

    dsij = np.dot(hinv, drij.T).T
    dsij = dsij - np.round(dsij)
    drij = np.dot(h, dsij.T).T
    
    return drij

################### neighbor lists ######################

def celllist(r, h, Ns, Ny=None, Nz=None):
    '''Construct cell list in 3D
    
        This function takes the **real coordinates** of atoms `r` and the
        simulation box size `h`. Grouping atoms into Nx x Ny x Nz cells
        
        Parameters
        ----------
        r : float, dimension (nparticles, nd)
            *real* coordinate of atoms
        h : float, dimension (nd, nd)
            Periodic box size h = (c1|c2|c3)
        Ns : tuple, dimension (nd, )
            number of cells in x, y, z direction
        Ny : int
            if not None, represent number of cells in y direction, use with Nx = Ns
        Nz : int
            if not None, represent number of cells in z direction
            
        Returns
        -------
        cell : list, dimension (Nx, Ny, Nz)
            each element cell[i][j][k] is also a list recording all 
            the indices of atoms within the cell[i][j][k].
            (0 <= i < Nx, 0 <= j < Ny, 0 <= k < Nz)
        cellid : int, dimension (nparticles, nd)
            for atom i:
            ix, iy, iz = (cellid[i, 0], cellid[i, 1], cellid[i, 2])
            atom i belongs to cell[ix][iy][iz]

    '''
    if Ny is not None:
        if Nz is None:
            Ns = (Ns, Ny)
        else:
            Ns = (Ns, Ny, Nz)

    nparticle, nd = r.shape
    if nd != 3 or len(Ns) != 3:
        raise TypeError('celllist: only support 3d cell')

    # create empty cell list of size Nx x Ny x Nz
    cell = np.empty(Ns, dtype=object)
    for i, v in np.ndenumerate(cell):
        cell[i] = []

    # find reduced coordinates of all atoms
    s = np.dot(np.linalg.inv(h), r.T).T
    # fold reduced coordinates into [0, 1) as scaled coordinates
    s = s - np.floor(s)

    # create cell list and cell id list
    cellid = np.floor(s*np.array(Ns)[np.newaxis, :]).astype(np.int)
    for i in range(nparticle):
        cell[tuple(cellid[i, :])].append(i)

    return cell, cellid

def verletlist(r, h, rv):
    '''Construct Verlet List (neighbor list) in 3D (vectorized)
    
        Uses celllist to achieve O(N)
    
        Parameters
        ----------
        r : float, dimension (nparticles, 3)
            *real* coordinate of atoms
        h : float, dimension (3, 3)
            Periodic box size h = (c1|c2|c3)
        rv : float
            Verlet cut-off radius
            
        Returns
        -------
        nn : int, dimension (nparticle, )
            nn[i] is the number of neighbors for atom i
        nindex : list, dimension (nparticle, nn)
            nindex[i][j] is the index of j-th neighbor of atom i,
            0 <= j < nn[i].
            
    '''
    nparticle, nd = r.shape
    if nd != 3:
        raise TypeError('celllist: only support 3d cell')

    # first determine the size of the cell list
    c1 = h[:, 0]; c2 = h[:, 1]; c3 = h[:, 2];
    V = np.abs(np.linalg.det(h))
    hx = np.abs( V / np.linalg.norm(np.cross(c2, c3)))
    hy = np.abs( V / np.linalg.norm(np.cross(c3, c1)))
    hz = np.abs( V / np.linalg.norm(np.cross(c1, c2)))
    
    # Determine the number of cells in each direction
    Nx = np.floor(hx/rv).astype(np.int)
    Ny = np.floor(hy/rv).astype(np.int)
    Nz = np.floor(hz/rv).astype(np.int)
    
    if Nx < 2 or Ny < 2 or Nz < 2:
        raise ValueError("Number of cells too small! Increase simulation box size.")

    # Inverse of the h matrix
    hinv = np.linalg.inv(h);
    cell, cellid = celllist(r, h, Nx, Ny, Nz)
    
    # Find the number of atoms
    nparticles = r.shape[0]
    
    # initialize Verlet list
    nn = np.zeros(nparticles, dtype=int)
    nindex = [[] for i in range(nparticles)]
    
    for i in range(nparticles):
        # position of atom i
        ri = r[i, :].reshape(1, 3)
        
        # find which cell (ix, iy, iz) that atom i belongs to
        ix, iy, iz = (cellid[i, 0], cellid[i, 1], cellid[i, 2])
        
        # go through all neighboring cells
        ixr = ix+1
        iyr = iy+1
        izr = iz+1
        if Nx < 3:
            ixr = ix
        if Ny < 3:
            iyr = iy
        if Nz < 3:
            izr = iz
        
        for nx in range(ix-1, ixr+1):
            for ny in range(iy-1, iyr+1):
                for nz in range(iz-1, izr+1):
                    # apply periodic boundary condition on cell id nnx, nny, nnz
                    nnx, nny, nnz = (nx%Nx, ny%Ny, nz%Nz)

                    # extract atom id in this cell
                    ind = cell[nnx][nny][nnz].copy()
                    nc = len(ind)

                    # vectorized implementation
                    if i in ind:
                        ind.remove(i)
                    rj = r[ind,:]
                    drij = pbc(rj - np.repeat(ri, len(ind), axis=0), h, hinv)

                    ind_nbrs = np.where(np.linalg.norm(drij, axis=1) < rv)[0].tolist()
                    if len(ind_nbrs) > 0:
                        nn[i] += len(ind_nbrs)
                        nindex[i].extend([ind[j] for j in ind_nbrs])

    return nn, nindex

def verletlist_old(r, h, rv):
    '''Construct Verlet List (neighbor list) in 3D
    
        Uses celllist to achieve O(N)
    
        Parameters
        ----------
        r : float, dimension (nparticles, 3)
            *real* coordinate of atoms
        h : float, dimension (3, 3)
            Periodic box size h = (c1|c2|c3)
        rv : float
            Verlet cut-off radius
            
        Returns
        -------
        nn : int, dimension (nparticle, )
            nn[i] is the number of neighbors for atom i
        nindex : list, dimension (nparticle, nn)
            nindex[i][j] is the index of j-th neighbor of atom i,
            0 <= j < nn[i].
            
    '''
    nparticle, nd = r.shape
    if nd != 3:
        raise TypeError('celllist: only support 3d cell')

    # first determine the size of the cell list
    c1 = h[:, 0]; c2 = h[:, 1]; c3 = h[:, 2];
    V = np.abs(np.linalg.det(h))
    hx = np.abs( V / np.linalg.norm(np.cross(c2, c3)))
    hy = np.abs( V / np.linalg.norm(np.cross(c3, c1)))
    hz = np.abs( V / np.linalg.norm(np.cross(c1, c2)))
    
    # Determine the number of cells in each direction
    Nx = np.floor(hx/rv).astype(np.int)
    Ny = np.floor(hy/rv).astype(np.int)
    Nz = np.floor(hz/rv).astype(np.int)
    
    if Nx < 2 or Ny < 2 or Nz < 2:
        raise ValueError("Number of cells too small! Increase simulation box size.")

    # Inverse of the h matrix
    hinv = np.linalg.inv(h);
    cell, cellid = celllist(r, h, Nx, Ny, Nz)
    
    # Find the number of atoms
    nparticles = r.shape[0]
    
    # initialize Verlet list
    nn = np.zeros(nparticles, dtype=int)
    nindex = [[] for i in range(nparticles)]
    
    for i in range(nparticles):
        # position of atom i
        ri = r[i, :].reshape(1, 3)
        
        # find which cell (ix, iy, iz) that atom i belongs to
        ix, iy, iz = (cellid[i, 0], cellid[i, 1], cellid[i, 2])
        
        # go through all neighboring cells
        ixr = ix+1
        iyr = iy+1
        izr = iz+1
        if Nx < 3:
            ixr = ix
        if Ny < 3:
            iyr = iy
        if Nz < 3:
            izr = iz
        
        for nx in range(ix-1, ixr+1):
            for ny in range(iy-1, iyr+1):
                for nz in range(iz-1, izr+1):
                    # apply periodic boundary condition on cell id nnx, nny, nnz
                    nnx, nny, nnz = (nx%Nx, ny%Ny, nz%Nz)

                    # extract atom id in this cell
                    ind = cell[nnx][nny][nnz]
                    nc = len(ind)

                    # go through all the atoms in the neighboring cells
                    for k in range(nc):
                        j = ind[k]
                        # update nn[i] and nindex[i]
                        if i == j:
                            continue
                        else:
                            rj = r[j, :].reshape(1, 3)

                            # obtain the distance between atom i and atom j
                            drij = pbc(rj - ri, h, hinv)

                            if np.linalg.norm(drij) < rv:
                                nn[i] += 1
                                nindex[i].append(j)

    return nn, nindex

################### potential functions ######################

def ljval_original(drij, sigma0, epsilon0):
    '''LJ potential for atom pair i, j
    
        Parameters
        ----------
        drij : float, dimension (npairs, 3)
            distance vectors (Angstrom)
        sigma0, epsilon0 : float
            parameters for Lennard-Jones potential

        Returns
        -------
        phi : float, dimension (npairs, )
            potential energies of each pair (eV)
        f : float, dimension (npairs, 3)
            force vector between each pair of atoms (eV/A)
            
    '''
    # Check the input
    if len(drij.shape) == 1:         # Only one pair
        drij = drij.reshape(1, -1)
    if (len(drij.shape) != 2):
        print('ljval: drij shape not correct, must be (npairs, nd), (nd = 2,3)')
        raise ValueError

    r = np.linalg.norm(drij, axis=-1, keepdims=True)
    rt = r/sigma0
    phi = 4*epsilon0*(rt**(-12)-rt**(-6))
    f = -4*epsilon0*( -12*rt**(-13) + 6*rt**(-7) )* drij /r/sigma0

    return phi, f

def ljval(drij, rc, sigma0, epsilon0):
    '''LJ potential for atom pair i, j in n-dimensions
    
        Parameters
        ----------
        drij : float, dimension (npairs, nd)
            distance vectors (Angstrom)
        rc : float
            cut-off radius (Angstrom)
        sigma0, epsilon0 : float
            parameters for Lennard-Jones potential (Angstrom, eV)

        Returns
        -------
        phi : float, dimension (npairs, )
            potential energies of each pair (eV)
        f : float, dimension (npairs, nd)
            force vector between each pair of atoms (eV/A)
            
    '''
    # Check the input
    if len(drij.shape) == 1:         # Only one pair
        drij = drij.reshape(1, -1)
    if (len(drij.shape) != 2):
        print('ljval: drij shape not correct, must be (npairs, nd), (nd = 2,3)')
        raise ValueError

    r = np.linalg.norm(drij, axis=-1, keepdims=True)
    rt = r/sigma0
    
    # Calculate constant values A and B based on rc, sigma0, epsilon0
    B = -4*epsilon0*( -12*(rc/sigma0)**(-13) + 6*(rc/sigma0)**(-7) )/sigma0
    A = -4*epsilon0*( (rc/sigma0)**(-12) - (rc/sigma0)**(-6) ) - B*rc

    # Calculate LJ potential phi
    phi = 4*epsilon0*(rt**(-12)-rt**(-6)) + A + B*r
    phi[r > rc] = 0
    f = -(4*epsilon0*( -12*rt**(-13) + 6*rt**(-7) )/sigma0 + B) * (drij/r)

    return phi, f

def ljpot(r, h, rc, sigma0, epsilon0):
    '''LJ potential for configurations, considering periodic boundary condition
    
        Parameters
        ----------
        r : float, dimension (nparticles, 3)
            *real* coordinate of atoms (Angstrom)
        h : float, dimension (3, 3)
            Periodic box size h = (c1|c2|c3)
        rc : float
            Lennard-Jones cut-off radius
        sigma0, epsilon0 : float
            parameters for Lennard-Jones potential

        Returns
        -------
        Epot : float
            total potential energy (eV)
        F : float, dimension (nparticle, 3)
            force vector on each atom (eV/A)
            
    '''
    # initialization
    nparticles, nd = r.shape
    Epot = 0
    F = np.zeros_like(r)
    hinv = np.linalg.inv(h)
    
    # interaction between atoms
    for i in range(nparticles):
        ri = r[i, :].reshape(1, nd)
        for j in range(i+1, nparticles):
            rj = r[j, :].reshape(1, nd)
            
            # obtain the distance between atom i and atom j
            drij = pbc(rj - ri, h, hinv)
            
            phi, fi = ljval(drij, rc, sigma0, epsilon0)
            Epot += phi
            F[i, :] = F[i, :] - fi
            F[j, :] = F[j, :] + fi

    return Epot, F

def ljpot_list(r, h, rc, nn, nindex, sigma0, epsilon0):
    '''LJ potential for configurations
        
        O(n) algorithm using Verlet list
    
        Parameters
        ----------
        r : float, dimension (nparticles, 3)
            *real* coordinate of atoms (Angstrom)
        h : float, dimension (3, 3)
            Periodic box size h = (c1|c2|c3)
        rc : float
            Lennard-Jones cut-off radius
        nn : int, dimension (nparticle, )
            nn[i] is the number of neighbors for atom i,
            output from verletlist().
        nindex : list, dimension (nparticle, nn)
            nindex[i][j] is the index of j-th neighbor of atom i,
            0 <= j < nn[i], output from verletlist().
        sigma0, epsilon0 : float
            parameters for Lennard-Jones potential

        Returns
        -------
        Epot : float
            total potential energy (eV)
        F : float, dimension (nparticle, 3)
            force vector on each atom (eV/A)
            
    '''
    # initialization
    nparticles, nd = r.shape
    Epot = 0
    F = np.zeros_like(r)
    hinv = np.linalg.inv(h)
    
    # interaction between atoms
    for i in range(nparticles):
        ri = r[i, :].reshape(1, nd)
        for j in nindex[i]:
            if j > i:
                rj = r[j, :].reshape(1, nd)
            
                # calculate distance vector between i and j
                drij = pbc(rj - ri, h, hinv)

                phi, fi = ljval(drij, rc, sigma0, epsilon0)
                Epot += phi
                F[i, :] = F[i, :] - fi
                F[j, :] = F[j, :] + fi

    return Epot, F