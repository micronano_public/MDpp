import sys, os, glob
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.append(os.path.join(mdpp_dir, 'bin'))
import mdpp
import numpy as np

# Obtain the file names of the chain, state A and state B
print(sys.argv)
if len(sys.argv) < 4:
    raise ValueError('Must have 3 arguments: chain, state A and B!')

scriptfile = sys.argv[0]
chainsfile = sys.argv[1]
stateAfile = sys.argv[2]
stateBfile = sys.argv[3]
chains_dir = os.path.dirname(chainsfile)
Echainfile = None
if len(sys.argv) > 4:
    chains_dir = sys.argv[4]
if len(sys.argv) > 5:
    Echainfile = sys.argv[5]

if not os.path.exists(chainsfile):
    if os.path.exists(chainsfile + '.cpu00'):
        nchain = len(glob.glob(chainsfile + '.cpu??'))
        parallel_flag = True
    else:
        raise NameError(sys.argv[1] + ' not found!')
else:
    parallel_flag = False

# Obtain the chain info from the chain file
if parallel_flag:
    with open(chainsfile + '.cpu00', 'r') as f:
        nchain_ref = int(f.readline()) + 1
        nfixed = int(f.readline())
    if nchain_ref != nchain:
        raise ValueError('Parallel file number inconsistent with core number')
    constrain_id = np.genfromtxt(chainsfile + '.cpu00', skip_header=2, max_rows=nfixed, dtype=int)
    constrain_rs = []
    for ichain in range(nchain):
        fchain = chainsfile + '.cpu%02d'%ichain
        print('loading... %s'%fchain)
        constrain_rs.append(np.genfromtxt(fchain, skip_header=2+nfixed)[..., :3])
    constrain_rs = np.stack(constrain_rs, axis = 0)
else:
    with open(chainsfile, 'r') as f:
        nchain = int(f.readline())+1
        nfixed = int(f.readline())
    constrain_id = np.genfromtxt(chainsfile, skip_header=2, max_rows=nfixed, dtype=int)
    constrain_rs = np.genfromtxt(chainsfile, skip_header=2+nfixed).reshape(nchain, nfixed, -1)[..., :3]
print('chain length: %d'%nchain)
print('# of constrain atom: %d'%nfixed)

mdpp.cmd("setnolog")
mdpp.cmd("setoverwrite")
mdpp.cmd("dirname = " + os.path.dirname(stateAfile))
mdpp.cmd("zipfiles = 1 NNM = 200")
mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'EAMDATA', 'eamdata.Ni.Rao99'))
mdpp.cmd('eamgrid = 5000 readeam')

mdpp.cmd('incnfile = %s readcn'%stateAfile)
mdpp.cmd('eval')
nparticles = mdpp.get('NP')
print('state A: %s'%stateAfile)
print('number of particles: %d'%nparticles)
hA = np.reshape(mdpp.get_array('H', 0, 3*3), (3, 3))
print('H matrix:')
print(hA)
E0 = mdpp.get('EPOT')

# Initialize the chain with linear interpolation, and construct the chain
rA = np.reshape(mdpp.get_array('R', 0, nparticles*3), (nparticles, 3))
Echain = np.zeros(nchain)
for i in range(nchain):
    rchain = rA.copy()
    rchain[constrain_id, :] = constrain_rs[i, ...]
    mdpp.set_array(rchain.flatten().tolist(), 'R', 0)
    mdpp.cmd('RHtoS clearR0 eval')
    Echain[i] = mdpp.get('EPOT')
    mdpp.cmd('finalcnfile = %s writecn'%os.path.join(chains_dir, 'chain.%02d.cn')%i)
    mdpp.cmd('finalcnfile = %s writeLAMMPS'%os.path.join(chains_dir, 'chain.%02d.lammps')%i)
if Echainfile is not None:
    print('E0 = %.17e'%E0)
    print('Echain = ', Echain)
    np.savetxt(Echainfile, Echain - E0)

import time
sleep_seconds = 10
print("Python is going to sleep for " + str(sleep_seconds) + " seconds.")
time.sleep(sleep_seconds)
