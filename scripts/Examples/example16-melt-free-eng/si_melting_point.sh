#!/bin/bash

echo "Run all steps of melting_cubic.py"

material='Si-sw'

make clean; make sworig build=R SYS=$MDPLUS_SYS PY=yes

# for speed the following jobs can run in parallel when submitting to cluster (i.e. with &)

python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  1 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  2 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  3 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  4 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  5 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  6 1 $material

python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 14 1 $material

python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  7 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  8 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py  9 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 10 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 11 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 12 1 $material
python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 13 1 $material

python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py 15 1 $material
