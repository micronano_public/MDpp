''' 
Compute melting points of pure elements in MD++

Translated from melting_cubic.tcl in http://micro.stanford.edu/wiki/Computing_Melting_Point_by_Free_Energy_Method

Compile by:
  make sworig build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python3 scripts/Examples/example16-melt-free-eng/melting_cubic.py <workstep> <jobid> <material>

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))

import numpy as np
import mdpp
import matplotlib.pyplot as plt
from   free_eng_util import free_eng_util

if len(sys.argv) < 4:
    print("Please specify <workstep> <jobid> <material> in command line")
    print(" for example 1 1 Si-sw ")
    exit()
else:
    workstep =   int(sys.argv[1])
    jobid    =   int(sys.argv[2])
    material =       sys.argv[3]

runssubdir = os.path.join(runsdir, 'Single_Elem_Tm')
if not os.path.exists(runssubdir):
    os.makedirs(runssubdir)

# initialize util object
util = free_eng_util(runssubdir, folder_prefix=material, mdpp=mdpp, NxNyNz=[-1,-1,-1], material=material)

util.wait_for_flag(workstep)

if material == 'Ta-fs':
    print("material = %s"%material)
    util.read_fs_pot('ta_pot')
    util.a                  = 3.30579978 # (3.165 in melting_cubic.tcl)
    util.L                  = 6
    util.T_solid_0          = 3600
    util.eT_solid_0         = 0.002
    util.factor_solid_RS    = 0.8
    util.T_liquid_0         = 4500
    util.eT_liquid_0        = 0.02
    util.factor_liquid_RS   = 1.25
    util.factor_gauss       = 0.1
    util.atom_mass          = 180.948
    util.crystalstructure   = 'body-centered-cubic'
    util.natom_per_unitcell = 2
elif material == 'Si-sw':
    print("material = %s"%material)
    # no need to read potential file for sw potential
    util.a                  = 5.431
    util.L                  = 4
    util.T_solid_0          = 1600
    util.eT_solid_0         = 0.002
    util.factor_solid_RS    = 0.8
    util.T_liquid_0         = 2000
    util.eT_liquid_0        = 0.02
    util.factor_liquid_RS   = 1.25
    util.factor_gauss       = 0.1
    util.atom_mass          = 28.086
    util.crystalstructure   = 'diamond-cubic'
    util.natom_per_unitcell = 8
else:
    print("Error: unknown material (%s)"%(material))
    exit()

# supercell shape and size
util.sc         = [[1, 0, 0], [ 0, 1, 0], [0, 0, 1]]
util.Nx, util.Ny, util.Nz = [util.L, util.L, util.L]

# here you may increase the simulation steps for higher accuracy (default in free_eng_util.py)
#util.numsteps_equil_vol =  2000000
#util.numsteps_md_switch = 10000000

if workstep == 1:
    util.make_perfect_crystal(savefile='perf')

if workstep in [1,2,3,4,5,6,14,15]:
    util.free_energy_calculation(workstep, jobid, util.T_solid_0, 1, initcn='perf')
elif workstep in [7,8,9,10,11,12,13]:
    util.free_energy_calculation(workstep, jobid, util.T_liquid_0, 1)
else:
    print('unknown workstep %d'%workstep)

if workstep == 15:
    if util.solid_success and not util.liquid_success:
        plt.plot(util.T_solid,  util.Fs)
        plt.show()
    elif not util.solid_success and util.liquid_success:
        plt.plot(util.T_liquid, util.Fl)
        plt.show()
    elif util.solid_success and util.liquid_success:
        plt.plot(util.T_grid, util.Fs_interp, util.T_grid, util.Fl_interp)
        plt.show()
