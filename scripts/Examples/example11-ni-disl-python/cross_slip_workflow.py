import sys, os, glob, re, filecmp, gzip, subprocess, shutil, sysconfig, fileinput
import numpy as np
from cross_slip_util import cross_slip_util

mdpp_dir = os.environ['MDPLUS_DIR']
sys.path.insert(0, os.path.join(mdpp_dir, 'scripts', 'python'))
from mdutil import loadcn

def generate_header_str(header_dict, bashstr='/usr/bin/bash', option='SBATCH', additional=[]):
    '''Generate string for submit file given the settings in dictionary format
        By default, we use /bin/sh as bash and SBATCH for set up
    '''
    option_list = ['#%s --%s=%s'%(option, key, header_dict[key]) for key in header_dict]
    return '\n'.join(['#!%s'%bashstr, ] + option_list + additional)

def EscaigSchmidt2Cartesian(seg, ssc, sec):
    '''Calculate Cauchy stress tensor uncer Cartesian coordinates from Escaig Schmidt stress'''
    syz = -seg
    if seg == 0.0: syz = +0.0
    sxz = -3.*ssc/np.sqrt(2)/2
    szz = ((-9.*sec)-7.*syz)/np.sqrt(2)/2/2
    if szz == 0.0: szz = -0.0
    if sxz == 0.0: sxz = -0.0
    syy = -szz
    return [syz, szz, syy, sxz]

def Cartesian2EscaigSchmidt(sxx, syy, szz, sxy, sxz, syz):
    '''Calculate Escaig Schmidt stress from Cauchy stress tensor under Cartesian coordinates'''
    ssg = -sxy
    seg = -syz
    ssc = -(2.*np.sqrt(2)*sxz-sxy)/3.
    sec = -(7.*syz+2*np.sqrt(2)*(szz-syy))/9.
    return [ssg, seg, ssc, sec]

def mechstr2offset(mechstr):
    if mechstr == 'FE':
        return 0
    elif mechstr == 'Fl':
        return .5
    else:
        raise ValueError('Unknown mechanism (FE or Fl)')

def getHfromcn(filename):
    '''obtain H matrix from a cn file'''
    nparticles, rawdata, h = loadcn(filename, verbose = False)
    return h

def relax_state_A_status(seg, ssc, sec, offset, csu, maxstep=400, Etol=1e-6, stol=0.5, save_iter_data='energy_stress_iter.txt'):
    t = 'TSTRESSinMPa'
    props = [['EPOT'], ['%s_xx'%t, '%s_yy'%t, '%s_zz'%t, '%s_xy'%t, '%s_yz'%t, '%s_xz'%t], 
                       ['H_11', 'H_22', 'H_33', 'H_12', 'H_23', 'H_13']]
    stresscond = EscaigSchmidt2Cartesian(seg, ssc, sec); ns = [maxstep, ] + stresscond
    relaxed_A_config = os.path.join(csu.create_dirname(status = 5 + offset, ns = ns), 'ni-screw-gp-relaxed.cn*')

    header = '%9s%9s'%('dEA', 'dsA')
    error_str = ' '*18
    relax_state = 'A'
    Aavail = len(glob.glob(relaxed_A_config)) > 0
    if Aavail: # relax A state finished
        relax_state = 'a'
        dirnameA = csu.create_dirname(status = 5 + offset, ns = ns)
        iter_energyA, iter_stressA, iter_boxsizeA = csu.get_state_relax_iter(os.path.join(dirnameA, save_iter_data),
                                                                             props=props, separate=True)
        if len(iter_energyA) < 2:
            error_energy = iter_energyA[-1]
        else:
            error_energy = iter_energyA[-1] - iter_energyA[-2]
        refstress = np.array([0, stresscond[2], stresscond[1], 0, stresscond[0], stresscond[3]])
        dstress = np.abs(iter_stressA[-1, :] - refstress)
        error_stress = iter_stressA[-1, dstress.argmax()] - refstress[dstress.argmax()]
        
        # check whether the dislocation is not stable (Fleischer mechanisms)
        Astable = True
        disl_info_file = os.path.join(dirnameA, 'disl_info.txt')
        if os.path.exists(disl_info_file):
            disl_info = np.loadtxt(disl_info_file)
            Astable = disl_info[-1, 1] > 0
            if not Astable: relax_state = 'F'

        if Astable and np.abs(error_energy) < Etol and dstress.max() < stol:
            relax_state = ''
        error_str = '%9.2e%9.4f'%(error_energy, error_stress)
    return relax_state, header, error_str

def relax_state_B_status(seg, ssc, sec, offset, csu, maxstep=400, save_iter_data='energy_stress_iter.txt'):
    t = 'TSTRESSinMPa'
    props = [['EPOT'], ['%s_xx'%t, '%s_yy'%t, '%s_zz'%t, '%s_xy'%t, '%s_yz'%t, '%s_xz'%t], 
                       ['H_11', 'H_22', 'H_33', 'H_12', 'H_23', 'H_13']]
    stresscond = EscaigSchmidt2Cartesian(seg, ssc, sec); ns = [maxstep, ] + stresscond
    iselect = 9
    relax_B_config = os.path.join(csu.create_dirname(status = 6 + offset, ns = ns), 'ni-cross-slip-relax-*.cn*')
    relaxed_B_config = os.path.join(csu.create_dirname(status = 6 + offset, ns = ns), 'ni-cross-slip-relax-%d.cn*'%iselect)

    header = '%6s'%'itB'
    relax_state = 'B'
    Bavail = len(glob.glob(relaxed_B_config)) > 0
    niter_B= len(glob.glob(relax_B_config))
    error_str = '%6s'%niter_B
    if Bavail:
        relax_state = 'b'
        dirnameA = csu.create_dirname(status = 5 + offset, ns = ns)
        iter_energyA, iter_stressA, iter_boxsizeA = csu.get_state_relax_iter(os.path.join(dirnameA, save_iter_data),
                                                                             props=props, separate=True)
        dirnameB = csu.create_dirname(status = 6 + offset, ns = ns)
        iter_energyB, iter_stressB, iter_boxsizeB = csu.get_state_relax_iter(os.path.join(dirnameB, save_iter_data), 
                                                                             props=props, separate=True)
        B_use_same_H = np.allclose(iter_boxsizeA[-1, :], iter_boxsizeB[-1, :])
        if B_use_same_H: relax_state = ''
    return relax_state, header, error_str

def NEB_calculation_status(seg, ssc, sec, offset, csu, maxstep=400, 
                           Emax_tol = 1e-5, Eres_tol = 1e-3, navg = 20, 
                           curr=False, nquantile = 4):
    stresscond = EscaigSchmidt2Cartesian(seg, ssc, sec); ns = [maxstep, ] + stresscond
    relax_NEB      = os.path.join(csu.create_dirname(status =112+ offset, ns = ns), 'stringeng.out')

    header = '%29s%6s'%(r'residual/slope/Emax_curr', 'nitEb')
    error_str = ' '*35
    relax_state = 'N'
    range_Emax = 0
    MEPcurve = csu.get_MEP_curves(status = 112 + offset, ns = ns, curr=curr)
    if MEPcurve is not None and MEPcurve.shape[0] > navg:
        relax_state = 'n'
        Emax = MEPcurve.max(axis=-1)
        niter_Emax = Emax.size
        curr_Emax = np.median(Emax[-navg//2:])
        if Emax.size > 2*maxstep:
            xEmax     = np.arange(Emax.size-maxstep, Emax.size)
        else:
            xEmax     = np.arange(Emax.size//2, Emax.size)
        sorted_id = np.argsort(Emax[xEmax.astype(int)])
        quarterid = sorted_id[(sorted_id.size//nquantile):(sorted_id.size*(nquantile-1)//nquantile)]
        range_Emax = np.ptp(Emax[xEmax.astype(int)][quarterid])
        Xfit, Yfit = xEmax[quarterid], Emax[xEmax.astype(int)][quarterid]
        p, res, rank, SV, rcond = np.polyfit(Xfit, Yfit, deg=1, full=True)
        error_Emax, intercept = p
        if Emax[-1] < 0.01 or (niter_Emax > 2*maxstep+1 and Emax[-1] > 3):
            relax_state = 'F'
        elif MEPcurve.shape[0] > (maxstep + 1) and np.abs(error_Emax) < Emax_tol and np.abs(res) < Eres_tol:
            relax_state = ''
        error_str = '%10.2e/%9.2e/%9.4f%6d'%(res/curr_Emax, error_Emax, curr_Emax, niter_Emax)
        MEPcurve = MEPcurve[-maxstep:, :]
    else:
        Emax = None
        niter_Emax = 0
        error_Emax = curr_Emax = np.nan
    return relax_state, header, error_str, Emax, MEPcurve

def relaxed_stress_conditions(stress_list, mechstr, csu = None, debug_output=True, 
                              runsdir = 'runs/ni-Rao-cs', save_iter_data='energy_stress_iter.txt', 
                              Etol=1e-7, stol=1, maxstep=400, Emax_tol = 1e-5, Eres_tol=1e-3, navg = 20, 
                              curr=False, nquantile = 4, ncount=6):
    '''Determine the progress of each stress condition
    '''
    conditions = [] # all the stress conditions
    avail_cond = [] # stress conditions already relaxed
    Emax_list  = [] # Emax during NEB calculation
    MEP_list   = []

    if csu is None:
        csu = cross_slip_util(runsdir)
    offset = mechstr2offset(mechstr)

    for i in range(stress_list.shape[0]):
        seg, ssc, sec = stress_list[i, :]
        stresscond = EscaigSchmidt2Cartesian(seg, ssc, sec)
        relax_state_A, header_A, error_str_A = relax_state_A_status(seg, ssc, sec, offset, csu, 
                                                                    maxstep=maxstep, save_iter_data=save_iter_data,
                                                                    Etol=Etol, stol=stol)
        relax_state_B, header_B, error_str_B = relax_state_B_status(seg, ssc, sec, offset, csu, 
                                                                    maxstep=maxstep, save_iter_data=save_iter_data)
        relax_state_N, header_N, error_str_N, Emax, MEPcurve = NEB_calculation_status(seg, ssc, sec, offset, csu, 
                                                                                      maxstep=maxstep, navg = navg, 
                                                                                      Emax_tol = Emax_tol, Eres_tol = Eres_tol, 
                                                                                      curr=curr, nquantile = nquantile)
        
        if   relax_state_A == 'F':
            relax_state = 'aFL'
        elif relax_state_N == 'F':
            relax_state = 'nFL'
        else:
            relax_state = relax_state_A + relax_state_B + relax_state_N

        if debug_output:
            stress_str = '%9.2f%9.2f%9.2f%6d%6d%6d'%((stresscond[0], stresscond[2], stresscond[3], seg, ssc, sec))
            error_str = error_str_A + error_str_B + error_str_N
            if i == 0:
                header = header_A + header_B + header_N
                print('Non-existing Stress Conditions:')
                print('%9s%9s%9s%6s%6s%6s%4s'%('syz', '-szz/syy', 'sxz', 'seg', 'ssc', 'sec', 'ST') + header)
            if i%ncount == 0:
                print('case %d-%d'%(i, i+ncount-1))
            print(stress_str, '%3s'%relax_state, error_str)

        conditions.append(stresscond)
        avail_cond.append(relax_state)
        Emax_list.append(Emax)
        MEP_list.append(MEPcurve)
    return conditions, avail_cond, Emax_list, MEP_list

def generate_submit_stress(submit_relax, mechstr, conditions, avail_cond, use_init=None, nfile=1, mdppcmd='python3', header_dict={}, 
                           scripts='ni_Rao_screw_cross_slip.mdpp.py', status_idx = [0, 1], 
                           lammps2jpg=None, trackdislocation=None, ovitocmd=None, csu=None, replace=True):
    header = generate_header_str(header_dict, additional=['', 'conda activate lammps', ])
    offset = mechstr2offset(mechstr)
    ncpu = 1
    if ('nodes' in header_dict) and ('tasks-per-node' in header_dict):
        ncpu = int(header_dict['nodes'])*int(header_dict['tasks-per-node'])
    A0, A, B0, B = 0+offset, 5+offset, 2+offset, 6+offset
    status_list  = [A, B]

    for ifile in range(nfile):
        ncond = np.ceil(len(conditions)/nfile).astype(int)
        cond_range = (ifile*ncond, np.minimum((ifile + 1)*ncond, len(avail_cond)).astype(int))
        fsubmit = open(submit_relax + '_%03d.sh'%ifile, 'w')
        print(header, file=fsubmit)
        print("# Relax A state under stress condition (status == %.1f depends on status == %.1f)"%(A, A0), file=fsubmit)
        print("# Relax B' state under stress condition (status == %.1f depends on status == %.1f and status == %.1f)"%(B, B0, A), file=fsubmit)
        print('#' + ' '*106 + (' '*11).join(['status %', 'yz', 'zz', 'yy', 'xz']), file=fsubmit)
        for istatus in status_idx:         # debug purpose, only create state A or state B
            status = status_list[istatus]
            nlines = 0
            for i in range(*cond_range):
                cond = '%13.4f%13.4f%13.4f%13.4f'%tuple(conditions[i])
                # Change size of the cell
                if csu is not None:
                    NxNyNz_argument = '--NxNyNz %d %d %d'%(csu.Nx, csu.Ny, csu.Nz)
                else:
                    NxNyNz_argument = ''
                # Using another stress condition as initial condition
                initcond = ''
                if (use_init is not None) and status == A:
                    if use_init[i] >= 0:
                        initcond = '--use_init 0 %13.4f%13.4f%13.4f%13.4f'%tuple(conditions[use_init[i]])
                    elif use_init[i] == -1:
                        initcond = '--use_self'
                # write the command to submit file
                if ((avail_cond[i] == 'aFL') or
                    (status == A and ('A' not in avail_cond[i]) and ('a' not in avail_cond[i])) or 
                    (status == B and ('B' not in avail_cond[i]) and ('b' not in avail_cond[i]))):
                    prefix = '# ' # finished or failed
                else:
                    prefix = ''
                    nlines += 1
                    if status == A and ('a' in avail_cond[i]):
                        initcond = '--use_self'
                print(prefix + mdppcmd, scripts, status, 0, cond, initcond, NxNyNz_argument, '&', file=fsubmit) # writing the command
                if nlines%ncpu == 0 and nlines > 0:
                    print('wait', file=fsubmit)
            # write the command for visualizing relax process
            # replacestr = '--replace' if replace else ''
            # for i in range(*cond_range):
            #     cond = '%13.4f%13.4f%13.4f%13.4f'%tuple(conditions[i])
            #     if ovitocmd and lammps2jpg and trackdislocation and csu:
            #         dirname = csu.create_dirname(status, [0, ]+list(conditions[i]))
            #         relaxed_list = glob.glob(os.path.join(dirname, '*-relaxed.LAMMPS.gz'))
            #         relax_list = glob.glob(os.path.join(dirname, '*-relax-0.LAMMPS.gz'))
            #         if len(relaxed_list) == 1:
            #             lammpsfile = relaxed_list[0]
            #             print(ovitocmd, lammps2jpg, lammpsfile, replacestr, NxNyNz_argument, '&', file=fsubmit)
            #         if len(relax_list) == 1:
            #             lammpsfile = '"'+relax_list[0].replace('relax-0', 'relax-*')+'"'
            #             print(ovitocmd, trackdislocation, lammpsfile, replacestr, NxNyNz_argument, '&', file=fsubmit)
            print('wait', file=fsubmit)

def generate_submit_NEB(submit_relax, mechstr, conditions, avail_cond, maxstep=200, nfile=1, mdppcmd='python3', header_dict={}, csu=None,
                        scripts='scripts/Examples/example11-ni-disl-python/ni_Rao_screw_cross_slip.mdpp.py', convert_chain_only=False):
    header = generate_header_str(header_dict, additional=['', 'conda activate lammps', ]) 
    offset = mechstr2offset(mechstr)
    ncpu = 1
    if ('nodes' in header_dict) and ('tasks-per-node' in header_dict):
        ncpu = int(header_dict['nodes'])*int(header_dict['tasks-per-node'])
    A, B, NEB = 5+offset, 6+offset, 112+offset

    for ifile in range(nfile):
        ncond = np.ceil(len(conditions)/nfile).astype(int)
        cond_range = (ifile*ncond, np.minimum((ifile + 1)*ncond, len(avail_cond)).astype(int))
        with open(submit_relax + '_%03d.sh'%ifile, 'w') as fsubmit:
            print(header, file=fsubmit)
            print("# NEB calculation (status == %.1f depends on status == %.1f and %.1f)"%(NEB, A, B), file=fsubmit)
            print('#' + ' '*132 + 'status MAXSTEP' + ' '*8 + (' '*11).join(['yz', 'zz', 'yy', 'xz']), file=fsubmit)
            status = NEB
            for i in range(*cond_range):
                cond = '%13.4f%13.4f%13.4f%13.4f'%tuple(conditions[i])
                # Change size of the cell
                if csu is not None:
                    NxNyNz_argument = '--NxNyNz %d %d %d'%(csu.Nx, csu.Ny, csu.Nz)
                else:
                    NxNyNz_argument = ''
                if (avail_cond[i] in ['N', 'n']) and not convert_chain_only:
                    prefix = suffix = ''
                    print('wait', file=fsubmit)
                    print(prefix + 'mpirun -n %d'%ncpu, mdppcmd, scripts, status, maxstep, cond, NxNyNz_argument, file=fsubmit)
                else:
                    prefix = '# '; suffix = '&'
                # Convert NEB chain to lammps files
                print('python3 %s'%(scripts), status + 100, maxstep, cond, NxNyNz_argument, suffix, file=fsubmit)
            print('wait', file=fsubmit)
