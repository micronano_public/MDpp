'''
Workflow for running molecular dynamics simulation for cross-slip in fcc Ni.
If using gpu-tesla, use 

    LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --use_gpu 1 --gpu_node gpu-tesla

to call the script.
'''

import os
import sys
import time
import glob
import numpy as np

from log import log
from cross_slip_util import cross_slip_util
from cross_slip_md_util import cross_slip_md_util, cross_slip_md_util_args
import cross_slip_workflow as workflow

# from ovito.io import import_file
# from ovito.pipeline import FileSource
# from ovito.modifiers import AtomicStrainModifier

default_modulus_xx = default_modulus_yy = default_modulus_zz = 450e3 # MPa
default_modulus_xz = default_modulus_xy = default_modulus_yz = 100e3 # MPa
default_modulus    = [default_modulus_xx, default_modulus_yy, default_modulus_zz,
                      default_modulus_xy, default_modulus_xz, default_modulus_yz]

# parsing arguments
ap = cross_slip_md_util_args()
print(ap)
randseed = np.random.randint(10000000)

mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin_ni_cs'))

import mdpp
csu_md = cross_slip_md_util(ap, mdpp=mdpp)


############################ Main part of the script ###########################

if csu_md.status == 7:
    # Heat up the configuration at stress sig_ref
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 7 0 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --stol 20 --niters 50 --temp_damp 0.2 --runsteps 200

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    dtemp = 10
    runsteps = ap['runsteps']
    tempinit = 1
    niters = ap['niters']
    templist = np.arange(0, 610, dtemp) + dtemp

    # Set up LAMMPS session (current directory moved to csu_md.workdir)
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    # Load the restart dump file
    restartfile = glob.glob('force_applied_T*.dump')
    nrestart = len(restartfile)
    if 'force_applied_T001.dump' in restartfile:
        templist = templist[nrestart-1:]
        if nrestart > 1:
            tempinit = (nrestart - 1)*dtemp
        else:
            tempinit = 1
        read_restart_file = 'force_applied_T%03d.dump'%(tempinit)
        csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])
    else:
        csu_md.read_initfile(verbose=ap['verbose'])

    csu_md.read_Ni_eam()
    csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all()

    if ap['test_relax']:
        # Test output: free surface, bulk selection, stress calculation
        L.minimize(0.0, 1e-7, 100000, 1000000)
        dump_file_name = 'force_applied.dump.gz'
        L.write_dump('all atom', dump_file_name)
    else:
        # Heat up configuration
        L.fix('ave_stress', 'all', 'ave/time', 1, runsteps, runsteps, 
            ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
            'file', 'stress.txt')
        if nrestart == 0:
            L.velocity('all create', tempinit, randseed, 'temp compute_temp')
            csu_md.const_stress_simulation(tempinit, runsteps, niters, dumpfile='force_applied', sig_ref=csu_md.sig_ref,
                                        tempdamp=ap['temp_damp'], stress_drag=0.1, stol=ap['stol'],
                                        verbose=ap['verbose']
                                        )

        for temp in templist:
            L.log('log.T%03dK.lammps'%temp)
            L.fix('fix_nvt', 'all nvt', 'temp', tempinit, temp, ap['temp_damp'])
            L.fix_modify('fix_nvt', 'temp', 'compute_temp')
            L.run(int(temp-tempinit)*runsteps)
            L.unfix('fix_nvt')
            tempinit = temp
            csu_md.const_stress_simulation(tempinit, runsteps, niters, dumpfile='force_applied', 
                                           sig_ref=csu_md.sig_ref, stol=ap['stol'],
                                           tempdamp=ap['temp_damp'], stress_drag=ap['stress_damp'], 
                                           verbose=ap['verbose']
                                          )
elif csu_md.status == 8:
    # Equilibrate the configuration at stress sig_ref and temp_final (adjust Lx, Lz and Xz)
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 8 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --stol 1 --niters 50 --temp_damp 0.2 --runsteps 200

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    runsteps = ap['runsteps']
    niters = ap['niters']
    if niters is None: raise ValueError('must have argument --niters!')

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    if ap['initargs'] is None:
        initdir = csu_md.create_dirname(7, [0, syz, szz, syy, sxz])
        read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%(tempinit))
    else:
        init_temp_final = csu_md.parsed_initargs['temp_final']
        init_ns = [init_temp_final, ] + csu_md.parsed_initargs['stress_cartesian']
        print(init_ns)
        initdir = csu_md.create_dirname(csu_md.parsed_initargs['status'], init_ns)
        print(initdir)
        read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%init_temp_final)
    csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])

    # Apply potential and surface force
    csu_md.read_Ni_eam()
    csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all()
    csu_md.perturb_velocity(mu=0, sigma=1e-5)

    L.fix('ave_stress', 'all', 'ave/time', 1, runsteps, runsteps, 
          ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
          'file', 'stress.txt')

    csu_md.const_stress_simulation(tempinit, runsteps, niters, dumpfile='force_applied', 
                                   sig_ref=csu_md.sig_ref, tempdamp=ap['temp_damp'], 
                                   stress_drag=ap['stress_damp'], stol=ap['stol'], verbose=ap['verbose']
                                  )
    # L.write_dump('all local', 'local_test.dump', 'c_nlocal[1]', 'c_nlocal[2]', 'c_plocal[1]', 'c_plocal[2]', 'c_plocal[3]')

elif csu_md.status == 9:
    # Equilibrate the configuration at stress sig_ref and temp_final (nvt)
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 9 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 --niters 50

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    tempdamp = ap['temp_damp']
    runsteps = ap['runsteps']
    niters = ap['niters']
    if niters is None: raise ValueError('must have argument --niters!')

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    initdir  = csu_md.create_dirname(8, csu_md.ns)
    read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%(tempinit))
    csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])

    # Modify the box size to average size
    prev_logfile = os.path.join(initdir, 'log.lammps')
    l = log(prev_logfile, verbose=False)
    Lx, Lz, Xz = l.get('Lx', 'Lz', 'Xz')
    ntotalsteps = l.nlen
    avgLx = np.mean(Lx[ntotalsteps//2:])
    avgLz = np.mean(Lz[ntotalsteps//2:])
    avgXz = np.mean(Xz[ntotalsteps//2:])
    if ap['verbose']:
        print(avgLx, avgLz, avgXz)
    L.change_box('all', 'x final', 0, avgLx, 'z final', 0, avgLz, 'xz final', avgXz, 'remap', 'units box')
    print('Change box size (Lx, Lz, Xz) to: %.8f %.8f %.8f'%(L.eval('lx'), L.eval('lz'), L.eval('xz')))

    # Apply potential and surface force
    csu_md.read_Ni_eam()
    csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all()
    csu_md.perturb_velocity(mu=0, sigma=1e-5)

    L.fix('ave_stress', 'all', 'ave/time', 1, runsteps, runsteps, 
          ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
          'file', 'stress.txt')

    L.fix('fix_nvt', 'all nvt', 'temp', tempinit, tempinit, tempdamp)
    L.fix_modify('fix_nvt', 'temp', 'compute_temp')
    for i in range(niters):
        tic = time.time()
        L.run(runsteps)
        toc = time.time()
        if ap['verbose']:
            print('iter %d: %.4fs'%(i, toc-tic))
        dump_file_name = 'inter%04d.dump.gz'%i
        L.write_dump('all atom', dump_file_name)
    L.unfix('fix_nvt')
    dump_file_name = 'force_applied_T%03d.dump'%tempinit
    L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')

elif csu_md.status == 10:
    # Heat up perfect crystal to finite temperature and constant stress
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 10 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 --niters 50
    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    runsteps = ap['runsteps']
    tempinit = csu_md.temp_final
    niters = ap['niters']
    if niters is None: raise ValueError('must have argument --niters!')

    # Set up LAMMPS session (current directory moved to csu_md.workdir)
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    # Load the restart dump file
    csu_md.read_initfile()
    csu_md.read_Ni_eam()

    # Heat up configuration
    L.compute('compute_temp all temp')
    L.velocity('all create', tempinit, randseed, 'temp compute_temp')
    L.fix('npt', 'all npt', 'temp', tempinit, tempinit, ap['temp_damp'], 
                            'x',  sxx*10, sxx*10, ap['temp_damp']*10,
                            'y',  syy*10, syy*10, ap['temp_damp']*10,
                            'z',  szz*10, szz*10, ap['temp_damp']*10,
                            'xy', sxy*10, sxy*10, ap['temp_damp']*10,
                            'yz', syz*10, syz*10, ap['temp_damp']*10,
                            'xz', sxz*10, sxz*10, ap['temp_damp']*10)
    L.thermo(10)
    L.thermo_style('custom step temp press etotal',
                   'pxx pyy pzz pxy pyz pxz', 'lx ly lz xy yz xz')
    L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.17g %.17g %.17g %.17g %.17g %.17g"', 'temp', 'compute_temp')
    for i in range(ap['niters']):
        tic = time.time()
        L.run(ap['runsteps'])
        toc = time.time()
        print('iter %d: %.4fs'%(i, toc-tic))
    dump_file_name = 'force_applied_T%03d.dump'%tempinit
    L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')

elif csu_md.status == 11:
    # Equilibrate finite temperature and constant stress
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 11 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 --niters 50
    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    runsteps = ap['runsteps']
    tempinit = csu_md.temp_final
    niters = ap['niters']
    if niters is None: raise ValueError('must have argument --niters!')

    # Set up LAMMPS session (current directory moved to csu_md.workdir)
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    # Load the restart dump file
    initdir = csu_md.create_dirname(10, csu_md.ns)
    dumpfile = os.path.join(initdir, 'force_applied_T%03d.dump'%tempinit)
    csu_md.read_initfile(read_restart_file=dumpfile, verbose=ap['verbose'])
    csu_md.read_Ni_eam()

    # Modify the box size to average size
    prev_logfile = os.path.join(initdir, 'log.lammps')
    l = log(prev_logfile, verbose=False)
    Lx, Ly, Lz, Xy, Yz, Xz = l.get('Lx', 'Ly', 'Lz', 'Xy', 'Yz', 'Xz')
    ntotalsteps = l.nlen
    avgLx = np.mean(Lx[ntotalsteps//2:])
    avgLy = np.mean(Ly[ntotalsteps//2:])
    avgLz = np.mean(Lz[ntotalsteps//2:])
    avgXy = np.mean(Xy[ntotalsteps//2:])
    avgYz = np.mean(Yz[ntotalsteps//2:])
    avgXz = np.mean(Xz[ntotalsteps//2:])
    if ap['verbose']:
        print(avgLx, avgLy, avgLz, avgXy, avgYz, avgXz)
    L.change_box('all', 'x final', 0, avgLx, 'y final', 0, avgLy, 'z final', 0, avgLz,
                        'xy final', avgXy, 'yz final', avgYz, 'xz final', avgXz, 
                 'remap', 'units box')
    print('Change box size (Lx, Ly, Lz, Xy, Yz, Xz) to: %.8f %.8f %.8f %.8f %.8f %.8f'%(L.eval('lx'), L.eval('ly'), L.eval('lz'), L.eval('xy'), L.eval('yz'), L.eval('xz')))

    # Heat up configuration
    L.compute('compute_temp all temp')
    L.velocity('all create', tempinit, randseed, 'temp compute_temp')
    L.fix('nvt', 'all nvt', 'temp', tempinit, tempinit, ap['temp_damp'])
    L.thermo(10)
    L.thermo_style('custom step temp press etotal',
                   'pxx pyy pzz pxy pyz pxz', 'lx ly lz xy yz xz')
    L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.17g %.17g %.17g %.17g %.17g %.17g"', 'temp', 'compute_temp')
    for i in range(ap['niters']):
        tic = time.time()
        L.run(ap['runsteps'])
        toc = time.time()
        print('iter %d: %.4fs'%(i, toc-tic))
    dump_file_name = 'force_applied_T%03d.dump.gz'%tempinit
    L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')
    L.velocity('all set 0.0 0.0 0.0') # Zero out the velocities to avoid kinetic energy
    print('Relax the current box to 0K')
    L.minimize(0.0, 1e-7, 100000, 1000000)
    dump_file_name = 'force_applied.relaxed.dump.gz'
    L.write_dump('all atom', dump_file_name)

elif csu_md.status == 12:
    # Find the corresponding shear stress condition at 0K for perfect crystal tau(eps(sigma,T),0)
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 12 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    # Load the initial configuration (0K, zero stress)
    csu_md.read_initfile()
    csu_md.read_Ni_eam()
    Lx0, Ly0, Lz0 = L.eval('lx'), L.eval('ly'), L.eval('lz')
    H0 = np.array([[Lx0, 0, 0], [0, Ly0, 0], [0, 0, Lz0]])

    # Thermo settings
    L.thermo(100000)
    L.thermo_style('custom step temp press etotal',
                   'pxx pyy pzz pxy pyz pxz', 'lx ly lz xy yz xz')
    L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.17g %.17g %.17g %.17g %.17g %.17g"')

    if tempinit > 0:
        # Load the final configuration from step 11 (TK, tau)
        initdir  = csu_md.create_dirname(11, csu_md.ns)
        prev_logfile = os.path.join(initdir, 'log.lammps')
        l = log(prev_logfile, verbose=False)
        Lx, Ly, Lz, Xy, Yz, Xz = l.get('Lx', 'Ly', 'Lz', 'Xy', 'Yz', 'Xz')
        Press0 = l.get('Press')[-1]
        H1 = np.array([[Lx[-1], Xy[-1], Xz[-1]], [0, Ly[-1], Yz[-1]], [0, 0, Lz[-1]]])
        # Find the strain tensor
        F = H1.dot(np.linalg.inv(H0)) - np.identity(3)
        Fhat = np.trace(F)/3
        Bhat = Press0/Fhat # unit: bar
        
        # Recursively change the hydrostatic strain to get a 0 pressure configuration
        L.log('log.iter')
        Press = Press0 * 1.0
        Fs = F.copy()
        for i_iter in range(100):
            Fhat_curr = Press/Bhat
            print(Fhat_curr)
            if np.abs(Fhat_curr) < 1e-11: break
            Fs = Fs - Fhat_curr*np.identity(3)
            Hs = (Fs + np.identity(3)).dot(H0)
            Lxs, Lys, Lzs, Xys, Yzs, Xzs = tuple(Hs[[0,1,2,0,1,0], [0,1,2,1,2,2]])
            print(Hs)

            L.change_box('all', 'x final', 0, Lxs, 'y final', 0, Lys, 'z final', 0, Lzs,
                                'xy final', Xys,'yz final', Yzs,'xz final', Xzs,
                        'remap', 'units box')

            L.minimize(0.0, 1e-7, 100000, 1000000)
            Press = L.eval('press')
            print('iter %d: p = %.4f MPa'%(i_iter, Press/10))
            print(('  Stresses: ' + '%.4f '*6 + 'MPa')%tuple(np.array([L.eval('pxx'), L.eval('pyy'),
                                                                       L.eval('pzz'), L.eval('pxy'),
                                                                       L.eval('pyz'), L.eval('pxz')])/10))
    else: # (0K, tau), starting from perfect crystal (0K, tau=0)
        # Recursively change the shear strain to get (0K, tau) configuration
        L.log('log.iter')
        L.velocity('all set 0.0 0.0 0.0') # Zero out the velocities to avoid kinetic energy
        sig_ref = np.array([sxx, syy, szz, sxy, syz, sxz])
        sig_curr = np.zeros(6)
        Lxs, Lys, Lzs, Xys, Yzs, Xzs = (Lx0, Ly0, Lz0, 0, 0, 0)
        stress_drag = 1.0
        for i_iter in range(100):
            Hcurr = np.array([Lxs, Lys, Lzs, Xys, Yzs, Xzs])
            Hbase = np.array([Lxs, Lys, Lzs, Lys, Lzs, Lzs])
            dsig = sig_curr - sig_ref
            eps = dsig/default_modulus*stress_drag
            if np.abs(eps).max() < 1e-10: break
            Hnext = Hcurr + Hbase * eps
            Lxs, Lys, Lzs, Xys, Yzs, Xzs = tuple(Hnext)

            L.change_box('all', 'x final', 0, Lxs, 'y final', 0, Lys, 'z final', 0, Lzs,
                                'xy final', Xys, 'yz final', Yzs, 'xz final', Xzs,
                        'remap', 'units box')

            L.minimize(0.0, 1e-7, 100000, 1000000)
            Press = L.eval('press')/10
            sig_curr = np.array([L.eval('pxx'), L.eval('pyy'),
                                 L.eval('pzz'), L.eval('pxy'),
                                 L.eval('pyz'), L.eval('pxz')])/10
            print('iter %d: p = %.4f MPa'%(i_iter, Press))
            print(('  Stresses: ' + '%.4f '*6 + 'MPa')%tuple(sig_curr))
            # print(('  Stresses: ' + '%.4f '*6 + 'MPa')%tuple(sig_curr))
            print(('  Strain: ' + '%.4f '*6)%tuple(eps))
            print(('  Hcurr: ' + '%.4f '*6)%tuple(Hcurr))
            print(('  Hnext: ' + '%.4f '*6)%tuple(Hnext))
    L.log('log.lammps')
    L.minimize(0.0, 1e-7, 100000, 1000000)
    dump_file_name = 'strain_applied.relaxed.dump.gz'
    L.write_dump('all atom', dump_file_name)

elif csu_md.status == 22:
    # Calculate elastic constant using stress fluctuation method
    from itertools import product

    nsteps = ap['runsteps']

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    strainincr = ap['strainincr']
    savepropfreq=ap['dump_freq']

    # Elastic constant file only contains the Born term
    # Finite temperature results are calculated in the Tutorial
    lattice_file_name = 'lattice_fluctuation.txt'
    elastic_file_name = 'elastic_fluctuation.txt'
    lattice_data = []
    elastic_data = []

    # read init file from status 11
    mdpp.cmd('dirname = %s'%csu_md.workdir)
    csu_md.csu.readeam_ni_Rao()
    # perffile = os.path.join(csu_md.csu.create_dirname(0), 'perf.cn')
    if tempinit > 0:
        initfile = os.path.join(csu_md.create_dirname(11, csu_md.ns), 'force_applied.relaxed.dump.gz')
    else:
        initfile = os.path.join(csu_md.create_dirname(12, csu_md.ns), 'strain_applied.relaxed.dump.gz')
    csu_md.csu.load_file(initfile)
    print('loaded %s'%initfile)
    csu_md.csu.write_file('init_file')

    NP, V0, EPOT0, a0, Ecoh0 = csu_md.csu.get_nve()
    print(NP, V0, EPOT0, a0, Ecoh0)

    # Compute elastic constants using stress fluctuation method
    # Calculate DVIRIAL (Born elastic constants)
    mdpp.cmd(' Dexx = %.18g eval'%strainincr)
    energy_unit_conversion = 0.62415091259e-2 # eV/A^3 / GPa
    sig_tensor = mdpp.get_array('VIRIAL', 0, 9)
    sig_vector = [sig_tensor[0], sig_tensor[4], sig_tensor[8], sig_tensor[1], sig_tensor[5], sig_tensor[2], ]
    CB0 = []
    for ip, jp, kp, lp in product(range(3), range(3), range(3), range(3)):
        CB0.append(mdpp.get('DVIRIAL_%d%d%d%d'%(ip+1, jp+1, kp+1, lp+1)))

    if tempinit > 0:
        csu_md.csu.run_setup(savepropfreq=savepropfreq)
        mdpp.cmd('fixbox=1')
        
        # Setup MD property output format
        virial_output = 'VIRIAL_xx VIRIAL_yy VIRIAL_zz VIRIAL_xy VIRIAL_yz VIRIAL_xz'
        Dvirial_output_list = ['DVIRIAL_%d%d%d%d'%(ip+1,jp+1,kp+1,lp+1) for ip,jp,kp,lp in product(range(3), range(3), range(3), range(3))]
        Dvirial_output = ' '.join(Dvirial_output_list)
        output_format_str = 'curstep EPOT Tinst ' + virial_output + ' ' + Dvirial_output + ' ' + 'OMEGA'
        mdpp.cmd(' output_fmt="%s" writeall=1 '%output_format_str)

        # Setup NVT ensemble
        mdpp.cmd(' NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NVTC" integrator_type = "VVerlet" ')
        mdpp.cmd(' T_OBJ = %g initvelocity totalsteps = %d '%(tempinit, nsteps))

        mdpp.cmd(' run closepropfile ') # Run MD simulation
    else:
        with open('prop.out', 'w') as f:
            print(('%26d %26.17e %26.17e'%(0, EPOT0, tempinit)) +
                  (' %26.17e'*88)%tuple(sig_vector+CB0+[V0, ]) , file=f)
        csu_md.csu.write_file('inter0001')

elif csu_md.status == 13:
    # Equilibrate the configuration at stress sig_ref and temp_final (nvt)
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 13 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 (--niters 50) --nsample 0

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    tempdamp = ap['temp_damp']
    runsteps = ap['runsteps']
    niters = ap['niters']
    nsample = ap['nsample']

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    initdir  = csu_md.create_dirname(8, csu_md.ns)
    read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%(tempinit))
    csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])

    # Modify the box size to average size
    prev_logfile = os.path.join(initdir, 'log.lammps')
    l = log(prev_logfile, verbose=False)
    Lx, Lz, Xz = l.get('Lx', 'Lz', 'Xz')
    ntotalsteps = l.nlen
    avgLx = np.mean(Lx[ntotalsteps//2:])
    avgLz = np.mean(Lz[ntotalsteps//2:])
    avgXz = np.mean(Xz[ntotalsteps//2:])
    if ap['verbose']:
        print(avgLx, avgLz, avgXz)
    L.change_box('all', 'x final', 0, avgLx, 'z final', 0, avgLz, 'xz final', avgXz, 'remap', 'units box')
    print('Change box size (Lx, Lz, Xz) to: %.8f %.8f %.8f'%(L.eval('lx'), L.eval('lz'), L.eval('xz')))

    # Apply potential and surface force
    csu_md.read_Ni_eam()
    csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all()
    csu_md.perturb_velocity(mu=0, sigma=1e-4, verbose=ap['verbose'])

    # NVT simulation of cross-slip
    L.fix('fix_nvt', 'all nvt', 'temp', tempinit, tempinit, tempdamp)
    L.fix_modify('fix_nvt', 'temp', 'compute_temp')
    i = 0
    while niters is None or niters > i:
        # if i == 1: csu_md.perturb_velocity(mu=0, sigma=1e-4, verbose=ap['verbose'])
        tic = time.time()
        L.run(runsteps)
        toc = time.time()
        dislocation_size = L.eval('c_CS')
        if ap['verbose']:
            print('iter %d: %.4fs; time: %.2fps; dislocation size: %d'%(i, toc-tic, (i+1)*runsteps*ap['timestep'],dislocation_size))
        if dislocation_size > 500 or dislocation_size == 0: break
        dump_file_name = 'temp.%d.dump'%(i%100)
        L.write_dump('all atom', dump_file_name)
        i += 1
    L.unfix('fix_nvt')
    dump_file_name = 'force_applied_T%03d.dump'%tempinit
    L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')

elif csu_md.status == 23:
    # Equilibrate the configuration at stress sig_ref (nve)
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 23 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 (--niters 50) --nsample 0

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    tempdamp = ap['temp_damp']
    runsteps = ap['runsteps']
    niters = ap['niters']
    nsample = ap['nsample']

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    initdir  = csu_md.create_dirname(8, csu_md.ns)
    read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%(tempinit))
    csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])

    # Modify the box size to average size
    prev_logfile = os.path.join(initdir, 'log.lammps')
    l = log(prev_logfile, verbose=False)
    Lx, Lz, Xz = l.get('Lx', 'Lz', 'Xz')
    ntotalsteps = l.nlen
    avgLx = np.mean(Lx[ntotalsteps//2:])
    avgLz = np.mean(Lz[ntotalsteps//2:])
    avgXz = np.mean(Xz[ntotalsteps//2:])
    if ap['verbose']:
        print(avgLx, avgLz, avgXz)
    L.change_box('all', 'x final', 0, avgLx, 'z final', 0, avgLz, 'xz final', avgXz, 'remap', 'units box')
    print('Change box size (Lx, Lz, Xz) to: %.8f %.8f %.8f'%(L.eval('lx'), L.eval('lz'), L.eval('xz')))

    # Apply potential and surface force
    csu_md.read_Ni_eam()
    csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all()
    csu_md.perturb_velocity(mu=0, sigma=1e-4, verbose=ap['verbose'])

    # NVE simulation of cross-slip
    # L.fix('fix_nvt', 'all nvt', 'temp', tempinit, tempinit, tempdamp)
    L.fix('fix_nvt', 'all nve')
    # L.fix_modify('fix_nvt', 'temp', 'compute_temp')
    i = 0
    while niters is None or niters > i:
        # if i == 1: csu_md.perturb_velocity(mu=0, sigma=1e-4, verbose=ap['verbose'])
        tic = time.time()
        L.run(runsteps)
        toc = time.time()
        dislocation_size = L.eval('c_CS')
        if ap['verbose']:
            print('iter %d: %.4fs; time: %.2fps; dislocation size: %d'%(i, toc-tic, (i+1)*runsteps*ap['timestep'],dislocation_size))
        if dislocation_size > 500 or dislocation_size == 0: break
        dump_file_name = 'temp.%d.dump'%(i%100)
        L.write_dump('all atom', dump_file_name)
        i += 1
    L.unfix('fix_nvt')
    dump_file_name = 'force_applied_T%03d.dump'%tempinit
    L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')

elif csu_md.status == -1:
    # for debug and development
    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final

elif csu_md.status == 14:
    # Relax the configuration with the same strain
    # Command to call:
    # LD_LIBRARY_PATH=$LAMMPS_DIR_2/src:$LD_LIBRARY_PATH python3 ni_screw_cross_slip_MD.lammps.py --runs_dir ni-Rao-cs-20x20x10 15 100 0.0 -0.0 0.0 -0.0 --NxNyNz 20 20 10 --use_gpu 1 --gpu_node gpu-tesla --verbose --temp_damp 0.2 --runsteps 200 (--niters 50)

    from ovito.io import import_file, export_file
    from ovito.data import NearestNeighborFinder

    sxx, syy, szz, sxy, sxz, syz = csu_md.sig_ref # in MPa
    tempinit = csu_md.temp_final
    tempdamp = ap['temp_damp']
    runsteps = ap['runsteps']
    niters = ap['niters']
    nsample = ap['nsample']

    # Set up LAMMPS session
    csu_md.init_lammps(logfile='log.lammps')
    L, lmp = csu_md.LAMMPS

    initdir  = csu_md.create_dirname(8, csu_md.ns)
    read_restart_file = os.path.join(initdir, 'force_applied_T%03d.dump'%(tempinit))
    csu_md.read_initfile(read_restart_file=read_restart_file, verbose=ap['verbose'])

    # Modify the box size to average size
    prev_logfile = os.path.join(initdir, 'log.lammps')
    l = log(prev_logfile, verbose=False)
    Lx, Lz, Xz = l.get('Lx', 'Lz', 'Xz')
    ntotalsteps = l.nlen
    avgLx = np.mean(Lx[ntotalsteps//2:])
    avgLz = np.mean(Lz[ntotalsteps//2:])
    avgXz = np.mean(Xz[ntotalsteps//2:])
    if ap['verbose']:
        print(avgLx, avgLz, avgXz)
    L.change_box('all', 'x final', 0, avgLx, 'z final', 0, avgLz, 'xz final', avgXz, 'remap', 'units box')
    print('Change box size (Lx, Lz, Xz) to: %.8f %.8f %.8f'%(L.eval('lx'), L.eval('lz'), L.eval('xz')))

    # Apply potential and surface force
    csu_md.read_Ni_eam()
    # csu_md.apply_force_on_surface(sigma_y=[sxy, syy, syz], define_surface=True, verbose=ap['verbose'])
    csu_md.apply_force_on_surface(sigma_y=[0, 0, 0], define_surface=True, verbose=ap['verbose'])
    csu_md.compute_all(surface_force=True)
    L.group('disl', 'variable', 'is_disl')
    L.compute('y_disl', 'disl', 'reduce ave y')
    L.compute('z_disl', 'disl', 'reduce ave z')
    L.thermo_style('custom step temp v_press etotal', 
                   ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
                   'c_upper_fx c_upper_fy c_upper_fz',
                   'c_lower_fx c_lower_fy c_lower_fz',
                   'c_y_disl c_z_disl',
                   'c_CS lx lz xz v_vacuumratio')
    L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.4e %.4e %.4e %.4e %.4e %.4e %.2e %.2e %.0f %.17g %.17g %.17g %.4f"', 'temp', 'compute_temp')
    L.run(0)

    # Relax structure at the same strain
    print('stress:' + (' %.4f'*6)%tuple([L.eval('v_stress_bulk_inMPa[%d]'%(k+1)) for k in range(6)]))
    energy_unit_conversion = 0.62415091259e-5 # eV/A^3 / MPa
    Axz = L.eval('lx')*L.eval('lz')*energy_unit_conversion
    print(L.eval('c_upper_fx')/Axz, L.eval('c_lower_fx')/Axz)
    print(L.eval('c_upper_fy')/Axz, L.eval('c_lower_fy')/Axz)
    print(L.eval('c_upper_fz')/Axz, L.eval('c_lower_fz')/Axz)
    sigma_y = [ 0,
               -(L.eval('c_upper_fy')-L.eval('c_lower_fy'))/2/Axz, 
                (L.eval('c_upper_fz')-L.eval('c_lower_fz'))/2/Axz]
    sig_ref = [sxx, sigma_y[1], szz, sigma_y[0], sxz, sigma_y[2]]
    print(sigma_y)
    L.fix('fixtop', 'topsurface', 'setforce', '0.0 0.0 0.0')
    L.fix('fixbot', 'botsurface', 'setforce', '0.0 0.0 0.0')
    L.minimize(0.0, 1e-7, 100000, 1000000)
    L.unfix('fixtop')
    L.unfix('fixbot')
    L.run(0)
    changeH = csu_md.changeH_to_relax_stress(sig_ref=sig_ref, change=[], verbose=ap['verbose'])
    sigma_y = [ 0,
                changeH['sig'][1],
                changeH['sig'][5]]
    sig_ref = [sxx, sigma_y[1], szz, sigma_y[0], sxz, sigma_y[2]]
    print(sigma_y)
    csu_md.apply_force_on_surface(sigma_y=sigma_y, define_surface=False, verbose=ap['verbose'])
    for i in range(1):
        L.minimize(0.0, 1e-7, 100000, 1000000)
        changeH = csu_md.changeH_to_relax_stress(sig_ref=sig_ref, change=[1, 3, 5], verbose=ap['verbose'])
        # changeH = csu_md.changeH_to_relax_stress(sig_ref=sig_ref, change=[], verbose=ap['verbose'])
        sig_ref = [sxx, changeH['sig'][1], szz, 0, sxz, changeH['sig'][5]]
        csu_md.apply_force_on_surface(sigma_y=sigma_y, define_surface=False, verbose=ap['verbose'])
        # print('stress:' + (' %.4f'*6)%tuple([-L.eval('v_stress_bulk_inMPa[%d]'%(k+1)) for k in range(6)]))
        if np.abs(changeH['dsig'][[1, 3, 5]]).max() < 0.05: break
        print(L.eval('c_y_disl'), L.eval('ly'), L.eval('c_z_disl'), L.eval('lz'))
        # L.displace_atoms('all move', '0 %.17f %.17f'%(L.eval('ly')/2 - L.eval('c_y_disl'), 
        #                                               L.eval('lz')/2 - L.eval('c_z_disl'))
        #                 )
        dump_file_name = 'force_applied_shifted.dump.gz'
        L.write_dump('all atom', dump_file_name)
    Dz = L.eval('lz')/2 - L.eval('c_z_disl')
    pipeline = import_file(dump_file_name)
    data = pipeline.compute()
    NP = data.particles.count
    R0 = data.particles.positions[...]
    cell = data.cell[...]
    dz = cell[2, 2]/ap['NxNyNz'][2]/2
    Dz = np.round(Dz/dz)*dz
    print('shift v = [%.4f, 0, %.4f]'%(0, Dz))
    R1 = R0 + np.array([0, 0, Dz])
    shift_ind = np.zeros(NP, dtype=int)
    finder = NearestNeighborFinder(1, data)
    for i in range(NP):
        coords = tuple(R1[i, :])
        for neigh in finder.find_at(coords):
            shift_ind[neigh.index] = i
    data.particles_.positions_[...] = R1[shift_ind, :]
    export_file(data, 'force_applied.dump.gz', 'lammps/dump', 
                columns=["Particle Identifier", "Particle Type", "Position.X", "Position.Y", "Position.Z"])

    with open(os.path.join(csu_md.runs_dir, 'submits', 'submit.NEB.T400.useinit.sh'), 'w') as f:
        print('''#!/bin/sh
#SBATCH --job-name=NEBfT
#SBATCH --partition=cpu
#SBATCH --nodes=1
#SBATCH --tasks-per-node=32
''', file=f)
        print('python3 %s'%os.path.join(csu_md.curr_dir, 'ni_Rao_screw_cross_slip.mdpp.py'),
            #   '5 0', '%.4f %.4f %.4f %.4f %.4f %.4f'%(sigma_y[2], changeH['sig'][2], sigma_y[1],
            #                                           changeH['sig'][4], changeH['sig'][0], sigma_y[0]),
            '5 0', '%.4f %.4f %.4f %.4f %.4f %.4f'%(changeH['sig'][5], changeH['sig'][2],
                                                    changeH['sig'][1], changeH['sig'][4],
                                                    changeH['sig'][0], 0.0),
            '--runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10',
            '--use_init', '%s'%os.path.join(csu_md.workdir, 'force_applied.dump.gz'), file=f
            )
        print('python3 %s'%os.path.join(csu_md.curr_dir, 'ni_Rao_screw_cross_slip.mdpp.py'),
            '6 0', '%.4f %.4f %.4f %.4f %.4f %.4f'%(changeH['sig'][5], changeH['sig'][2],
                                                    changeH['sig'][1], changeH['sig'][4],
                                                    changeH['sig'][0], 0.0),
            '--runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10', file=f
            )
        for i in range(5):
            print('mpirun -n 32', 'python3 %s'%os.path.join(csu_md.curr_dir, 'ni_Rao_screw_cross_slip.mdpp.py'),
                '112 400', '%.4f %.4f %.4f %.4f %.4f %.4f'%(changeH['sig'][5], changeH['sig'][2],
                                                            changeH['sig'][1], changeH['sig'][4],
                                                            changeH['sig'][0], 0.0),
                '--runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10', file=f
                )
        print('mpirun -n 32', 'python3 %s'%os.path.join(csu_md.curr_dir, 'ni_Rao_screw_cross_slip.mdpp.py'),
            '112 400', '%.4f %.4f %.4f %.4f %.4f %.4f'%(changeH['sig'][5], changeH['sig'][2],
                                                        changeH['sig'][1], changeH['sig'][4],
                                                        changeH['sig'][0], 0.0),
            '--runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10', '--nebrelax 400', file=f
            )
        print('python3 %s'%os.path.join(csu_md.curr_dir, 'ni_Rao_screw_cross_slip.mdpp.py'),
            '212 400', '%.4f %.4f %.4f %.4f %.4f %.4f'%(changeH['sig'][5], changeH['sig'][2],
                                                        changeH['sig'][1], changeH['sig'][4],
                                                        changeH['sig'][0], 0.0),
            '--runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10 --replace', file=f
            )

else:
    raise ValueError('Unrecognized status %.1f!'%csu_md.status)
