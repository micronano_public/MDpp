'''Cross-slip mechanism in fcc Nickel under stress

This script includes the following parts:

* Create dislocation structure A before cross-slip
* Create dislocation structure B after cross-slip
* Create dislocation structure B' for different cross-slip mechanisms
* Relax dislocation structures under stress
* Calculate minimum energy path and energy barrier using parallel string-relax method

Use the following commands to compile the python library (on mc2):

```
echo MDPLUS_DIR=~/Codes/MD++.git
echo MDPLUS_SYS=mc2_mpiicc
cd $MDPLUS_DIR
mkdir bin_ni_cs
make clean
make eam build=R SYS=$MDPLUS_SYS SPEC+=-DMAXCONSTRAINATOMS=800001 PY=yes
cp bin/* bin_ni_cs
```

'''

import sys, os, shutil, glob, argparse, time
import numpy as np
from cross_slip_util import cross_slip_util, list_to_str

mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin_ni_cs'))

import mdpp

#********************************************
# Set up parameters
#********************************************

default_cross_slip_angle = np.arccos(1./3.)
# t = 'TSTRESSinMPa'      # true stress calculated by MD++
t = 's_sub'               # use subsurface stress (removed surface effect)

# Input arguments
runs_dir = os.path.join(mdpp_dir, 'runs', 'ni-Rao-cs-paper')
parser = argparse.ArgumentParser()
# Basic arguments
parser.add_argument('args',       type=float, nargs='+', default=[0, ],
                    help='status, [maxstep, syz, szz, syy, sxz]')
parser.add_argument('--NxNyNz',   type=int,   nargs=3,   default=[30, 30, 20],
                    help='(Nx, Ny, Nz) for the initial configuration')
parser.add_argument('--runs_dir', type=str,   default='ni-Rao-cs',
                    help='directory of the running process')
parser.add_argument('--setnolog', action='store_true',
                    help='If enabled, not saving log file to A.log')
parser.add_argument('--obtuse',   action='store_true',
                    help='Cross-slip on another direction with an obtuse angle')
# For status 112: running parallel nebrelax with/without climbimage options, and manual cut options
parser.add_argument('--use_init', type=str,
                    help='Initialize NEB chain from another stress condition')
parser.add_argument('--use_self', action='store_true',
                    help='Continue NEB calculation')
parser.add_argument('--saveforce',   action='store_true',
                    help='Only save the configurations to LAMMPS and FORCE format')
parser.add_argument('--nebrelax',   type=int,
                    help='Number of steps to run climbimage nebrelax')
parser.add_argument('--manualcut',  type=int,   nargs=2,
                    help='Manually trim the left and right ends of the stringrelax chain')
parser.add_argument('--energythreshold', type=float, nargs=2, default=[0.5, 0.02],
                    help='Parameter energythreshold for stringrelax_parallel')
# For status 114: running dimer method to get the exact saddle state
parser.add_argument('--dimer_alpha', type=float, default=0.01,
                    help='Magnitude coefficient for the steepest descent method')
parser.add_argument('--dimer_Fmag_th', type=float, default=1e-7,
                    help='Threshold of the force magnitude for the saddle state')
parser.add_argument('--dimer_maxiter', type=int, default=200,
                    help='Max iterations of the dimer algorithm')
parser.add_argument('--dimer_shrink_factor', type=float, default=0.9,
                    help='Factor for shrinking the dimer length')
parser.add_argument('--dimer_maxiter_shrink', type=int, default=100,
                    help='Max iterations of dimer shrinking')
parser.add_argument('--dimer_lr_shrink_factor', type=float, default=1,
                    help='Factor for shrinking the learning rate')
# For status 212: convert stringrelax chain files to standard config files (cn, LAMMPS, FORCE)
parser.add_argument('--replace',  action='store_true',
                    help='Overwrite the existing file')
parser.add_argument('--steps',    type=int, nargs='+', default=[0, ],
                    help='Convert selected saved neb steps, by default the current')
# parser.add_argument('--get_subsurface_stress', action='store_true')
# For status 213: generating Hessian matrix
parser.add_argument('--atom_range', type=int, nargs='+', default=[-1, ],
                    help='Only generate submatrix of atom id n1 - n2, by default all atoms')
parser.add_argument('--timestep' ,type=float, default=0.0001, 
                    help='Incremental dx for moving an atom in the system')
# For status 214: linear interpolation of the Goldstone mode
parser.add_argument('--linspace', type=int, default=1000)
ap = parser.parse_args()

status = ap.args[0]; ns = ap.args[1:]
if ap.use_self:
    ap.use_init = ns
runs_dir = os.path.join(mdpp_dir, 'runs', ap.runs_dir)
if ap.obtuse:
    default_cross_slip_angle += np.pi

#********************************************
# Main program starts here
#********************************************

if status == 0 or status == 0.5:
# prepare and relax dislocation structure on glide plane (state A under zero stress)
    csu = cross_slip_util(runs_dir, status, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    csu.make_perfect_crystal(savefile='perf', translation_symmetry_map='x')
    csu.make_screw_dipole(store = True) # create a screw dislocation at the center with full burger's vector
    Fleischer = (status == 0.5)         # Select mechanism
    csu.make_glide_partials(store = False, Fleischer = Fleischer) # dissociate screw dislocation in glide plane
    csu.write_file(filename='ni-screw-gp-init', format=['LAMMPS'])
    csu.cgrelax_stress(cg_niter = 100, cg_ftol=1e-4, cg_fevalmax=100, 
                       fname = 'ni-screw-gp', save_iter_data='energy_stress_iter.txt') # Relax configuration

elif status == 1 or status == 1.5: # (not used)
    # prepare and relax dislocation structure on cross-slip plane (state B under zero stress)
    csu = cross_slip_util(runs_dir, status, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    csu.make_perfect_crystal()
    csu.make_screw_dipole(store = True) # create a screw dislocation at the center with full burger's vector
    Fleischer = (status == 1.5)         # Select mechanism
    csu.make_glide_partials(store = False, Fleischer = Fleischer, theta = default_cross_slip_angle) # dissociate screw dislocation in cross-slip plane
    csu.write_file(filename='ni-screw-cp-init', format=['LAMMPS'])
    csu.cgrelax_stress(cg_niter = 100, cg_ftol=1e-4, cg_fevalmax=20,
                       fname = 'ni-screw-cp', save_iter_data='energy_stress_iter.txt') # Relax configuration

elif status == 2 or status == 2.5:
    # prepare and relax intermediate structure (state B' under zero stress)
    csu = cross_slip_util(runs_dir, status, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    csu.make_perfect_crystal()
    csu.make_screw_dipole(store = True) # create a screw dislocation at the center with full burger's vector
    Fleischer = (status == 2.5)         # Select mechanism
    csu.make_cross_slip(store = False, Fleischer = Fleischer, theta = default_cross_slip_angle) # dissociate screw dislocation in cross-slip plane
    csu.write_file(filename='ni-cross-slip-init', format=['LAMMPS'])
    csu.cgrelax_stress(cg_niter = 100, cg_ftol=1e-4, cg_fevalmax=20,
                       fname = 'ni-cross-slip', save_iter_data='energy_stress_iter.txt') # Relax configuration

elif status == 5 or status == 5.5:
    # relax screw dislocation structure A under stresses
    # glide plane and surface normal: y-axis
    # dislocation line/Burger vector: x-axis
    #
    # Schmid stress on glide plane:      sxy (must be 0)
    # Escaig stress on glide plane:      syz (applied by Fext)
    # Escaig stress on cross slip plane: szz (applied by H)
    # Escaig stress on cross slip plane: syy (applied by Fext)
    # Schmid stress on cross slip plane: sxz (applied by H)
    # Thermal expansion:                 sxx (applied by H)
    
    print('status %.1f: relax state A under stress'%status)
    sxx = sxy = 0
    syz, szz, syy, sxz = ns[1:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]
    
    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    
    # Load configuration of state A (from status = 0 or 0.5)
    basename = 'ni-screw-gp-relaxed.cn'
    if ap.use_init is not None:
        # initfile = os.path.join(csu.create_dirname(status = status, ns=ap.use_init), basename)
        initfile = ap.use_init
        changeH  = False
    else:
        initfile = os.path.join(csu.create_dirname(status = status - 5), basename)
        changeH  = True

    # Relax state A under stress (within sigma_tol(MPa))
    csu.cgrelax_stress(initfile=initfile, def_subsurface_region = True, changeH = changeH,
                       sigma_tol = 0.05, sigma = [sxx, syy, szz, sxy, syz, sxz],
                       cg_niter = 100, cg_ftol = 1e-4, cg_fevalmax = 1000, cg_Etol=1e-12,
                       fname = 'ni-screw-gp', save_iter_data = 'energy_stress_iter.txt')

elif status == 6 or status == 6.5:
    # relax screw dislocation structure B' with the same box and forces as A
    # glide plane and surface normal: y-axis
    # dislocation line/Burger vector: x-axis
    #
    # Schmid stress on glide plane:      sxy (must be 0)
    # Escaig stress on glide plane:      syz (applied by Fext)
    # Escaig stress on cross slip plane: szz (applied by H)
    # Escaig stress on cross slip plane: syy (applied by Fext)
    # Schmid stress on cross slip plane: sxz (applied by H)
    # Thermal expansion:                 sxx (applied by H)

    print("status %.1f: relax state B' under stress"%status)
    sxx = sxy = 0
    syz, szz, syy, sxz = ns[1:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]

    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()

    # Choose a good starting point for the dislocation configuration
    smin = np.abs([syy, syz, sxz]).min()
    if smin < 300:
        iselect = 40
    elif smin < 500:
        iselect = (np.round((500 - smin)/200*35) + 5).astype(int)
    else:
        iselect = 5
    # For Fleischer, the cross-slip disappears after step 5, we choose 1 here.
    if status == 6.5 or ap.NxNyNz[0] < 30: iselect = 1
    print('iselect = %d'%iselect)
    initfile = os.path.join(csu.create_dirname(status = status - 4), 'ni-cross-slip-relax-%i.cn'%iselect)
    # Load configuration of state B', and put into the simulation box of state A
    # Relax state B' configuration under stress
    csu.cgrelax_stress(initfile=initfile, useHfromA = True, def_subsurface_region = True, 
                       sigma = [sxx, syy, szz, sxy, syz, sxz], 
                       cg_niter = 50, cg_ftol = 1e-4, cg_fevalmax = 20, sigma_tol = None, cg_Etol = None,
                       fname = 'ni-cross-slip', save_iter_data = 'energy_stress_iter.txt')

elif status == 112 or status == 112.5:
    # Parallel NEB relaxation (for structures relaxed under stress)
    # to run (in SBATCH file)
    #   mpirun -n $ncpu python3 ni_Rao_screw_cross_slip.mdpp.py 112 400 0 0 0 0
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    ncpu = comm.Get_size()
    #mdpp.cmd('zipfile = 0')
    mdpp.cmd("myDomain = %d "%rank)
    mdpp.cmd("numDomains = %d "%ncpu)
    mdpp.cmd("Start_Slaves")

    print('status %.1f: parallel NEB relaxation under stress using %d cpus'%(status, ncpu))
    sxx = sxy = 0; offset = status - int(status)
    nstep, syz, szz, syy, sxz = ns[0:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]

    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    # Back up previous results
    dirname = csu.create_dirname(status=status, ns=ns)
    nfile = len(glob.glob(os.path.join(dirname, 'stringeng_step*')))
    csu.readeam_ni_Rao()
    print('successfully loaded EAM potential')
    mdpp.cmd('Broadcast_EAM_Param')
    if ap.nebrelax: mdpp.cmd('NNM = 800')

    basename = 'ni-screw-gp-relaxed.cn'
    print(basename)
    stateAfile = os.path.join(csu.create_dirname(status = 5 + offset, ns=ns), basename)
    NEBinit    = os.path.join(csu.create_dirname(status = status, ns=ns), 'NEBinit.cn')
    addext     = ''
    print(stateAfile)
    if not os.path.exists(stateAfile):
        addext = '.gz'
        if not os.path.exists(stateAfile + addext): raise FileNotFoundError(stateAfile)
    if not os.path.exists(NEBinit + addext):
        os.symlink(stateAfile + addext, NEBinit + addext)

    iselect = 5
    if ap.obtuse: iselect = 9
    stateBfile = os.path.join(csu.create_dirname(status = 6 + offset, ns=ns), 'ni-cross-slip-relax-%i.cn'%iselect)
    NEBfinal   = os.path.join(csu.create_dirname(status = status, ns=ns), 'NEBfinal.cn')
    addext     = '' 
    if not os.path.exists(stateBfile):
        addext = '.gz'
        if not os.path.exists(stateBfile + addext): raise FileNotFoundError(stateBfile + addext)
    if not os.path.exists(NEBfinal + addext):
        os.symlink(stateBfile + addext, NEBfinal + addext)

    # Load configuration of state A and B' for NEB calculation
    csu.load_file(NEBinit);  mdpp.cmd('setconfig1') # state A
    csu.load_file(NEBfinal); mdpp.cmd('setconfig2') # state B
    csu.load_init_file(NEBinit)
    
    # apply forces for sigma xy, yy and yz
    csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface=True)
    mdpp.cmd('Broadcast_Atoms')

    # Fix all atoms to be constrain atoms
    mdpp.cmd('fixallatoms constrain_fixedatoms freeallatoms')
    mdpp.cmd('chainlength = %d'%(ncpu - 1))

    # Save left-end potential energy as initE0 (see mdparallel.cpp), Yifan 2019.5.10
    mdpp.cmd('eval'); initE0 = mdpp.get('EPOT')
    print('initE0 = %23.17e'%initE0)
    mdpp.cmd('initE0 = %23.17e'%initE0)

    # Parameter setup for stringrelax_parallel/nebrelax_parallel
    if ap.nebrelax is not None:
        mdpp.cmd('timestep = 0.07 printfreq = 1')
        # 0: relax_surround, 0: spring_const(auto), 0: moveleft, 0:moveright,  1: yesclimbimage
        mdpp.cmd('nebspec = [ 0 0 0 0 1 ]')
        mdpp.cmd('totalsteps = %d equilsteps = %d'%(ap.nebrelax, max(ap.nebrelax - 400, 0)))
    else:
        # 0: relax_surround, 1: redistrib_freq, 3: moveleft, 3:moveright,  0: yesclimbimage,  0 0, 0 0 0,  1->20: cgstepmid, 10->40:cgsteprightend, 0: cgstepall (using steepest descent)
        mdpp.cmd('timestep = 0.07 printfreq = 1')
        if ap.manualcut is not None:
            manualcut_str = '1 %d %d'%tuple(ap.manualcut)
        else:
            manualcut_str = '0 0 0'
        if (ap.obtuse and nfile > 2):
            mdpp.cmd('energythreshold = [ 0.5 0.05 ]')
            mdpp.cmd('nebspec = [ 0 1 3 3  0   0 0 %s 10 20 7 ]'%manualcut_str)
        elif (not ap.obtuse and nfile > 1):
            mdpp.cmd('energythreshold = [ %f %f ]'%tuple(ap.energythreshold))
            mdpp.cmd('nebspec = [ 0 1 3 3  0   0 0 %s 10 20 7 ]'%manualcut_str)
        else:
            mdpp.cmd('energythreshold = [ %f %f ]'%tuple(ap.energythreshold))
            mdpp.cmd('nebspec = [ 0 1 0 3  0   0 0 %s 10 20 7 ]'%manualcut_str)
        mdpp.cmd('totalsteps = %d equilsteps = %d'%(nstep, max(nstep - 400, 0)))
    
    # Setup parallel settings
    mdpp.cmd('Slave_chdir') # Slave change to same directory as Master (dirname)
    mdpp.cmd('allocchain_parallel initRchain_parallel')
    if len(glob.glob(os.path.join(dirname, 'stringrelax.chain.cn.cpu*'))) > 0:
        mdpp.cmd('incnfile = %s readRchain_parallel'%os.path.join(dirname, 'stringrelax.chain.cn'))
    mdpp.cmd('eval')
    
    # Run neb simulation
    mdpp.cmd('Broadcast_nebsetting')
    if not ap.saveforce:
        if ap.nebrelax is not None:
            mdpp.cmd('nebrelax_parallel')
        else:
            mdpp.cmd('stringrelax_parallel')
        mdpp.cmd('finalcnfile = %s writeRchain_parallel'%os.path.join(dirname, 'stringrelax.chain.cn'))
        # Save stringrelax checkpoint
        if ap.nebrelax is not None:
            shutil.copy(os.path.join(dirname, 'nebeng.out'), os.path.join(dirname, 'stringeng_step%d.out')%(nfile+1))
            shutil.copy(os.path.join(dirname, 'nebeng.out'), os.path.join(dirname, 'stringeng.out'))
        else:
            shutil.copy(os.path.join(dirname, 'stringeng.out'), os.path.join(dirname, 'stringeng_step%d.out')%(nfile+1))
        for i in range(ncpu):
            shutil.copy(os.path.join(dirname, 'stringrelax.chain.cn.cpu%02d')%i, os.path.join(dirname, 'stringrelax.chain_step%d.cn.cpu%02d')%(nfile+1, i))
    else:
        # save LAMMPS and FORCE output of replica
        savestep = nfile if ap.saveforce else (nfile + 1)
        mdpp.cmd('copyRchaintoCN_parallel')
        mdpp.cmd('finalcnfile = %s writefinalcnfile_parallel'%os.path.join(dirname, 'stringrelax.chain.savecn'))

elif status in [114, 114.5]:
    # Dimer method to obtain the exact saddle point, after step 212/212.5
    sxx = sxy = 0; offset = status - int(status)
    nstep, syz, szz, syy, sxz = ns[0:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]
    # f = open('dimer.out', 'w')
    verbose = False

    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    dirname = csu.create_dirname(status=status - 2, ns=ns)
    chaindir = os.path.join(dirname, 'stringrelax.chain')
    Echain  = np.genfromtxt(os.path.join(chaindir, 'Echain.txt'))
    EmaxDomain = Echain[:-1].argmax()
    config0file = os.path.join(chaindir, 'chain.%02d.cn'%(EmaxDomain))
    config1file = os.path.join(chaindir, 'chain.%02d.cn'%(EmaxDomain-1))
    config2file = os.path.join(chaindir, 'chain.%02d.cn'%(EmaxDomain+1))
    print('EmaxDomain =', EmaxDomain)
    # Load initial dimer
    csu.load_file(config0file); csu.mdpp.cmd('SHtoR'); NP = csu.mdpp.get('NP')
    R0 = np.reshape(csu.mdpp.get_array('R', 0, NP*3), (NP*3, ))
    csu.load_file(config1file); csu.mdpp.cmd('SHtoR'); NP = csu.mdpp.get('NP')
    R1 = np.reshape(csu.mdpp.get_array('R', 0, NP*3), (NP*3, ))
    csu.load_file(config2file); csu.mdpp.cmd('SHtoR'); NP = csu.mdpp.get('NP')
    R2 = np.reshape(csu.mdpp.get_array('R', 0, NP*3), (NP*3, ))
    dR01 = np.linalg.norm(R1-R0)
    dR02 = np.linalg.norm(R2-R0)
    print('dR01 =', dR01, 'dR02 =', dR02)
    if dR01 < dR02:
        R2 = R0 + (R0-R1)*1/2
        R1 = R0*2 - R2
    else:
        R1 = R0 + (R0-R2)*1/2
        R2 = R0*2 - R1
    R0orig = R0
    csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface=True)
    # Initialize the dimer
    dimer = dict(R1=R1, R2=R2)
    dimer = csu.dimer_RtoF(dimer)
    dimer = csu.dimer_Fdag(dimer)
    dimer = csu.dimer_trueFR(dimer)
    trueFRmag = np.linalg.norm(dimer['trueFR'])
    print('init true FR: %.4e'%trueFRmag)#, file=f)
    E0 = dimer['trueER']
    print('init true ER: %.4e'%E0)

    # Load saved dimer
    dimerfile  = 'dimer.npz'
    if os.path.exists(dimerfile):
        rawdata = np.load(dimerfile)
        R1, R2, R0 = rawdata['R1'], rawdata['R2'], rawdata['R']
        print('dR =', rawdata['dR'])#, file=f)

        # Initialize the dimer
        dimer = dict(R1=R1, R2=R2)
        dimer = csu.dimer_RtoF(dimer)
        dimer = csu.dimer_Fdag(dimer)
        dimer = csu.dimer_trueFR(dimer)
        trueFRmag = np.linalg.norm(dimer['trueFR'])
        print('saved true FR: %.4e'%trueFRmag)
        E0 = dimer['trueER']
        print('init true ER: %.4e'%E0, dimer['E1'], dimer['E2'])
    csu.mdpp.set_array(dimer['R'].tolist(), 'R', 0)
    csu.mdpp.cmd('RHtoS eval')
    csu.write_file('dimer_final')
    print(dimer['R1'][:3], dimer['F1'][:3])
    print(dimer['R2'][:3], dimer['F2'][:3])
    # raise ValueError()

    # dimer method iterations
    lr = ap.dimer_alpha
    pdimer = dimer
    for it_sh in range(ap.dimer_maxiter_shrink):
        print('iter of shrinking %d: lr = %.8e dR = %.8e'%(it_sh, lr, pdimer['dR']))#, file=f)
        FRmag_prev = 0
        output_list = []
        print('  itdimer %4s:%16s%16s%16s%16s%16s%16s'%('it', 'FRmag_curr', 'Fdag_mag', 'Frot_mag', 'Dt', 'dE', 'DR'))
        for it in range(ap.dimer_maxiter):
            # Rotating dimer
            Dt = csu.dimer_minimizeFrot(pdimer, epsilon=1e-7, verbose=verbose, estimate=True)
            if Dt < 1e-5: Dt *= 0.5
            rotdimer = csu.dimer_Fdag(csu.dimer_Rrot(Dt, pdimer, verbose=verbose))
            FRmag_curr = np.linalg.norm(rotdimer['FR'])
            Fdag_mag = np.linalg.norm(rotdimer['Fdag'])
            Frot_mag = np.linalg.norm(rotdimer['Frot'])
            DR = np.linalg.norm(rotdimer['R']-R0orig)
            output_list.append((it+1, FRmag_curr, Fdag_mag, Frot_mag, Dt, rotdimer['E0']-E0, DR))
            print('  itdimer %4d:%16.8e%16.8e%16.8e%16.8e%16.8e%16.8e'%(it+1, FRmag_curr, Fdag_mag, Frot_mag, Dt, rotdimer['E0']-E0, DR))
            # if np.abs(FRmag_curr - FRmag_prev) < ap.dimer_Fmag_th: break
            if FRmag_curr < ap.dimer_Fmag_th: break
            FRmag_prev = FRmag_curr
            # Translating dimer
            rotR1, rotR2, Fdag = rotdimer['R1'], rotdimer['R2'], rotdimer['Fdag']
            pdimer = csu.dimer_RtoF(dict(R1=rotR1+lr*Fdag, R2=rotR2+lr*Fdag), verbose=verbose)
        # Calculate true FR magnitude
        pdimer = csu.dimer_trueFR(rotdimer)
        trueFRmag = np.linalg.norm(pdimer['trueFR'])
        output_list.append((0, trueFRmag, 0, 0, 0, pdimer['trueER']-E0, 0))
        print('    true FR: %.8e %.8e'%(trueFRmag, pdimer['trueER']-E0))#, file=f)
        np.savetxt('dimer_data_%d.txt'%it_sh, output_list, fmt='%20d %24.12e %24.12e %24.12e %24.12e %24.12e %24.12e')
        np.savez_compressed('dimer', **pdimer)
        np.savez_compressed('dimer_%d'%it_sh, **pdimer)
        if trueFRmag < ap.dimer_Fmag_th: break
        # Shrink the dimer
        # if pdimer['E1'] < pdimer['E2']:
        #     R1new, R2new = pdimer['R'] + pdimer['N']*pdimer['dR']/2*ap.dimer_shrink_factor, pdimer['R2']
        # else:
        #     R1new, R2new = pdimer['R1'], pdimer['R'] - pdimer['N']*pdimer['dR']/2*ap.dimer_shrink_factor
        R1new = pdimer['R'] + pdimer['N']*pdimer['dR']*ap.dimer_shrink_factor
        R2new = pdimer['R'] - pdimer['N']*pdimer['dR']*ap.dimer_shrink_factor
        lr *= ap.dimer_lr_shrink_factor
        pdimer = csu.dimer_RtoF(dict(R1=R1new, R2=R2new), verbose=verbose)
        # Save the dimer configuration
        csu.mdpp.set_array(pdimer['R'].flatten().tolist(), 'R', 0)
        csu.mdpp.cmd('RHtoS eval')
        csu.write_file('dimer_final')
    # f.close()
    
    # Save the dimer configuration
    csu.mdpp.set_array(pdimer['R'].flatten().tolist(), 'R', 0)
    csu.mdpp.cmd('RHtoS eval')
    csu.write_file('dimer_final')

elif status in [212, 212.5]:
    # Post processing of status 112 for harmonic analysis of free energy
    # Step 1. Convert stringrelax.chain to separate cn files
    # python3 ni_Rao_screw_cross_slip.mdpp.py 212 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --steps 0 1 2 --replace --atom_range 0 16 17

    print('status %.1f: convert stringrelax.chain files to cn and LAMMPS files'%(status))
    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()
    if ap.atom_range == [-1, ]: ap.atom_range = None
    # only convert the latest stringrelax.chain files if ap.steps is 0
    csu.save_nebchain(status - 100, ns, replace=ap.replace, steps=ap.steps, atom_range=ap.atom_range,
                      get_subsurface_stress=ap.get_subsurface_stress)

elif status == 213:
    # Post processing of status 112 for harmonic analysis of free energy
    # Step 2. Generating Hessian matrix of state n
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range -1 0 5400 --steps 46
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range -1 5400 10800
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range -1 10800 16200
    # ...
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range 13 0 5400
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range 13 5400 10800
    # python3 ni_Rao_screw_cross_slip.mdpp.py 213 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range 13 10800 16200
    # ...

    if len(ap.atom_range) == 3:
        print('status %.1f: Calculating Hessian matrix for atom %d-%d'%(status, ap.atom_range[1], ap.atom_range[2]))
    else:
        print('status %.1f: Calculating Hessian matrix'%(status))
    sxx = sxy = 0
    syz, szz, syy, sxz = ns[1:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]

    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz, atom_range=ap.atom_range)
    csu.readeam_ni_Rao()
    # print(os.path.join(csu.create_dirname(214, ns)))
    # print(glob.glob(os.path.join(csu.create_dirname(214, ns), 'state_[0-9][0-9]')))
    dirname214 = glob.glob(os.path.join(csu.create_dirname(214, ns, atom_range='none'), 'state_[0-9][0-9]'))[0]
    if ap.atom_range[0] < 0:
        # if ap.atom_range[0] == -1:
        #     postprocfile = os.path.join(csu.create_dirname(5, ns), 'ni-screw-gp-relaxed.cn')
        postprocfile = os.path.join(dirname214, 'A_SR_relaxed.cn')
    else:
        # if ap.steps[0] == 0:
        #     postprocfile = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain', 'chain.%02d.cn')%ap.atom_range[0]
        # else:
        #     postprocfile = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain_step%d'%ap.steps[0], 'chain.%02d.cn'%ap.atom_range[0])
        # if len(ap.atom_range) == 3 and ap.atom_range[0] == 0:
        #     postprocfilename = os.path.join(csu.create_dirname(213, ns, atom_range=[0,]), 'ni-screw-gp-relaxed.cn')
        #     if os.path.exists(postprocfilename) or os.path.exists(postprocfilename+'.gz'):
        #         postprocfile = postprocfilename
        postprocfile = os.path.join(dirname214, 'step0', 'config_ctr.cn')
    print('status %.1f: creating hessian matrix of %s'%(status, postprocfile))
    if not os.path.exists(postprocfile) and not os.path.exists(postprocfile+'.gz'):
        raise FileNotFoundError('Cannot find %s'%postprocfile)
    csu.load_file(postprocfile)
    
    # Apply force on the surface
    csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface = True)

    # Relax the left end
    if len(ap.atom_range) == 1 and ap.atom_range[0] == 0:
        print('Relax left end configuration')
        csu.cgrelax_stress(changeH = False, apply_force = False,
                           cg_niter = 100, cg_ftol = 1e-4, cg_fevalmax = 1000, 
                           fname = 'ni-screw-gp', save_iter_data = 'energy_stress_iter.txt')
    else:
        NP = csu.mdpp.get('NP')
        csu.mdpp.cmd('timestep = %f'%ap.timestep)
        if len(ap.atom_range) == 1:
            mdpp.cmd('input = 0 calHessian')
        elif len(ap.atom_range) == 3:
            if ap.atom_range[1] > NP or ap.atom_range[1] < 0 or ap.atom_range[2] > NP or ap.atom_range[2] < 0:
                raise ValueError('atom_range values out of range')
            core_region = np.arange(ap.atom_range[1], ap.atom_range[2]).astype(int)
            print('calculate hessian matrix for state %d: subset of atoms [%d, %d)'%tuple(ap.atom_range))
            mdpp.cmd('input = %s calHessian'%(list_to_str(np.insert(core_region, 0, core_region.size))))
        else:
            raise ValueError('you have to specify either 1 or 3 values for atom_range')

elif status == 214:
    # Post processing of status 112 for harmonic analysis of free energy
    # Step 3. Generating Goldstone mode and glide mode
    # python3 ni_Rao_screw_cross_slip.mdpp.py 214 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000 --atom_range 13 --steps 21 --linspace 1000
    # second argument of atom_range indicates the linear interpolation points of the energy landscape

    from ovito.io import import_file
    from ovito.data import NearestNeighborFinder
    from scipy.io import savemat
    sxx = sxy = 0
    syz, szz, syy, sxz = ns[1:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]
    
    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz, atom_range=ap.atom_range)
    csu.readeam_ni_Rao()

    # Load coordinates of state A for reference
    stateAfile = os.path.join(csu.create_dirname(5, ns), 'ni-screw-gp-relaxed.cn')
    print('load state A configuration', stateAfile)
    csu.load_file(stateAfile)

    # Apply force on the surface and relax state A
    csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface = True)
    csu.mdpp.cmd('eval')
    EA0 = csu.mdpp.get('EPOT')
    print('E0 of state A, load from step 5:', EA0)
    
    # relaxAfile = os.path.join(csu.create_dirname(213, ns, atom_range=[0,]), 'ni-screw-gp-relaxed')
    # csu.load_file(relaxAfile+'.cn')
    # csu.mdpp.cmd('eval')
    relaxAfile = 'A_SR_relaxed'
    if not os.path.exists(relaxAfile + '.cn.gz'):
        stateAfile = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain', 'chain.00.cn')
        print('load state A configuration', stateAfile)
        csu.load_file(stateAfile)
        csu.mdpp.cmd('eval')
        print('E of step 112 - chain0:', csu.mdpp.get('EPOT'))
        csu.relax_fixbox(filename='A_SR_relaxed')
        time.sleep(10)
    else:
        print('load relaxed state A configuration', relaxAfile)
        csu.load_file(relaxAfile + '.cn')
        csu.mdpp.cmd('eval')
    csu.mdpp.cmd('SHtoR')
    NP = csu.mdpp.get('NP')
    SR_A = np.reshape(csu.mdpp.get_array('SR', 0, NP*3), (NP, 3))
    H    = np.reshape(csu.mdpp.get_array('H', 0, 9), (3, 3))
    E0 = csu.mdpp.get('EPOT')
    Lx = csu.mdpp.get('H_11')
    Lz = csu.mdpp.get('H_33')
    dx = Lx/120
    dz = Lz/80
    print(H)
    print('E0 of relaxed state A, load from step 112 - chain0:', E0)
    print('dx = %.4f/120 = %.4f; dz = %.4f/40 = %.4f'%(Lx, dx, Lz, dz))
    RA = np.reshape(mdpp.get_array('R', 0, 3*NP), (NP, 3))
    RAshift = RA + np.array([dx, 0, dz])
    print('R0.shape =', RA.shape)

    # Generate gliding mode of state A
    stateAlammpsfile = relaxAfile + '.LAMMPS.gz'
    print('load state A configuration', stateAfile)
    pipeline = import_file(stateAlammpsfile)
    data = pipeline.compute()
    R0 = data.particles.positions[...]
    cell = data.cell[...]
    dx = cell[0, 0]/ap.NxNyNz[0]/4
    dz = cell[2, 2]/ap.NxNyNz[2]/4
    print('shift v = [%.4f, 0, %.4f]'%(dx, dz))
    R1 = R0 + np.array([dx, 0, dz])
    shift_ind = np.zeros(NP, dtype=int)
    finder = NearestNeighborFinder(1, data)
    for i in range(NP):
        coords = tuple(R1[i, :])
        for neigh in finder.find_at(coords):
            shift_ind[neigh.index] = i
    csu.mdpp.set_array(RAshift[shift_ind, :].flatten().tolist(), 'R', 0)
    csu.mdpp.cmd('RHtoS eval')
    SA1 = np.reshape(mdpp.get_array('SR', 0, 3*NP), (NP, 3))
    E1 = csu.mdpp.get('EPOT')
    print('Shifted E of state A:', E1)
    print('E1 - E0 =', E1-E0)
    savefile = 'dAG.mat'
    dSA = (SA1 - SR_A) - np.round(SA1 - SR_A)
    dRA = np.dot(H, dSA.T).T
    print('Max displacement =', np.max(np.linalg.norm(dRA, axis=1)))
    savemat(savefile, {'dAG': dRA, 'dAG_S': dSA, 'E0': EA0, 'E1': E1, 'shift_ind': shift_ind})

    savefile_dir = os.path.join(os.getcwd(), '..', 'state_-1')
    os.makedirs(savefile_dir, exist_ok=True)
    for f in ['dAG.mat', 'A_SR_relaxed.LAMMPS.gz', 'A_SR_relaxed.cn.gz']:
        if not os.path.exists(os.path.join(savefile_dir, f)):
            shutil.copy(f, os.path.join(savefile_dir, f))

    # Load coordinates of state S
    # if ap.atom_range[0] < 0:
    #     postprocfile = os.path.join(csu.create_dirname(5, ns), 'ni-screw-gp-relaxed.cn')
    # else:
    #     if ap.steps[0] == 0:
    #         postprocfile = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain', 'chain.%02d.cn')%ap.atom_range[0]
    #     else:
    #         postprocfile = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain_step%d', 'chain.%02d.cn')%(ap.steps[0], ap.atom_range[0])
    postprocfile = os.path.join(csu.create_dirname(114, ns), 'dimer_final.cn')
    print('status %.1f: generating goldstone mode of %s'%(status, postprocfile))
    if not os.path.exists(postprocfile) and not os.path.exists(postprocfile+'.gz'):
        raise FileNotFoundError('Cannot find %s'%postprocfile)
    csu.load_file(postprocfile)
    csu.mdpp.cmd('eval')
    E_ctr = csu.mdpp.get('EPOT')
    csu.write_file('config_ctr')
    print('Saved center configuration config_ctr.cn')

    SR_S = np.reshape(csu.mdpp.get_array('SR', 0, NP*3), (NP, 3))

    # Calculate the displacement
    dSR_AS = SR_S - SR_A

    # Load shift index mapping for goldstone mode (see status == 0, make_perfect_crystal function)
    shift_map_file = os.path.join(csu.create_dirname(0), 'shift_map.txt')
    shift_map_data = np.loadtxt(shift_map_file, dtype=int)
    shift_ind_fwd, shift_ind_bwd = shift_map_data[:, 1], shift_map_data[:, 2]
    print('Loaded shift map for goldstone mode from', shift_map_file)
    
    # Generate forward  (-x direction) shifting configuration
    dSR_AS_fwd = dSR_AS[shift_ind_fwd]
    csu.mdpp.set_array((SR_A + dSR_AS_fwd).flatten().tolist(), 'SR', 0)
    csu.mdpp.cmd('eval')
    E_fwd = csu.mdpp.get('EPOT')
    csu.write_file('config_fwd')
    print('Saved forward configuration config_fwd.cn')

    # Generate backward (+x direction) shifting configuration
    dSR_AS_bwd = dSR_AS[shift_ind_bwd]
    csu.mdpp.set_array((SR_A + dSR_AS_bwd).flatten().tolist(), 'SR', 0)
    csu.mdpp.cmd('eval')
    E_bwd = csu.mdpp.get('EPOT')
    csu.write_file('config_bwd')
    print('Saved backward configuration config_bwd.cn')

    # Save the goldstone mode and output the energies
    print('Energy (eV) of forward, center, and backward configurations: %23.17e %23.17e %23.17e'%(E_fwd - E_ctr, 0, E_bwd - E_ctr))
    np.savez_compressed('dGM', dGM_ctr=np.dot(dSR_AS_fwd-dSR_AS_bwd, H.T), 
                               dGM_fwd=np.dot(dSR_AS_fwd-dSR_AS, H.T), 
                               dGM_bwd=np.dot(dSR_AS_bwd-dSR_AS, H.T)
                       )
    
    # Evaluate the energy in between saddle point sites
    nsplit = ap.linspace
    E_bwd_list = []
    E_fwd_list = []
    for i in range(nsplit + 1):
        print('evaluating reaction coordinate %d/%d: '%(i, nsplit), end='')
        csu.mdpp.set_array((SR_A + dSR_AS + i/nsplit*(dSR_AS_fwd - dSR_AS)).flatten().tolist(), 'SR', 0)
        csu.mdpp.cmd('eval')
        E_fwd_list.append(csu.mdpp.get('EPOT'))
        csu.mdpp.set_array((SR_A + dSR_AS + i/nsplit*(dSR_AS_bwd - dSR_AS)).flatten().tolist(), 'SR', 0)
        csu.mdpp.cmd('eval')
        E_bwd_list.append(csu.mdpp.get('EPOT'))
        print(E_fwd_list[-1], E_bwd_list[-1])
    np.savez_compressed('E_GM', E_fwd = np.array(E_fwd_list) - E_ctr, E_bwd = np.array(E_bwd_list) - E_ctr)

    stepdir = 'step%d'%ap.steps[0]
    os.makedirs(stepdir, exist_ok=True)
    for f in ['E_GM.npz', 'dGM.npz', 
              'config_ctr.cn.gz', 'config_fwd.cn.gz', 'config_bwd.cn.gz',
              'config_ctr.LAMMPS.gz', 'config_fwd.LAMMPS.gz', 'config_bwd.LAMMPS.gz']:
        shutil.move(f, os.path.join(stepdir, f))

elif status == 105:
    # NEB of state A glide mode
    # to run (in SBATCH file)
    #   mpirun -n $ncpu python3 ni_Rao_screw_cross_slip.mdpp.py 105 400 852.2913 -5007.9675 -223.6200 905.3898 -2598.3164 0.0000  --runs_dir ni-Rao-cs-20x20x10 --NxNyNz 20 20 10
    from mpi4py import MPI
    from scipy.io import loadmat, savemat
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    ncpu = comm.Get_size()
    #mdpp.cmd('zipfile = 0')
    mdpp.cmd("myDomain = %d "%rank)
    mdpp.cmd("numDomains = %d "%ncpu)
    mdpp.cmd("Start_Slaves")

    print('status %.1f: parallel NEB relaxation under stress using %d cpus'%(status, ncpu))
    sxx = sxy = 0; offset = status - int(status)
    nstep, syz, szz, syy, sxz = ns[0:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]

    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz, atom_range=ap.atom_range)
    # Back up previous results
    dirname = csu.create_dirname(status=status, ns=ns)
    csu.readeam_ni_Rao(); mdpp.cmd('Broadcast_EAM_Param')
    if ap.nebrelax: mdpp.cmd('NNM = 600')

    state_dir = csu.create_dirname(status = 214, ns=ns, atom_range=[-1,])
    savefile  = os.path.join(state_dir, 'dAG.mat')
    dAG_S     = loadmat(savefile)['dAG_S'].reshape(-1, 3)
    stateAfile = os.path.join(state_dir, 'A_SR_relaxed.cn')
    # stateAfile = os.path.join(csu.create_dirname(213, ns, atom_range=[0,]), 'ni-screw-gp-relaxed.cn')
    NEBinit    = os.path.join(csu.create_dirname(status = status, ns=ns), 'NEBinit.cn')
    addext     = ''
    if not os.path.exists(stateAfile):
        addext = '.gz'
        if not os.path.exists(stateAfile + addext):
            raise FileNotFoundError(stateAfile)
    if not os.path.exists(NEBinit + addext):
        os.symlink(stateAfile + addext, NEBinit + addext)

    # Load configuration of state A and B' for NEB calculation
    csu.load_file(NEBinit);  mdpp.cmd('setconfig1') # state A
    NP = mdpp.get('NP')
    SA = np.reshape(mdpp.get_array('SR', 0, 3*NP), (NP, 3))
    H  = np.reshape(csu.mdpp.get_array('H', 0, 9), (3, 3))
    mdpp.set_array((SA + dAG_S).flatten().tolist(), 'SR', 0)
    mdpp.cmd('setconfig2') # state B
    csu.load_init_file(NEBinit)

    # apply forces for sigma xy, yy and yz
    csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface=True)
    mdpp.cmd('Broadcast_Atoms')

    # Fix all atoms to be constrain atoms
    mdpp.cmd('fixallatoms constrain_fixedatoms freeallatoms')
    mdpp.cmd('chainlength = %d'%(ncpu - 1))

    # Save left-end potential energy as initE0 (see mdparallel.cpp), Yifan 2019.5.10
    mdpp.cmd('eval'); initE0 = mdpp.get('EPOT')
    print('initE0 = %23.17e'%initE0)
    mdpp.cmd('initE0 = %23.17e'%initE0)

    # Parameter setup for stringrelax_parallel/nebrelax_parallel
    mdpp.cmd('timestep = 0.07 printfreq = 1')
    # 0: relax_surround, 0: spring_const(auto), 0: moveleft, 0:moveright,  1: yesclimbimage
    mdpp.cmd('nebspec = [ 0 0 0 0 1 ]')
    mdpp.cmd('totalsteps = %d equilsteps = %d'%(nstep, max(nstep - 400, 0)))
    
    # Setup parallel settings
    mdpp.cmd('Slave_chdir') # Slave change to same directory as Master (dirname)
    mdpp.cmd('allocchain_parallel initRchain_parallel')
    mdpp.cmd('eval')
    
    # Run neb simulation
    mdpp.cmd('Broadcast_nebsetting')
    mdpp.cmd('nebrelax_parallel')
    mdpp.cmd('finalcnfile = %s writeRchain_parallel'%os.path.join(dirname, 'stringrelax.chain.cn'))

    # Save neb configurations
    mdpp.cmd('copyRchaintoCN_parallel')
    mdpp.cmd('finalcnfile = %s writefinalcnfile_parallel'%os.path.join(dirname, 'stringrelax.chain.savecn'))

    # Update dAG file
    # if rank == 0:
    time.sleep(10)
    rawdata = loadmat(savefile)
    savedata = {'dAG': rawdata['dAG'], 'dAG_S': rawdata['dAG_S'], 
                'E0': rawdata['E0'], 'E1': rawdata['E1'], 
                'shift_ind': rawdata['shift_ind']}
    savedata['Rvec'] = np.zeros(ncpu)
    # savedata['E_dAG'] = np.zeros(ncpu)
    for ifile in range(ncpu):
        filename = os.path.join(dirname, 'stringrelax.chain.savecn.cpu%02d.gz'%ifile)
        fileloadname = os.path.join(dirname, 'stringrelax.chain.cpu%02d.cn.gz'%ifile)
        shutil.move(filename, fileloadname)
        csu.load_file(fileloadname)
        # csu.mdpp.cmd('SHtoR eval')
        # savedata['E_dAG'][ifile] = csu.mdpp.get('EPOT')
        SRi = np.reshape(mdpp.get_array('SR', 0, 3*NP), (NP, 3))
        dSRi = (SRi - SA) - np.round(SRi - SA)
        dRi  = np.dot(H, dSRi.T).T
        if ifile == 1: savedata['dRA_fwd'] = np.copy(dRi)
        if ifile == ncpu - 2: SR_bwd = np.copy(SRi)
        savedata['Rvec'][ifile] = np.linalg.norm(dRi)
        print('frame%4d/%d: %.8gA'%(ifile, ncpu-1, savedata['Rvec'][ifile]))
    dSR_bwd = (SR_bwd - SRi) - np.round(SR_bwd - SRi)
    savedata['dRA_bwd'] = np.zeros_like(savedata['dRA_fwd'])
    savedata['dRA_bwd'][savedata['shift_ind']] = np.dot(H, dSR_bwd.T).T
    print('Forward/backward displacement: %.8g %.8g'%(np.linalg.norm(savedata['dRA_fwd']), np.linalg.norm(savedata['dRA_bwd'])))
    savemat(savefile, savedata)

elif status == -1:
    # For testing:
    # ipython -i -- ni_Rao_screw_cross_slip.mdpp.py -1 400 647.3559 -6123.5515 -1821.4873 961.0315 -3940.3488 0.0000
    
    print('status %.1f: relax state A under stress'%status)
    sxx = sxy = 0
    syz, szz, syy, sxz = ns[1:5]
    if len(ns) > 5: sxx, sxy = ns[5:7]
    offset = status - np.ceil(status)
    
    csu = cross_slip_util(runs_dir, status, ns, mdpp=mdpp, setnolog=ap.setnolog, NxNyNz=ap.NxNyNz)
    csu.readeam_ni_Rao()

    # initfile = os.path.join(csu.create_dirname(0), 'ni-screw-gp-relaxed.cn')
    # csu.load_file(initfile)

    # csu.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface=True)

    # from scipy.io import loadmat
    # dirname = glob.glob(os.path.join(csu.create_dirname(214, ns), 'state_1?'))[0]
    # saddlefile = os.path.join(dirname, 'step0', 'config_ctr.cn')
    # dirname = glob.glob(os.path.join(csu.create_dirname(213, ns), 'state_1?'))[0]
    # eigfile = os.path.join(dirname, 'eig_sa_10.mat')
    # eigdata = loadmat(eigfile)
    # print('eigfile = %s'%eigfile)
    # V, W = eigdata['V'], eigdata['W']
    # dirname = os.path.join(csu.create_dirname(112, ns), 'stringrelax.chain')
    # Echain = np.loadtxt(os.path.join(dirname, 'Echain.txt'))
    # nchain = Echain[:-1].argmax()
    # saddlefile = os.path.join(dirname, 'chain.%02d.cn')%nchain
    # print('saddlefile = %s'%saddlefile)
    # csu.load_file(saddlefile)
    # csu.mdpp.cmd('SHtoR eval')
    # print('Energy = %.4f eV'%csu.mdpp.get('EPOT'))
    # NP = csu.mdpp.get('NP'); R = np.array(csu.mdpp.get_array('R', 0, 3*NP))

    # v = V[:, 0]
    # kvals = np.linspace(-10, 10, 201)
    # Elist = []
    # for i in range(0, kvals.size, 10):
    #     print('%d/%d'%(i+1, kvals.size))
    #     Rk = R + kvals[i]*v
    #     csu.mdpp.set_array(Rk.tolist(), 'R', 0)
    #     csu.mdpp.cmd('RHtoS eval')
    #     Elist.append(csu.mdpp.get('EPOT'))
    #     csu.write_file('R_%d'%i, format=['atomeyecfg'])
    # np.savetxt('Elist.txt', Elist)
    # np.savez_compressed('Elist', Elist)
    
    # Load configuration of state A (from status = 0 or 0.5)
    # initfile = os.path.join(csu.create_dirname(status = 17+offset, ns=ns), 'ni-screw-gp-0.dump.gz')
    # csu.mdpp.cmd('input = [ 100 1 ] incnfile = %s readLAMMPS'%initfile)
    # NP = csu.mdpp.get('NP')

    # Apply force on the surface
    # syadd = [sxy, syy, syz]
    # csu.apply_force_on_surface(sigma_y = syadd, define_surface = True)
    # csu.def_subsurface_region()

    # Set up Nose-Hoover thermostat
    # mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    # mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    # mdpp.cmd("T_OBJ = %d DOUBLE_T = 0 randseed = 12345 srand48 initvelocity"%ns[0])
    # mdpp.cmd("timestep = 0.001")

    # Run MD simulation
    # for i in range(200):
    #     print('running step %d for 0.01ps; '%i, end='')
    #     tic = time.time()
    #     mdpp.cmd("totalsteps = 10 run")
    #     toc = time.time()
    #     print('Wall time: %.6f s'%(toc-tic))
    #     print('Total Stress')
    #     position = np.reshape(csu.mdpp.get_array('SR', 0, 3*NP), (-1, 3))
    #     y_top = position[csu.top_surface, 1].mean()
    #     y_bot = position[csu.bot_surface, 1].mean()
    #     vacuumratio = 1 - np.abs(y_top - y_bot)
    #     print('vacuumratio =', vacuumratio)
    #     mdpp.cmd('vacuumratio = %g'%vacuumratio)
    #     print('%.4f %.4f %.4f %.4f %.4f %.4f'%(mdpp.get('TSTRESSinMPa_xx'), mdpp.get('TSTRESSinMPa_yy'), 
    #                                            mdpp.get('TSTRESSinMPa_zz'), mdpp.get('TSTRESSinMPa_xy'),
    #                                            mdpp.get('TSTRESSinMPa_yz'), mdpp.get('TSTRESSinMPa_xz')
    #                                           )
    #          )
    #     print('%.4f %.4f %.4f %.4f %.4f %.4f'%(csu.get_subsurface_stress('s_sub_xx', verbose=False), csu.get_subsurface_stress('s_sub_yy', verbose=False), 
    #                                            csu.get_subsurface_stress('s_sub_zz', verbose=False), csu.get_subsurface_stress('s_sub_xy', verbose=False),
    #                                            csu.get_subsurface_stress('s_sub_yz', verbose=False), csu.get_subsurface_stress('s_sub_xz', verbose=False)
    #                                           )
    #          )
    #     print('%.4f %.4f %.4f %.4f %.4f %.4f'%(csu.get_subsurface_stress('s_sub_xx', verbose=False, kinetic=True), csu.get_subsurface_stress('s_sub_yy', verbose=False, kinetic=True), 
    #                                            csu.get_subsurface_stress('s_sub_zz', verbose=False, kinetic=True), csu.get_subsurface_stress('s_sub_xy', verbose=False, kinetic=True),
    #                                            csu.get_subsurface_stress('s_sub_yz', verbose=False, kinetic=True), csu.get_subsurface_stress('s_sub_xz', verbose=False, kinetic=True)
    #                                           )
    #          )
    #     csu.write_file('ni-screw-gp-%d'%i)

else:
    raise ValueError('unknown status = %d'%status)
    
#---------------------------------------------
# Sleep for a couple of seconds
if not 'sleep_seconds' in locals():
    sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
if status == -1:
    pass
elif status in [105, 112, 112.5]:
    mdpp.cmd('quit_all')
else:
    mdpp.cmd('quit')
