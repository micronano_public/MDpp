import sys, os, itertools, glob, filecmp, subprocess, gzip
import numpy as np
from scipy.optimize import minimize

#********************************************
# Definition of utility functions
#********************************************

codedir = os.path.join(os.environ['MDPLUS_DIR'], 'scripts', 'Examples', 'example11-ni-disl-python')
default_Nx, default_Ny, default_Nz         = 30, 30, 20
default_modulus_xx = default_modulus_yy = default_modulus_zz = 450e3 # MPa
default_modulus_xz = default_modulus_xy = default_modulus_yz = 100e3 # MPa
default_modulus    = [default_modulus_xx, default_modulus_yy, default_modulus_zz,
                      default_modulus_xy, default_modulus_yz, default_modulus_xz]

energy_unit_conversion = 0.62415091259e-5 # # eV/A^3 / MPa
NA = 6.02214076e23
default_atommass = 58.6934
default_lattice_constant = 3.52
default_poisson_ratio = 0.3
default_split_width   = 30.0
default_natom_per_unitcell = 4 # fcc
default_output_format = ['cn', 'LAMMPS']
debug_output = False
list_to_str = lambda lst: '[ %s ]'%(' '.join([str(it) for it in lst]))

class cross_slip_util:
    def __init__(self, runs_dir, status=0, ns=[], mdpp=None, setnolog=True, setoverwrite=True, zipfiles=True, NxNyNz=None, atom_range='none'):
        ''' Initialize MD++ simulation
            If mdpp is not given, the class can only be used for creating directory name
        '''
        self.mdpp = mdpp
        self.runs_dir = runs_dir
        self.status = status
        self.offset = status - int(status)                # offset = 0: Freidel-Escaig; offset = 0.5: Fleischer
        self.ns = ns
        if NxNyNz is not None:                            # repeat of lattice vectors in x, y, z direction
            self.Nx, self.Ny, self.Nz = NxNyNz
        else:
            self.Nx, self.Ny, self.Nz = default_Nx, default_Ny, default_Nz
        self.atom_range = atom_range                      # for status 213 only (see create_dirname)

        self.a = default_lattice_constant                 # lattice constant, unit length in Angstrom
        self.nu = default_poisson_ratio                   # Poisson's ratio
        self.d  = default_split_width                     # split width in real length units

        self.dirname = self.create_dirname(status, ns)
        os.makedirs(self.dirname, exist_ok=True)

        if mdpp is not None:
            if setnolog:
                self.mdpp.cmd("setnolog")
            if setoverwrite:
                self.mdpp.cmd("setoverwrite")
            self.mdpp.cmd("dirname = " + self.dirname)
            self.mdpp.cmd("zipfiles = %d"%zipfiles)

#*******************************************
# Definition of utility functions
#*******************************************

    def create_dirname(self, status, ns=[], Nx=None, Ny=None, Nz=None, atom_range=None):
        '''Create dirname based on status and arguments ns
        '''
        if Nx is None: Nx = self.Nx
        if Ny is None: Ny = self.Ny
        if Nz is None: Nz = self.Nz
        if atom_range is None: atom_range = self.atom_range
        strNxNyNz = '%dx%dx%d'%(Nx, Ny, Nz)
        if status < 4:
            dirname = os.path.join(self.runs_dir, 'ni-Rao-screw-%s-%.1f'%(strNxNyNz, status))
        elif status < 300:
            stress_cond = '%.4f_%.4f_%.4f_%.4f'%tuple(ns[1:5])
            if len(ns) > 5: stress_cond = '%.4f_%.4f_%.4f_%.4f_%.4f_%.4f'%tuple(ns[1:7])
            if status < 20 or status > 200:
                dirname = os.path.join(self.runs_dir, 'ni-Rao-screw-%s-%.1f'%(strNxNyNz, status), 'stress_%s'%stress_cond)
                # if status in [17, 18]: dirname = os.path.join(dirname, 'T%dK')%ns[0]
                if status > 200 and atom_range != 'none':
                    dirname = os.path.join(dirname, 'state_%d')%atom_range[0]
                    if len(atom_range) == 3:
                        dirname = os.path.join(dirname, 'atom_range_%d_%d')%(atom_range[1], atom_range[2])
            else:
                dirname = os.path.join(self.runs_dir, 'ni-Rao-cs-%.1f_%d_%s'%(status, ns[0], stress_cond))
        else:
            raise ValueError('create_dirname: invalid status!')
        return dirname

    def readeam_ni_Rao(self):
        '''Read eam potential from Rao99
        '''
        potential_path = os.path.join(os.environ['MDPLUS_DIR'], 'potentials', 'EAMDATA', 'eamdata.Ni.Rao99')
        self.mdpp.cmd('potfile = %s'%potential_path)
        self.mdpp.cmd('eamgrid = 5000 readeam')
        self.mdpp.cmd('NNM = 800')
        self.mdpp.cmd('nspecies = 1 element0 = Ni atommass = 58.6934')

    def write_file(self, filename, format = default_output_format):
        '''Write current configuration into file
        '''
        if filename is None:
            return
        for ext in format:
            if ext == 'png':
                ovitos = os.path.join(os.environ['OVITOS_BIN'], 'ovitos')
                script = os.path.join(codedir, 'lammps2jpg.ovito.py')
                subprocess.run([ovitos, script, filename + '.LAMMPS.gz', filename + '.png', '--replace'])
            else:
                self.mdpp.cmd('writeall = 1 finalcnfile = %s.%s write%s'%(filename, ext, ext))

    def load_file(self, filename):
        '''Load a configuration into MD++, now only support reading .cn file
        '''
        base, ext = os.path.splitext(filename)
        if (ext == '.gz' and os.path.splitext(base)[1] == '.cn') or (ext == '.cn'):
            self.mdpp.cmd('incnfile = %s readcn'%filename)
        elif (ext == '.gz' and os.path.splitext(base)[1] == '.dump') or (ext == '.dump'):
            if ext == '.gz':
                f = gzip.open(filename, 'rb')
                for line in f:
                    linesplit = line.decode().split()
                    if linesplit == ['ITEM:', 'TIMESTEP']:
                        nframe = int(f.readline().decode().split()[0])
                    if linesplit[:2] == ['ITEM:', 'ATOMS']:
                        use_scale = 1 if 'xs' in linesplit else 0
                        break
            else:
                f = open(filename, 'r')
                for line in f:
                    linesplit = line.split()
                    if linesplit == ['ITEM:', 'TIMESTEP']:
                        nframe = int(f.readline().split()[0])
                    if linesplit[:2] == ['ITEM:', 'ATOMS']:
                        use_scale = 1 if 'xs' in linesplit else 0
                        break
            
            # print(nframe, use_scale)
            f.close()
            self.mdpp.cmd('incnfile = %s input = [ %d %d ] readLAMMPS'%(filename, nframe, use_scale))
        else:
            raise TypeError('load_file: only support .cn and .dump file!')
        # if ((ext == '.gz' and os.path.splitext(base) != '.cn') or 
        #     (ext != '.gz' and ext != '.cn')):
        #     raise TypeError('load_file: only support .cn file!')
        

    def relax_fixbox(self, ftol=1e-7, fevalmax=1000, itmax=1000, fixboxvec=None, filename = None, format = default_output_format):
        '''Conjugate-Gradient relaxation
        '''
        self.mdpp.cmd('conj_itmax = %e'%itmax)
        self.mdpp.cmd('conj_ftol = %e'%ftol)
        self.mdpp.cmd('conj_fevalmax = %d'%fevalmax)
        if fixboxvec is not None:
            self.mdpp.cmd('conj_fixboxvec = %s'%list_to_str(fixboxvec))
        self.mdpp.cmd('conj_fixbox = 1 relax')
        self.write_file(filename, format = format)
        self.mdpp.cmd('eval')   # evaluate the potential of relaxed configuration

    def create_perfect_crystal(self, crystalstructure, latticeconst, supercellsize, 
                               filename = None, format = default_output_format):
        ''' Create Perfect Lattice Configuration
        Parameters
        ----------
        crystalstructure : string
            Crystal structure to be created, must be one of the following:
            {'body-centered-cubic', 'face-centered-cubic', ...}
        latticeconst : float
            Lattice constant in Angstrom
        supercellsize : ndarray of shape (3, 3)
            supercellsize = [[a_1, a_2, a_3, repeat_a],
                             [b_1, b_2, b_3, repeat_b],
                             [c_1, c_2, c_3, repeat_c]]
            where [a_1, a_2, a_3] specifies the first repeat vector of the simulation
            supercell in Miller indices and repeat_a is the number of the repeats in
            that direction. [b_1, b_2, b_3] and [c_1, c_2, c_3] represent the second
            and third direction respectively, repeat repeat_b and repeat_c times.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''

        self.mdpp.cmd('crystalstructure = %s'%crystalstructure)
        self.mdpp.cmd('latticeconst = ' + str(latticeconst) + ' #(A)')
        if supercellsize.shape != (3, 4):
            raise ValueError('create_perfect_crystal: supercellsize must have shape of (3, 4)')
        supercell_size_str = list_to_str(supercellsize.flatten())
        self.mdpp.cmd('latticesize = %s makecrystal'%supercell_size_str)
        self.write_file(filename, format = format)
        self.mdpp.cmd('eval') # evaluate the potential of perfect crystal

    def create_dislocation_dipole(self, linedirection, separationdir, burgersvector, x0, y0, y1, nu,
                                  num_images=[-10, 10, -10, 10], tilt_box=True, sz_limit=None, store=False,
                                  filename = None, format = default_output_format):
        ''' Create Dislocation Dipole
        Parameters
        ----------
        linedirection : number
            dislocation line direction, 1,2,3 = x,y,z
        separationdir : number
            separation direction between two dislocations in the dipole, 1,2,3 = x,y,z
        burgersvector : ndarray of shape (3, )
            Burgers vector, [bx, by, bz] in scaled coordinates
        x0,y0,y1 : float
            (x,y) position of the dislocation dipole, (x0, y0) and (x0, y1)
        nu : float
            Poisson's ratio of the material
        num_images : list of 4 int numbers
            Number of periodic image copies to calculate displacement of atoms
        tilt_box : bool
            if simulation box is tilted to keep periodic boundary condition
        sz_limit : tuple of size 2
            (sz_min, sz_max) represents the limit of dislocation in z direction
        store : bool
            If True, only define dislocation but not displace atoms.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''
        if sz_limit:
            limit_sz = [1, sz_limit[0], sz_limit[1]]
        else:
            limit_sz = [0, 0, 0]
        dipole_input = [ linedirection, separationdir,
                         burgersvector[0], burgersvector[1], burgersvector[2],
                         x0, y0, y1, nu ] + num_images + [int(tilt_box), ] + limit_sz + [int(store), ]
        input_str = list_to_str(dipole_input)
        print('create_dislocation_dipole: input =', input_str)
        self.mdpp.cmd('input = %s  makedipole '%input_str)
        self.write_file(filename, format = format)
        if not store:
            self.mdpp.cmd('eval') # evaluate the potential of dislocation configuration

    def create_disl_polygon(self, burgersvector, xlist, ylist, zlist, nu, a, store=False, 
                            filename = None, format = default_output_format):
        ''' Create Dislocation Dipole
        Parameters
        ----------
        burgersvector : ndarray of shape (3, )
            Burgers vector, [bx, by, bz] in scaled coordinates
        xlist,ylist,zlist : 1d-ndarray of same length
            (x,y,z) positions of the dislocation polygon
        nu : float
            Poisson's ratio of the material
        a : float
            a length scale (in Å) for the Burgers vector
        store : bool
            If True, only define dislocation but not displace atoms.
        filename : string, optional
            The file name prefix to save the perfect crystal file.
        format : list of strings
            MD++ output formats, refer to [http://micro.stanford.edu/MDpp/entries?search=write] ,
            by default is defined in default_output_format
        '''
        if len(xlist) != len(ylist) or len(ylist) != len(zlist) or len(xlist) != len(zlist):
            raise ValueError('create_disl_polygon: xlist, ylist, zlist size not consistent!')
        xyzlist = np.stack([xlist, ylist, zlist], axis=-1).flatten()
        dislpolygon_input = [ 1, int(store), nu, a, burgersvector[0], burgersvector[1], burgersvector[2],
                              len(xlist)] + xyzlist.tolist()
        input_str = list_to_str(dislpolygon_input)
        print('create_disl_polygon: input =', input_str)
        self.mdpp.cmd('input = %s  makedislpolygon'%input_str)
        self.write_file(filename, format = format)
        if not store:
            self.mdpp.cmd('eval') # evaluate the potential of dislocation configuration

#*******************************************
# Definition of procedures
#*******************************************

    def make_perfect_crystal(self, savefile = None, nu = None, a = None, create_free_surface = True, translation_symmetry_map = None):
        if nu is None: nu = self.nu
        if a is None: a = self.a
        supercell_size = np.array([[ 1, -1,  0,  self.Nx],
                                   [ 1,  1,  1,  self.Ny],
                                   [-1, -1,  2,  self.Nz]
                                  ])
        self.create_perfect_crystal('face-centered-cubic', self.a, supercell_size)
        if savefile is not None:
            self.write_file(savefile)
        
        # remove top and bottom layers of atoms
        if create_free_surface:
            self.mdpp.cmd('input = [ 1 -1 1 -1 -0.40 -1 1 ] fixatoms_by_position')
            self.mdpp.cmd('input = [ 1 -1 1  0.40  1 -1 1 ] fixatoms_by_position')
            self.mdpp.cmd('removefixedatoms')
            self.mdpp.cmd('input = [ 10 ] setfixedatomsgroup')
            self.mdpp.cmd('freeallatoms')
            self.mdpp.cmd('eval')
            if savefile is not None:
                self.write_file(savefile+'_free_surf')

        # Obtain the translational symmetry map of the perfect crystal: R = R[shift_ind]
        if translation_symmetry_map is not None:
            if translation_symmetry_map != 'x':
                raise NotImplementedError('Only x-direction translation is supported')
            else:
                print('generating translational symmetry shift index mapping...')
                shift_unit = 1.0 / self.Nx / 2.
                self.mdpp.cmd('eval RHtoS')
                NP = self.mdpp.get('NP')
                S0 = np.reshape(self.mdpp.get_array('SR', 0, NP*3), (NP, 3)) + 1e-6
                S1 = S0 + np.array([shift_unit, 0, 0])
                S0 = S0 - np.round(S0)
                S1 = S1 - np.round(S1)
                pos0 = np.zeros(NP, dtype=[('x', float), ('y', float), ('z', float)])
                pos1 = np.zeros(NP, dtype=[('x', float), ('y', float), ('z', float)])
                pos0['x'], pos0['y'], pos0['z'] = S0[:, 0], S0[:, 1], S0[:, 2]
                pos1['x'], pos1['y'], pos1['z'] = S1[:, 0], S1[:, 1], S1[:, 2]
                ind0 = np.argsort(pos0, order=['x', 'y', 'z'])
                ind1 = np.argsort(pos1, order=['x', 'y', 'z'])
                shift_fwd = np.zeros(NP, dtype=int)
                shift_fwd[ind1] = ind0
                shift_bwd = np.zeros(NP, dtype=int)
                shift_bwd[ind0] = ind1
                pos_error = np.linalg.norm(S0[shift_fwd] - S1, axis=-1).max()
                if pos_error > 1e-10:
                    raise ValueError('error is %.8e, shift map not correct!'%(pos_error))
                else:
                    print('shift map error is %.8e, shift map saved in %s'%(pos_error, os.path.join(self.dirname, 'shift_map.txt')))
                    np.savetxt('shift_map.txt', np.stack([np.arange(NP), shift_fwd, shift_bwd], axis=-1), fmt='%8d')

    def make_screw_dipole(self, store, nu = None):
        if nu is None: nu = 0.305

        # create a screw dislocation at the center with full burger's vector
        line_direction = 1                 # x, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole
        bs = [1.0/(2*self.Nx), 0, 0]       # burger's vector in scaled coordinates

        # position of the dislocation dipole in y direction
        y0 = 0.111/self.Nx
        dz = 0.125/(self.Ny)
        # position of the dislocation dipole in z direction
        z0 = -0.50 + dz
        z1 =  0.00 + dz

        self.create_dislocation_dipole(line_direction, separation_dir, bs, y0, z0, z1, nu, store=store)
    
    def make_glide_partials(self, store, Fleischer=False, theta=0, xrange=[1., 1., -1., -1.], nu=None, a=None, d=None):
        Lx = self.mdpp.get('H_11')
        Ly = self.mdpp.get('H_22')
        Lz = self.mdpp.get('H_33')
        br = np.array([np.sqrt(2)/(2*2), 0.0, 0.0])  # burger's vector in real coordinates in units of lattice constant a
        if d is None: d = self.d
        if nu is None: nu = self.nu
        if a is None: a = self.a

        xlist = np.array(xrange) * Lx
        y0 = Ly*0.125/self.Ny
        # define the rotation of dislocation in x-direction by theta
        yshift = np.array([ d*np.sin(theta)/2, 0, 0, d*np.sin(theta)/2 ])
        zshift = np.array([ d*np.cos(theta)/2, 0, 0, d*np.cos(theta)/2 ])
        if Fleischer:
            # create the dislocation polygon in half-width with 1/2 burger's vector
            self.create_disl_polygon(br, xlist, y0+yshift*2, zshift*2, nu, a, store=store)
        else:
            # create the dislocation polygon in half-width with 1/2 burger's vector
            self.create_disl_polygon(br, xlist, y0+yshift, zshift, nu, a, store=True)
            # create the dislocation polygon for the other half-width with 1/2 burger's vector (FE mechanism only, total width is d)
            self.create_disl_polygon(br, xlist, y0-yshift,-zshift, nu, a, store=store)

    def make_cross_slip(self, store, Fleischer=False, theta=0, nu=None, a=None, d=None):
        if Fleischer:
            if theta == np.arccos(1./3.):   # acute cross-slip
                self.make_glide_partials(store=True, xrange=[1., 1., -1., -1.], Fleischer=True, theta=0, nu=nu, a=a, d=d)
                self.make_glide_partials(store=store, xrange=[1./4, 1./4, -1./4, -1./4], Fleischer=True, theta=theta, nu=nu, a=a, d=d)
            else:                           # obtuse cross-slip
                self.make_glide_partials(store=True, xrange=[1., 1., -1., -1.], Fleischer=True, theta=0, nu=nu, a=a, d=self.d/2)
                self.make_glide_partials(store=store, xrange=[1./4, 1./4, -1./4, -1./4], Fleischer=True, theta=theta, nu=nu, a=a, d=d)
        else:
            self.make_glide_partials(store=True, xrange=[1., 1., 1./4, 1./4], Fleischer=False, theta=0, nu=nu, a=a, d=d)
            self.make_glide_partials(store=True, xrange=[-1./4, -1./4, -1., -1.], Fleischer=False, theta=0, nu=nu, a=a, d=d)
            self.make_glide_partials(store=store, xrange=[1./4, 1./4, -1./4, -1./4], Fleischer=False, theta=theta, nu=nu, a=a, d=d)

    def group_from_list(self, id_list, group):
        for i in id_list:
            self.mdpp.cmd(r'group(%d) = %d'%(i+1, group))

    def load_init_file(self, filename, useHfromA = False, def_subsurface_region = False):
        '''load state A or state B' into MD++, and calculate vacuum ratio if needed
        '''
        offset = self.offset
        if useHfromA:
            # Read state A (for its box and surrounding atoms, save coordinates to R0)
            # use new A (intermediate configuration)
            stateAfile = os.path.join(self.create_dirname(status = 5 + offset, ns = self.ns), 'ni-screw-gp-relaxed.cn')
            self.load_file(stateAfile)
            self.mdpp.cmd('saveH SHtoR RtoR0')

        self.load_file(filename)
        if useHfromA:
            # Read state B' (for the atoms around the dislocation)
            # Copy the atoms near dislocation to state A environment
            self.mdpp.cmd('restoreH')
            if offset == 0.5:  # larger cylinder cut for Fleischer
                self.mdpp.cmd('freeallatoms input = [ 1 2 0 0 40 1 -10 10 ]')
            else:
                self.mdpp.cmd('freeallatoms input = [ 1 2 0 0 20 1 -10 10 ]')
            self.mdpp.cmd('makecylinder input = 3 setfixedatomsgroup')
            self.mdpp.cmd('freeallatoms')
            self.mdpp.cmd('SHtoR input = [ 1  3 ] R0toR_by_group RHtoS')
        if def_subsurface_region:
            self.group_from_list(self.subsurface, 3)
            self.group_from_list(self.layer_above, 4)
            self.group_from_list(self.layer_below, 5)
            self.group_from_list(self.upper_layer, 6)
            self.group_from_list(self.lower_layer, 7)

    def get_state_relax_iter(self, save_iter_data, props=None, separate=False):
        '''Obtain energy and stress information during state A relaxation
        '''
        iter_data = np.loadtxt(save_iter_data)
        if iter_data.size == 0:
            raise ValueError('get_state_relax_iter: %s has no content'%save_iter_data)
        if len(iter_data.shape) == 1:
            iter_data = iter_data.reshape(1, -1)
        if props is not None:
            loc = 0; iter_data_sep = []
            for i in range(len(props)):
                iter_data_sep.append(iter_data[:, loc:(loc+len(props[i]))])
                loc += len(props[i])
            iter_data = np.hstack(iter_data_sep)
        if separate:
            return iter_data_sep
        else:
            return iter_data

    def get_MEP_curves(self, status, ns, verbose=False, curr=False, overwrite=True):
        '''Obtain minimum energy path (MEP)
        '''
        if status in [105, 112.0, 112.5]:
            dirname   = self.create_dirname(status = status, ns = ns)
            MEPdatafile = os.path.join(dirname, 'stringeng.npz')
            if overwrite or (not os.path.exists(MEPdatafile)):
                if status == 105:
                    curr_file = os.path.join(dirname, 'nebeng.out')
                else:
                    curr_file = os.path.join(dirname, 'stringeng.out')
                datafiles = sorted(glob.glob(os.path.join(dirname, 'stringeng_step*.out')))
                datafiles = [os.path.join(dirname, 'stringeng_step%d.out')%(ifile+1) for ifile in range(len(datafiles))]
                MEP_data  = None
                if os.path.exists(curr_file) and curr:
                    if not (len(datafiles) > 0 and filecmp.cmp(curr_file, datafiles[-1], shallow=False)):
                        datafiles.append(curr_file)
                for file in datafiles:
                    if verbose:
                        print('processing... %s'%file)
                    rawdata = np.loadtxt(file)
                    if len(rawdata.shape) == 1:
                        rawdata = rawdata.reshape(1, -1)
                    Ec0 = rawdata[:, -2:-1]; E0 = rawdata[:, -1:]
                    if status == 105:
                        MEP_curve = rawdata[:, 2:-2:6] + Ec0 - E0
                    else:
                        MEP_curve = rawdata[:, 2:-2:5] + Ec0 - E0
                    if MEP_data is None:
                        MEP_data = MEP_curve
                    else:
                        if MEP_curve.shape[1] != MEP_data.shape[1]: # if the file is nebeng format
                            MEP_curve = rawdata[:, 2:-2:6] + Ec0 - E0
                        MEP_data = np.vstack([MEP_data, MEP_curve])
                if MEP_data is not None:
                    np.savez_compressed(MEPdatafile, MEP_data=MEP_data)
            else:
                MEP_data = np.load(MEPdatafile)['MEP_data']
            return MEP_data
        else:
            raise ValueError('Invalid status, must be 112 or 112.5')

    def save_nebchain(self, status, ns, steps=None, Echainfile='Echain.txt', replace=False, atom_range=None, get_subsurface_stress=False):
        '''Convert stringrelax.chain.cn files to MD configuration files (cn and lammps)
        '''
        dirname = self.create_dirname(status, ns)
        stateAfile = os.path.join(dirname, 'NEBinit.cn')
        n_stringrelax_steps  = len(glob.glob(os.path.join(dirname, 'stringeng_step*.out')))
        if self.mdpp is None:
            raise TypeError('nebchain2lammps: You must use the cross_slip_util instance with mdpp set up!')

        sxx = sxy = 0; offset = status - int(status)
        nstep, syz, szz, syy, sxz = ns[0:5]
        if len(ns) > 5: sxx, sxy = ns[5:7]
        self.load_init_file(stateAfile)
        self.apply_force_on_surface(sigma_y = [sxy, syy, syz], define_surface=True)
        if get_subsurface_stress: self.def_subsurface_region()
        nparticles = self.mdpp.get('NP')
        rA = np.reshape(self.mdpp.get_array('R', 0, nparticles*3), (nparticles, 3))
        E0 = self.mdpp.get('EPOT')
        print('E0 = %.17e'%E0)

        if steps is None: steps = list(range(0, n_stringrelax_steps + 1))
        for step in steps:
            if step > n_stringrelax_steps:
                print('stringrelax.chain_step%d.cn does not exist!'%step)
                continue
            chainsfile = os.path.join(dirname, 'stringrelax.chain_step%d.cn')%step
            chainsdir = os.path.join(dirname, 'stringrelax.chain_step%d')%step
            if step == 0:
                chainsfile = os.path.join(dirname, 'stringrelax.chain.cn')
                chainsdir = os.path.join(dirname, 'stringrelax.chain')
            os.makedirs(chainsdir, exist_ok=True)
            nchain    = len(glob.glob(chainsfile + '.cpu??'))
            if not replace and len(glob.glob(os.path.join(chainsdir, 'chain.??.png'))) == nchain: # and step < n_stringrelax_steps:
                print('%s conversion finished'%chainsfile)
                continue
            print('converting %s'%chainsfile)
            
            # Read configurations of the constrain atoms
            with open(chainsfile + '.cpu00', 'r') as f:
                nchain_ref = int(f.readline()) + 1
                nfixed = int(f.readline())
            if nchain_ref != nchain:
                raise ValueError('Parallel file number inconsistent with core number')
            constrain_id = np.genfromtxt(chainsfile + '.cpu00', skip_header=2, max_rows=nfixed, dtype=int)
            if get_subsurface_stress:
                schain = np.zeros([nchain, 6])
                props = ['s_sub_%s'%(k) for k in ['xx', 'yy', 'zz', 'xy', 'yz', 'xz']]
            Echain = np.ones(nchain)*E0
            if atom_range is None: atom_range = list(range(nchain))
            for i in atom_range:
                fchain = chainsfile + '.cpu%02d'%i
                print('loading... %s'%fchain)
                constrain_rs = np.genfromtxt(fchain, skip_header=2+nfixed)[..., :3]
                rchain = rA.copy()
                rchain[constrain_id, :] = constrain_rs
                self.mdpp.set_array(rchain.flatten().tolist(), 'R', 0)
                self.mdpp.cmd('RHtoS clearR0 eval')
                if get_subsurface_stress:
                    for j in range(len(props)):
                        schain[i, j] = self.get_subsurface_stress(props[j], verbose=False)
                Echain[i] = self.mdpp.get('EPOT')
                print(Echain[i])
                savefile = os.path.join(chainsdir, 'chain.%02d')%i
                print('saving... %s'%(savefile+'.cn.gz'))
                self.write_file(savefile, format = ['LAMMPS', 'cn', 'FORCE'])

            print('chain length: %d'%nchain)
            print('# of constrain atom: %d'%nfixed)
            
            # Save energy of the chain to file
            if Echainfile is not None:
                print('E0 = %.17e'%E0)
                print('Echain - E0 = ', Echain - E0)
                if get_subsurface_stress:
                    np.savetxt(os.path.join(chainsdir, Echainfile), 
                               np.vstack([Echain-E0, schain, np.ones(nchain)*E0]).T,
                               header=' '.join(['#', 'Echain-E0'] + props + ['E0',]))
                else:
                    np.savetxt(os.path.join(chainsdir, Echainfile), np.append(Echain - E0, E0))

    def def_subsurface_region(self, perf_file = None, threshold = [-0.195, 0.195], verbose=True,
                              threshold_x = None, threshold_z = None, dx = None, dz = None):
        if perf_file is None:
            status = self.status - int(self.status)
            perf_file = os.path.join(self.create_dirname(status = status), 'perf_free_surf.cn')

        self.load_file(perf_file)
        self.NPperf = self.mdpp.get('NP')
        position = np.reshape(self.mdpp.get_array('SR', 0, 3*self.NPperf), (self.NPperf, 3))
        
        dy = 1/(self.Ny * 3)               # [111] direction has 3 layers
        
        self.layer_above, = np.nonzero(np.logical_and(position[:, 1] > threshold[1], position[:, 1] < threshold[1] + dy))
        self.layer_below, = np.nonzero(np.logical_and(position[:, 1] < threshold[0], position[:, 1] > threshold[0] - dy))
        self.upper_layer, = np.nonzero(np.logical_and(position[:, 1] > threshold[1] - dy, position[:, 1] < threshold[1]))
        self.lower_layer, = np.nonzero(np.logical_and(position[:, 1] < threshold[0] + dy, position[:, 1] > threshold[0]))
        self.subsurface,  = np.nonzero(np.logical_and(position[:, 1] > threshold[0], position[:, 1] < threshold[1]))

        if threshold_x is not None:
            if dx is None: dx = 1/(self.Nx * 4)
            self.left_layer, = np.nonzero(np.logical_and(position[:, 0]>threshold_x[1]-dx, position[:, 0]<threshold_x[1]+dx))
            self.right_layer,= np.nonzero(np.logical_and(position[:, 0]<threshold_x[0]+dx, position[:, 0]>threshold_x[0]-dx))

        if threshold_z is not None:
            if dz is None: dz = 1/(self.Nz * 12)
            self.front_layer,=np.nonzero(np.logical_and(position[:, 2]>threshold_z[1]-3*dz, position[:, 2]<threshold_z[1]+3*dz))
            self.back_layer, =np.nonzero(np.logical_and(position[:, 2]<threshold_z[0]+3*dz, position[:, 2]>threshold_z[0]-3*dz))

        if verbose:
            print('subsurface :', self.subsurface.size,  position[self.subsurface, 1].mean())
            print('layer_above:', self.layer_above.size, position[self.layer_above, 1].mean())
            print('upper_layer:', self.upper_layer.size, position[self.upper_layer, 1].mean())
            print('lower_layer:', self.lower_layer.size, position[self.lower_layer, 1].mean())
            print('layer_below:', self.layer_below.size, position[self.layer_below, 1].mean())

    def def_core_region(self, rcut=None, verbose=True, rcut_file=None):
        if rcut is None:
            rcut = max(self.mdpp.get('H_22')/2, self.mdpp.get('H_33')/2)
            print('rcut not specified, default value = %.4f'%rcut)
        self.NPperf = self.mdpp.get('NP')
        self.mdpp.cmd('SHtoR')
        position = np.reshape(self.mdpp.get_array('R', 0, 3*self.NPperf), (self.NPperf, 3))
        pos2 = position**2
        core_region, = np.nonzero(pos2[:, 1] + pos2[:, 2] < rcut**2)
        if rcut_file is not None:
            np.savetxt(rcut_file, core_region, fmt='%d')
            print('save core region file %s'%rcut_file)
        if verbose:
            print('core region:', core_region.size)
        return core_region
    
    def get_subsurface_stress(self, stress_str, threshold=None, verbose=True, kinetic=False):
        if stress_str[:5] != 's_sub':
            raise ValueError('Invalid stress component %s'%stress_str)
        idir     = {'x': 0, 'y': 1, 'z': 2}
        NP = self.mdpp.get('NP')
        position = np.reshape(self.mdpp.get_array('SR', 0, 3*NP), (-1, 3))
        virial   = np.reshape(self.mdpp.get_array('VIRIAL_IND', 0, 9*NP), (-1, 3, 3))
        if kinetic:
            atommass = default_atommass
            timestep = self.mdpp.get('timestep') * 1e-12 # s
            if verbose: print(NP, atommass, timestep)
            velocity = np.reshape(self.mdpp.get_array('VR', 0, 3*NP), (-1, 3)) / timestep * 1e-10 # m/s
            pstress  = velocity[:, :, None] * velocity[:, None, :] * atommass * 1e-3 / NA * 6.241509e18 # eV
            pstress  = (np.swapaxes(pstress, -1, -2) + pstress)/2
        else:
            pstress  = np.zeros_like(virial)

        if threshold is None:
            v_sub = np.sum(virial[self.subsurface, idir[stress_str[-2]], idir[stress_str[-1]]])
            p_sub = np.sum(pstress[self.subsurface, idir[stress_str[-2]], idir[stress_str[-1]]])
            y_layer_above = position[self.layer_above, 1].mean()
            y_layer_below = position[self.layer_below, 1].mean()
            y_lower_layer = position[self.lower_layer, 1].mean()
            y_upper_layer = position[self.upper_layer, 1].mean()
        else:
            threshold= np.array(threshold) * np.ptp(position[:, 1])
            ind      = np.logical_and(threshold[0] < position[:, 1], position[:, 1] < threshold[1])
            v_sub    = np.sum(virial[ind, idir[stress_str[-2]], idir[stress_str[-1]]])
            p_sub    = np.sum(pstress[ind, idir[stress_str[-2]], idir[stress_str[-1]]])
            y_layer_above = position[position[:, 1] > threshold[1], 1].min()
            y_layer_below = position[position[:, 1] < threshold[0], 1].max()
            y_upper_layer = position[ind, 1].max()
            y_lower_layer = position[ind, 1].min()

        vacuumratio = 1 - (np.abs(y_upper_layer - y_layer_below) + np.abs(y_layer_above - y_lower_layer))/2.0
        if verbose:
            print('y_upper_layer - y_layer_below = %f - %f = %f'%(y_upper_layer, y_layer_below, y_upper_layer - y_layer_below))
            print('y_layer_above - y_lower_layer = %f - %f = %f'%(y_layer_above, y_lower_layer, y_layer_above - y_lower_layer))
            print('vacuumratio = %f'%vacuumratio)
        OMEGA = self.mdpp.get('H_11')*self.mdpp.get('H_22')*self.mdpp.get('H_33')
        return (v_sub+p_sub)/(OMEGA*(1-vacuumratio))/energy_unit_conversion # in MPa

    def get_current_state(self, props, save_iter_data=None, verbose=True, separate=True):
        '''Read current state of the configuration
        '''
        rawdata = []
        for i in range(len(props)):
            prop = props[i]
            rawdata_item = []
            for j in range(len(prop)):
                if prop[j][:5] == 's_sub':
                    rawdata_item.append(self.get_subsurface_stress(prop[j], verbose=verbose))
                else:
                    rawdata_item.append(self.mdpp.get(prop[j]))
            rawdata.append(rawdata_item)
            if verbose:
                print('Property ', prop, ':')
                print(rawdata[-1])
        iter_curr = np.hstack(rawdata)
        if save_iter_data is not None:
            prop_list = tuple(itertools.chain.from_iterable(props))
            if os.path.exists(save_iter_data):
                iter_data = self.get_state_relax_iter(save_iter_data, props=props, separate=False)
                iter_data = np.vstack([iter_data, iter_curr])
            else:
                iter_data = iter_curr
            np.savetxt(save_iter_data, iter_data, fmt='%23.17e', header=('%23s'*len(prop_list))%prop_list)
        if separate:
            return tuple(rawdata)
        else:
            return iter_curr

    def changeH_to_relax_stress(self, sig_ref, verbose=True,
                                changeH_components=['H_11', 'H_22', 'H_33', 'H_12', 'H_23', 'H_13'],
                                changeH_base = ['H_11', 'H_22', 'H_33', 'H_22', 'H_33', 'H_33'],
                                changeH_back = ['H_12', 'H_22', 'H_23']):
        modulus = np.array(default_modulus)
        t = 's_sub' #'TSTRESSinMPa'
        stress_components = ['%s_xx'%t, '%s_yy'%t, '%s_zz'%t, '%s_xy'%t, '%s_yz'%t, '%s_xz'%t]
        sig, Hcurr, Hback, Hbase = self.get_current_state(props=[stress_components, changeH_components, changeH_back, changeH_base], verbose=False, separate=True)
        dsig  = np.array(sig) - np.array(sig_ref)
        eps   = dsig/modulus
        Hnext = np.array(Hcurr) + np.array(Hbase)*eps

        changeH_keepS = ' '.join(['%s = '%changeH_components[i] + '%.17e'%Hnext[i] for i in range(len(Hnext))])
        if verbose:
            print('box size modified to:', changeH_keepS)
        self.mdpp.cmd(changeH_keepS + ' eval SHtoR')
        changeH_keepR = ' '.join(['%s = '%changeH_back[i] + '%.17e'%Hback[i] for i in range(len(Hback))])
        if verbose:
            print('box size modified back to:', changeH_keepR)
        self.mdpp.cmd(changeH_keepR + ' RHtoS eval')

    def apply_force_on_surface(self, sigma_y = [0, 0, 0], define_surface=False, verbose=True):
        if define_surface:
            NP = self.mdpp.get('NP')
            # Calculate Centrosymmetry parameter
            self.mdpp.cmd('plot_color_axis = 2 eval')
            # Select top surface atoms
            self.mdpp.cmd('input = [ 1 -10 10 0.2 10 -10 10 10 100 ] fixatoms_by_pos_topol')
            self.mdpp.cmd('input = 1 setfixedatomsgroup')
            N1 = self.mdpp.get('NPfixed')
            self.top_surface, = np.nonzero(self.mdpp.get_array('fixed', 0, NP))
            self.mdpp.cmd('freeallatoms')
            # Select bottom surface atoms
            self.mdpp.cmd('input = [ 1 -10 10 -10 -0.2 -10 10 10 100 ] fixatoms_by_pos_topol')
            self.mdpp.cmd('input = 2 setfixedatomsgroup')
            N2 = self.mdpp.get('NPfixed')
            self.bot_surface, = np.nonzero(self.mdpp.get_array('fixed', 0, NP))
            self.mdpp.cmd('freeallatoms')
            self.N1, self.N2 = N1, N2
        else:
            N1, N2 = self.N1, self.N2
            
        # The number of atoms of the surface atom groups
        if verbose:
            print('Ntop = %d (should be 4800)'%self.top_surface.size)
            print('Nbot = %d (should be 4800)'%self.bot_surface.size)

        Axz = self.mdpp.get('H_11')*self.mdpp.get('H_33')
        
        f1 = -np.array(sigma_y)*Axz/N1*energy_unit_conversion
        f2 =  np.array(sigma_y)*Axz/N2*energy_unit_conversion
        ftot = [f1, f2]  # forces on top and bottom surfaces
        flist = np.append([len(ftot), ], ftot).tolist()

        self.mdpp.cmd('extforce = %s eval'%list_to_str(flist))
        
    def cgrelax_stress(self, initfile=None, useHfromA = False, def_subsurface_region = False,
                       cg_niter=100, cg_ftol=1e-4, cg_fevalmax=1000, cg_Etol=None, 
                       apply_force=True, changeH=True, 
                       changeH_components=['H_11', 'H_22', 'H_33', 'H_12', 'H_32', 'H_13'],
                       changeH_base = ['H_11', 'H_22', 'H_33', 'H_22', 'H_22', 'H_33'],
                       changeH_back = ['H_12', 'H_22', 'H_32'],
                       sigma=None, sigma_tol = None, verbose = True, PID_params=None,
                       fname=None, save_iter_data=None, saveformat=default_output_format, debug_output=debug_output):
        '''Relax configuration using Conjugate-Gradient algorithm
            If given apply_force and changeH, the relaxation is under stress tensor sigma (Voigt notation):
                Schmid stress on glide plane:      sxy (must be 0)
                Escaig stress on glide plane:      syz (applied by Fext)
                Escaig stress on cross slip plane: szz (applied by H)
                Escaig stress on cross slip plane: syy (applied by Fext)
                Schmid stress on cross slip plane: sxz (applied by H)
            the intermediate data will be saved in `save_iter_data` file
        '''
        np.set_printoptions(suppress=True)
        if def_subsurface_region:
            self.def_subsurface_region()
            t = 's_sub'
        else:
            t = 'TSTRESSinMPa'
        props   = [['EPOT', ], 
                   ['%s_xx'%t, '%s_yy'%t, '%s_zz'%t, '%s_xy'%t, '%s_yz'%t, '%s_xz'%t],
                   changeH_components]
        # Initialization
        apply_force = apply_force and (sigma is not None)
        changeH     = changeH and (sigma_tol is not None)
        if initfile is not None:
            self.load_init_file(initfile, useHfromA, def_subsurface_region)
        if (save_iter_data is not None) and (os.path.dirname(save_iter_data) == ''):
            save_iter_data = os.path.join(self.dirname, save_iter_data)
            if os.path.exists(save_iter_data):
                iter_data = self.get_state_relax_iter(save_iter_data, props=props, separate=False)
                n_start   = iter_data.shape[0]
            else:
                n_start   = 0

        if apply_force:
            sxx_ref, syy_ref, szz_ref, sxy_ref, syz_ref, sxz_ref = sigma
            syadd = [sxy_ref, syy_ref, syz_ref]
            self.apply_force_on_surface(sigma_y = syadd, define_surface = True)

        # Relax configuration
        ftol = cg_ftol
        fevalmax = cg_fevalmax
        for i in range(cg_niter):
            if verbose:
                print('iter %d:'%i)
            Epot, sig, _ = self.get_current_state(props, save_iter_data=save_iter_data, separate=True, verbose=verbose)
            stress_converged = (sigma_tol is not None) and (np.abs(np.array(sig) - np.array(sigma)).max() < sigma_tol)
            # Change H to satisfy stress components, apply force to satisfy y direction stress components
            if changeH and (not stress_converged):
                self.changeH_to_relax_stress(sigma, changeH_components=changeH_components, changeH_base=changeH_base, changeH_back=changeH_back)
                if apply_force:
                    syadd = [sxy_ref, syy_ref, syz_ref]
                    self.apply_force_on_surface(sigma_y = syadd, define_surface = False)

            # For the first half of the iterations, use looser tolerance for adjusting H (5 and 5.5 only)
            ftol = 1.0 if i + n_start < 10 and int(self.status) == 5 else cg_ftol
            # Fixbox relaxation and save configuration
            filename = '%s-relax-%i'%(fname, i) if (fname is not None) else None
            self.relax_fixbox(ftol=ftol, fevalmax=fevalmax, filename=filename, format=saveformat)
            energy_converged = (cg_Etol   is not None) and (np.abs(1 - Epot[0]/self.mdpp.get('EPOT')) < cg_Etol)

            # Check if the stress and energy tolerance is reached
            if verbose:
                print('dE = %23.17eeV'% (self.mdpp.get('EPOT') - Epot[0]))
                if sigma is not None:
                    print('ds(xx, yy, zz, xy, yz, xz) =', np.array(sig) - np.array(sigma))
            if (stress_converged and energy_converged and ftol == cg_ftol):
                break

        # Save final configuration and status
        self.write_file(filename='%s-relaxed'%fname, format=default_output_format)
        Epot, sig, _ = self.get_current_state(props, save_iter_data=save_iter_data, separate=True, verbose=verbose)
        if verbose:
            print('final:')
            print('dE = %23.17eeV'% (self.mdpp.get('EPOT') - Epot[0]))
            if sigma is not None:
                print('ds(xx, yy, zz, xy, yz, xz) =', np.array(sig) - np.array(sigma))

    def dimer_RtoF(self, dimer, verbose=False, trueEF=True):
        R1, R2 = dimer['R1'], dimer['R2']
        dR = np.linalg.norm(R1 - R2)/2
        R  = (R1 + R2)/2
        N  = (R1 - R2).flatten()/np.linalg.norm(R1 - R2)
        self.mdpp.set_array(R1.flatten().tolist(), 'R', 0); self.mdpp.cmd('RHtoS eval')
        NP = self.mdpp.get('NP')
        E1 = self.mdpp.get('EPOT')
        F1 = np.reshape(self.mdpp.get_array('F', 0, NP*3), (NP*3, ))
        self.mdpp.set_array(R2.flatten().tolist(), 'R', 0); self.mdpp.cmd('RHtoS eval')
        E2 = self.mdpp.get('EPOT')
        F2 = np.reshape(self.mdpp.get_array('F', 0, NP*3), (NP*3, ))
        if trueEF:
            dimer['R'] = R
            dimer = self.dimer_trueFR(dimer)
            E0 = dimer['trueER']
            FR = dimer['trueFR']
        else:
            E0 = (E1 + E2)/2 + dR/4*(F1 - F2).dot(N)
            FR = (F1 + F2)/2
        F1par = N*F1.dot(N); F1per = F1 - F1par
        F2par = N*F2.dot(N); F2per = F2 - F2par
        # FRpar = N*FR.dot(N) # F1par + F2par
        # FRper = FR - FRpar
        FRpar = (F1par + F2par)/2
        FRper = F1per - F2per
        T  = FRper/np.linalg.norm(FRper)
        Frot = np.linalg.norm(FRper)/dR
        C  = np.dot(F2 - F1, N)/2/dR
        if verbose:
            print(r'|R1| = %12.8e, E1 = %12.8f, |F1| = %12.8e'%(np.linalg.norm(R1), E1, np.linalg.norm(F1)))
            print(r'|R2| = %12.8e, E2 = %12.8f, |F2| = %12.8e'%(np.linalg.norm(R2), E2, np.linalg.norm(F2)))
            print(r'|R|  = %12.8e, ER = %12.8f, |FR| = %12.8e'%(np.linalg.norm(R),  E0, np.linalg.norm(FR)))
            print(r'E0 = %8.4f, Fper = %12.8e, Frot = %12.8e'%(E0, np.linalg.norm(FRper), Frot))
        return dict(R1=R1, R2=R2, R=R, dR=dR, N=N, T=T, E1=E1, E2=E2, F1=F1, F2=F2, FR=FR, Frot=Frot,
                F1par=F1par, F1per=F1per, F2par=F2par, F2per=F2per, FRpar=FRpar, FRper=FRper,
                E0=E0, C=C)

    def dimer_Rrot(self, dtheta, dimer, verbose=False):
        # Rotate dimer by dtheta (radian)
        ct, st = np.cos(dtheta), np.sin(dtheta)
        R1s = dimer['R'] + (dimer['N']*ct + dimer['T']*st)*dimer['dR']
        R2s = dimer['R']*2 - R1s
        return self.dimer_RtoF(dict(R1=R1s, R2=R2s), verbose=verbose)

    def dimer_Fdag(self, dimer):
        # Calculate translational force (F^{\textdagger} in Henkelman et al., 1999)
        if dimer['C'] > 0:
            dimer['Fdag'] = -dimer['FRpar']
        else:
            dimer['Fdag'] = dimer['FR'] - 2*dimer['FRpar']
        return dimer
    
    def dimer_minimizeFrot(self, dimer, epsilon=1e-7, verbose=False, estimate=True):
        if estimate:
            rotdimer = self.dimer_Rrot(epsilon, dimer, verbose=verbose)
            Fprime = (rotdimer['Frot'] - dimer['Frot'])/epsilon
            Dt = -1./2*np.arctan(2*dimer['Frot']/Fprime)
            if verbose: print('Fprime = %.8e, minimizer Dt = %.8e'%(Fprime, Dt))
        else:
            Frot = lambda dt: np.abs(self.dimer_Rrot(dt, dimer)['Frot'])
            Dt = minimize(Frot, 0.0, method='Powell').x[0]
            if verbose: print('minimizer Dt = %.8e'%(Dt))
        return Dt

    def dimer_trueFR(self, dimer):
        self.mdpp.set_array(dimer['R'].flatten().tolist(), 'R', 0); self.mdpp.cmd('RHtoS eval')
        NP = self.mdpp.get('NP')
        dimer['trueER'] = self.mdpp.get('EPOT')
        dimer['trueFR'] = np.reshape(self.mdpp.get_array('F', 0, NP*3), (NP*3, ))
        return dimer

    def get_nve(self):
        self.mdpp.cmd('eval ')
        NP   = self.mdpp.get('NP')
        V    = self.mdpp.get('OMEGA')
        EPOT = self.mdpp.get('EPOT')
        a    = np.cbrt((V/NP)*default_natom_per_unitcell)
        Ecoh = EPOT / NP
        return NP, V, EPOT, a, Ecoh

    def run_setup(self, savepropfreq=100):
        self.mdpp.cmd('equilsteps=0 timestep=0.0001 DOUBLE_T=0 savecn=1 savecnfreq=999999 saveprop=1 savepropfreq=%d'%savepropfreq)
        self.mdpp.cmd('outpropfile = prop.out')
        self.mdpp.cmd('intercnfile = inter.cn')
        self.mdpp.cmd('openpropfile openintercnfile wallmass=2e3 vt2=2e28 boxdamp=1e-3 saveH')
        