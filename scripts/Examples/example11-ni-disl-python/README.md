### MD simulation of dislocation cross-slip in FCC Ni

This subfolder of Python scripts is written to provide the general workflow and simulation data for the following articles:

1. Kuykendall, W. P., Wang, Y. & Cai, W. Stress effects on the energy barrier and mechanisms of cross-slip in FCC nickel. Journal of the Mechanics and Physics of Solids 144, 104105 (2020). ([link](https://doi.org/10.1016/j.jmps.2020.104105))
2. Wang, Y. & Cai, W. Stress-dependent activation entropy in thermally activated
cross-slip of dislocations. Proceedings of the National Academy of Sciences, accepted (2023). ([arXiv link](https://arxiv.org/abs/2303.06589))

### Authors

* Yifan Wang (yfwang09@stanford.edu)
* Wei Cai (caiwei@stanford.edu)

### High-throughput energy barrier calculations of cross-slip at different stress conditions (JMPS, 2020)

The analysis code and the analyzed data for plotting are presented in the following jupyter notebook:

* Cross-slip Workflow.ipynb

The simulation code can be found in the following Python (MD++) script:

* ni_Rao_screw_cross_slip.mdpp.py

Please contact the authors for the detailed procedures for replicating the simulation presented in the paper.

### MD simulation of cross-slip and rate theory predictions (PNAS, 2023)

The analysis code and the analyzed data for plotting are presented in the following jupyter notebook:

* Finite temperature cross-slip MD workflow-forRevision.ipynb

The simulation code can be found in the following Python (LAMMPS) script:

* ni_screw_cross_slip_MD.lammps.py

Please contact the authors for the detailed procedures of replicating the simulation presented in the paper.
