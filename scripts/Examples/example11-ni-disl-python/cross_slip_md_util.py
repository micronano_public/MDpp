import os, sys, shutil, time, gzip, argparse, glob
import numpy as np
# import matplotlib.pyplot as plt

############### Make sure MD++ is compiled using:
# cd ~/Codes/MD++.git
# make eam SYS=mc2_mpiicc PY=yes build=R
# mkdir bin_ni_cs
# mv bin/* bin_ni_cs

# Define directories and files
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin_ni_cs'))

# Import MD++ packages
# import mdpp
from cross_slip_util import cross_slip_util
import cross_slip_workflow as workflow

# Import LAMMPS packages
# lammps_dir = os.environ['LAMMPS_DIR_2']
# sys.path.insert(0, os.path.join(lammps_dir, 'python'))
# from lammps import lammps, PyLammps

# Import OVITO packages
# from ovito.io import import_file, export_file
# from ovito.modifiers import DislocationAnalysisModifier
# from ovito.data import DislocationNetwork

# Default values and directories
default_potential_file = 'Ni_Rao99_KWK.eam.fs'
default_thermo_freq    = 10
default_cs_threshold   = [1, 10]   # centro-symmetry threshold where dislocations show up
default_loc_th         = [0.25, 0.75] # filter out noise atoms in the matrix
default_cna_cutoff     = 3.08
default_modulus_xx = default_modulus_yy = default_modulus_zz = 450e3 # MPa
default_modulus_xz = default_modulus_xy = default_modulus_yz = 100e3 # MPa
default_modulus    = [default_modulus_xx, default_modulus_yy, default_modulus_zz,
                      default_modulus_xy, default_modulus_xz, default_modulus_yz]
energy_unit_conversion = 0.62415091259e-5 # # eV/A^3 / MPa

def cross_slip_md_util_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('args',       type=float, nargs='+', default=[7, 600, 0.0000, -0.0000, 0.0000, -0.0000],  help='status, [T, syz, szz, syy, sxz] or [T, seg, ssc, sec]')
    parser.add_argument('--machine',  type=str, default='gpu_serial')
    parser.add_argument('--stol',     type=float, default=5.0)
    parser.add_argument('--use_gpu',  type=int, default=0)
    parser.add_argument('--gpu_node', type=str, default='gpu-tesla')
    parser.add_argument('--runs_dir', type=str,   default='ni-Rao-cs',        help='directory of the running process')
    parser.add_argument('--temp_damp',type=float, default=0.5)
    parser.add_argument('--stress_damp', type=float, default=0.1)
    parser.add_argument('--runsteps', type=int,   default=200)
    parser.add_argument('--dump_freq',type=int,   default=10)
    parser.add_argument('--timestep', type=float, default=0.002)
    parser.add_argument('--strainincr', type=float, default=0.0001)
    parser.add_argument('--niters',   type=int)
    parser.add_argument('--initargs', type=float, nargs='+', default=None,  help='status, [T, syz, szz, syy, sxz] or [T, seg, ssc, sec]')
    # parser.add_argument('--equil_time', type=float, default=10,   help='total time to equilibrate system (ps)')
    # parser.add_argument('--total_time', type=float, default=1000, help='total time to run nvt simulation (ps)')
    # parser.add_argument('--centro_symmetry_threshold', type=float, nargs=2, default=[3., 10.], help='centro-symmetry threshold where dislocations show up')
    # parser.add_argument('--disl_loc_th',   type=float, nargs=2, default=[0.25, 0.75], help='location threshold of dislocation')
    parser.add_argument('--nsample',  type=int)
    parser.add_argument('--NxNyNz',   type=int, nargs=3, default=[30, 30, 20])
    parser.add_argument('--subsurface_threshold', type=float, nargs=2, default=[-0.195, 0.195])
    parser.add_argument('--verbose',  action='store_true')
    parser.add_argument('--test_relax', action='store_true', help='only run energy minimization for test purposes (status 7)')
    if args is None:
        return vars(parser.parse_args())
    else:
        return vars(parser.parse_args(args))

############################# Define functions ###############################

class cross_slip_md_util:
    def __init__(self, ap, mdpp=None, curr_dir=None):
        ''' Initialize MD simulation '''
        # Parsing arguments
        self.ap          = ap
        self.NxNyNz      = tuple(ap['NxNyNz'])
        self.temp_damp   = ap['temp_damp']
        self.stress_damp = ap['stress_damp']
        self.timestep    = ap['timestep']
        self.dump_freq   = ap['dump_freq']
        self.machine     = ap['machine']
        self.use_gpu     = ap['use_gpu']
        self.gpu_node    = ap['gpu_node']
        self.nsample     = ap['nsample']
        self.subsurface_threshold = ap['subsurface_threshold']

        parsed_arg = self.parse_arg_stress(ap['args'])
        self.status = parsed_arg['status']
        self.temp_final = parsed_arg['temp_final']
        self.ns = parsed_arg['ns']
        self.sig_ref = parsed_arg['sig_ref']
        self.stress_cartesian = parsed_arg['stress_cartesian']
        if ap['initargs'] is not None:
            self.parsed_initargs = self.parse_arg_stress(ap['initargs'])

        # Define directories
        self.curr_dir = os.path.join(mdpp_dir, 'scripts', 'Examples', 'example11-ni-disl-python') if curr_dir is None else curr_dir
        self.runs_dir = os.path.join(mdpp_dir, 'runs', ap['runs_dir'])
        self.csu      = cross_slip_util(self.runs_dir, mdpp=mdpp, setnolog=False, NxNyNz=self.NxNyNz)
        if mdpp is not None: self.csu.readeam_ni_Rao()

        self.workdir = self.create_dirname(self.status, self.ns, nsample=self.nsample)
        os.makedirs(self.workdir, exist_ok=True)
        os.chdir(self.workdir)
        print('created work_dir: %s'%self.workdir)

    def parse_arg_stress(self, args):
        parsed_arg = {}
        parsed_arg['status']     = args[0]
        parsed_arg['temp_final'] = args[1]
        if len(args) == 8:
            parsed_arg['sig_ref'] = args[2:8]
            sxx, syy, szz, sxy, sxz, syz = parsed_arg['sig_ref']
            parsed_arg['stress_cartesian'] = [syz, szz, syy, sxz]
            parsed_arg['ns'] = args[1:]
        else:
            if len(args) == 5:
                stress_condition = args[2:5]
                parsed_arg['stress_cartesian'] = workflow.EscaigSchmidt2Cartesian(*stress_condition)
            elif len(args) == 6:
                parsed_arg['stress_cartesian'] = args[2:6]
            else:
                raise ValueError('argument number incorrect')
            syz, szz, syy, sxz = tuple(parsed_arg['stress_cartesian']); sxx = sxy = 0
            parsed_arg['sig_ref'] = (sxx, syy, szz, sxy, sxz, syz)
            parsed_arg['ns'] = [parsed_arg['temp_final'], ] + parsed_arg['stress_cartesian']
        return parsed_arg

#*******************************************
# Definition of utility functions
#*******************************************

    def create_dirname(self, status, ns=[], nsample=None):
        '''Create dirname based on status and arguments ns
        '''
        strNxNyNz = '%dx%dx%d'%self.NxNyNz
        if len(ns) == 5:
            stress_cond = '%.4f_%.4f_%.4f_%.4f'%tuple(ns[1:5])
        elif len(ns) == 7:
            stress_cond = '%.4f_%.4f_%.4f_%.4f_%.4f_%.4f'%tuple(ns[1:7])
        dirname = os.path.join(self.runs_dir, 'ni-screw-md-%s-%.1f'%(strNxNyNz, status), 'T_%dK'%ns[0],'stress_%s'%stress_cond)
        if nsample is not None:
            dirname = os.path.join(dirname, 'sample_%d'%nsample)
        return dirname

    def init_lammps(self, machine=None, use_gpu=None, timestep=None, logfile='log.lammps', verbose=True, gpu_node=None):
        '''Initialize lammps object, lammps is compiled using icc_serial with gpu package 
        '''
        if machine is None: machine = self.machine
        if use_gpu is None: use_gpu = self.use_gpu
        if timestep is None: timestep = self.timestep
        if gpu_node is None: gpu_node = self.gpu_node

        cmdargs = ('-sf gpu -pk gpu %d'%use_gpu).split() if use_gpu > 0 else []
        lammps_dir = os.environ['LAMMPS_DIR']
        sys.path.insert(0, os.path.join(lammps_dir, 'build'))
        from lammps import lammps, PyLammps
        # if gpu_node == 'gpu-geforce':
        #     # Import LAMMPS packages
        #     lammps_dir = os.environ['LAMMPS_DIR']
        #     # sys.path.insert(0, os.path.join(lammps_dir, 'python'))
        #     from lammps import lammps, PyLammps
        # else:
        #     # Import LAMMPS packages
        #     lammps_dir = os.environ['LAMMPS_DIR_2']
        #     sys.path.insert(0, os.path.join(lammps_dir, 'python'))
        #     from lammps import lammps, PyLammps
        lmp = lammps(name=machine, cmdargs=cmdargs)
        L = PyLammps(ptr=lmp)
        L.log(logfile)
        L.units('metal')
        L.boundary('p p p')
        L.atom_style('atomic')
        L.atom_modify('map', 'yes')
        L.timestep(timestep)
        if use_gpu: L.package('gpu', 1, 'neigh', 'no')
        if verbose: print('successfully set lammps session')
        self.LAMMPS = (L, lmp)

    def read_data(self, datafile, initcopy=None):
        '''Read lammps data file '''
        L, lmp = self.LAMMPS
        if os.path.splitext(datafile)[1] == '.gz':
            if initcopy is None: initcopy = datafile[:-3]
            f_in = gzip.open(datafile, 'rb')
        else:
            if initcopy is None: initcopy = datafile
            f_in = open(datafile, 'rb')
        
        with open(initcopy, 'wb') as fout:
            shutil.copyfileobj(f_in, fout)
        f_in.close()
        L.read_data(initcopy)

    def read_initfile(self, read_restart_file=None, verbose=False):
        '''Read input files. There are two modes:
            read_restart_file == None: load initfile from status 5 (0K under stress) only
            read_restart_file != None: load restart dump file (must include x y z vx vy vz) at timestep Nstep
        '''
        L, lmp = self.LAMMPS
        # Copy over and decompress the initial file
        # GPU compilation does not recognize .gz file, maybe a bug in LAMMPS (2020.08.17 Yifan)
        if read_restart_file is not None:
            self_ns = [0, 0.0, -0.0, 0.0, -0.0]
        else:
            self_ns = self.ns
        if self.status in [10, 11, 12]:
            mdppfile = os.path.join(self.csu.create_dirname(0), 'perf.LAMMPS.gz')
        else:
            mdppfile = os.path.join(self.csu.create_dirname(5, self_ns), 'ni-screw-gp-relaxed.LAMMPS.gz')
        initcopy = os.path.join(self.workdir, 'ni-md-init.lammps')
        self.read_data(mdppfile, initcopy)

        if read_restart_file is not None:
            # if os.path.splitext(read_restart_file)[1] == '.gz':
            #     f = gzip.open(read_restart_file, 'rb')
            # else:
            with open(read_restart_file, 'r') as f:
                for line in f:
                    if line.split() == ['ITEM:', 'TIMESTEP']:
                        nextline = f.readline()
                        Nstep = int(nextline.split()[0])
                        break
            # f.close()
            L.read_dump(read_restart_file, Nstep, 'x y z vx vy vz', 'box yes', 'replace yes')
            print('restart file %s is loaded, with timestep %d'%(read_restart_file, Nstep))


    def read_Ni_eam(self, potential_file=None, verbose=True):
        '''Define potential (eam potential from KW Kang, translated from MD++ Rao99)'''
        L, lmp = self.LAMMPS
        if potential_file is None: potential_file = os.path.join(self.curr_dir, default_potential_file)
        L.pair_style('eam/fs')
        L.pair_coeff('* *', potential_file, 'Ni_Rao99')
        if verbose: print('successfully load potential file: %s'%potential_file)

    def compute_all(self, cna_cutoff=default_cna_cutoff, surface_force=False):
        '''Define the computes for evaluating stress and centro-symmetry'''
        L, lmp = self.LAMMPS
        # L.neigh_modify('one', '2000')
        L.compute('compute_temp all temp')
        # L.compute('compute_press all pressure compute_temp')

        # Calculate Virial stress
        L.compute('virial_peratom', 'all', 'stress/atom NULL')               # use NULL to include kinetic terms
        L.compute('virial all reduce sum', ' '.join(['c_virial_peratom[%d]'%(i+1) for i in range(6)]))
        L.compute('virial_bulk bulk reduce sum', ' '.join(['c_virial_peratom[%d]'%(i+1) for i in range(6)]))
        
        # Calculate Ly of the subsurface region
        L.compute('y_layer_above layer_above reduce ave y')
        L.compute('y_layer_below layer_below reduce ave y')
        L.compute('y_upper_layer upper_layer reduce ave y')
        L.compute('y_lower_layer lower_layer reduce ave y')

        # Calculate stress
        L.variable('vacuumratio equal "1 - (abs(c_y_upper_layer - c_y_layer_below) + abs(c_y_layer_above - c_y_lower_layer))/(2*ly)"')
        L.variable('stress_bulk','vector', '"c_virial_bulk / ( vol * (1 - v_vacuumratio) ) "')
        L.variable('stress_bulk_inMPa', 'vector', '"v_stress_bulk/10"')
        L.variable('press_bulk_inMPa', 'vector', '-v_stress_bulk_inMPa')
        L.variable('press equal -(c_virial[1]+c_virial[2]+c_virial[3])/(3*vol)')

        # Calculate strain
        # L.compute('nlocal', 'all', 'property/local', 'natom1', 'natom2')
        # # L.compute('plocal', 'all', 'pair/local', 'dist', 'eng', 'force')
        # L.dump('local_test all local 1000', 'local_test.*.dump', 'c_nlocal[1]', 'c_nlocal[2]')# , 'c_plocal[1]', 'c_plocal[2]', 'c_plocal[3]')

        # Calculate CNA and dislocation size
        L.compute('cna', 'all', 'cna/atom', cna_cutoff)
        L.variable('is_disl', 'atom', '(c_cna==2)')
        L.compute('CS', 'all', 'reduce sum', 'v_is_disl')

        # Forces on the surfaces
        if surface_force:
            L.compute('upper_fx topsurface reduce sum fx')
            L.compute('upper_fy topsurface reduce sum fy')
            L.compute('upper_fz topsurface reduce sum fz')
            L.compute('lower_fx botsurface reduce sum fx')
            L.compute('lower_fy botsurface reduce sum fy')
            L.compute('lower_fz botsurface reduce sum fz')

        # Thermo output setting
        L.thermo(default_thermo_freq)
        if surface_force:
            L.thermo_style('custom step temp v_press etotal', 
                        ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
                        'c_upper_fx c_upper_fy c_upper_fz',
                        'c_lower_fx c_lower_fy c_lower_fz',
                        'c_CS lx lz xz v_vacuumratio')
            L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.4e %.4e %.4e %.4e %.4e %.4e %.0f %.17g %.17g %.17g %.4f"', 'temp', 'compute_temp')
        else:
            L.thermo_style('custom step temp v_press etotal', 
                        ' '.join(['v_stress_bulk_inMPa[%d]'%(i+1) for i in range(6)]), 
                        'c_CS lx lz xz v_vacuumratio')
            L.thermo_modify('format', 'line', '"%ld %.2f %.2f %.8f %.4f %.4f %.4f %.4f %.4f %.4f %.0f %.17g %.17g %.17g %.4f"', 'temp', 'compute_temp')
        # L.thermo_modify('format', 'float', '%.4f', 'temp', 'compute_temp')
        L.run(0)

    def create_group(self, group_name, group_index_list):
        '''Given a group_index_list and create a group named group_name'''
        L, lmp = self.LAMMPS
        group_index = np.zeros(L.system.natoms, dtype=np.ctypeslib.as_ctypes_type(int))
        group_index[group_index_list] = 1
        # Ugly but working, use type array to define group, then copy the original type array back
        type_ctype_0=lmp.gather_atoms('type', 0, 1)
        type_ctype = lmp.gather_atoms('type', 0, 1)
        for i in range(len(group_index)): type_ctype[i] = group_index[i]
        lmp.scatter_atoms('type', 0, 1, type_ctype)
        lmp.command('group %s type 1'%group_name)
        lmp.scatter_atoms('type', 0, 1, type_ctype_0)

    def apply_force_on_surface(self, sigma_y = [0, 0, 0], define_surface=False, bulk_range=None, verbose=False):
        L, lmp = self.LAMMPS
        csu = self.csu
        if bulk_range is None: bulk_range = self.subsurface_threshold
        if define_surface:
            ## Define bulk region
            csu.def_subsurface_region(threshold=bulk_range, verbose=verbose)
            self.create_group('layer_above', csu.layer_above)
            self.create_group('layer_below', csu.layer_below)
            self.create_group('upper_layer', csu.upper_layer)
            self.create_group('lower_layer', csu.lower_layer)
            self.bulkid = csu.subsurface
            ## Define subsurface region
            csu.apply_force_on_surface(sigma_y=sigma_y, define_surface=True, verbose=verbose)
            group_list = np.array(csu.mdpp.get_array('group', 0, csu.mdpp.get('NP')))
            self.create_group('topsurface', np.nonzero(group_list == 1)[0])
            self.create_group('botsurface', np.nonzero(group_list == 2)[0])
            self.create_group('bulk', self.bulkid)
            if verbose:
                np.savetxt(os.path.join(self.curr_dir, 'topsurface.txt'), np.nonzero(group_list == 1)[0]+1, fmt='%d', newline=' ')
                np.savetxt(os.path.join(self.curr_dir, 'botsurface.txt'), np.nonzero(group_list == 2)[0]+1, fmt='%d', newline=' ')
                np.savetxt(os.path.join(self.curr_dir, 'bulk.txt'), self.bulkid, fmt='%d', newline=' ')
        else:
            L.unfix('force1')
            L.unfix('force2')
        N1 = L.eval('count(topsurface)')
        N2 = L.eval('count(botsurface)')

        # The number of atoms of the surface atom groups
        if verbose:
            print('Ntop = %d (should be 4800 for 30x30x20, 1600 for 20x20x10)'%N1)
            print('Nbot = %d (should be 4800 for 30x30x20, 1600 for 20x20x10)'%N2)

        # Calculate applied force
        Axz = L.eval('lz')*L.eval('lx')
        f1 = -np.array(sigma_y)*Axz/N1*energy_unit_conversion
        f2 =  np.array(sigma_y)*Axz/N2*energy_unit_conversion

        # Add force to the system
        L.fix('force1', 'topsurface', 'addforce', '%s %s %s'%tuple(f1))
        L.fix('force2', 'botsurface', 'addforce', '%s %s %s'%tuple(f2))
        L.fix_modify('force1', 'energy yes') #, 'virial yes')
        L.fix_modify('force2', 'energy yes') #, 'virial yes')

    def perturb_velocity(self, mu=0, sigma=1e-5, verbose=False):
        '''perturb the initial velocity by a Gaussian distribution'''
        L, lmp = self.LAMMPS
        if verbose: print('test perturb_velocity, before:', L.atoms[0].velocity)
        v_ctype = lmp.gather_atoms('v', 1, 3)
        np.random.seed(int(time.time()))
        dv_numpy= np.random.normal(loc=mu, scale=sigma, size=(len(v_ctype)))
        for i in range(len(v_ctype)): v_ctype[i] += dv_numpy[i]
        lmp.scatter_atoms('v', 1, 3, v_ctype)
        if verbose: print('test perturb_velocity, after:', L.atoms[0].velocity)
        L.velocity('all', 'zero', 'linear')
        if verbose: print('test perturb_velocity, zeroed:', L.atoms[0].velocity)

    def changeH_to_relax_stress(self, sig_name='v_stress_bulk_inMPa', sig_ref=None, change=[0,1,2,3,4,5], modulus=None, verbose=False, stress_tol=0.0, stress_drag=1.0):
        L, lmp = self.LAMMPS
        if sig_ref is None: sig_ref= self.sig_ref
        ### Get simulation cell info
        tic = time.time()
        NP = L.system.natoms
        boxlo,boxhi,xy,yz,xz,_ ,_ = lmp.extract_box()
        lx, ly, lz = tuple(np.array(boxhi)-np.array(boxlo))
        Hcurr = np.array([lx, ly, lz, xy, xz, yz])
        Hbase = np.array([lx, ly, lz, ly, lz, lz])
        if modulus is None: modulus = np.array(default_modulus)
        ### Calculate strain based on stress difference
        sig = -np.array([L.eval('%s[%d]'%(sig_name, i+1)) for i in range(6)]) # add negative sign to match with MD++ convention
        dsig = np.array(sig) - np.array(sig_ref)
        eps = dsig/modulus*stress_drag
        Hnext = np.array(Hcurr) + np.array(Hbase)*eps
        if verbose:
            print('sigcurr: %.4f %.4f %.4f %.4f %.4f %.4f'%tuple(sig))
            print('sig_ref: %.4f %.4f %.4f %.4f %.4f %.4f'%tuple(sig_ref))
            print('dsig: %.4f %.4f %.4f %.4f %.4f %.4f'%tuple(dsig))
            print('Hcurr: %.4f %.4f %.4f %.4f %.4f %.4f'%tuple(Hcurr))
            print('Hnext: %.4f %.4f %.4f %.4f %.4f %.4f'%tuple(Hnext))
        if verbose: print('initialization: %.2fs'%(time.time() - tic))
        # Move atoms to release stress
        tic = time.time()
        if np.abs(dsig[5]) > stress_tol and 5 in change:
            Hcmat = np.array([[Hcurr[0], Hcurr[3], Hcurr[4]], [0, Hcurr[1], Hcurr[5]], [0, 0, Hcurr[2]]])
            Hnmat = np.array([[Hcurr[0], Hcurr[3], Hcurr[4]], [0, Hcurr[1], Hcurr[5]], [0, Hnext[5], Hcurr[2]]])
            orig = np.array([L.eval('xlo'), L.eval('ylo'), L.eval('zlo')])
            x_ctype = lmp.gather_atoms('x', 1, 3)
            R = np.ctypeslib.as_array(x_ctype).reshape(NP, 3) - orig
            S = np.linalg.inv(Hcmat).dot(R.T).T
            x_array = np.reshape(Hnmat.dot(S.T).T + orig, (3*NP, ))
            lmp.scatter_atoms('x', 1, 3, np.ctypeslib.as_ctypes(x_array))
        if verbose: print('release syz: %.2fs'%(time.time() - tic))
        tic = time.time()
        if np.linalg.norm(dsig[[0, 2, 4]], ord=np.inf) > stress_tol:
            if 0 in change and 2 in change and 4 in change:
                L.change_box('all', 'x scale', Hnext[0]/Hcurr[0], 'z scale', Hnext[2]/Hcurr[2], 'xz final', Hnext[4], 'remap', 'units box')
            # if 0 not in change and 2 not in change and 4 not in change:
            #     L.change_box('all', 'x scale', Hcurr[0]/Hnext[0], 'z scale', Hcurr[2]/Hnext[2], 'xz final', Hcurr[4], 'remap', 'units box')
        if verbose: print('release sxx, szz, sxz: %.2fs'%(time.time() - tic))
        tic = time.time()
        if np.linalg.norm(dsig[[1, 3]], ord=np.inf) > stress_tol and 1 in change and 3 and change:
            L.change_box('all', 'y scale', Hnext[1]/Hcurr[1], 'xy final', Hnext[3], 'remap', 'units box')
            L.change_box('all', 'y scale', Hcurr[1]/Hnext[1], 'xy final', Hcurr[3], 'units box')
        if verbose: print('release syy, sxy: %.2fs'%(time.time() - tic))
        
        for i in [1, 3, 5]: Hnext[i] = Hcurr[i]
        return {'dsig': dsig, 'sig': sig, 'eps': eps, 'Hcurr': Hcurr, 'Hnext': Hnext}

    def const_stress_simulation(self, tempinit, runsteps, niters, dumpfile=None, sig_ref=None, tempdamp=1.0, stress_drag=0.1, stol=5.0, verbose=False):
        L, lmp = self.LAMMPS
        L.fix('fix_nvt', 'all nvt', 'temp', tempinit, tempinit, tempdamp)
        L.fix_modify('fix_nvt', 'temp', 'compute_temp')
        istep = 0
        while True:
            L.run(runsteps)
            print('iter %d'%istep)
            changeH = self.changeH_to_relax_stress(sig_ref=sig_ref, sig_name='f_ave_stress', verbose=verbose, stress_tol=stol, stress_drag=stress_drag)
            if verbose:
                print('stress drag term:', stress_drag)
                print('dislocation size:', L.eval('c_CS'))
            if (niters is not None and istep > niters) or np.abs(changeH['dsig']).max() < stol: break
            istep += 1
        L.unfix('fix_nvt')
        if dumpfile is not None:
            dump_file_name = dumpfile + '_T%03d.dump'%tempinit
            L.write_dump('all custom', dump_file_name, 'id type x y z vx vy vz')
