import sys, os, glob
import numpy as np

import ovito
from ovito import dataset
from ovito.data import *
from ovito.io import import_file, export_file
from ovito.vis import *
from ovito.modifiers import *

# set up POV
vp = Viewport()

# Obtain the file names of the chain, state A and state B
scriptfile = sys.argv[0]
lammpsfile = sys.argv[1]
if len(sys.argv) > 2 and sys.argv[2] != '--replace': 
    outputname = sys.argv[2]
else:
    outputname = os.path.join(os.path.dirname(lammpsfile), 'disl_info.txt')
replace    = True if '--replace' in sys.argv else False

if (replace or not os.path.exists(outputname)):
    print('summarizing %s to %s'%(lammpsfile, outputname))
    node = import_file(lammpsfile)
    node.add_to_scene()
    # node.source.cell.display.render_cell = False

    # add modifiers to get dislocation information
    Affmod = AffineTransformationModifier(transform_particles = True, transform_box = True, 
                                          transformation = [[1,0,0,0], [0,0,1,0], [0,1,0,0]])
    CNAmod = CommonNeighborAnalysisModifier()
    Csym   = CentroSymmetryModifier()
    # node.modifiers.append(Affmod)
    node.modifiers.append(CNAmod)
    node.modifiers.append(Csym)
    selmod = SelectExpressionModifier(expression = 'Centrosymmetry < 0.1 || Centrosymmetry > 10')
    delmod = DeleteSelectedParticlesModifier()
    node.modifiers.append(selmod)
    node.modifiers.append(delmod)
    rawdata = np.zeros((node.source.num_frames, 7))

    for frame in range(node.source.num_frames):
        print('processing frame %d...'%frame)
        dataset.anim.current_frame = frame
        node.compute(frame)
        
        # render image
        imagename = os.path.join(os.path.dirname(lammpsfile), 'relax-%d.png'%frame)
        vp.type = Viewport.Type.PERSPECTIVE
        vp.camera_pos = (490, -10, 150)
        vp.camera_dir = (-.9, 0.25, -0.17)
        # vp.camera_pos = (300, -221, 324)
        # vp.camera_dir = (-0.50, 0.66, -0.55)
        vp.fov = np.deg2rad(35.0)
        vp.zoom_all()
        settings = RenderSettings(filename = imagename, size = (1600, 1600), generate_alpha=True, renderer = TachyonRenderer())
        vp.render(settings)
        # other perspectives
        vp_types_list = [Viewport.Type.LEFT, Viewport.Type.FRONT, Viewport.Type.TOP]
        camera_pos_list = [(0, 0, 0), (0, 0, 0), (0, 0, 0)]
        camera_dir_list = [(1, 0, 0), (0, 1, 0), (0, 0,-1)]
        for i in range(3):
            vp.type = vp_types_list[i]
            newname = os.path.splitext(imagename)[0] + '_%d.png'%i
            print('converting... %s'%newname)
            vp.zoom_all()
            newsettings = RenderSettings(filename = newname, size = (1600, 1600), generate_alpha=True, renderer = TachyonRenderer())
            vp.render(newsettings)

        timestep = frame # node.source.attributes['Timestep']
        natom = node.output.number_of_particles
        if natom > 0:
            pos   = node.output.particle_properties['Position'].array
            ypos  = pos[:, 1].mean()
            zpos  = pos[:, 2].mean()
            yext  = 0
            zext  = np.ptp(pos[:, 2])
            disl_ext = np.sqrt(yext**2 + zext**2)
        else:
            ypos = zpos = yext = zext = disl_ext = np.nan
        rawdata[frame, :] = [timestep, natom, ypos, zpos, yext, zext, disl_ext]
    np.savetxt(outputname, rawdata, fmt=['%10d', '%10d', '%23.17e', '%23.17e', '%23.17e', '%23.17e', '%23.17e'])

else:
    print('target %s exists!'%outputname)
