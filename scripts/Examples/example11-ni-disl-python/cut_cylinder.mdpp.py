'''
Run the script by:
    cd $MDPLUS_DIR
    python3 $MDPLUS_DIR/scripts/Examples/example11-ni-disl-python/cut_cylinder.mdpp.py --postprocfile $MDPLUS_DIR/runs/ni-Rao-cs/ni-Rao-screw-30x30x20-5.0/stress_852.2913_-5007.9675_-223.6200_905.3898_-2598.3164_0.0000/ni-screw-gp-relaxed.cn --rcut 3 --hessian_file $MDPLUS_DIR/runs/ni-Rao-cs/ni-Rao-screw-30x30x20-5.0/stress_852.2913_-5007.9675_-223.6200_905.3898_-2598.3164_0.0000/rcut_6.0000/hessian.out
'''

import sys, os, shutil, glob, argparse
import numpy as np
from scipy import sparse
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin_ni_cs'))

import mdpp
from cross_slip_util import cross_slip_util, list_to_str

runs_dir = os.path.join(mdpp_dir, 'runs', 'ni-Rao-cs')
parser = argparse.ArgumentParser()
parser.add_argument('--rcut'     ,type=float)
parser.add_argument('--postprocfile', type=str, help='filename for post processing (status 212)')
parser.add_argument('--hessian_file', type=str)
ap = parser.parse_args()

rcut_file = os.path.join(os.path.dirname(ap.postprocfile), 'rcut%g.txt'%ap.rcut)

csu = cross_slip_util(runs_dir, 0, [], mdpp=mdpp)
csu.load_file(ap.postprocfile)
NP = mdpp.get('NP')
core_region = np.sort(csu.def_core_region(rcut = ap.rcut, rcut_file = rcut_file)).astype(int)
ncore = core_region.size
sparse_ind  = np.empty(NP, dtype=int)
sparse_ind[core_region] = np.arange(ncore, dtype=int)
print('loaded core region:', ncore)
print(core_region)

print('saved core region file %s: %d atoms'%(rcut_file, core_region.size))

if ap.hessian_file is not None:
    hess_file = ap.hessian_file
    hess_data = np.loadtxt(hess_file)
    print('loaded hessian data:', hess_data.shape)
    i_avail = np.isin(hess_data[:, 0].astype(int), core_region)
    j_avail = np.isin(hess_data[:, 2].astype(int), core_region)
    ijavail = np.logical_and(i_avail, j_avail)
    print(np.nonzero(ijavail))
    print('rows within core region:', np.count_nonzero(ijavail))

    i_sp = sparse_ind[hess_data[ijavail, 0].astype(int)]
    j_sp = sparse_ind[hess_data[ijavail, 2].astype(int)]
    print('i_sp:', i_sp, i_sp.shape)
    print('j_sp:', j_sp, j_sp.shape)
    I = np.repeat(i_sp * 3 + (hess_data[ijavail, 1].astype(int) - 1), 3)
    J = np.vstack([j_sp*3, j_sp*3+1, j_sp*3+2]).T.flatten()
    V = hess_data[ijavail, 3:6].flatten()
    print('I:', I, I.shape)
    print('J:', J, J.shape)
    print('V:', V, V.shape)

    A = sparse.coo_matrix((V,(I,J)),shape=(ncore*3, ncore*3))

    hess_npz = os.path.join(os.path.dirname(hess_file), 'hessian_rcut%g.npz'%ap.rcut)
    sparse.save_npz(hess_npz, A.tocsr())
    print('saved hessian matrix:', hess_npz)
