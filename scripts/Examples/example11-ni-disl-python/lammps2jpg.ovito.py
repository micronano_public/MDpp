import sys, os, glob
import numpy as np

import ovito
from ovito.data import *
from ovito.io import import_file, export_file
from ovito.vis import *
from ovito.modifiers import *

# set up POV
vp = Viewport()

# Obtain the file names of the chain, state A and state B
scriptfile = sys.argv[0]
lammpsfile = sys.argv[1]
if len(sys.argv) > 2 and sys.argv[2] != '--replace':
    outputname = sys.argv[2]
else:
    outputname = os.path.join(os.path.dirname(lammpsfile), 'relaxed.png')
replace    = True if '--replace' in sys.argv else False

if (replace or not os.path.exists(outputname)) and os.path.exists(lammpsfile):
    print('converting... %s'%(outputname))
    node = import_file(lammpsfile)
    node.add_to_scene()
    # node.source.cell.display.render_cell = False
    # ovito.dataset.selected_node = node

    # add modifiers to get dislocation information
    Affmod = AffineTransformationModifier(transform_particles = True, transform_box = True, 
                                          transformation = [[1,0,0,0], [0,0,1,0], [0,1,0,0]])
    CNAmod = CommonNeighborAnalysisModifier()
    Csym   = CentroSymmetryModifier()
    # node.modifiers.append(Affmod)
    node.modifiers.append(CNAmod)
    node.modifiers.append(Csym)
    node.compute()
    selmod = SelectExpressionModifier(expression = 'Centrosymmetry < 0.1 || Centrosymmetry > 10')
    delmod = DeleteSelectedParticlesModifier()
    node.modifiers.append(selmod)
    node.modifiers.append(delmod)
    node.compute()
    vp.type = Viewport.Type.PERSPECTIVE
    vp.camera_pos = (490, -10, 150)
    vp.camera_dir = (-.9, 0.25, -0.17)
    # vp.camera_pos = (300, -221, 324)
    # vp.camera_dir = (-0.50, 0.66, -0.55)
    vp.fov = np.deg2rad(35.0)
    vp.zoom_all()

    settings = RenderSettings(filename = outputname, size = (1600, 1600), generate_alpha=True, renderer = TachyonRenderer())
    vp.render(settings)
    # other perspectives
    vp_types_list = [Viewport.Type.LEFT, Viewport.Type.FRONT, Viewport.Type.TOP]
    camera_pos_list = [(0, 0, 0), (0, 0, 0), (0, 0, 0)]
    camera_dir_list = [(1, 0, 0), (0, 1, 0), (0, 0,-1)]
    for i in range(3):
        vp.type = vp_types_list[i]
        newname = os.path.splitext(outputname)[0] + '_%d.png'%i
        print('converting... %s'%newname)
        vp.zoom_all()
        newsettings = RenderSettings(filename = newname, size = (1600, 1600), generate_alpha=True, renderer = TachyonRenderer())
        vp.render(newsettings)
    
    pos_R = node.output.particle_properties.position.array
    Hcell = np.asarray(node.output.cell.matrix)
    pos_S = np.dot(np.linalg.inv(Hcell[:, :3]), (pos_R - Hcell[:, 3].reshape(1, 3)).T).T - 0.5
    np.savetxt(os.path.join(os.path.dirname(outputname), "disl_loc.txt"), pos_S)
elif os.path.exists(outputname):
    print('target %s exists!'%outputname)
else:
    print('lammps2jpg.ovito.py: lammps input file %s does not exist!'%lammpsfile)
