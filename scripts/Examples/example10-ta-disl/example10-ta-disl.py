# Examples on creating edge and screw dislocations in BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Convert python list to MD++ string
list_to_str = lambda lst: '[ %s ]'%(' '.join([str(it) for it in lst]))


#--------------------------------------------
# Initialization
def initmd(runs_dir):
    mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    os.makedirs(runs_dir, exist_ok=True)
    mdpp.cmd("dirname = " + runs_dir)
    mdpp.cmd("zipfiles = 1 NNM = 200")

#--------------------------------------------
# Read the potential file
def readpot():
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
    mdpp.cmd('readpot')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_perfect_crystal(crystalstructure, latticeconst, supercellsize, 
                           filename='', cn_output=True, lammps_output=True):
    ''' Create Perfect Lattice Configuration
    Parameters
    ----------
    crystalstructure : string
        Crystal structure to be created, must be one of the following:
        {'body-centered-cubic', 'face-centered-cubic', ...}
    latticeconst : float
        Lattice constant in Angstrom
    supercellsize : ndarray of shape (3, 3)
        supercellsize = [[a_1, a_2, a_3, repeat_a],
                         [b_1, b_2, b_3, repeat_b],
                         [c_1, c_2, c_3, repeat_c]]
        where [a_1, a_2, a_3] specifies the first repeat vector of the simulation
        supercell in Miller indices and repeat_a is the number of the repeats in
        that direction. [b_1, b_2, b_3] and [c_1, c_2, c_3] represent the second
        and third direction respectively, repeat repeat_b and repeat_c times.
    filename : string, optional
        The file name prefix to save the perfect crystal file.
    cn_output : bool
        write MD++ output file .cn format
    lammps_output : bool
        write LAMMPS data file format
    '''

    mdpp.cmd('crystalstructure = %s'%crystalstructure)
    mdpp.cmd('latticeconst = ' + str(latticeconst) + ' #(A)')
    if supercellsize.shape != (3, 4):
        raise ValueError('create_perfect_crystal: supercellsize must have shape of (3, 4)')
    supercell_size_str = list_to_str(supercellsize.flatten())
    mdpp.cmd('latticesize = %s makecrystal'%supercell_size_str)
    
    if filename == '':
        filename = 'perf'
    else:
        filename = filename + '_perf'
    if cn_output:
        mdpp.cmd('finalcnfile = %s.cn writecn'%filename)
    if lammps_output:
        mdpp.cmd('finalcnfile = %s.lammps writeLAMMPS'%filename)
    mdpp.cmd('eval') # evaluate the potential of perfect crystal

#--------------------------------------------
def create_dislocation_dipole(linedirection, separationdir, burgersvector, x0, y0, y1, nu,
                              num_images=[-10, 10, -10, 10], tilt_box=True, sz_limit=None, store=False,
                              filename='', cn_output=True, lammps_output=True):
    ''' Create Dislocation Dipole
    Parameters
    ----------
    linedirection : number
        dislocation line direction, 1,2,3 = x,y,z
    separationdir : number
        separation direction between two dislocations in the dipole, 1,2,3 = x,y,z
    burgersvector : ndarray of shape (3, )
        Burgers vector, [bx, by, bz] in scaled coordinates
    x0,y0,y1 : float
        (x,y) position of the dislocation dipole, (x0, y0) and (x0, y1)
    nu : float
        Poisson's ratio of the material
    num_images : list of 4 int numbers
        Number of periodic image copies to calculate displacement of atoms
    tilt_box : bool
        if simulation box is tilted to keep periodic boundary condition
    sz_limit : tuple of size 2
        (sz_min, sz_max) represents the limit of dislocation in z direction
    store : bool
        If True, only define dislocation but not displace atoms.
    filename : string, optional
        The file name prefix to save the perfect crystal file.
    cn_output : bool
        write MD++ output file .cn format
    lammps_output : bool
        write LAMMPS data file format
    '''
    if sz_limit:
        limit_sz = [1, sz_limit[0], sz_limit[1]]
    else:
        limit_sz = [0, 0, 0]
    dipole_input = [ linedirection, separationdir,
                     burgersvector[0], burgersvector[1], burgersvector[2],
                     x0, y0, y1, nu ] + num_images + [int(tilt_box), ] + limit_sz + [int(store), ]
    input_str = list_to_str(dipole_input)
    mdpp.cmd('input = %s  makedipole '%input_str)
    if filename == '':
        filename = 'dipole'
    else:
        filename = filename + '_dipole'
    if cn_output:
        mdpp.cmd('finalcnfile = %s.cn writecn'%filename)
    if lammps_output:
        mdpp.cmd('finalcnfile = %s.lammps writeLAMMPS'%filename)
    mdpp.cmd('eval') # evaluate the potential of dislocation configuration

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width = 600 win_height = 600
    atomcolor = cyan highlightcolor = purple  bondcolor = red
    color00 = "red" color01 = "blue" color02 = "green"
    color03 = "magenta" color04 = "cyan" color05 = "purple"
    color06 = "gray80" color07 = "white" color08 = "orange"
    plot_color_axis = 2  NCS = 8  writeall = 1 # compute central symmetry deviation (CSD) parameter
    plot_color_windows = [ 5
                           0.6 6   1  
                           6  10   5
                           10 20   6
                           20 50   8
                           0.1  0.6  4
                         ]
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 1 # display reduced coordinates of atoms when clicked
    #plot_atom_info = 2 # display real coordinates of atoms
    #plot_atom_info = 3 # displayenergy of atoms
    plotfreq = 10
    rotateangles = [ 0 0 0 1.5 ]
    ''')

def openwindow():
    setup_window()
    # mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# Conjugate-Gradient relaxation with box fixed
def relax_fixbox(ftol=1e-7, fevalmax=1000, filename='', cn_output=True, lammps_output=True):
# Conjugate-Gradient relaxation
    mdpp.cmd('conj_ftol = %e'%ftol)
    mdpp.cmd('conj_fevalmax = %d'%fevalmax)
    mdpp.cmd('conj_fixbox = 1 relax')
    if filename == '':
        filename = 'relaxed'
    else:
        filename = filename + '_relaxed'
    if cn_output:
        mdpp.cmd('finalcnfile = %s.cn writecn'%filename)
    if lammps_output:
        mdpp.cmd('finalcnfile = %s.lammps writeLAMMPS'%filename)
    mdpp.cmd('eval') # evaluate the potential of relaxed configuration

#*******************************************
# Main program starts here
#*******************************************
# status 0: create an edge dislocation dipole in PBC box
#        1: create an edge dislocation in a simulation box with free surfaces
#        2: create a screw dislocation dipole in PBC box
#        3: create a screw dislocation in a simulation box with free surfaces
#        4: create a screw dislocation with a kink pair 
#        5: calculate energy barrier for kink pair nucleation on the screw dislocation
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status == %d'%(status))

initmd(os.path.join(mdpp_dir, 'runs', 'ta_disl', 'status_%d'%status))
readpot()

if status in [0, 1]:
    # Create an edge dislocation dipole in PBC box or single edge with free surfaces
    if status == 0:
        print("status == 0: create edge dislocation dipole")
        filename = 'Ta_edge_dipole'
    else:
        print("status == 1: create single edge dislocation")
        filename = 'Ta_edge_single'
    
    supercell_size = np.array([[ 1,   1,   1,  30],
                               [-1,   1,   0,  20],
                               [-1,  -1,   2,   2]])
    create_perfect_crystal('body-centered-cubic', 3.3058, supercell_size, filename=filename)

    Nx = supercell_size[0, 3]
    Ny = supercell_size[1, 3]
    line_direction = 3                 # z, dislocation line direction
    separation_dir = 2                 # y, separation direction between two dislocations in the dipole
    b = [1.0/(2*Nx), 0, 0]             # burger's vector in scaled coordinates
    
    # position of the dislocation dipole in x direction
    x0 = 0.111/Nx
    dy = -1.0/(4*Ny)
    nu = 0.35
    # position of the dislocation dipole in y direction
    if status == 0:                    # create dipole at 1/4 and 3/4 of the simulation box
        y0 = -0.25 + dy
        y1 =  0.25 + dy
    else:                              # create a single dislocation at the center
        y0 = -0.50 + dy
        y1 =  0.00 + dy

    create_dislocation_dipole(line_direction, separation_dir, b, x0, y0, y1, nu, filename=filename)

    if status == 1:                    # introducing free surface in y direction
        mdpp.cmd('input = [ 2 2 0.1 ] changeH_keepR')

    openwindow()

    relax_fixbox(filename=filename)
    E_edge = mdpp.get('EPOT')

    print('Energy = %.17eeV'%E_edge)

elif status in [2, 3, 4]:
    # Create an screw dislocation dipole in PBC box or single screw with free surfaces
    if status == 2:
        print("status == 2: create screw dislocation dipole")
        filename = 'Ta_screw_dipole'
    elif status == 3:
        print("status == 3: create single screw dislocation")
        filename = 'Ta_screw_single'
    else:
        print("status == 4: create single screw dislocation with a kink pair")
        filename = 'Ta_screw_kink'

    supercell_size = np.array([[ 1,   1,  -2,  20], 
                               [ 1,  -1,   0,  10], 
                               [ 1,   1,   1,  40]])
    create_perfect_crystal('body-centered-cubic', 3.3058, supercell_size, filename=filename)

    Nx = supercell_size[0, 3]
    Ny = supercell_size[1, 3]
    Nz = supercell_size[2, 3]
    line_direction = 3                 # z, dislocation line direction
    separation_dir = 2                 # y, separation direction between two dislocations in the dipole
    b = np.array([0, 0, 1.0/(2*Nz)])   # burger's vector in scaled coordinates
    
    # position of the dislocation dipole in x direction
    x0 = 0.111/Nx
    dy = -1.0/(4*Ny)
    nu = 0.35
    # position of the dislocation dipole in y direction
    if status == 2:                    # create dipole at 1/4 and 3/4 of the simulation box
        y0 = -0.25 + dy
        y1 =  0.25 + dy
    else:                              # create a single dislocation at the center
        y0 = -0.50 + dy
        y1 =  0.00 + dy

    if status == 4:
        kink_direction = 1; sz_limit = (-0.4, 0.4)
        create_dislocation_dipole(line_direction, separation_dir, b, x0, y0, y1, nu, store=True)
        create_dislocation_dipole(line_direction, kink_direction, -b, y1, x0, x0+1./4/Nx, nu, 
                                  store=False, filename=filename, sz_limit=sz_limit)
    else:
        create_dislocation_dipole(line_direction, separation_dir, b, x0, y0, y1, nu, filename=filename)

    if status in [3, 4]:
        mdpp.cmd('input = [ 2 2 0.1 ] changeH_keepR')

    openwindow()

    if status == 4:
        for its in range(50):
            relax_fixbox(fevalmax=20, filename=filename+'_%d'%its)
            print('Energy = %.17eeV'%mdpp.get('EPOT'))
    else:
        relax_fixbox(filename=filename)
    E_edge = mdpp.get('EPOT')

    print('Energy = %.17eeV'%E_edge)
    
else:
    print("unknown status = %d"%(status))

#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)

