# Parallel NEB relaxation (for structures relaxed under stress)
# to compile
#  make fs build=R SYS=mc2_mpiicc SPEC+=-DMAXCONSTRAINATOMS=800001
# to run (in sbatch file)
#  mpirun -n $ncpu bin/fs_mc2_mpiicc scripts/Examples/ta_disl/example10-ta-disl-stringrelax.tcl

#*******************************************
# Definition of procedures
#*******************************************

#*******************************************
# Main Program
#*******************************************

# MD++ setnolog
MD++ setoverwrite

set ncpu [ MD++_Get numDomains ]
if { $ncpu < 1 } { set ncpu 1 }
puts "ncpu = $ncpu"

file mkdir "runs/ta_disl"
MD++ dirname = runs/ta_disl/ta_kink_$ncpu
MD++ NNM = 200

MD++ writeall = 1

set NEBMAXSTEP 500
set EQUILSTEP  [expr $NEBMAXSTEP - 100 ]
if { $EQUILSTEP < 0 } {
 set EQUILSTEP 0
}

# Chooses the relaxed configuration to use as B' state
set iselect 6
puts "iselect = $iselect"


exec ln -f -s ../status_3/Ta_screw_single_relaxed.cn.gz          NEBinit.cn.gz
exec ln -f -s ../status_4/Ta_screw_kink_${iselect}_relaxed.cn.gz NEBfinal.cn.gz

# rename previous result files in order to not overwrite them
set check [ glob -nocomplain "stringrelax.chain.cn.cpu00" ]
set exist [ llength $check ]
if { $exist > 0 } {
 for { set i 0 } { $i < $ncpu } { incr i 1 } {
     set ival [ format %02d $i ] 
     exec cp stringrelax.chain.cn.cpu$ival Prev_stringrelax.chain.cn.cpu$ival
 }
 if { [ llength [ glob -nocomplain "stringeng.out" ] ] > 0 } { 
   exec cp stringeng.out Prev_stringeng.out
 }
}

MD++ potfile = "$::env(MDPLUS_DIR)/potentials/ta_pot" readpot
MD++ Broadcast_FS_Param

MD++ incnfile = NEBinit.cn  readcn setconfig1 #A
MD++ incnfile = NEBfinal.cn readcn setconfig2 #B
MD++ incnfile = NEBinit.cn  readcn            #A

MD++ Broadcast_Atoms

MD++ fixallatoms constrain_fixedatoms freeallatoms
MD++ chainlength = [expr $ncpu-1]

MD++ timestep = 0.07 printfreq = 1

# parameter setting for stringrelax_parallel
# 0: relax_surround, 1: redistrib_freq, -1: moveleft, 3:moveright,  0: yesclimbimage,  0 0, 0 0 0,  1->20: cgstepmid, 10->40:cgsteprightend, 0: cgstepall (using steepest descent)
MD++ energythreshold = \[ 0.5 0.01 \] 
MD++ nebspec = \[ 0 1 3 3  0   0 0 0 0 0  10 20 7 \]

MD++ totalsteps = $NEBMAXSTEP equilsteps = $EQUILSTEP
## [ relax_surround redistr_freq moveleftend moverightend yesclimbimage skip skip skip skip skip cgstepsmid cgstepsrightend ]

MD++ Slave_chdir # Slave change to same directory as Master (dirname)
MD++ allocchain_parallel initRchain_parallel

# Save left-end potential energy as initE0 (see mdparallel.cpp), Yifan 2019.5.10
MD++ eval
set initE0 [MD++_Get "EPOT"]
puts "initE0 = $initE0"
MD++ initE0 = $initE0

if { $exist > 0 } {
MD++ incnfile = stringrelax.chain.cn readRchain_parallel
}

MD++ eval

# choose which stringrelax implementation to run
MD++ Broadcast_nebsetting
MD++ stringrelax_parallel

MD++ finalcnfile = stringrelax.chain.cn writeRchain_parallel
set check [ glob -nocomplain "stringeng_step*" ]
set nfile [ llength $check]
exec cp stringeng.out stringeng_step[expr $nfile+1].out
for { set i 0 } { $i < $ncpu } { incr i 1 } {
     set ival [ format %02d $i ]
     exec cp stringrelax.chain.cn.cpu$ival stringrelax.chain_step[expr $nfile+1].cn.cpu$ival
     exec gzip stringrelax.chain_step[expr $nfile+1].cn.cpu$ival
}

MD++ quit_all
