import sys, os, time
import numpy as np

mdppdir = os.environ['MDPLUS_DIR']
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))
from   mdpp_util import mdpp_util

default_split_width      = 10.0           # Angstrom

class ni_disl_util(mdpp_util):
    def __init__(self, runs_dir, folder_prefix=None, mdpp=None, setnolog=True, setoverwrite=True, zipfiles=True,
                 supercell=None, NxNyNz=None, crystalstructure=None, lattice_constant=None, poisson_ratio=0.3,
                 cn_output_format=['cn', 'LAMMPS', 'POSCAR'], make_output_dir=True, material=None):

        super(ni_disl_util, self).__init__(runs_dir, folder_prefix=folder_prefix, mdpp=mdpp,
              setnolog=setnolog, setoverwrite=setoverwrite, zipfiles=zipfiles, supercell=supercell, NxNyNz=NxNyNz,
              crystalstructure=crystalstructure, lattice_constant=lattice_constant, poisson_ratio=poisson_ratio,
              cn_output_format=cn_output_format, make_output_dir=make_output_dir)

        self.d  = default_split_width                     # split width in real length units

    def readeam_ni_Rao(self):
        '''Read eam potential from Rao99
        '''
        potential_path = os.path.join(os.environ['MDPLUS_DIR'], 'potentials', 'EAMDATA', 'eamdata.Ni.Rao99')
        self.mdpp.cmd('potfile = %s'%potential_path)
        self.mdpp.cmd('eamgrid = 5000 readeam')
        self.mdpp.cmd('NNM = 200')

    '''
    overwriting functions defined in mdpp_util
    '''
    def setup_window(self):
        '''Plot Configuration
        '''
        self.mdpp.cmd("""
            atomradius = 1.0 bondradius = 0.3 bondlength = 0
            atomcolor = cyan highlightcolor = purple backgroundcolor = gray
            bondcolor = red fixatomcolor = yellow
            color00 = "orange"  color01 = "red"    color02 = "green"
            color03 = "magenta" color04 = "cyan"   color05 = "purple"
            color06 = "gray80"  color07 = "white"  color08 = "blue"
            #
            plot_color_axis = 2  #2: use CSD (default 0: use local energy)
            #
            plot_color_windows = [ 3
                                   1  3  8 #color08 = blue
                                   3 20  6 #color06 = gray80
                                  40 100 0 #color00 = orange (surface atom CSD = 30)
                                   1
                                  ]
            #plot_limits = [ 1 -10 10 -10 10 -0.37 0.37 ]
            plot_atom_info = 3  plotfreq = 10
            rotateangles = [ 0 0 0 1.3 ]
            #
            win_width = 600 win_height = 600
            """)

    '''
    overwriting functions defined in mdpp_util
    '''
    def set_screw_dipole_geometry(self):
        # burger's vector in scaled coordinates
        self.bs = [1.0/(self.Nx), 0, 0]            # burger's vector in scaled coordinates
        # position of the dislocation dipole in z direction
        self.z0 = 0.111/self.Nz
        dy = -0.125/(self.Ny)
        # position of the dislocation dipole in y direction
        self.y0 = -0.50 + dy
        self.y1 =  0.00 + dy

    '''
    overwriting functions defined in mdpp_util
    '''
    def make_screw_dipole(self, store, nu = None):
        if nu is None: nu = self.nu

        # create a screw dislocation at the center with full burger's vector
        line_direction = 1                 # x, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole

        self.set_screw_dipole_geometry()
        self.create_dislocation_dipole(line_direction, separation_dir, self.bs, self.z0, self.y0, self.y1, nu, store=store)
    
    '''
    overwriting functions defined in mdpp_util
    '''
    def shear_atomic_plane(self):
        self.set_screw_dipole_geometry()
        # make the cut-plane of the dipole cut across entire crystal
        self.create_dislocation_dipole(1, 2, self.bs, self.z0, self.y0, self.y0+1.0, self.nu, store=False)

    '''
    overwriting functions defined in mdpp_util
    '''
    def make_screw_partials(self, store, Fleischer=False, theta=0, xrange=[1., 1., -1., -1.], nu=None, a=None, d=None):
        Lx = self.mdpp.get('H_11')
        Ly = self.mdpp.get('H_22')
        Lz = self.mdpp.get('H_33')
        br = np.array([np.sqrt(2)/(2*2), 0.0, 0.0])  # burger's vector in real coordinates in units of lattice constant a
        if d is None: d = self.d
        if nu is None: nu = self.nu
        if a is None: a = self.a

        xlist = np.array(xrange) * Lx
        # define the rotation of dislocation in x-direction by theta
        yshift = np.array([ d*np.sin(theta)/2, 0, 0, d*np.sin(theta)/2 ])
        zshift = np.array([ d*np.cos(theta)/2, 0, 0, d*np.cos(theta)/2 ])
        if Fleischer:
            # create the dislocation polygon in half-width with 1/2 burger's vector
            self.create_disl_polygon(br, xlist, self.y1*Ly+yshift*2, zshift*2, nu, a, store=store)
        else:
            # create the dislocation polygon in half-width with 1/2 burger's vector
            self.create_disl_polygon(br, xlist, self.y1*Ly+yshift, zshift, nu, a, store=True)
            # create the dislocation polygon for the other half-width with 1/2 burger's vector (FE mechanism only, total width is d)
            self.create_disl_polygon(br, xlist, self.y1*Ly-yshift,-zshift, nu, a, store=store)

    '''
    overwriting functions defined in mdpp_util
    '''
    def set_edge_dipole_geometry(self):
        # burger's vector in scaled coordinates
        self.be = [1.0/(self.Nx), 0, 0]            # burger's vector in scaled coordinates
        # position of the dislocation dipole in x direction
        self.x0 = 0.111/self.Nx
        dy = -0.125/(self.Ny)
        # position of the dislocation dipole in y direction
        self.y0 = -0.50 + dy
        self.y1 =  0.00 + dy

    '''
    overwriting functions defined in mdpp_util
    '''
    def make_edge_dipole(self, store, nu = None):
        if nu is None: nu = self.nu

        # create a screw dislocation at the center with full burger's vector
        line_direction = 3                 # z, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole

        self.set_edge_dipole_geometry()
        self.create_dislocation_dipole(line_direction, separation_dir, self.be, self.x0, self.y0, self.y1, nu, store=store)

    '''
    overwriting functions defined in mdpp_util
    '''
    def make_edge_partials(self, store, theta=0, zrange=[1., 1., -1., -1.], nu=None, a=None, d=None):
        Lx = self.mdpp.get('H_11')
        Ly = self.mdpp.get('H_22')
        Lz = self.mdpp.get('H_33')
        br = np.array([np.sqrt(2)/(2*2), 0.0, 0.0])  # burger's vector in real coordinates in units of lattice constant a
        if d is None: d = self.d
        if nu is None: nu = self.nu
        if a is None: a = self.a
        self.set_edge_dipole_geometry()

        zlist = np.array(zrange) * Lx
        # define the rotation of dislocation in x-direction by theta
        yshift = np.array([ d*np.sin(theta)/2, 0, 0, d*np.sin(theta)/2 ])
        xshift = np.array([ d*np.cos(theta)/2, 0, 0, d*np.cos(theta)/2 ])

        # create the dislocation polygon in half-width with 1/2 burger's vector
        self.create_disl_polygon(-br, xshift, self.y1*Ly+yshift, zlist, nu, a, store=True)
        # create the dislocation polygon for the other half-width with 1/2 burger's vector (FE mechanism only, total width is d)
        self.create_disl_polygon(-br, -xshift, self.y1*Ly-yshift, zlist, nu, a, store=store)
