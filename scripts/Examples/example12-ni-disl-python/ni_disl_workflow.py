''' 
Create dislocation structures in FCC Ni (dissociated edge and screw dislocations in crystal with surfaces)

Compile by:
  make eam build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python3 scripts/Examples/example12-ni-disl-python/ni_disl_workflow.py <edge/screw> [Nx Ny Nz]

To do:
 1. allow by_slice to specify direction (done)
 2. create edge dislocation (done)
 3. create surface by extending box instead of removing atoms (done)
 4. set default box orientation matrix (done)
 5. make edge dislocation dissociate (done)

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))

import numpy as np
import mdpp
from   ni_disl_util import ni_disl_util

if len(sys.argv) < 2:
    print("Please specify disl_type (edge or screw) in command line")
    disl_type = 'unspecified' # default (edge/screw)
else:
    disl_type = sys.argv[1]

if len(sys.argv) < 5:
    default_Nx, default_Ny, default_Nz = [16, 8, 5]
    print("default NxNyNz = %d, %d, %d"%(default_Nx, default_Ny, default_Nz))
else:
    default_Nx, default_Ny, default_Nz = [int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])]
    print("changed NxNyNz = %d, %d, %d"%(default_Nx, default_Ny, default_Nz))

# initialize util object
supercell = [[ 0.5, -0.5,  0], [ 1,  1,  1], [-1, -1, 2]]
util = ni_disl_util(runsdir, folder_prefix="ni-disl", mdpp=mdpp, supercell=supercell,
                    NxNyNz=[default_Nx,default_Ny,default_Nz],
                    crystalstructure="face-centered-cubic", lattice_constant=3.52, zipfiles=False)

util.readeam_ni_Rao()

if disl_type == 'edge':
    # by_slice indicate the direction the configuration can be cut into thin slices later
    util.make_perfect_crystal(by_slice=3) # for screw dislocation, the line direction is along z
    util.cmd('finalcnfile = "perf.cn" writecn ') # perfect crystal needed for upper layer

    util.cmd('saveH ')
    util.remove_atomic_plane()
    util.cmd('input = [ 1 1 -0.1 ] changeH_keepS ') # compress box to help heal the cut
    util.relax_fixbox()
    util.cmd('restoreH ')
    util.relax_fixbox()
    util.cmd('finalcnfile = "perf-markremoved.cn" writecn ') # perfect crystal needed for lower layer

    util.cmd('incnfile = "perf-markremoved.cn" readcn setconfig2 ')
    util.cmd('incnfile = "perf.cn"             readcn setconfig1 ')

    # mark surface layer atomic groups
    util.cmd('input = [ 1 -10 10   %f 0.6 -10 10 ] fixatoms_by_position input = 1 setfixedatomsgroup freeallatoms '%( 0.5-2.1/3.0/default_Ny))
    util.cmd('input = [ 1 -10 10  -0.6 %f -10 10 ] fixatoms_by_position input = 2 setfixedatomsgroup freeallatoms '%(-0.5+1.9/3.0/default_Ny))

    # create dissociated dislocations
    util.make_edge_partials(store=True)     # let edge dislocation dissociate
    util.make_edge_dipole(store=True)       # make edge dislocation
    util.cmd("commit_storedr ")

    # apply surface layer atoms from perfect crystals
    util.cmd('input = [ 1 1 ] SR1toSR_by_group ')
    util.cmd('input = [ 1 2 ] SR2toSR_by_group ')
    util.cmd('commit_removeatoms ')

    # make y-axis vertical
    util.cmd('SHtoR H_12 = 0 H_32 = 0 RHtoS ')

    # create surface by expanding H box with R fixed
    util.cmd('input = [ 2 2 0.2 ] changeH_keepR ')
    util.cmd('input = [ 2 1 2 ] fixatoms_by_group clearR0 refreshnnlist ')

    util.cmd("NCS = 12 eval calcentralsymmetry ")
    util.openwindow()
    util.relax_fixbox()
    util.cmd("finalcnfile = edge_%dx%dx%d.cn     writecn "%(util.Nx,util.Ny,util.Nz))

    # cut the configuration into a single slice along dislocation line
    print('pause for 5 seconds before cutting configuration to thin slice')
    time.sleep(5)
    util.cut_thin_slice()
    util.cmd("finalcnfile = edge_%dx%dx%d.cn     writecn     "%(util.Nx,util.Ny,1))
    util.cmd("finalcnfile = edge_%dx%dx%d.POSCAR writePOSCAR "%(util.Nx,util.Ny,1))

    util.sleep_at_end()

elif disl_type == 'screw':
    # by_slice indicate the direction the configuration can be cut into thin slices later
    util.make_perfect_crystal(by_slice=1) # for screw dislocation, the line direction is along x
    util.cmd('finalcnfile = "perf.cn" writecn ') # perfect crystal needed for upper layer

    util.shear_atomic_plane()
    util.relax_fixbox()
    util.cmd('finalcnfile = "perf-sheared.cn" writecn ') # perfect crystal needed for lower layer

    util.cmd('incnfile = "perf-sheared.cn" readcn setconfig2 ')
    util.cmd('incnfile = "perf.cn"         readcn setconfig1 ')

    # mark surface layer atomic groups
    util.cmd('input = [ 1 -10 10   %f 0.6 -10 10 ] fixatoms_by_position input = 1 setfixedatomsgroup freeallatoms '%( 0.5-2.1/3.0/default_Ny))
    util.cmd('input = [ 1 -10 10  -0.6 %f -10 10 ] fixatoms_by_position input = 2 setfixedatomsgroup freeallatoms '%(-0.5+1.9/3.0/default_Ny))

    # create dissociated dislocations
    util.make_screw_dipole(store=True)       # make non-dissociated screw dislocation
    util.make_screw_partials(store=True)     # let screw dislocation dissociate
    util.cmd("commit_storedr ")

    # apply surface layer atoms from perfect crystals
    util.cmd('input = [ 1 1 ] SR1toSR_by_group ')
    util.cmd('input = [ 1 2 ] SR2toSR_by_group ')
    util.cmd('commit_removeatoms ')       # no effect here (there are no atoms to remove)

    # make y-axis vertical
    util.cmd('SHtoR H_12 = 0 H_32 = 0 RHtoS ')

    # create surface by expanding H box with R fixed
    util.cmd('input = [ 2 2 0.2 ] changeH_keepR ')
    util.cmd('input = [ 2 1 2 ] fixatoms_by_group clearR0 refreshnnlist ')

    util.openwindow()
    util.relax_fixbox()
    util.cmd("finalcnfile = screw_%dx%dx%d.cn     writecn "%(util.Nx,util.Ny,util.Nz))

    # cut the configuration into a single slice along dislocation line
    print('pause for 5 seconds before cutting configuration to thin slice')
    time.sleep(5)
    util.cut_thin_slice()
    util.cmd("finalcnfile = screw_%dx%dx%d.cn     writecn     "%(1,util.Ny,util.Nz))
    util.cmd("finalcnfile = screw_%dx%dx%d.POSCAR writePOSCAR "%(1,util.Ny,util.Nz))

    util.sleep_at_end()

else:
    print("Error: unknown disl_type (%s)"%(disl_type))
