import sys, os, time
import numpy as np

mdppdir = os.environ['MDPLUS_DIR']
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))
from   mdpp_util import mdpp_util

default_pressure         = 2.5
default_relative_split_width  = 0.5          # dimensionless

class he_disl_util(mdpp_util):
    def __init__(self, runs_dir, folder_prefix=None, mdpp=None, setnolog=True, setoverwrite=True, zipfiles=True,
                 supercell=None, NxNyNz=None, crystalstructure=None, lattice_constant=None, poisson_ratio=0.3,
                 cn_output_format=['cn', 'LAMMPS', 'POSCAR'], make_output_dir=True, material=None):

        super(he_disl_util, self).__init__(runs_dir, folder_prefix=folder_prefix, mdpp=mdpp,
              setnolog=setnolog, setoverwrite=setoverwrite, zipfiles=zipfiles, supercell=supercell, NxNyNz=NxNyNz,
              crystalstructure=crystalstructure, lattice_constant=lattice_constant, poisson_ratio=poisson_ratio,
              cn_output_format=cn_output_format, make_output_dir=make_output_dir)

        self.ds = default_relative_split_width                     # split width in scaled length units (dimensionless)
        self.pressure = default_pressure

    '''
    overwriting functions defined in mdpp_util
    '''
    def setup_window(self):
        '''Plot Configuration
        '''
        self.mdpp.cmd("""
            atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0 #2.8285 #for Si
            #
            atomcolor = cyan highlightcolor = purple  bondcolor = red
            fixatomcolor = yellow backgroundcolor = gray70
            color00 = "red" color01 = "blue" color02 = "green"
            color03 = "magenta" color04 = "cyan" color05 = "purple"
            color06 = "gray80" color07 = "white" color08 = "orange"
            #
            plot_color_axis = 2
            plot_color_windows = [ 5
                                   0.01 6   1  
                                   6  10   5
                                   40 200   8
                                   0 0.01  4
                                   10 40   6
                                 ]
            plot_atom_info = 2 plotfreq = 10
            rotateangles = [ 0 0 0 1.5 ]
            win_width = 600 win_height = 600
            """)

    def set_edge_dipole_vsplit_geometry(self):
        # burger's vector in scaled coordinates
        self.be = [1.0/(self.Nx), 0, 0]
        # position of the dislocation dipole in x direction
        self.x0 = -0.000123 + 1.0/6.0/self.Nx
        dy = -0.125/(self.Ny)
        # position of the dislocation dipole in y direction
        self.y0 = -0.50 + dy
        self.y1 =  0.00 + dy - 0.5*self.dvs
        self.y2 =  0.00 + dy + 0.5*self.dvs

    def make_edge_dipole_vsplit(self, store, nu=None):
        if nu is None: nu = self.nu
        self.set_edge_dipole_vsplit_geometry()

        # create a edge dislocation at the center (with offset) with full burger's vector
        line_direction = 3                 # z, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole

        self.create_dislocation_dipole(line_direction, separation_dir, self.be, self.x0, self.y0, self.y1, nu, store=store)

    def make_edge_partials_vsplit(self, store, nu=None, bs_mul=1.0):
        if nu is None: nu = self.nu
        self.set_edge_dipole_vsplit_geometry()

        line_direction = 3                 # z, dislocation line direction
        separation_dir = 2                 # y, separation direction between two dislocations in the dipole
        bp1 = [ 0.5/(self.Nx), 0, bs_mul*(-1.0)/6.0/(self.Nz)] # burger's vector in scaled coordinates

        self.create_dislocation_dipole(line_direction, separation_dir, bp1, self.x0, self.y1, self.y2, nu, store=store)
