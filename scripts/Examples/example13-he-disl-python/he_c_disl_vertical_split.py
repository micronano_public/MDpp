''' 
Create dissociated dislocations with Burgers vector along c-axis

Compile by:
  make heaziz build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python3 scripts/Examples/example13-he-disl-python/he_c_disl_vertical_split.py <edge/screw> [Nx Ny Nz] [dvs]

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))

import numpy as np
import mdpp
from   he_disl_util import he_disl_util

if len(sys.argv) < 2:
    print("Please specify disl_type (edge or screw) in command line")
    disl_type = 'unspecified' # default (edge/screw)
else:
    disl_type = sys.argv[1]

if len(sys.argv) < 5:
    default_Nx, default_Ny, default_Nz = [10, 20, 6]
    print("default NxNyNz = %d, %d, %d"%(default_Nx, default_Ny, default_Nz))
else:
    default_Nx, default_Ny, default_Nz = [int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])]
    print("changed NxNyNz = %d, %d, %d"%(default_Nx, default_Ny, default_Nz))

if len(sys.argv) < 6:
    default_relative_vsplit_width = 0.2          # dimensionless
    print("default_relative_vsplit_width = %d"%(default_relative_vsplit_width))
else:
    default_relative_vsplit_width = float(sys.argv[5])
    print("changed relative_vsplit_width = %d"%(default_relative_vsplit_width))

if disl_type == 'edge':
    # initialize util object
    supercell = [[0,0,1],[1,0,0],[0,1,0]]
    util = he_disl_util(runsdir, folder_prefix="he-c-disl", mdpp=mdpp, supercell=supercell,
                        NxNyNz=[default_Nx,default_Ny,default_Nz],
                        crystalstructure="hexagonal-ortho", lattice_constant=[3.577, 0, 5.841], zipfiles=False)

    util.dvs = default_relative_vsplit_width  # split width in scaled length units (dimensionless)

    # by_slice indicate the direction the configuration can be cut into thin slices later
    util.make_perfect_crystal(by_slice=3) # for screw dislocation, the line direction is along z
    util.cmd('finalcnfile = "perf.cn" writecn ') # perfect crystal needed for upper layer

    util.cmd('saveH ')
    util.remove_atomic_plane()
    util.cmd('input = [ 1 1 -0.1 ] changeH_keepS ') # compress box to help heal the cut
    util.relax_fixbox()
    util.cmd('restoreH ')
    util.relax_fixbox()
    util.cmd('finalcnfile = "perf-markremoved.cn" writecn ') # perfect crystal needed for lower layer

    util.cmd('incnfile = "perf-markremoved.cn" readcn setconfig2 ')
    util.cmd('incnfile = "perf.cn"             readcn setconfig1 ')

    # mark surface layer atomic groups
    util.cmd('input = [ 1 -10 10   %f 0.6 -10 10 ] fixatoms_by_position input = 1 setfixedatomsgroup freeallatoms '%( 0.5-1.1/default_Ny))
    util.cmd('input = [ 1 -10 10  -0.6 %f -10 10 ] fixatoms_by_position input = 2 setfixedatomsgroup freeallatoms '%(-0.5+0.9/default_Ny))

    # create vertically dissociated dislocations
    util.ds = 0.2  # relative split width in scaled units
    util.set_edge_dipole_vsplit_geometry()
    util.make_edge_partials_vsplit(store=True, bs_mul=0)     # let edge dislocation dissociate
    util.make_edge_dipole_vsplit(store=True)                        # make edge dislocation
    util.cmd("commit_storedr ")

    # apply surface layer atoms from perfect crystals
    util.cmd('input = [ 1 1 ] SR1toSR_by_group ')
    util.cmd('input = [ 1 2 ] SR2toSR_by_group ')
    util.cmd('commit_removeatoms ')

    # make y-axis vertical
    util.cmd('SHtoR H_12 = 0 H_32 = 0 RHtoS ')

    # create surface by expanding H box with R fixed
    util.cmd('input = [ 2 2 0.2 ] changeH_keepR ')
    util.cmd('input = [ 2 1 2 ] fixatoms_by_group clearR0 refreshnnlist ')

    util.cmd("NCS = 12 eval calcentralsymmetry ")
    util.openwindow()
    util.relax_fixbox(fevalmax=200)
    util.cmd("finalcnfile = edge_%dx%dx%d.cn     writecn "%(util.Nx,util.Ny,util.Nz))

    # cut the configuration into a single slice along dislocation line
    print('pause for 5 seconds before cutting configuration to thin slice')
    time.sleep(5)
    util.cut_thin_slice()
    util.cmd("finalcnfile = edge_%dx%dx%d.cn     writecn     "%(util.Nx,util.Ny,1))
    util.cmd("finalcnfile = edge_%dx%dx%d.POSCAR writePOSCAR "%(util.Nx,util.Ny,1))

    util.sleep_at_end()

elif disl_type == 'screw':
    # initialize util object
    supercell = [[0,1,0],[1,0,0],[0,0,-1]]
    util = he_disl_util(runsdir, folder_prefix="he-c-disl", mdpp=mdpp, supercell=supercell,
                        NxNyNz=[default_Nx,default_Ny,default_Nz],
                        crystalstructure="hexagonal-ortho", lattice_constant=[3.577, 0, 5.841], zipfiles=False)

    print("Error: disl_type (%s) not fully implemented"%(disl_type))

else:
    print("Error: unknown disl_type (%s)"%(disl_type))
