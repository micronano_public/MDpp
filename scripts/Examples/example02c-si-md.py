# -*- python script-*-
# MD code of Stinger-Weber Silicon
import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

# Definition of procedures

import mdpp

def initmd(T) :
    mdpp.cmd('setnolog setoverwrite')
    mdpp.cmd('dirname = runs/si-example-{0}'.format(T))

def makecrystal(nx,ny,nz) :
    # create perfect lattice configuration
    mdpp.cmd('crystalstructure = diamond-cubic')
    mdpp.cmd('latticeconst = 5.4309529817532409') # (A) for Si
    mdpp.cmd('element0 = Silicon')
    mdpp.cmd('latticesize = [ 1 0 0 {0} 0 1 0 {1} 0 0 1 {2} ]'.format(nx,ny,nz))
    mdpp.cmd('makecrystal  finalcnfile = perf.cn writecn')

# mdpp.cmd('')

def setup_window() :
    # plot settings
    mdpp.cmd('''
     atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si 
     atomcolor = orange highlightcolor = purple 
     bondcolor = red backgroundcolor = gray70
    #plot_color_bar = [ 1 -4.85 -4.50 ]  highlightcolor = red
     plotfreq = 10  rotateangles = [ 0 0 0 1.25 ]
    ''')

def openwindow() :
    #Configure and open a plot
    setup_window()

    #### setup_window
    mdpp.cmd('openwin  alloccolors rotate saverot eval plot')
    

def relax_fixbox() :
    #Conjugate-Gradient relaxation

    mdpp.cmd('''
    conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000 
    conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1 
    relax 
    ''')
    

def relax_freebox() :
    #Conjugate-Gradient relaxation

    mdpp.cmd('''
    conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000 
    conj_fixbox =  #conj_monitor = 1 conj_summary = 1 
    relax 
    ''')
    

def setup_md() :
    # MD settings (without running MD)

    mdpp.cmd('''
    equilsteps = 0  totalsteps = 100 timestep = 0.0001 # (ps) 
    atommass = 28.0855 # (g/mol) 
   #atomTcpl = 200.0 boxTcpl = 20.0 
    DOUBLE_T = 0 
    srand48bytime 
    initvelocity totalsteps = 10000 saveprop = 0 
    saveprop = 1 savepropfreq = 10 openpropfile 
    savecn = 1  savecnfreq = 100 
    writeall = 1 
    savecfg = 1 savecfgfreq = 100 
    ensemble_type = "NVT" integrator_type = "VVerlet" 
    implementation_type = 0 vt2=1e28 

    totalsteps = 20000
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESS_xx TSTRESS_yy TSTRESS_zz" 
    plotfreq = 50 #autowritegiffreq = 10
    ''')
    
             

# Main program

# from tkinter import *

import sys


if len(sys.argv) == 1 :
    status = 0
elif len(sys.argv) > 1 :
    status = int(sys.argv[1])
print(status)

if len(sys.argv) <= 2 :
    T_OBJ = 300.0
elif len(sys.argv) > 2 :
    T_OBJ = float(sys.argv[2])
print(T_OBJ)

if status == 0 :
    #create crystal, relax, run MD/NVT simulation

    initmd(T_OBJ)

    makecrystal(4,4,4)

    sx = mdpp.get_array('SR',0,30,3)
    sy = mdpp.get_array('SR',1,31,3)
    sz = mdpp.get_array('SR',2,32,3)

    print('sx = ', sx)
    print('sy = ', sy)
    print('sz = ', sz)

    #mdpp.cmd('eval')

    print('modifying position of atom 3 and 5')

    sx[3] = sx[3] + 0.220
    sy[3] = sy[3] - 0.029
    sz[3] = sz[3] + 0.014

    sx[5] = sx[5] + 0.091
    sy[5] = sy[5] + 0.382
    sz[5] = sz[5] + 0.064

    mdpp.set_array(sx, 'SR', 0, 3)
    mdpp.set_array(sy, 'SR', 1, 3)
    mdpp.set_array(sz, 'SR', 2, 3)

    print('after set')
    print('sx = ', sx)
    print('sy = ', sy)
    print('sz = ', sz)

    sxnew = mdpp.get_array('SR',0,30,3)
    synew = mdpp.get_array('SR',1,31,3)
    sznew = mdpp.get_array('SR',2,32,3)

    print('sxnew = ', sxnew)
    print('synew = ', synew)
    print('sznew = ', sznew)

    mdpp.cmd('finalcnfile = si512.cn writecn')

    #mdpp.cmd('eval')

    # Uncomment the following lines to run MD simulation
    #openwindow()
    #setup_md()
    #mdpp.cmd('T_OBJ = {0}'.format(T_OBJ))
    #mdpp.cmd('initvelocity run')
    #mdpp.cmd('sleep quit')
    
elif status == 1 :
    #read in relaxed, NVT

    initmd(T_OBJ)
    mdpp.cmd('incnfile = si512.cn readcn')

    openwindow()
    
    mdpp.cmd('sleep quit')
else :
    print('unknown argument. {0}'.format(status))

    mdpp.cmd('quit')

# Plot simulation data
#print "Plot simulation data using pylab"
#from pylab import *
#prop = loadtxt('prop.out')
#plot( prop[:,0], prop[:,3] )
#xlabel("steps")
#ylabel("T (K)")
#ion()
#show()

# Sleep 
#import time
#sleep_seconds = 60
#print "Python is going to sleep for " + str(sleep_seconds) + " seconds."
#time.sleep(sleep_seconds)
