#!/usr/bin/env Rscript
# MD code of Stinger-Weber Silicon
# Run as: 
#   Rscript scripts/Examples/example02a-si-md.r

# Load MD++ library
dyn.load("bin/sw_r.so")

source("scripts/Examples/R/startup.r")

mdpp.init("si-md")

mdpp.cmd("setnolog")
mdpp.cmd("setoverwrite")

mdpp.cmd("dirname = runs/test")

mdpp.cmd("crystalstructure = diamond-cubic")

mdpp.cmd("latticeconst = 5.4309529817532409 #(A) for Si")
mdpp.cmd("latticesize  = [ 1 0 0 4  0 1 0 4  0 0 1 4 ]")
mdpp.cmd("makecrystal  writecn")

mdpp.cmd("atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si")
mdpp.cmd("atomcolor = orange highlightcolor = purple  bondcolor = red backgroundcolor = gray70")
mdpp.cmd("#plot_color_bar = [ 1 -4.85 -4.50 ]  highlightcolor = red")
mdpp.cmd("plotfreq = 10	rotateangles = [ 0 0 0 1.25 ] #[ 0 -90 -90 1.5 ]")

mdpp.cmd("openwin  alloccolors rotate saverot eval plot")
#mdpp.cmd("sleep quit")

#Calculate Hessian matrix
#mdpp.cmd("timestep = 0.001 calHessian")

#Conjugate-Gradient relaxation
mdpp.cmd("conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000")
mdpp.cmd("conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1")
mdpp.cmd("relax finalcnfile = relaxed.cn writecn")

#MD settings           
mdpp.cmd("T_OBJ = 300 #Kelvin")
mdpp.cmd("equilsteps = 0  totalsteps = 100 timestep = 0.0001 # (ps)")
mdpp.cmd("atommass = 28.0855 # (g/mol)")
mdpp.cmd("DOUBLE_T = 0")
mdpp.cmd("srand48bytime")
mdpp.cmd("initvelocity totalsteps = 10000 saveprop = 0")
mdpp.cmd("saveprop = 1 savepropfreq = 10 openpropfile ")
mdpp.cmd("ensemble_type = NVE integrator_type = VVerlet")
mdpp.cmd("totalsteps = 10000")
mdpp.cmd("saveprop = 1 savepropfreq = 10 openpropfile")
mdpp.cmd("plotfreq = 1 #autowritegiffreq = 10")
mdpp.cmd("run finalcnfile = si100.cn writecn")
mdpp.cmd("sleep")
mdpp.cmd("quit")

