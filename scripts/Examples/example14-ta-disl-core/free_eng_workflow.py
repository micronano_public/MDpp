''' 
Compute free energy of perfect crystals and crystals containing dislocation dipole at finite temperature

Compile by:
  make eam build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py <workstep> <jobid> <material> <temperature> <disloc> <mult>

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))

import numpy as np
import matplotlib.pyplot as plt
import mdpp
from   free_eng_util import free_eng_util

if len(sys.argv) < 7:
    print("Please specify <workstep> <jobid> <material> <temperature> <disloc> <mult> in command line")
    print(" for example 1 1 Ta-fs 300 0 1 ")
    exit()
else:
    workstep =   int(sys.argv[1])
    jobid    =   int(sys.argv[2])
    material =       sys.argv[3]
    T        = float(sys.argv[4])
    disl     =   int(sys.argv[5])
    mult     =   int(sys.argv[6])

# initialize util object
util = free_eng_util(runsdir, folder_prefix="ta-disl-core", mdpp=mdpp,
        supercell=[[ 0, 1, -1], [ 0, 1, 1], [1, 0, 0]], NxNyNz=[8,12,3],
        crystalstructure="body-centered-cubic", lattice_constant=3.3058,
        material=material)

util.wait_for_flag(workstep)

if material == 'Ta-fs':
    print("material = %s"%material)
    util.read_fs_pot('ta_pot')
    util.a                  = 3.30579978 # (3.165 in melting_cubic.tcl)
    util.L                  = 6
    util.T_solid_0          = T          # Important: specified from command line (e.g. 300 K)
    util.eT_solid_0         = 0.002
    util.factor_solid_RS    = 0.5
    util.T_liquid_0         = None
    util.eT_liquid_0        = 0.02
    util.factor_liquid_RS   = None
    util.factor_gauss       = 0.1
    util.atom_mass          = 180.948
    util.crystalstructure   = 'body-centered-cubic'
    util.natom_per_unitcell = 2
elif material == 'Ta-eam':
    print("material = %s"%material)
    util.read_eam_pot('EAMDATA/eamdata.Ta.Li03')
    util.a                  = 3.30579978 # (3.165 in melting_cubic.tcl)
    util.L                  = 6
    util.T_solid_0          = T          # Important: specified from command line (e.g. 300 K)
    util.eT_solid_0         = 0.002
    util.factor_solid_RS    = 0.5
    util.T_liquid_0         = None
    util.eT_liquid_0        = 0.02
    util.factor_liquid_RS   = None
    util.factor_gauss       = 0.1
    util.atom_mass          = 180.948
    util.crystalstructure   = 'body-centered-cubic'
    util.natom_per_unitcell = 2
else:
    print("Error: unknown material (%s)"%(material))
    exit()

# here you may increase the simulation steps for higher accuracy (default in free_eng_util.py)
#util.numsteps_equil_vol =  2000000
#util.numsteps_md_switch = 10000000

if workstep == 1:
    if disl == 0:
        util.make_perfect_crystal(savefile='perf')
    else:
        print("Error: disl != 0 not implemented!")
        exit()

    if mult != 1:
        print("Error: mult != 1 not implemented!")
        exit()

if workstep in [1,2,3,4,5,6,14,15]:
    util.free_energy_calculation(workstep, jobid, T, mult, initcn='perf')
elif workstep in [7,8,9,10,11,12,13]:
    print('we are not supposed to run the liquid simulations in this study')
else:
    print('unknown workstep %d'%workstep)

if workstep == 15:
    if util.solid_success:
        plt.plot(util.T_solid,  util.Fs)
        plt.show()
