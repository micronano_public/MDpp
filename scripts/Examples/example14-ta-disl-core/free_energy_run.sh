#!/bin/bash

echo "Run all steps of free_energy_workflow.py"

material='Ta-fs'
T=300
L=1

make clean; make fs build=R SYS=$MDPLUS_SYS PY=yes

python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  1 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  2 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  3 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  4 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  5 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  6 1 $material $T 0 $L
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py 14 1 $material $T 0 $L

python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py 15 1 $material $T 0 $L

