#!/bin/sh
#SBATCH --job-name=ta_core
#SBATCH --output=ta_core.log
#SBATCH --error=ta_core.err
#SBATCH --time=7-00:00:00
#SBATCH --partition=cpu
#SBATCH --nodes=1
#SBATCH --tasks-per-node=4

echo "Run all steps of melting_cubic.py"

material='Ta-fs'
T=300
L=1

#make clean; make fs build=R SYS=$MDPLUS_SYS PY=yes

# for speed the following jobs can run in parallel when submitting to cluster (i.e. with &)
#   need to adjust --tasks-per-node when running jobs in parallel

python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  1 1 $material $T 0 $L &
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  2 1 $material $T 0 $L &
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  3 1 $material $T 0 $L &
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  4 1 $material $T 0 $L &
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  5 1 $material $T 0 $L &
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py  6 1 $material $T 0 $L &

wait

python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py 14 1 $material $T 0 $L 
python3 scripts/Examples/example14-ta-disl-core/free_eng_workflow.py 15 1 $material $T 0 $L

