#!/bin/bash

echo "Core free energy"

T=300
L=1

bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 1 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 2 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 3 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 4 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 5 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 6 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 14 1 Ta-fs $T 0 $L
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 15 1 Ta-fs $T 0 $L

#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 1 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 2 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 3 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 4 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 5 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 6 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 14 1 Ta-eam $T 0 $L
#bin/eam_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 15 1 Ta-eam $T 0 $L
