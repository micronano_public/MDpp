# Core free energy calculation at finite temperature

This is a Tcl script for computing core free energy of pure elements in MD++. This script is adapted from free energy calculation to compute melting point [melting_cubic.tcl](http://micro.stanford.edu/wiki/Computing_Melting_Point_by_Free_Energy_Method).

### Syntax
Compile MD++
```
make fs build=R SYS=$MDPLUS_SYS
```
Run the tcl script
```
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl workstep jobid material temperature disloc mult
```

### Steps

The script contains the following steps:

workstep 1: determine thermal expansion of solid at T1 K  
workstep 2: compute hessian matrix of quasi-harmonic crystal(QHC)  
workstep 3: adiabatic switching from real solid to QHC  
workstep 4: adiabatic switching from QHC to real solid  
workstep 5: reversible scaling from T1 to T1/lambda1  
workstep 6: reversible scaling from T1/lambda to T1

workstep 14: generate matlab file to compute eigenvalues of the Hessian matrix  
workstep 15: generate matlab script to extract free energy  
workstep 16: tar all relevant output files


To perform the full calculation, the following jobs need to be run:  

(Determine equilibrium volume of solid and compute hessian matrix)  
```
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  1 1 Ta-fs 300 0 1  
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  2 1 Ta-fs 300 0 1  
```

(Determine free energy of solid)  
```
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  3 1 Ta-fs 300 0 1 
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  4 1 Ta-fs 300 0 1  
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  5 1 Ta-fs 300 0 1  
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl  6 1 Ta-fs 300 0 1  
```

(Generate matlab files from simulation data to determine core free energy  
(14: Hessian_Ta.m, 15: Free_Energy_of_Ta.m, 16: tar_Ta )  
```
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 14 1 Ta-fs 300 0 1  
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 15 1 Ta-fs 300 0 1  
bin/fs_mac scripts/Examples/example14-ta-disl-core/core_free_energy.tcl 16 1 Ta-fs 300 0 1  
``` 
