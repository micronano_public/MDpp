''' 
 status 0: NVE simulation
        1: NPT simulation
        2: NVT simulation with adjusting volume to zero stress
        3: NVT simulation

Compile by:
  make fs build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python scripts/Examples/example15-thermal-elastic/ta_perf_workflow.py <workstep> <material> <maxtemp> 

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))

import numpy as np
import matplotlib.pyplot as plt
import mdpp
from   mdpp_util import mdpp_util


if len(sys.argv) == 3 :
    workstep =   int(sys.argv[1])
    material =       sys.argv[2]
    Tmax     = 300
elif len(sys.argv) == 2:
    workstep =   int(sys.argv[1])
    material =    'Ta-fs'
    Tmax     = 300
elif len(sys.argv) == 1:
    workstep =   0
    material =    'Ta-fs'
    Tmax     = 300
else:
    workstep =   int(sys.argv[1])
    material =       sys.argv[2]
    Tmax     = float(sys.argv[3])
    if len(sys.argv) > 4:
        disl = int(sys.argv[4])
    else:
        disl = 0
    if len(sys.argv) > 5:
        mult = int(sys.argv[5])
    else:
        mult = 1

    # initialize util object
supercell = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
util = mdpp_util(runsdir, folder_prefix="ta-perf-workflow", mdpp=mdpp, supercell=supercell, NxNyNz=[5,5,5],
                     crystalstructure="body-centered-cubic", lattice_constant=3.3058, zipfiles=False,setnolog=False, setoverwrite=True)

if material == 'Ta-fs':
    print("material = %s"%material)
    util.read_fs_pot('ta_pot')
    util.a         = 3.3058
    util.atom_mass = 180.94788
    util.natom_per_unitcell = 2
elif material == 'Ta-eam':
    print("material = %s"%material)
    util.read_eam_pot('EAMDATA/eamdata.Ta.Li03')
    util.a         = 3.3026327
    util.atom_mass = 180.94
    util.natom_per_unitcell = 2
else:
    print("Error: unknown material (%s)"%(material))
    exit()
'''
if disl != 0:
    print("Error: disl != 0 not implemented!")
    exit()

if mult != 1:
    print("Error: mult != 1 not implemented!")
    exit()
'''

if workstep == 0:
    '''
	Run an NVE simulation
	'''
    util.make_perfect_crystal(savefile='perf')
    util.setup_md(util.atom_mass)
    util.run_nve()
	
elif workstep == 1:
    '''
	Run an NPT simulation
	'''
    util.make_perfect_crystal(savefile='perf')
    util.setup_md(util.atom_mass)
    util.run_npt()


elif workstep == 3:
    '''
	Run an NVT simulation
	'''
    util.make_perfect_crystal(savefile='perf')
    util.setup_md(util.atom_mass)
    util.run_nvt()

elif workstep == 2:
    '''
	Run an NVT simulation followed by stress relaxation
	'''
    util.make_perfect_crystal(savefile='perf')
    util.setup_md(util.atom_mass)
    util.run_nvt_and_relax()

else:
    print("Error: unknown workstep (%d)"%(workstep))
