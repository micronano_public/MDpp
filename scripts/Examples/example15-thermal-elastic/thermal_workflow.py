''' 
Compute lattice end elastic constants of perfect crystals 
and crystals containing dislocation dipole at finite temperature

Compile by:
  make eam build=R SYS=$MDPLUS_SYS PY=yes

Run by:
  python scripts/Examples/example15-thermal-elastic/thermal_workflow.py <workstep> <material> <maxtemp> [<disloc> <mult>]
  python3 scripts/Examples/example15-thermal-elastic/thermal_workflow.py <workstep> <material> <maxtemp> [<disloc> <mult>]

'''

import sys, os, time
mdppdir = os.environ['MDPLUS_DIR']
runsdir = os.path.join(mdppdir, 'runs')
sys.path.insert(0, os.path.join(mdppdir, 'bin'))
sys.path.insert(0, os.path.join(mdppdir, 'scripts', 'python'))

import numpy as np
import matplotlib.pyplot as plt
import mdpp
from   mdpp_util import mdpp_util

if len(sys.argv) < 4:
    print("Please specify <workstep> <material> <maxtemp> [<disloc> <mult>] in command line")
    print(" for example 1 Ta-fs 300 ")
    workstep = 'unspecified' # default (edge/screw)
else:
    workstep =   int(sys.argv[1])
    material =       sys.argv[2]
    Tmax     = float(sys.argv[3])
    if len(sys.argv) > 4:
        disl = int(sys.argv[4])
    else:
        disl = 0
    if len(sys.argv) > 5:
        mult = int(sys.argv[5])
    else:
        mult = 1

    # initialize util object
    supercell = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    util = mdpp_util(runsdir, folder_prefix="ta-thermal", mdpp=mdpp, supercell=supercell, NxNyNz=[6,6,6],
                     crystalstructure="body-centered-cubic", lattice_constant=3.3058, zipfiles=False)

if material == 'Ta-fs':
    print("material = %s"%material)
    util.read_fs_pot('ta_pot')
    util.a         = 3.30579978
    util.atom_mass = 180.94
    util.natom_per_unitcell = 2
elif material == 'Ta-eam':
    print("material = %s"%material)
    util.read_eam_pot('EAMDATA/eamdata.Ta.Li03')
    util.a         = 3.3026327
    util.atom_mass = 180.94
    util.natom_per_unitcell = 2
else:
    print("Error: unknown material (%s)"%(material))
    exit()

if disl != 0:
    print("Error: disl != 0 not implemented!")
    exit()

if mult != 1:
    print("Error: mult != 1 not implemented!")
    exit()

if workstep == 1:
    
    nsteps = 10000
    
    # get 0K properties and save into files
    util.make_perfect_crystal(savefile='perf')
    util.relax_freebox(filename='relaxed')
    util.cmd('saveH eval ')

    # get lattice constant at 0K
    NP, V0, EPOT0, a0, Ecoh0 = util.get_nve()
    with open('lattice.dat', 'w') as f:
        f.write("%f %.18g %.18g\n"%(0, a0, 0.0))
        
    # get elastic constants at 0K
    util.cmd('conj_fixbox = 1')
    numincr = 5
    strainincr = 0.0001
    S11 = np.zeros((numincr,))
    S22 = np.zeros((numincr,))
    S12 = np.zeros((numincr,))
    # get C11 and C12
    for i in range(numincr):
        util.cmd('input = [ 1 1 %e ] shiftbox relax eval'%strainincr)
        stress = util.get_array('TSTRESSinMPa')
        S11[i] = stress[0]
        S22[i] = stress[4]
    util.cmd('restoreH ')
    # get C44
    for i in range(numincr):
        util.cmd('input = [ 1 2 %e ] shiftbox relax eval'%strainincr)
        stress = util.get_array('TSTRESSinMPa')
        S12[i] = stress[1]
    util.cmd('restoreH ')
    # compute elastic constants by fitting a line
    strain = strainincr*np.arange(numincr)
    C11 = np.abs(np.polyfit(strain, S11, 1)[0])/1e3
    C12 = np.abs(np.polyfit(strain, S22, 1)[0])/1e3
    C44 = np.abs(np.polyfit(strain, S12, 1)[0])/1e3
    # write results in file
    with open('elastic.dat', 'w') as f:
        f.write("%f %.18g %.18g %.18g\n"%(0, C11, C12, C44))

        
    # Compute properties at various temperatures
    Tnum = 10
    T = np.linspace(0., Tmax, Tnum+1)
    for j in range(1, Tnum+1):
        
        # Equilibriate at T
        V = util.equilibrate_volume_at_temperature(T[j], nsteps, workstep, int(T[j]))
        util.cmd('saveH ')
        a = np.cbrt((V/NP)*util.natom_per_unitcell)
        eT = a/a0 - 1
        
        with open('lattice.dat', 'a+') as f:
            f.write("%f %.18g %.18g\n"%(T[j], a, eT))
            
        # Compute elastic constants
        # get C11 and C12
        for i in range(numincr):
            
            util.cmd('input = [ 1 1 %e ] shiftbox'%strainincr)
            
            # run MD simulation at T
            util.run_setup(util.atom_mass, workstep, int(T[j]), i)
            util.cmd('''
            fixboxvec=[ 0 1 1   1 0 1   1 1 0 ] stress= [ 0 0 0 0 0 0 0 0 0 ]
            output_fmt="curstep EPOT Tinst TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_xy"
            writeall=1
            NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NVTC" integrator_type = "VVerlet"
            ''')
            util.cmd('T_OBJ = %g initvelocity totalsteps = %d run '%(T[j],nsteps))
            util.cmd(' closepropfile ')
            
            S11[i] = util.get_average_from_datafile(util.get("outpropfile"), 3, 0.7, 1.0)
            S22[i] = util.get_average_from_datafile(util.get("outpropfile"), 4, 0.7, 1.0)
        
        util.cmd('restoreH ')
        
        # get C44
        for i in range(numincr):
            
            util.cmd('input = [ 1 2 %e ] shiftbox'%strainincr)
            
            # run MD simulation at T
            util.run_setup(util.atom_mass, workstep, int(T[j]), numincr+i)
            util.cmd('''
            fixboxvec=[ 0 1 1   1 0 1   1 1 0 ] stress= [ 0 0 0 0 0 0 0 0 0 ]
            output_fmt="curstep EPOT Tinst TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_xy"
            writeall=1
            NHMass = [ 1e-2 1e-2 1e-2 1e-2 ] NHChainLen = 4 ensemble_type = "NVTC" integrator_type = "VVerlet"
            ''')
            util.cmd('T_OBJ = %g initvelocity totalsteps = %d run '%(T[j],nsteps))
            util.cmd(' closepropfile ')
            
            S12[i] = util.get_average_from_datafile(util.get("outpropfile"), 5, 0.7, 1.0)
        
        util.cmd('restoreH ')
        
        # compute elastic constants by fitting a line
        C11 = np.abs(np.polyfit(strain, S11, 1)[0])/1e3
        C12 = np.abs(np.polyfit(strain, S22, 1)[0])/1e3
        C44 = np.abs(np.polyfit(strain, S12, 1)[0])/1e3
        # write results in file
        with open('elastic.dat', 'a+') as f:
            f.write("%f %.18g %.18g %.18g\n"%(T[j], C11, C12, C44))


elif workstep == 2:
    
    # Plot results
    data = np.loadtxt(util.dirname + '/lattice.dat')
    fig, ax1 = plt.subplots()
    ax1.plot(data[:,0], data[:,1])
    ax1.set_xlabel('Tempertature (K)')
    ax1.set_ylabel(r'Lattice constant ($\AA$)')
    ax2 = ax1.twinx()
    ax2.plot(data[:,0], data[:,2]+1.0)
    ax2.set_ylabel(r'Expansion ($a/a_0$)')
    
    data = np.loadtxt(util.dirname + '/elastic.dat')
    fig, ax = plt.subplots()
    ax.plot(data[:,0], data[:,1], label='C11')
    ax.plot(data[:,0], data[:,2], label='C12')
    ax.plot(data[:,0], data[:,3], label='C44')
    ax.set_xlabel('Tempertature (K)')
    ax.set_ylabel('Elastic constants (GPa)')
    ax.legend()
    
    plt.show()

else:
    print("Error: unknown workstep (%d)"%(workstep))
