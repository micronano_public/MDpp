import sys
sys.path.insert(0, "./bin_py")

import mdpp

# Run MD++ script
mdpp.cmd("setnolog")
mdpp.cmd("setoverwrite")
mdpp.cmd("dirname = runs/si-example")

#Create Perfect Lattice Configuration
mdpp.cmd("crystalstructure = diamond-cubic")
mdpp.cmd("latticeconst = 5.4309529817532409 #(A) for Si")
mdpp.cmd("latticesize  = [ 1 0 0 4 0 1 0 4 0 0 1 4 ]")
mdpp.cmd("makecrystal  writecn")

#Plot Configuration
mdpp.cmd("atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si")
mdpp.cmd("atomcolor = orange highlightcolor = purple  bondcolor = red backgroundcolor = gray70")
#mdpp.cmd("plot_color_bar = [ 1 -4.85 -4.50 ]  highlightcolor = red")
mdpp.cmd("plotfreq = 10  rotateangles = [ 0 0 0 1.25 ] #[ 0 -90 -90 1.5 ]")
mdpp.cmd("openwin  alloccolors rotate saverot eval plot")

#Conjugate-Gradient relaxation
#
mdpp.cmd("conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000")
mdpp.cmd("conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1")
mdpp.cmd("relax finalcnfile = relaxed.cn writecn")

#MD settings           
mdpp.cmd("T_OBJ = 300 #Kelvin")
mdpp.cmd("equilsteps = 0  totalsteps = 100 timestep = 0.0001 # (ps)")
mdpp.cmd("atommass = 28.0855 # (g/mol)")
#mdpp.cmd("atomTcpl = 200.0 boxTcpl = 20.0")
mdpp.cmd("DOUBLE_T = 0")
mdpp.cmd("srand48bytime")
mdpp.cmd("initvelocity totalsteps = 10000 saveprop = 0")
#mdpp.cmd("integrator_type = 1 #0: Gear6, 1: Velocity Verlet")
mdpp.cmd("saveprop = 1 savepropfreq = 10 openpropfile #run")
#mdpp.cmd("usenosehoover = 1  vt2 = 2e28  #1e28 2e28 5e28")
mdpp.cmd("ensemble_type = NVE integrator_type = VVerlet")

#Test MD run
mdpp.cmd("totalsteps = 10000")
mdpp.cmd("saveprop = 1 savepropfreq = 10 openpropfile")
mdpp.cmd("plotfreq = 1 #autowritegiffreq = 10")
mdpp.cmd("run finalcnfile = si100.cn writecn")


# Plot simulation data
#print("Plot simulation data using pylab")
#from pylab import *
#prop = loadtxt('prop.out')
#plot( prop[:,0], prop[:,9] )
#xlabel("steps")
#ylabel("T (K)")
#ion()
#show()

# Sleep 
import time
sleep_seconds = 60
print("Python is going to sleep for " + str(sleep_seconds) + " seconds.")
time.sleep(sleep_seconds)

