function [griddata, elemdata, Frhodata, rhordata, phirdata] = readLAMMPSeamfs(potfilename)
% read LAMMPS potential file of the eam.fs type
% notice: phirdata stores r*phi (eV*Angstrom)

fid = fopen(potfilename,'r');

% Following comments are copied from lammps documentation.
% FS EAM files in the potentials directory of the LAMMPS distribution have an
% “.eam.fs” suffix. They are formatted as follows:
%
%    lines 1,2,3 = comments (ignored)
myline = fgets(fid);
myline = fgets(fid);
myline = fgets(fid);

%    line 4: Nelements Element1 Element2 … ElementN
myline = fgets(fid);
tmp = strread(myline,'%s');
Nele = sscanf(myline,'%d',1);
for i=1:Nele,
    elename{i} = tmp(i+1,:);
end

%    line 5: Nrho, drho, Nr, dr, cutoff
myline = fgets(fid);
tmp = sscanf(myline,'%e',5);
Nrho = tmp(1); drho = tmp(2); Nr = tmp(3); dr = tmp(4); rcut = tmp(5);
griddata = struct('Nele',Nele,'Nrho',Nrho,'drho',drho,'Nr',Nr,'dr',dr,'rcut',rcut);

disp(sprintf('Nele = %d  Nr = %d',Nele,Nr));

% The 5-line header section is identical to an EAM setfl file.

% Following the header are Nelements sections, one for each element I, 
% each with the following format:

%    line 1 = atomic number, mass, lattice constant, lattice type (e.g. FCC)
%    embedding function F(rho) (Nrho values)
%    density function rho(r) for element I at element 1 (Nr values)
%    density function rho(r) for element I at element 2
%    …
%    density function rho(r) for element I at element Nelement

% The units of these quantities in line 1 are the same as for setfl files. 
% Note that the rho(r) arrays in Finnis/Sinclair can be asymmetric (i,j != j,i)
% so there are Nelements^2 of them listed in the file.

% loop over each element
for iele = 1:Nele,
    % read element info
    myline = fgets(fid);
    tmp = sscanf(myline,'%e',3);
    Z=tmp(1); atommass=tmp(2); latticeconst=tmp(3);
    elemdata{iele} = struct('Z',Z,'atommass',atommass,'latticeconst',latticeconst);
    
    % read F(rho)
    Frho = zeros(Nrho,1);
    disp(sprintf('reading F(rho) of element %d',iele));
    j = 0;
    while j < Nrho,
        myline = fgets(fid);
        tmp = sscanf(myline,'%e',inf);
        Frho(j+1:j+length(tmp)) = tmp;
        j = j+length(tmp);
    end
    Frhodata{iele} = Frho;
    figure(iele); subplot(1,3,1);
    plot([0:Nrho-1]*griddata.drho,Frho);
    title(['F_',num2str(iele),'(\rho)']);
    
    % read rho(r)
    
    subplot(1,3,2); hold on;
    rhor = zeros(Nr,1);
    for jele = 1:Nele,
        disp(sprintf('reading rho(r) of element %d at element %d',...
                     jele, iele));
        n = 0;
        while (n < Nr),
            myline = fgets(fid);
            tmp = sscanf(myline,'%e',inf);
            rhor(n+1:n+length(tmp)) = tmp;
            n = n+length(tmp);
        end
        rhordata_iele{jele} = rhor;
        plot([0:Nr-1]*dr,rhor);
    end
    rhordata{iele} = rhordata_iele;
    hold off; title(['\rho_',num2str(iele),'(r)']);
end

% Following the Nelements sections, Nr values for each pair potential phi(r)
% array are listed in the same manner (r*phi, units of eV-Angstroms) as in
% EAM setfl files. Note that in Finnis/Sinclair, the phi(r) arrays are still
% symmetric, so only phi arrays for i >= j are listed.
    
for iele = 1:Nele,
    % read phi(r)
    phir = zeros(Nr, 1);
    for jele = 1:iele,
        disp(sprintf('reading phi(r) between element %d and %d',iele,jele));
        n = 0;

        while (n < Nr),
            myline = fgets(fid);
            tmp = sscanf(myline,'%e',inf);
            phir(n+1:n+length(tmp)) = tmp;
            n = n+length(tmp);
        end

        phirdata_iele{jele} = phir;
    end

    phirdata{iele} = phirdata_iele;
end

% plot phi(r)
for iele = 1:Nele,
    figure(iele); subplot(1,3,3); 
    title(['\phi_',num2str(iele),'(r)']); hold on;
    for jele = 1:Nele,
        if (jele>iele),
            phirdata{iele}{jele} = phirdata{jele}{iele};
        end
        plot([0:Nr-1]*dr,phirdata{iele}{jele});
    end
    ylim([0 100]); hold off;
end

fclose(fid);