function writeMDPPeamfs(potfilename,griddata,elemdata,rhor,phirtimesr,Frho)
% write EAM potential for binary elements into MD++ format
% the derivative of the data needs to be computed first
% this program will convert phi(r)*r into phi(r)

% test:
% [griddata, elemdata, Frhodata, rhordata, phirdata] = ...
%    readLAMMPSeamfs('LAMMPS_potential/CuZr.eam.fs');
% writeMDPPeam('eamdata.CuZr.LAMMPS.eam.fs',griddata, ...
%    elemdata,rhordata,phirdata,Frhodata);

% To load the eam/fs potential, use the following tcl script:
% proc readeam_Cu_Zr_fs {} { MD++ {
%     potfile = "../../potentials/EAMDATA/eamdata.CuZr.LAMMPS.eam.fs"
%     eamgrid = 10000 eamfiletype = 4 # binary fs type potential
%     readeam
%     atommass = [ 63.546 91.224 ] # Cu Zr
%     element0 = "Cu" element1 = "Zr"
%     NNM = 600
% }
% puts "The EAM Cu-Zr fs potential is sucessfully loaded"
% }

Nr   = griddata.Nr;
Nrho = griddata.Nrho;
Nele = griddata.Nele;
if (Nr~=Nrho),
    disp(sprintf('Error: Nr = %d  Nrho = %d  not equal to each other!',Nr,Nrho));
    return;
end
if (Nele > 2),
    disp(sprintf('Error: MD++ does not support eam potential for more than 2 elements'));
    return;
end

fid = fopen(potfilename,'w');

fprintf(fid, 'EAM potential file for MD++ written by writeMDPPeamfs.m\n');
for i = 1:Nele,
    fprintf(fid, ' %4d %20.6E %20.6E',elemdata{i}.Z,elemdata{i}.atommass,elemdata{i}.latticeconst);
end
fprintf(fid, '\n');

rmin = 0;
fprintf(fid,'%26.16E %26.16E %26.16E %26.16E\n',griddata.dr,griddata.drho,griddata.rcut,rmin);

% compute derivatives
r = [0:Nr-1]*griddata.dr; rho = [0:Nrho-1]*griddata.drho;
for i = 1:Nele,
  for j = 1:Nele,
    cs = spline(r,rhor{i}{j});
    csp=cs; csp.order=3; csp.coefs=[cs.coefs(:,1)*3,cs.coefs(:,2)*2,cs.coefs(:,3)];
    rhopr{i}{j} = ppval(csp, r);
  end
end

for i = 1:Nele,
  for j = 1:Nele,
    phir{i}{j} = phirtimesr{i}{j}(2:end)./r(2:end)'; phir{i}{j} = phir{i}{j}([1,1:end]);
    phir{i}{j}(1) = 2*phir{i}{j}(2)-phir{i}{j}(3);
    cs = spline(r,phir{i}{j});
    csp=cs; csp.order=3; csp.coefs=[cs.coefs(:,1)*3,cs.coefs(:,2)*2,cs.coefs(:,3)];
    phipr{i}{j} = ppval(csp,r);
  end
end

for i = 1:Nele,
    cs = spline(rho,Frho{i});
    csp=cs; csp.order=3; csp.coefs=[cs.coefs(:,1)*3,cs.coefs(:,2)*2,cs.coefs(:,3)];
    Fprho{i} = ppval(csp,rho);
end

if (Nele == 1),
    rhor{2}{2} = rhor{1}{1}; rhopr{2}{2} = rhopr{1}{1};
    rhor{1}{2} = rhor{1}{1}; rhopr{1}{2} = rhopr{1}{1};
    rhor{2}{1} = rhor{1}{1}; rhopr{2}{1} = rhopr{1}{1}; 
    phir{2}{2} = phir{1}{1}; phipr{2}{2} = phipr{1}{1};
    phir{1}{2} = phir{1}{1}; phipr{1}{2} = phipr{1}{1};
    phir{2}{1} = phir{1}{1}; phipr{2}{1} = phipr{1}{1};
    Frho{2} = Frho{1}; Fprho{2} = Fprho{1};
end

% write rho_ii(r) and rhop_ii(r)
for i=1:Nr,
  fprintf(fid,'%26.16E %26.16E %26.16E %26.16E\n',rhor{1}{1}(i),rhopr{1}{1}(i),rhor{2}{2}(i),rhopr{2}{2}(i));
end

% write phi_ii(r) and phip_ii(r)
for i=1:Nr,
  fprintf(fid,'%26.16E %26.16E %26.16E %26.16E\n',phir{1}{1}(i),phipr{1}{1}(i),phir{2}{2}(i),phipr{2}{2}(i));
end

% write phi_ij(r) and phip_ij(r), symmetric
for i=1:Nr,
  fprintf(fid,'%26.16E %26.16E\n',phir{1}{2}(i),phipr{1}{2}(i));
end

% write F(rho) and Fp(rho)
for i=1:Nr,
  fprintf(fid,'%26.16E %26.16E %26.16E %26.16E\n',Frho{1}(i),Fprho{1}(i),Frho{2}(i),Fprho{2}(i));
end

% write rho_ij(r) and rhop_ij(r)
for i=1:Nr,
  fprintf(fid,'%26.16E %26.16E %26.16E %26.16E\n',rhor{1}{2}(i),rhopr{1}{2}(i),rhor{2}{1}(i),rhopr{2}{1}(i));
end

fclose(fid);

% plot data
for i = 1:Nele,
  figure(str2num(['1', num2str(i-1), '0'])); hold on;
  for j = 1:Nele,
    p1=plot(r,rhor{i}{j},r,rhopr{i}{j}); set(p1(1),'LineWidth',3); 
  end
  xlabel('r');  ylabel('\rho(r)'); hold off;
  figure(str2num(['1', num2str(i-1), '1'])); hold on;
  for j = 1:Nele,
    p2=plot(r,phir{i}{j},r,phipr{i}{j}); set(p2(1),'LineWidth',3);
  end
  xlabel('r');  ylabel('\phi(r)'); hold off;
  figure(str2num(['1', num2str(i-1), '2'])); hold on;
  p3=plot(rho,Frho{i},rho,Fprho{i}); set(p3(1),'LineWidth',3);
  xlabel('\rho');  ylabel('F(\rho)'); hold off;
end
