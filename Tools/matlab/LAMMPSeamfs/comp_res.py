import numpy as np
import matplotlib.pyplot as plt

data_mdpp = np.genfromtxt('force_lammps.txt')
force_mdpp = data_mdpp[:, 1:4]

data_mdpp_py = np.genfromtxt('force_mdpp_py.txt')
force_mdpp_py = data_mdpp_py[:, 1:4]

data_lammps = np.genfromtxt('dump.0.mg', skip_header=9)
force_lammps = data_lammps[:, -3:]

force_diff = np.linalg.norm(force_mdpp-force_lammps, axis=-1)
nonzero_idx= np.nonzero(force_diff > 1e-4)
force_diff2 = np.linalg.norm(force_mdpp_py-force_mdpp, axis=-1)
nonzero_idx2= np.nonzero(force_diff2 > 1e-8)

print('The difference between tcl and lammps:', nonzero_idx[0].size)
print('The difference between tcl and python:', nonzero_idx2[0].size)
#print(data_mdpp[nonzero_idx, :])
#print(data_lammps[nonzero_idx, :])
if nonzero_idx[0].size > 0 or nonzero_idx2[0].size > 0:
    print('TEST FAILED!')
else:
    print('TEST SUCCEED!')
