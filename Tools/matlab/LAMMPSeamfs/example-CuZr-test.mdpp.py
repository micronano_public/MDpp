from __future__ import print_function # only necessary if using Python 2.x


import sys, os, fnmatch

sys.path.insert(0, os.path.join(os.environ['MDPLUS_DIR'], 'bin_py'))
import mdpp
import numpy as np

# -*-python-script-*-
# MD code of Metallic Glass CuZr
#  test code for python script, 
#  should generate same result as tcl
#
# Compile by: make eam SYS=gpp PY=yes
# run by:     bin/eam_gpp scripts/work/cuzr_mg/cuzr_NEB_rearrange.mdpp.py /.../relaxed_states

def readeam_Cu_Zr_fs():
    # Make sure the sequence of the atom spcies is correct and the eamgrid exactly match the pot file
    # eam potential with rho_ab(r)
    mdpp.cmd('''
        potfile = "eamdata.CuZr.ZJU.eam.fs"
        eamgrid = 10000 eamfiletype = 4 # binary fs type potential
        readeam
        atommass = [ 63.546 91.224 ] # Cu Zr
        element0 = "Cu" element1 = "Zr"
        NNM = 600
    ''')
    print("The EAM Cu-Zr fs potential is sucessfully loaded")

def readLAMMPSdump(filename):
    mdpp.cmd('incnfile =' + filename)
    print('loading regular file...' + filename)
    with open(filename, 'r') as f:
        if f.readline().split()[1] == 'TIMESTEP':
            timestep = f.readline().split()[0]
            print('timestep = '+timestep)
        else:
            print('format error')
            sys.exit(1)
    mdpp.cmd('input = [ ' + timestep + ' 0 ] readLAMMPS')
    print('LAMMPS dump file is sucessfully loaded')

#------------------------------------------------------------
# Start of the main script
#

#Read in LAMMPS *DUMP* file and evalulate energy and force
readeam_Cu_Zr_fs()
readLAMMPSdump('init_config.dump')
mdpp.cmd('eval')
mdpp.cmd('finalcnfile = force_mdpp_py.txt writeFORCE')
mdpp.cmd('nspecies = 2')
mdpp.cmd('quit')

# Visualize results
#  octave
#   data=load('stringeng.out'); figure(1); plot(data(end-10:end,3:5:end)', '*-');
#   Emax = max(data(:,3:5:end),[],2); figure(2); plot(Emax,'*-')
