#!/bin/sh

echo "Test case for CuZr binary eam/fs potential in MD++ and LAMMPS..."

# test readLAMMPSeamfs.m
#      writeMDPPeamfs.m
#      eam implementation (src/eam.cpp)
#      python build

# compile md++ using:
#   make eam SYS=gpp
#   mkdir bin_gpp
#   mv bin/eam_gpp bin_gpp
#   make eam SYS=gpp PY=yes
#   mkdir bin_py
#   mv bin/eam_gpp bin_py

# change the following lines and then
# directly run the script
# chmod 754 run.sh
# ./run.sh

M_BINARY="matlab -nodisplay -nosplash -nodesktop -r " # "octave -W"        # or "matlab -run"
MDPP_TCL_BUILD="bin_ni/eam_mc2" # your build of md++ with tcl
MDPP_PY_BUILD="python3" # your build of md++ with python
LAMMPS_BUILD=${HOME}/Codes/lammps/src/lmp_serial # your build of lammps
PY3_BINARY="python3"     # your python3 (numpy required)

SCRIPT_DIR="Tools/matlab/LAMMPSeamfs" # current folder
ROOT_DIR=${HOME}/Codes/MD++.git
# SCRIPT_DIR="scripts/work/CuZr_test" # current folder

# no need to change below
RUN_DIR="runs/ZrCu-test-300-0"
MDPP_TCL="example-CuZr-test.tcl 2"
MDPP_PY="example-CuZr-test.mdpp.py"
LAMMPS_SCRIPT="in.lammps"

echo "generate md++ eam/fs potential"
# generate mdpp eam potential
${M_BINARY} "run(\"eamfsLAMMPS2MDPP\"); exit;"
echo "run lammps"
# run lammps
${LAMMPS_BUILD} -in ${LAMMPS_SCRIPT}

echo "run md++-python"
# run md++ python build
${MDPP_PY_BUILD} ${MDPP_PY}

echo "run md++"
# run md++
cd ${ROOT_DIR}
${MDPP_TCL_BUILD} ${SCRIPT_DIR}/${MDPP_TCL}

# copy the results
cp ${RUN_DIR}/force_lammps.txt ${SCRIPT_DIR}
cp ${RUN_DIR}/A.log ${SCRIPT_DIR}
cd ${SCRIPT_DIR}

echo "%%%%%%%%%%%%%%%%%% COMPARE THE RESULTS %%%%%%%%%%%%%%%%%%%"
# compare the results
${PY3_BINARY} comp_res.py

# clean the results
# rm A.log dump.0.mg log.lammps force_lammps.txt 
# rm force_mdpp_py.txt eamdata.CuZr.ZJU.eam.fs
# rm -rf ${RUN_DIR}
echo "%%%%%%%%%%%%% cleaned the temporary files %%%%%%%%%%%%%%%%"
