/*
  lj2.cpp
  by Wei Cai  caiwei@mit.edu
  Last Modified : Mon Jan  1 21:26:52 2007

  FUNCTION  :  MD simulation package of Si using Stillinger-Weber potential
               and Lennard-Jones potential (depending on group ID)
*/

#include "lj2.h"

void LJ2Frame::initparser()
{
    MDPARALLELFrame::initparser();

    /* input */
    bindvar("C12_00",&_ALJ_00,DOUBLE);
    bindvar("C6_00",&_BLJ_00,DOUBLE);

    bindvar("C12_11",&_ALJ_11,DOUBLE);
    bindvar("C6_11",&_BLJ_11,DOUBLE);

    bindvar("C12_01",&_ALJ_01,DOUBLE);
    bindvar("C6_01",&_BLJ_01,DOUBLE);
    
    bindvar("LJ_ENERGY",&LJ_ENERGY,DOUBLE);
    bindvar("LJ_LENGTH",&LJ_LENGTH,DOUBLE);
    bindvar("Rcut",&LJ_RC,DOUBLE);

    bindvar("LJ_2D",&LJ_2D,INT);
    bindvar("LJ_SHIFT",&LJ_SHIFT,INT);

    bindvar("JE",&JE,DOUBLE);
    bindvar("JE_INT",&JE_INT,DOUBLE);
}

int LJ2Frame::exec(const char *name)
{
    if(MDPARALLELFrame::exec(name)==0) return 0;
    bindcommand(name,"initLJ",initLJ());
    bindcommand(name,"makeliquid",makeliquid());
    bindcommand(name,"randomposition_rc",randomposition_rc(LJ_LENGTH*0.1));
    bindcommand(name,"clear_vz",clear_vz());
    return -1;
}

void LJ2Frame::initvars()
{
    LJ_ENERGY = (119.8*P_KB/P_E);          /* argon energy (sigma) */
    LJ_LENGTH = 3.405;                     /* argon length (epsilon) */
    LJ_RC     = (2.37343077641*LJ_LENGTH); /* 4 neighbor */
    _RLIST=LJ_RC+1.1;
    _SKIN=_RLIST-LJ_RC;

    JE.clear(); 
    JE_INT.clear();
    MDPARALLELFrame::initvars();
}

void LJ2Frame::initLJ()
{
    ALJ_00=_ALJ_00*LJ_ENERGY*POW12(LJ_LENGTH);
    BLJ_00=_BLJ_00*LJ_ENERGY*POW6(LJ_LENGTH);
    if (LJ_SHIFT) {
        Uc_00=(ALJ_00*POW6(1/LJ_RC)-BLJ_00)*POW6(1/LJ_RC);
        DUDRc_00=-(12*ALJ_00/POW6(LJ_RC)-6*BLJ_00)/POW6(LJ_RC)/LJ_RC;
    }

    ALJ_11=_ALJ_11*LJ_ENERGY*POW12(LJ_LENGTH);
    BLJ_11=_BLJ_11*LJ_ENERGY*POW6(LJ_LENGTH);
    if (LJ_SHIFT) {
        Uc_11=(ALJ_11*POW6(1/LJ_RC)-BLJ_11)*POW6(1/LJ_RC);
        DUDRc_11=-(12*ALJ_11/POW6(LJ_RC)-6*BLJ_11)/POW6(LJ_RC)/LJ_RC;
    }

    ALJ_01=_ALJ_01*LJ_ENERGY*POW12(LJ_LENGTH);
    BLJ_01=_BLJ_01*LJ_ENERGY*POW6(LJ_LENGTH);
    if (LJ_SHIFT) {
        Uc_01=(ALJ_01*POW6(1/LJ_RC)-BLJ_01)*POW6(1/LJ_RC);
        DUDRc_01=-(12*ALJ_01/POW6(LJ_RC)-6*BLJ_01)/POW6(LJ_RC)/LJ_RC;
    }
    INFO_Printf("ALJ_00 = %e BLJ_00 = %e Uc_00 = %e DUDRc_00 = %e\n",ALJ_00,BLJ_00,Uc_00,DUDRc_00);
    INFO_Printf("ALJ_11 = %e BLJ_11 = %e Uc_11 = %e DUDRc_11 = %e\n",ALJ_11,BLJ_11,Uc_11,DUDRc_11);
    INFO_Printf("ALJ_01 = %e BLJ_01 = %e Uc_01 = %e DUDRc_01 = %e\n",ALJ_01,BLJ_01,Uc_01,DUDRc_01);
}

void LJ2Frame::calcprop()
{
    MDPARALLELFrame::calcprop();
}

void LJ2Frame::lennard_jones_2()
{
/*
   U= potential
   Vir= -1/r(dU/dr)
   F= 1/r^2(d^2U/dr^2-1/r(dU/dr))
 */

    /* !!! Parallel version not implemented !!! */
    int ipt,jpt,j;
    double U,r,r2,ri6;
    double ALJ,BLJ,Uc,DUDRc;   
    Vector3 sij, rij, fij, vi, vj;
    
    DUMP(HIG"Lennard Jones"NOR);
        
    refreshneighborlist();
    
    _EPOT=0;

    for(ipt=0;ipt<_NP;ipt++)
    {
        _F[ipt].clear(); _EPOT_IND[ipt]=0;
    }
    _VIRIAL.clear();
    JE.clear();

    for(ipt=0;ipt<_NP;ipt++)
    {
        vi = _H*_VSR[ipt]/_TIMESTEP;
        for(j=0;j<nn[ipt];j++)
        {

            jpt=nindex[ipt][j];
            if(ipt>jpt) continue;
            sij=_SR[jpt]-_SR[ipt];
            vj = _H*_VSR[jpt]/_TIMESTEP;

            /* Limit the model to 2D */
            if(LJ_2D) sij.z = 0;

            sij.subint();
            rij=_H*sij;
            r2=rij.norm2();
            r=sqrt(r2);
            if(r<=LJ_RC)
            {
                if((species[ipt]==0)&&(species[jpt]==0))
                {
                    ALJ=ALJ_00; BLJ=BLJ_00; Uc=Uc_00; DUDRc=DUDRc_00;
                }
                else if((species[ipt]==1)&&(species[jpt]==1))
                {
                    ALJ=ALJ_11; BLJ=BLJ_11; Uc=Uc_11; DUDRc=DUDRc_11;
                }
                else
                {
                    ALJ=ALJ_01; BLJ=BLJ_01; Uc=Uc_01; DUDRc=DUDRc_01;
                }

                //INFO_Printf("ALJ = %e BLJ = %e Uc = %e DUDRc = %e\n",ALJ,BLJ,Uc,DUDRc);
                
                ri6=1./(r2*r2*r2);
                
                U=(ALJ*ri6-BLJ)*ri6-r*DUDRc+(LJ_RC*DUDRc-Uc);
                fij=rij*((12*ALJ*ri6-6*BLJ)*ri6/r2+DUDRc/r);

                if(LJ_2D) fij.z  = 0;
                _F[ipt]-=fij;
                _F[jpt]+=fij;
                _EPOT_IND[ipt]+=U*0.5;
                _EPOT_IND[jpt]+=U*0.5;
                _EPOT+=U;
                _VIRIAL.addnvv(1.,fij,rij);

                /* heat flux - for thermal conductivity calculations */
                JE += rij * (dot(vi, fij) + dot(vj, fij)) * 0.5;
            }
        }
    }
    for(ipt=0;ipt<_NP;ipt++)
    {
        vi = _H*_VSR[ipt]/_TIMESTEP;
        JE += vi*(0.5*_ATOMMASS[species[ipt]]*MASSCONVERT * vi.norm2() + _EPOT_IND[ipt]);
    }
    JE_INT += JE * _TIMESTEP; /* should move to integrator */
}

void LJ2Frame::makeliquid()
{
    int N0, N1, i;

    _H.clear();
    _H[0][0] = input[0];
    _H[1][1] = input[1];
    _H[2][2] = input[2];

    N0 = (int) input[3];
    N1 = (int) input[4];

    _NP = N0 + N1;
    Alloc();

    for(i=0;i<_NP;i++)
    {
        if (i < N0) species[i] = 0;
        else species[i] = 1;
    }
    randomposition_rc(LJ_LENGTH*0.5);
}

void LJ2Frame::randomposition_rc(double rc)
{
    int i, j, too_close, max_trial, itrial;
    Vector3 dr, dr1, ds;

    max_trial = 100000;
    for(i=0;i<_NP;i++)
    {
        for(itrial=0;itrial<max_trial;itrial++)
        {
            _SR[i].set(drand48()-0.5, drand48()-0.5, drand48()-0.5);
            if (LJ_2D) _SR[i].z = 0;

            too_close = 0;
            for(j=0;j<i;j++)
            {
               ds = _SR[j] - _SR[i];
               ds.subint();
               dr = _H*ds;
               if(dr.norm() < rc)
               {
                   too_close = 1;
                   break;
               }
           }

           if (! too_close) break;
       }
       if (too_close) ERROR("makeliquid: cannot find a location to place atom");
    }
}

void LJ2Frame::clear_vz()
{
    int i;
    for(i=0;i<_NP;i++) _VSR[i].z = 0;
}

void LJ2Frame::potential()
{
    lennard_jones_2();
}

#ifdef _TEST

/* Main Program Begins */
class LJ2Frame sim;

/* The main program is defined here */
#include "main.cpp"

#endif//_TEST

