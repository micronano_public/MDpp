% More tests are needed to make sure derivatives are consistent with free energy
% Potential problems: 
%  1. sometimes free energy goes up if dt(orientation field) is too high
%  2. orientation field (q) is not constant inside crystal bulk region 

clear all;
close all;
N_grid = [100 100 2];
Niter = 10000;
%model_param = [0.03 1.0 1.0 1e-3 0.005 0.05 1e-5]; 
model_param = [0.03 1.0 1.0 1e-3 0.005 0.05 2e-5]; 

for i = 1:N_grid(1), for j = 1:N_grid(2), for k = 1:N_grid(3),
        if abs(i-N_grid(1)/2) <=2 || abs(i-N_grid(1)) <=2 || i <=2,
            phi(i,j,k) = 1e-6;
        else
            phi(i,j,k) = 1- 1e-6;
        end        
end; end; end;   

for i = 1:N_grid(1), for j = 1:N_grid(2), for k = 1:N_grid(3),
        if i <= N_grid(1)/2,
            eta(i,j,k) = pi/3 + rand*1e-6;
        else
            eta(i,j,k) = pi/4 + rand*1e-6;    
        end;
end; end; end;

c = [1 1 1]/sqrt(3);

pfof3d_single_iso;

q2_sum = zeros(N_grid);
 for i = 1:4,
    q2_sum = q2_sum + q{i}.^2;
 end
